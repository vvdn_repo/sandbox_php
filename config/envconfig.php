<?php

class EnvConfig {

    private static $envcfg = array();
    private static $initialized = false;
    private static $confFilePath = "sandbox_cloud.conf";
    private static function initialize() {
        if (self::$initialized)
            return;
        $file = file_get_contents(self::$confFilePath, true);
        if ($file == FALSE) {
            echo "Not able to read from file";
            return NULL;
        }
        self::$envcfg = json_decode($file, true);
        self::$initialized = true;
    }

    public static function getConfig() {
        self::initialize();
        return self::$envcfg;
    }

}

?>


