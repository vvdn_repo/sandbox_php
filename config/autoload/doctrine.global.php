<?php
$envConfig = EnvConfig::getConfig();
return array(
    'doctrine' => array(
        'connection' => array(
            'orm_default' => array(
                'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
                    'params' => array(
                        'host' => $envConfig["sqlserver"],
                        'port' => '3306',
                        'dbname' => $envConfig["sqldbname"],
                ),
            ),
        )
));