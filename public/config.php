<?php
error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
ini_set('display_errors', true);
define('BASE_PATH', 'http://' .  $_SERVER['HTTP_HOST']);
define('APP_PATH', BASE_PATH.'/sandboxapp');
define('PAGE_TITLE', 'Sandbox');
define('APP_LOGO', 'sb-logo.png');
define('MODULE_PATH', dirname(__DIR__) . '/module');
define('MODULE_APPLICATION_PATH', MODULE_PATH.'/Application');
define('PUBLIC_PATH', dirname(__DIR__) . '/public');
define('VENDOR_PATH', dirname(__DIR__) . '/vendor');
