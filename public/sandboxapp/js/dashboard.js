var display_camera = $("#display_camera").val();
var available_camera = '';
var init_cameraIP = '';
/*** No of connected cameras listing ***/
$.ajax({
    url: "device/list",
    type: 'GET',
    success: function(result) {
        result = JSON.parse(result);
        if(result.data.length == 1) {
            $('#display_camera option[value!="1"]').hide();
        }
        else if(result.data.length == 2) {
            $('#display_camera option[value="4"]').hide();
        }
        for (var i = 0; i < result.data.length; i++) {
            available_camera += '<option value="' + result.data[i]['deviceIP'] + '">' + result.data[i]['deviceName'] + '</option>';
        }
        init_cameraIP = result.data[0]["deviceIP"];
        if (result.data.length == 0) {
            noDeviceFound();
            $('#display_camera').prop('disabled', true);
        }
        else {
            displayCameras(display_camera, init_cameraIP);
            $('#display_camera').prop('disabled', false);
        }
    },
    error: function(error) {
    }
});
/*** End of No of connected cameras listing ***/

/*** Calling no camera videos to live streaming ***/
var noOfCameras = function(selected) {
    display_camera = selected.value;
    displayCameras(display_camera, init_cameraIP);
};
/*** End of Calling no camera videos to live streaming ***/

/*** live streaming for selected camera ***/
var changeVideo = function(selected) {
    var current_class = selected.id;
    var output = '<object class="' + current_class + '" data="rtsp://' + selected.value + ':8551/PSIA/Streaming/channels/2?videoCodecType=H.264" events="True" id="vlc_1" classid="clsid:4AD27CDD-8C2B-455E-8E2A-463E550B718F">\n\
<param class="' + current_class + '" value="rtsp://' + selected.value + ':8551/PSIA/Streaming/channels/2?videoCodecType=H.264" name="Src">\n\
<param value="transparent" name="wmode"><param value="True" name="ShowDisplay">\n\
<param value="False" name="AutoLoop"><param value="True" name="AutoPlay">\n\
<embed class="' + current_class + ' embed-responsive-item" target="rtsp://' + selected.value + ':8551/PSIA/Streaming/channels/2?videoCodecType=H.264" loop="no" autoplay="yes" version="VideoLAN.VLCPlugin.2" pluginspage="http://www.videolan.org" type="application/x-vlc-plugin" id="vlc_1_embed" wmode="transparent">\n\
</object>';
    $("#displayVideo").html(output);
};
/*** End of live streaming for selected camera ***/

/*** Displaying no of Cameras ***/
function displayCameras(number, cam_ip) {
    var output = '<div class="row">';
    for (var i = 1; i <= number; i++) {
        if (i == 1 && number == 1) {
            $(".no_cameras").addClass('col-md-offset-1 col-lg-offset-1').removeClass('col-md-offset-2 col-lg-offset-2');
            output += '<div class="col-lg-10 col-md-10 col-md-offset-1 col-lg-offset-1">';
        }
        else {
            $(".no_cameras").addClass('col-md-offset-2 col-lg-offset-2').removeClass('col-md-offset-1 col-lg-offset-1');
            output += '<div class="col-lg-6 col-md-6">';
        }
        output += '<div class="row"><div class="col-md-12 col-sm-12 col-xs-12">\n\
<div class="margin-bottom-10"><div class="form-group"><label class="col-md-3">Select Camera:</label>\n\
<div class="col-md-6"><select class="form-control" id="video_' + i + '" onchange="changeVideo(this)">' + available_camera + '</select></div></div></div></div></div>\n\
<div class="row" style="padding-top: 10px;"><div class="col-md-12 col-sm-12 col-xs-12">\n\
<div class="embed-responsive embed-responsive-16by9" id="displayVideo">\n\
<object class="video_' + i + '" data="rtsp://' + cam_ip + ':8551/PSIA/Streaming/channels/2?videoCodecType=H.264" events="True" id="vlc_1" classid="clsid:4AD27CDD-8C2B-455E-8E2A-463E550B718F">\n\
<param class="video_' + i + '" value="rtsp://' + cam_ip + ':8551/PSIA/Streaming/channels/2?videoCodecType=H.264" name="Src">\n\
<param value="transparent" name="wmode"><param value="True" name="ShowDisplay">\n\
<param value="False" name="AutoLoop"><param value="True" name="AutoPlay">\n\
<embed class="video_' + i + ' embed-responsive-item" target="rtsp://' + cam_ip + ':8551/PSIA/Streaming/channels/2?videoCodecType=H.264" loop="no" autoplay="yes" version="VideoLAN.VLCPlugin.2" pluginspage="http://www.videolan.org" type="application/x-vlc-plugin" id="vlc_1_embed" wmode="transparent">\n\
</object></div></div></div></div>';
    }
    output += '</div>';
    $(".display_2").html(output);
}
;
/*** End of Displaying no of Cameras ***/

/*** No Device Found Condition***/
function noDeviceFound() {
    var output = '<div class="alert alert-danger" role="alert" style="margin-top:50px;">No Device Connected...</div>';
    $(".display_2").html(output);
}
/*** End of No Device Found Condition***/