
var displayAlarms = function() {
    var response = {"status": true, "authfailure": false, "data": [{"alarmid": 341, "alarmname": "Email Notificataion", "alarmTime": "10.15 AM", "alarm_type": "information"}, {"alarmid": 344, "alarmname": "Battery Alert", "alarmTime": "11.15 AM", "alarm_type": "information"}, {"alarmid": 345, "alarmname": "Security", "alarmTime": "10.15 AM", "alarm_type": "information"}]};
    var alarmList = new Array();
    if (response.status) {
        $.each(response.data, function(index, element) {
            var alarm = new Array();
            alarm.push(element.alarmname);
            alarm.push(element.alarmTime);
            alarm.push("<a href='#' class=' btn btn-success' style='border-radius:20%;' data-toggle='modal' data-target='#basicModal1' device_id=" + element.deviceid + " onclick='editDeviceFn(\"" + element.deviceid + "\")'><span class='glyphicon glyphicon-search'></span>&nbsp;&nbsp;</a>&nbsp;&nbsp;<a href='#' class='btn btn-danger'  style='border-radius:20%;' data-toggle='modal' data-target='#basicModal2' device_id=" + element.deviceid + " onclick='editDeviceFn(\"" + element.deviceid + "\")'><span class='glyphicon glyphicon-trash'></span>&nbsp;&nbsp;</a>");
            alarmList[index] = alarm;
            //alert('alarmList'+alarmList);
            return alarmList;
        });
    } else {
        $(".msgbox").show();
        $('.msgbox').html('No alarm to show..!!');
        $('.msgbox').fadeOut(3000);
    }
    dataTable = $('#alarmTable').dataTable({
        "data": alarmList
    });
}; /* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var listAlarms = function()
{
    $('#alarmsTable').dataTable({
        responsive: true,
        "bProcessing": true,
        "iDisplayLength": 10,
        "order": [[1, "desc"]],
        "ajax": {
            "url": "../alarm/list-alarms",
            "dataSrc": "response"
        },
        aoColumns: [
            {"sTitle": 'Type', mData: "alarmType"},
            {"sTitle": 'Date & Time', mData: null,
                mRender: function(o) {
                    return moment(o.alarmTimeStamp * 1000).format('MMMM Do YYYY, h:mm:ss a');
                }
            },
            {
                "sTitle": 'Action',
                mData: null,
                bSortable: false,
                mRender: function(o) {
                    return '<a class="btn btn-xs btn-primary" onclick="viewVideo(' + '\'' + o.lanUrl + '\'' + ',' + '\'' + o.alarmType + '\'' + ')"><span class="mobile-hide"><i class="glyphicon glyphicon-eye-open"></i>View</span></a>';
                }
            }
        ],
        "fnInitComplete": function(oSettings) {
            $('.dataTables_filter label').append("<i class='glyphicon glyphicon-search' style='position:relative; right:20px;'></i>");
        }
    });
};

var viewVideo = function(path, type) {
    $("#alarmVideoModalLabel").text(type);
    var output = '<object data="' + path + '" events="True" id="vlc_1" classid="clsid:4AD27CDD-8C2B-455E-8E2A-463E550B718F">'
            + '<param value="' + path + '" name="Src">'
            + '<param value="transparent" name="wmode">'
            + '<param value="True" name="ShowDisplay">'
            + '<param value="False" name="AutoLoop">'
            + '<param value="True" name="AutoPlay">'
            + '<embed class="embed-responsive-item" target="' + path + '" loop="no" autoplay="yes" version="VideoLAN.VLCPlugin.2" pluginspage="http://www.videolan.org" type="application/x-vlc-plugin" id="vlc_1_embed" wmode="transparent">'
            + '</object>';
    $(".viewAlarmVideoModalBody").html(output);
    $('#viewAlarmVideoModal').modal({backdrop: 'static'});
};
