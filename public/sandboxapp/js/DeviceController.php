<?php

/**
 * OrganizationController handles Organization related views
 * @package Organization
 * @author VVDNTechnologies < >
 */

namespace Device\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Zend\Config\Reader\Ini;
use Zend\Config\Reader\Json;
use Zend\Session\Container;
use Zend\Math\Rand;
use Zend\Http\Client;
use Zend\Serializer\Serializer;
use Zend\Http\Response;




class DeviceController extends AbstractActionController
{
    protected $sessionStorage;
    protected $authService;


    /*
     * This function will return ZF2 service instance
     * @param String
     * @return Object of ServiceLocatorInterface
     */

    protected function getServiceInstance($service)
    {
        return $this->getServiceLocator()->get($service);
    }


    public function registerAction()
    {
        $response = array();
        $request = $this->getRequest();
        $this->authService = $this->getServiceInstance('AuthService');

        if ($request->isPost()) {
            $data = json_decode(file_get_contents("php://input"), true);
            $formData = $data;
            //Validate Device MAC
            $deviceModel = $this->getServiceInstance('Device');
            $userModel = $this->getServiceInstance('User');
            $isUnique = $deviceModel->validateDevicemac($data['device_mac']);
            $user_id = $userModel->getUidfromtoken($data['user_token']);
            if ($user_id) {
                if ($isUnique == 0) {

                        // BEGIN-Create Device
                        $deviceEntity = $deviceModel->getEntityInstance();
                        $objectManager = $deviceModel->getObjectManager($this->getServiceLocator());
                        $created_time = time();
                        // $deviceEntity->setDeviceName(htmlspecialchars($data['device_name']));
                        $deviceEntity->setDeviceType(htmlspecialchars($data['device_type']));
                        $deviceEntity->setDeviceMac(htmlspecialchars($data['device_mac']));
                        $deviceEntity->setDeviceName(htmlspecialchars($data['device_name']));
                        $deviceEntity->setDeviceCreatedon($created_time);
                        $deviceEntity->setDeviceIP(htmlspecialchars($data['device_ip']));
                        $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxUser', $user_id);
                        $deviceEntity->setDeviceCreatedbyFk($userProxyInstance);
                        $deviceEntity->setDeviceStatus(0); //0-> InActive Account
			$deviceEntity->setDeviceMode(1);
			$deviceEntity->setDeviceDefault(0);
                        $deviceId = $deviceModel->saveDevice($deviceEntity);
                        $configModel = $this->getServiceInstance('DeviceConfig');
                        $configEntity = $configModel->getEntityInstance();
                        $deviceProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId);
                        $configEntity->setConfigDeviceId($deviceProxyInstance);
                        $configEntity->setConfigMotionDetection(1);
                        $configEntity->setConfigCubeRecording(0);
                        $configEntity->setConfigCloudRecording(0);
                        $configEntity->setConfigVideoQuality(1);
                        $configEntity->setConfigAudioEnable(1);
                        $configEntity->setConfigLowerThreshold(0);
                        $configEntity->setConfigUpperThreshold(0);
                        $configEntity->setConfigMotionBlock("FFF");
			

                        $configModel->saveConfig($configEntity);
                        $rand_num = Rand::getString(5, 'abcdefghijklmnopqrstuvwxyz0123456789', true);
                        $device_token = "sb_" . $deviceId . "_" . $rand_num;
                        $deviceEntity->setDeviceToken($device_token);
                        $device = $deviceModel->saveDevice($deviceEntity);
                        if($device){
                            //$PushNotificationModel = $this->getServiceLocator()->get('PushNotification');
                            // $msg = "You have a new voice mail"."_".$device_info[0]['deviceIP'];
                            // $PushNotificationModel->makePushNotification($user_id,$msg);
                            $status = true;
                            $message = "Device Added Successfully";
                            $config = parse_ini_file('config/config.ini');
                            $node_ip = $config['node_ip'];
                            $node_port = $config['node_port'];
			    $device_id = $device;

                        }
                        return new JsonModel(array("status" => $status, "message" => $message, "node_ip" => $node_ip, "node_port" => $node_port,"device_id" =>$device_id));
                }
                else {
                   
                    $isUniqueWithUser = $deviceModel->validateDevicemacWithUser($data['device_mac'],$user_id);
                    if($isUniqueWithUser){
                         $deviceId =  $isUniqueWithUser[0]['deviceId'];
                        $DeviceModel = $this->getServiceInstance('Device');
                        $delete = $DeviceModel->deleteDevice($deviceId);

                        // BEGIN-Create Device
                        $deviceEntity = $deviceModel->getEntityInstance();
                        $objectManager = $deviceModel->getObjectManager($this->getServiceLocator());
                        $created_time = time();
                        // $deviceEntity->setDeviceName(htmlspecialchars($data['device_name']));
                        $deviceEntity->setDeviceType(htmlspecialchars($data['device_type']));
                        $deviceEntity->setDeviceMac(htmlspecialchars($data['device_mac']));
                        $deviceEntity->setDeviceName(htmlspecialchars($data['device_name']));
                        $deviceEntity->setDeviceCreatedon($created_time);
                        $deviceEntity->setDeviceIP(htmlspecialchars($data['device_ip']));
                        $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxUser', $user_id);
                        $deviceEntity->setDeviceCreatedbyFk($userProxyInstance);
                        $deviceEntity->setDeviceStatus(0); //0-> InActive Account
			$deviceEntity->setDeviceMode(1);
			$deviceEntity->setDeviceDefault(0);
                        $deviceId = $deviceModel->saveDevice($deviceEntity);
                        $configModel = $this->getServiceInstance('DeviceConfig');
                        $configEntity = $configModel->getEntityInstance();
                        $deviceProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId);
                        $configEntity->setConfigDeviceId($deviceProxyInstance);
                        $configEntity->setConfigMotionDetection(1);
                        $configEntity->setConfigCubeRecording(0);
                        $configEntity->setConfigCloudRecording(0);
                        $configEntity->setConfigVideoQuality(1);
                        $configEntity->setConfigAudioEnable(1);
                        $configEntity->setConfigLowerThreshold(0);
                        $configEntity->setConfigUpperThreshold(0);
                        $configEntity->setConfigMotionBlock("FFF");
			

                        $configModel->saveConfig($configEntity);
                        $rand_num = Rand::getString(5, 'abcdefghijklmnopqrstuvwxyz0123456789', true);
                        $device_token = "sb_" . $deviceId . "_" . $rand_num;
                        $deviceEntity->setDeviceToken($device_token);
                        $device = $deviceModel->saveDevice($deviceEntity);
                        if($device){
                            //$PushNotificationModel = $this->getServiceLocator()->get('PushNotification');
                            // $msg = "You have a new voice mail"."_".$device_info[0]['deviceIP'];
                            // $PushNotificationModel->makePushNotification($user_id,$msg);
                            $status = true;
                            $message = "Device Added Successfully";
                            $config = parse_ini_file('config/config.ini');
                            $node_ip = $config['node_ip'];
                            $node_port = $config['node_port'];
			    $device_id = $device;

                        }
                        return new JsonModel(array("status" => $status, "message" => $message, "node_ip" => $node_ip, "node_port" => $node_port,"device_id" =>$device_id));
                        
                    }

                    else{
                        $status = false;
                        $message = "MAC Already Used";
                    }
                }
            } else {
                $status = false;
                $message = "No user found for given user_token";
            }
        }
        else {
            $status = false;
            $message = "Failed";
        }
        return new JsonModel(array("status" => $status, "message" => $message));
    }


    public function CheckRegisterAction()
    {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            if ($request->isPost()) {
                $data = json_decode(file_get_contents("php://input"), true);
                $mac = $data['device_mac'];
                $DeviceModel = $this->getServiceInstance('Device');
                $isRegistered = $DeviceModel->validateDevicemacWithUser($mac,$user_id);
                if($isRegistered) {
		    $modelCommon = $this->getServiceInstance('Common');
                    $currentTS = $modelCommon->getCurrentTimeStamp();
                    if($currentTS - $isRegistered[0]['deviceCreatedon'] <300 )
                    {
                        $status = true;
                        $message = "Camera is Registered";
                    }
                    
                    else{
                        $status = false;
                        $message = "Camera is not Registered";
                    }
                }
                else {
                    $status = false;
                    $message = "Camera is not Registered";
                }
            } else {
                $status = false;
                $message = "Fail";
            }
        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }



    public function checkNameAction()
    {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            if ($request->isPost()) {
                $data = json_decode(file_get_contents("php://input"), true);
                $DeviceModel = $this->getServiceInstance('Device');
                $isUniqueName = $DeviceModel->validateDeviceName($data['device_name'],$user_id);
                if($isUniqueName == 0) {
                        $status = true;
                        $message = "Name available";
                }
                else {
                    $status = false;
                    $message = "Name already used";
                }
            } else {
                $status = false;
                $message = "Fail";
            }
        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }


     public function deleteAction()
    {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            if ($request->isPost()) {
                $data = json_decode(file_get_contents("php://input"), true);
                $deviceId = $data['device_id'];
                $DeviceModel = $this->getServiceInstance('Device');
                $device_Id = $DeviceModel->getDeviceList($user_id,$deviceId);
                if($device_Id) {
                    $delete = $DeviceModel->deleteDevice($deviceId);
                    if($delete){
                        $status = true;
                        $message = "Device Deleted";
                    }

                }
                else {
                    $status = false;
                    $message = "Device Not Belong to Your Account";
                }
            } else {
                $status = false;
                $message = "Fail";
            }
        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }


    public function defaultCameraAction()
    {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            if ($request->isPost()) {
                $data = json_decode(file_get_contents("php://input"), true);
                $deviceId = $data['device_id'];
                $DeviceModel = $this->getServiceInstance('Device');
                $device_Id = $DeviceModel->getDeviceList($user_id,$deviceId);
                if($device_Id) {
                    $default = $DeviceModel->defaultDevice($user_id,$deviceId);
                    if($default){
                        $status = true;
                        $message = "Default Device Added";
                    }
                }
                else {
                    $status = false;
                    $message = "Device Not Belong to Your Account";
                }
            } else {
                $status = false;
                $message = "Fail";
            }
        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }


    public function registerCubeAction()
    {
        $response = array();
        $request = $this->getRequest();
        $this->authService = $this->getServiceInstance('AuthService');

        if ($request->isPost()) {
            $data = json_decode(file_get_contents("php://input"), true);
            $formData = $data;
            //Validate Device MAC
            $cubeModel = $this->getServiceInstance('Cube');
            $userModel = $this->getServiceInstance('User');
            $isUnique = $cubeModel->validateCubemac($data['cube_mac']);
            $user_id = $userModel->getUidfromtoken($data['user_token']);
            if ($user_id) {
                if ($isUnique == 0) {
                    // BEGIN-Create Device
                    $cubeEntity = $cubeModel->getEntityInstance();
                    $objectManager = $cubeModel->getObjectManager($this->getServiceLocator());
                    $created_time = time();
                    // $deviceEntity->setDeviceName(htmlspecialchars($data['device_name']));
                    $cubeEntity->setCubeMac(htmlspecialchars($data['cube_mac']));
                    $cubeEntity->setCubeCreatedon($created_time);
                    $cubeEntity->setCubePublicIP(htmlspecialchars($data['cube_publicip']));
                    $cubeEntity->setCubeLanIP(htmlspecialchars($data['cube_lanip']));
                    $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxUser', $user_id);
                    $cubeEntity->setCubeCreatedbyFk($userProxyInstance);
                    $cubeEntity->setCubeStatus(o); //0-> InActive Account
                    $cubeEntity->setCubePath(htmlspecialchars($data['cube_path']));
                    $cube_id = $cubeModel->saveCube($cubeEntity);
                    $rand_num = Rand::getString(5, 'abcdefghijklmnopqrstuvwxyz0123456789', true);
                    $cube_token = "sb_c_" . $cube_id . "_" . $rand_num;
                    $cubeEntity->setCubeToken($cube_token);
                    $cubeModel->saveCube($cubeEntity);
                    $status = true;
                    $message = "Cube Added Successfully";
                    $config = parse_ini_file('config/config.ini');
                    $node_ip = $config['node_ip'];
                    $node_port = $config['node_port'];
                    return new JsonModel(array("status" => $status, "message" => $message,"node_ip" =>$node_ip, "node_port" => $node_port));
                } else {
                    $status = false;
                    $message = "MAC Already Used";
                }
            } else {
                $status = false;
                $message = "No user found for given user_token";
            }
        }
        else {
            $status = false;
            $message = "Failed";
        }
        return new JsonModel(array("status" => $status, "message" => $message));
    }


     public function updateCubeAction()
    {
            $response = array();
            $request = $this->getRequest();
            $data = json_decode(file_get_contents("php://input"), true);
            $user_token = $data['user_token'];
            $UserModel = $this->getServiceInstance('User');
            $user_id = $UserModel ->getUidfromtoken($user_token);
            $CubeModel = $this->getServiceInstance('Cube');
            $cube_Info = $CubeModel->validateCube('cubeCreatedbyFk',$user_id,'cubeMac',$data['params']['cube_mac']);
            if($cube_Info) {
                $cubeModel = $this->getServiceInstance('Cube');
                $result = $cubeModel->updateCubeResult($data,$user_id);
                if($result) {
                    $status = true;
                    $message = "Cube Details Updated Successfully";
                }
                else{
                    $status = false;
                    $message = "Cube Details Not Updated";
                }
            }
            else{
                $status = false;
                $message = "Cube not belong to given user_token";
            }
        return new JsonModel(array("status" => $status, "message" => $message));
    }


    public function listAction()
    {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            $this->authService = $this->getServiceInstance('AuthService');
            $data = json_decode(file_get_contents("php://input"), true);
            $formData = $data;
            $deviceModel = $this->getServiceInstance('Device');
            $deviceEntity = $deviceModel->getEntityInstance();
            $objectManager = $deviceModel->getObjectManager($this->getServiceLocator());
            $device_list = $deviceModel->getDeviceList($user_id);

            if ($device_list) {
                $response['status'] = true;
                $response['data'] = $device_list;
                $response = $this->getResponseWithHeader()
                    ->setContent(json_encode($response));
                return $response;
            }
            else{
                $status = false;
                $message = "No Device found for this user";
            }
        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }


    public function deviceIdentityAction()
    {
        $response = array();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = json_decode(file_get_contents("php://input"), true);
            $commandId = $data['commandId'];
            $deviceMAC = $data['device_mac'];
            $userToken = $data['user_token'];
            if(!empty($deviceMAC) and !empty($userToken)) {
                $deviceModel = $this->getServiceInstance('Device');
                $deviceEntity = $deviceModel->getEntityInstance();
                $objectManager = $deviceModel->getObjectManager($this->getServiceLocator());
                $userModel = $this->getServiceInstance('User');
                $userId=$userModel->getUidfromtoken($data['user_token']);
                $deviceData=$deviceModel->validateDevice($deviceMAC,$userId);
		$cubeModel = $this->getServiceInstance('Cube');
                $cubeData = $cubeModel->validateCube('cubeCreatedbyFk',$userId,'cubeMac',$deviceMAC);
                if($deviceData){
                    $commandId = $data['commandId'];
                    $status = true;
                    $message = "Device Found";
                    $device_token = $deviceData[0][0]['deviceToken'];
                    $device_id = $deviceData[0][0]['deviceId'];
                }
		elseif($cubeData)
                {
                    $commandId = $data['commandId'];
                    $status = true;
                    $message = "Device Found";
                    $device_token = $cubeData[0]['cubeToken'];
                    $cube_id = "cube_".$cubeData[0]['cubeId'];
                    $device_id = $cube_id;
                }
                else{
                    $commandId = $data['commandId'];
                    $status = false;
                    $message = "Device Not Found";
                    $device_token = "";
                    $device_id = "";
                }
            }
            else{
                $commandId = $data['commandId'];
                $status = false;
                $message = "device_mac And user_token can not be null";
                $device_token = "";
                $device_id = "";
            }
        }
        else{
            $commandId = "";
            $status = false;
            $message = "Fail";
            $device_token = "";
            $device_id = "";
        }
        return new JsonModel(array("commandId" =>$commandId , "connection"=> "ok","status" => $status, "message" => $message, "device_token" =>$device_token,"device_id" =>$device_id));
    }


    public function scanSensorAction()
    {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            if ($request->isPost()) {
                $data = json_decode(file_get_contents("php://input"), true);
                $deviceId = $data['device_id'];
                $DeviceModel = $this->getServiceInstance('Device');
                $device_Id = $DeviceModel->getDeviceList($user_id,$deviceId);
                if($device_Id) {
                    $deviceModel = $this->getServiceInstance('Device');
                    $device = $deviceModel->Device_info('deviceId',$deviceId);
                    if($device){
                        $CommandModel = $this->getServiceInstance('Command');
                        $modelCommon = $this->getServiceInstance('Common');
                        $currentTS = $modelCommon->getCurrentTimeStamp();
                       /* $value = $CommandModel->checkCommandTS($deviceId,$currentTS);
                        if($value)
                        {
                            $status = true;
                            $response = $value[0]['response'];
                            return new JsonModel(array("status" => $status,"response" => $response));
                        }
                        else {*/
                            //insert to command table

                            $commandEntity = $CommandModel->getEntityInstance();
                            $objectManager = $CommandModel->getObjectManager($this->getServiceLocator());
                            $commandEntity->setCommandId(htmlspecialchars($data['commandId']));
                            $commandEntity->setUserToken(htmlspecialchars($data['user_token']));
                            $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId);
                            $commandEntity->setDeviceIdFK($userProxyInstance);
                            $commandEntity->setStatus('Pending'); //0-> InActive Account
                            $commandEntity->settimeStamp($currentTS);
                            $requestId = $CommandModel->saveCommand($commandEntity);
                            $data['requestId'] = $requestId;
                            $response = $this->callhttp($data);

                            $device_status = json_decode($response->getBody() , true);
                            if ($device_status['device_status'] == "open") {
                                $config = parse_ini_file('config/config.ini');
                                $status = true;
                                $message = "Sensor Scan Started";
                                $interval = $config['scan-time'];
                                return new JsonModel(array("status" => $status, "message" => $message, "requestId" => $requestId, "interval" => $interval));
                            } else {
                                $status = false;
                                $message = "Device is Not Connected";
                            }
                       // }

                    }
                    else {
                        $status = false;
                        $message = "device_id is invalid";
                    }

                } else {
                    $status = false;
                    $message = "Device Not Belong to Your Account";
                }
            } else {
                $status = false;
                $message = "Fail";
            }
        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }



    public function pairSensorAction()
    {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            if ($request->isPost()) {
                $data = json_decode(file_get_contents("php://input"), true);
                $deviceId = $data['device_id'];
                $DeviceModel = $this->getServiceInstance('Device');
                $device_Id = $DeviceModel->getDeviceList($user_id,$deviceId);
                if($device_Id) {
                    $SensorModel = $this->getServiceInstance('Sensor');
                    $validate_mac = $SensorModel->validateMac($data);
                    if($validate_mac == "true") {
                        $deviceModel = $this->getServiceInstance('Device');
                        $device = $deviceModel->Device_info('deviceId', $deviceId);
                        if ($device) {
                            $CommandModel = $this->getServiceInstance('Command');
                            $modelCommon = $this->getServiceInstance('Common');
                            $currentTS = $modelCommon->getCurrentTimeStamp();
                            /*$value = $CommandModel->checkCommandTS($deviceId,$currentTS);
                            if($value)
                            {
                                $status = true;
                                $response = $value[0]['response'];
                                return new JsonModel(array("status" => $status,"response" => $response));
                            }
                            {*/
                            //insert to command table

                            $commandEntity = $CommandModel->getEntityInstance();
                            $objectManager = $CommandModel->getObjectManager($this->getServiceLocator());
                            $commandEntity->setCommandId(htmlspecialchars($data['commandId']));
                            $commandEntity->setUserToken(htmlspecialchars($data['user_token']));
                            $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId);
                            $commandEntity->setDeviceIdFK($userProxyInstance);
                            $commandEntity->setStatus('Pending'); //0-> InActive Account
                            $commandEntity->settimeStamp($currentTS);
                            $requestId = $CommandModel->saveCommand($commandEntity);
                            $data['requestId'] = $requestId;
			    $data['count'] = 1;
                            $response = $this->callhttp($data);
                            $device_status = json_decode($response->getBody(), true);
                            if ($device_status['device_status'] == "open") {
                                    $om = $SensorModel->getObjectManager($SensorModel->serviceManager);
                                    $sensorEntity = $SensorModel->getEntityInstance();
                                    $sensorEntity->setSensorMac(htmlspecialchars($data['params']['sensor-mac']));
                                    $objectManager = $SensorModel->getObjectManager($this->getServiceLocator());
                                    $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId);
                                    $sensorEntity->setSensorDeviceId($userProxyInstance);
                                    $sensorEntity->setSensorName($data['params']['sensor-name']);
                                    $sensorEntity->setSensorStatus(1);
                                    $om->persist($sensorEntity);
                                    $om->flush();
                                    $om->clear();
                                $status = true;
                                $message = "Sensor Paired";
                                return new JsonModel(array("status" => $status, "message" => $message));
                            } else {
                                $status = false;
                                $message = "Device is Not Connected";
                            }
                            // }

                        } else {
                            $status = false;
                            $message = "device_id is invalid";
                        }
                    }
                    else {
                        $status = false;
                        $message = $message = $validate_mac." is already paired..Please unpair it";
                    }
                }
                else {
                    $status = false;
                    $message = $message = "Device Not Belong to Your Account";
                }
            } else {
                $status = false;
                $message = "Fail";
            }
        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }
 

    public function unpairSensorAction()
    {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            if ($request->isPost()) {
                $data = json_decode(file_get_contents("php://input"), true);
                $deviceId = $data['device_id'];
                $DeviceModel = $this->getServiceInstance('Device');
                $device_Id = $DeviceModel->getDeviceList($user_id,$deviceId);
                if($device_Id) {
                    $deviceModel = $this->getServiceInstance('Device');
                    $device = $deviceModel->Device_info('deviceId',$deviceId);
                    if($device){
                        $CommandModel = $this->getServiceInstance('Command');
                        $modelCommon = $this->getServiceInstance('Common');
                        $currentTS = $modelCommon->getCurrentTimeStamp();
                        /*$value = $CommandModel->checkCommandTS($deviceId,$currentTS);
                        if($value)
                        {
                            $status = true;
                            $response = $value[0]['response'];
                            return new JsonModel(array("status" => $status,"response" => $response));
                        }
                        {*/
                        //insert to command table

                        $commandEntity = $CommandModel->getEntityInstance();
                        $objectManager = $CommandModel->getObjectManager($this->getServiceLocator());
                        $commandEntity->setCommandId(htmlspecialchars($data['commandId']));
                        $commandEntity->setUserToken(htmlspecialchars($data['user_token']));
                        $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId);
                        $commandEntity->setDeviceIdFK($userProxyInstance);
                        $commandEntity->setStatus('Pending'); //0-> InActive Account
                        $commandEntity->settimeStamp($currentTS);
                        $requestId = $CommandModel->saveCommand($commandEntity);
                        $data['requestId'] = $requestId;
                        $response = $this->callhttp($data);
                        $device_status = json_decode($response->getBody() , true);
                        if ($device_status['device_status'] == "open") {
			
			    $SensorModel = $this->getServiceInstance('Sensor');
                            $updateUnpair = $SensorModel->updateUnpair($data);
                            $status = true;
                            $message = "Sensor Unpaired";
                            return new JsonModel(array("status" => $status, "message" => $message));
                        } else {
                            $status = false;
                            $message = "Device is Not Connected";
                        }
                        // }

                    }
                    else {
                        $status = false;
                        $message = "device_id is invalid";
                    }

                }
                else {
                    $status = false;
                    $message = $message = "Device Not Belong to Your Account";
                }
            } else {
                $status = false;
                $message = "Fail";
            }
        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }


     public function listPairedSensorAction()
    {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            $this->authService = $this->getServiceInstance('AuthService');
            $data = json_decode(file_get_contents("php://input"), true);
            $device_id = $data['device_id'];
            $DeviceModel = $this->getServiceInstance('Device');
            $device_Id = $DeviceModel->getDeviceList($user_id,$device_id);
            if($device_Id) {
                $sensorModel = $this->getServiceInstance('Sensor');
                $sensor_list = $sensorModel->getPairedList($device_id);
                if ($sensor_list) {
                    $response['status'] = true;
                    $response['data'] = $sensor_list;
                    $response = $this->getResponseWithHeader()
                        ->setContent(json_encode($response));
                    return $response;
                } else {
                    $status = false;
                    $message = "No Sensor found for this Device";
                    $data = "";
                }
            }
            else {
                $status = false;
                $message = $message = "Device Not Belong to Your Account";
            }
        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message,"data" => $data));
    }


    public function checkStatusAction()
    {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            if ($request->isPost()) {
                $data = json_decode(file_get_contents("php://input"), true);
                $request_id = $data['requestId'];
                if ($request_id) {
                    $commandModel = $this->getServiceInstance('Command');
                    $value = $commandModel->checkStatus($data);
                    if($value){
                        $modelCommon = $this->getServiceInstance('Common');
                        $currentTS = $modelCommon->getCurrentTimeStamp();
                        $time_elapsed = $currentTS - $value[0]['timeStamp'];
                        if ($time_elapsed > 180000000) {
                            //expired
                            $status = false;
                            $message = "Please Try again";
                        }
                        else{

 	            $response['status'] = true;
                    $response['response']= $value[0]['response'];
echo $response['response'];die;
                    $response = $this->getResponseWithHeader()
                        ->setContent(json_encode($response));
                    return $response;
                            //$status = true;
                            //$sensor_list =$value[0]['response'];
                            
                            //return new JsonModel(array("status" => $status,"response" => $sensor_list));
                        }
                    }
                    else{
                        $status = false;
                        $message = "No data corresponding to given requestId";
                    }
                }
                else {
                    $status = false;
                    $message = "requestId can not be null";
                }
            }
            else {
                $status = false;
                $message = "Fail";
            }
        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }
        return new JsonModel(array("status" => $status, "message" => $message));
    }

    public function scanSensorResultAction()
    {
        $response = array();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = json_decode(file_get_contents("php://input"), true);
            $requestId = $data['requestId'];
            $response = json_encode($data['response']);

            if (!empty($requestId)) {
                $commandModel = $this->getServiceInstance('Command');
                $value = $commandModel->updateScanResult($response,$requestId);
                if ($value) {
                    $status = true;
                    $message = "Scan Result Updated";
                }
                else{
                    $status = false;
                    $message = "Scan Result Updation Failed";
                }
            }
            else{
                $status = false;
                $message = "RequestId is invalid";
            }
        }
        else{
            $status = false;
            $message = "Fail";
        }
        return new JsonModel(array("status" => $status, "message" => $message));
    }


    public function registerSensorAction()
    {
        $response = array();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = json_decode(file_get_contents("php://input"), true);
            $sensorMAC = $data['sensor_mac'];
            $deviceToken = $data['device_token'];
            if(!empty($sensorMAC) and !empty($deviceToken)) {
                $deviceModel = $this->getServiceInstance('Device');
                $isUnique = $deviceModel->validateSensormac($data['sensor_mac']);
                if ($isUnique == 0) {
                    $sensorEntity = $deviceModel->getEntityInstance1();
                    $objectManager = $deviceModel->getObjectManager($this->getServiceLocator());
                    $deviceData = $deviceModel->infoFromDeviceToken($deviceToken);
                    if ($deviceData) {
                        $sensorEntity->setSensorMac(htmlspecialchars($data['sensor_mac']));
                        $sensorEntity->setSensorDeviceId(htmlspecialchars($deviceData[0]['deviceId']));
                        $sensorEntity->setSensorUserId(htmlspecialchars($deviceData[0]['device_createdby_fk']));
                        $deviceModel->saveSensor($sensorEntity);
                        $commandId = $data['commandId'];
                        $status = true;
                        $message = "Sensor Registered";
                    } else {
                        $commandId = $data['commandId'];
                        $status = false;
                        $message = "No Device Found for given Device Token";
                    }
                }
                else{
                    $commandId = $data['commandId'];
                    $status = false;
                    $message = "MAC Already used";
                }
            }
            else{
                $commandId = $data['commandId'];
                $status = false;
                $message = "device_mac And user_token can not be null";
            }
        }
        else{
            $commandId = "";
            $status = false;
            $message = "Fail";
        }

        return new JsonModel(array("commandId" =>$commandId , "connection"=> "ok","status" => $status, "message" => $message));
    }


    public function updateIpAction() {
        $response = array();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = json_decode(file_get_contents("php://input"), true);
            $mac = $data['device_mac'];
            $ip = $data['device_ip'];
            if(!empty($ip) and !empty($mac)) {
                $deviceModel = $this->getServiceInstance('Device');
                //$deviceEntity = $deviceModel->getEntityInstance();
                //$objectManager = $deviceModel->getObjectManager($this->getServiceLocator());;
                $deviceData = $deviceModel->Deviceinfo($mac);
                if ($deviceData) {
                    $update = $deviceModel->updateIP($data);
                    if ($update)
                    {
                        $status = true;
                        $message = "IP Updated";
                    }
                    else{
                        $status = false;
                        $message = "IP Not Updated";
                    }

                } else {
                    $status = false;
                    $message = "No Device Found for given Device Token";
                }
            }
            else{
                $status = false;
                $message = "device_mac And device_ip can not be null";
            }
        }
        else{
            $status = false;
            $message = "Fail";
        }
        return new JsonModel(array("status" => $status, "message" => $message));

    }

    public function editDeviceAction() {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            if ($request->isPost()) {
                $data = json_decode(file_get_contents("php://input"), true);
                $deviceId = $data['device_id'];
                $deviceModel = $this->getServiceInstance('Device');
                $deviceData = $deviceModel->Device_info('deviceId',$deviceId);
                if ($deviceData) {
                    $device_Id = $deviceModel->getDeviceList($user_id,$deviceId);
                    if($device_Id) {
                        $objectManager = $deviceModel->getObjectManager($this->getServiceLocator());
                        $result = $deviceModel->editDevice($data);
                        if($result){
                            $status = true;
                            $message = "Device Edited Successfully";
                            return new JsonModel(array("status" => $status, "message" => $message));
                        }
                    }
                    else{
                        $status = false;
                        $message = "Device Not Belong to your Account";
                    }

                }
                else{
                    $status = false;
                    $message = "No device found for given device_id";
                }
            }
            else{
                $status = false;
                $message = "Fail";
            }
        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }
        return new JsonModel(array("status" => $status, "message" => $message));

    }


    public function updateConfigAction() {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            $this->authService = $this->getServiceInstance('AuthService');
            $data = json_decode(file_get_contents("php://input"), true);
            $deviceId = $data['device_id'];
            $DeviceModel = $this->getServiceInstance('Device');
            $device_Id = $DeviceModel->getDeviceList($user_id,$deviceId);
            if($device_Id) {
                $deviceConfigModel = $this->getServiceInstance('DeviceConfig');
                $CommandModel = $this->getServiceInstance('Command');
                $modelCommon = $this->getServiceInstance('Common');
                $currentTS = $modelCommon->getCurrentTimeStamp();
                // $value = $CommandModel->checkCommandTS($deviceId, $currentTS);
                /*if ($value) {
                    $status = true;
                    $response = $value[0]['response'];
                    return new JsonModel(array("status" => $status, "response" => $response));
                } else {*/
                //insert to command table
                $commandEntity = $CommandModel->getEntityInstance();
                $objectManager = $CommandModel->getObjectManager($this->getServiceLocator());
                $commandEntity->setCommandId(htmlspecialchars($data['commandId']));
                $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId);
                $commandEntity->setDeviceIdFK($userProxyInstance);
                $commandEntity->setStatus('Pending'); //0-> InActive Account
                $commandEntity->settimeStamp($currentTS);
                $requestId = $CommandModel->saveCommand($commandEntity);
                $data['requestId'] = $requestId;
                $response = $this->callhttp($data);
                $device_status = json_decode($response->getBody() , true);
                if ($device_status['device_status'] == "open") {
		    $result = $deviceConfigModel->updateConfig($data);
                    $config = parse_ini_file('config/config.ini');
                    $status = true;
                    $message = "Configuring Settings";
                    $interval = $config['scan-time'];
                    return new JsonModel(array("status" => $status, "message" => $message, "requestId" => $requestId, "interval" => $interval));
                } else {
                    $status = false;
                    $message = "Device is Not Connected";
                }
                // }
            }
            else{
                $status = false;
                $message = "Device Not Belong to your Account";
            }

        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }

    public function viewConfigAction()
    {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            $data = json_decode(file_get_contents("php://input"), true);
            $deviceId = $data['device_id'];
            $deviceModel = $this->getServiceInstance('Device');
            $device = $deviceModel->Device_info('deviceId',$deviceId);
            if($device){
                $configModel = $this->getServiceInstance('DeviceConfig');
                $config = $configModel->getDeviceConfig($deviceId);
                if ($config) {
                    $response['status'] = true;
                    $response['response']['motion-detection'] = $config[0]['configMotionDetection'];
                    $response['response']['cube-recording'] = $config[0]['configCubeRecording'];
                    $response['response']['cloud-recording'] = $config[0]['configCloudRecording'];
                    $response['response']['audio-enable'] = $config[0]['configAudioEnable'];
                    $response['response']['hd-enable'] = $config[0]['configVideoQuality'];
		    $response['response']['voice-mail'] = $config[0]['configVoiceMail'];
		    $response['response']['lower-threshold'] = $config[0]['configLowerThreshold'];
                    $response['response']['upper-threshold'] = $config[0]['configUpperThreshold'];
		    $response['response']['lower-threshold-alert'] = $config[0]['configLowerThresholdAlert'];
                    $response['response']['upper-threshold-alert'] = $config[0]['configUpperThresholdAlert'];
                    $response = $this->getResponseWithHeader()
                        ->setContent(json_encode($response));
                    return $response;
                }
               else{
                    $status = false;
                    $message = "Configuration not found";
                }
            }

            else{
                $status = false;
                $message = "No Device found";
            }

        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }
        return new JsonModel(array("status" => $status, "message" => $message));

    }


    public function setLowerThresholdAction() {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            $this->authService = $this->getServiceInstance('AuthService');
            $data = json_decode(file_get_contents("php://input"), true);
            $deviceId = $data['device_id'];
            $DeviceModel = $this->getServiceInstance('Device');
            $device_Id = $DeviceModel->getDeviceList($user_id,$deviceId);
            if($device_Id) {
                $CommandModel = $this->getServiceInstance('Command');
                $modelCommon = $this->getServiceInstance('Common');
                $currentTS = $modelCommon->getCurrentTimeStamp();
                // $value = $CommandModel->checkCommandTS($deviceId, $currentTS);
                /*if ($value) {
                    $status = true;
                    $response = $value[0]['response'];
                    return new JsonModel(array("status" => $status, "response" => $response));
                } else {*/
                //insert to command table & Config Table
                $deviceConfigModel = $this->getServiceInstance('DeviceConfig');
                $commandEntity = $CommandModel->getEntityInstance();
                $objectManager = $CommandModel->getObjectManager($this->getServiceLocator());
                $commandEntity->setCommandId(htmlspecialchars($data['commandId']));
                $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId);
                $commandEntity->setDeviceIdFK($userProxyInstance);
                $commandEntity->setStatus('Pending'); //0-> InActive Account
                $commandEntity->settimeStamp($currentTS);
                $requestId = $CommandModel->saveCommand($commandEntity);
                $data['requestId'] = $requestId;
                $response = $this->callhttp($data);
                $device_status = json_decode($response->getBody() , true);
                if ($device_status['device_status'] == "open") {
                    $result = $deviceConfigModel->updateLowerThreshold($data);
                    $config = parse_ini_file('config/config.ini');
                    $status = true;
                    $message = "Setting lower Threshold Temperature ";
                    $interval = $config['scan-time'];
                    return new JsonModel(array("status" => $status, "message" => $message, "requestId" => $requestId, "interval" => $interval));
                } else {
                    $status = false;
                    $message = "Device is Not Connected";
                }
                // }
            }
            else{
                $status = false;
                $message = "Device Not Belong to your Account";
            }

        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }


    public function viewLowerThresholdAction()
    {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            $data = json_decode(file_get_contents("php://input"), true);
            $deviceId = $data['device_id'];
            $deviceModel = $this->getServiceInstance('Device');
            $device = $deviceModel->Device_info('deviceId',$deviceId);
            if($device){
                $configModel = $this->getServiceInstance('DeviceConfig');
                $config = $configModel->getDeviceConfig($deviceId);
                if ($config) {
                    $response['status'] = true;
                    $response['response']['temp'] = $config[0]['configLowerThreshold'];
                    $response = $this->getResponseWithHeader()
                        ->setContent(json_encode($response));
                    return $response;
                }
                else{
                    $status = false;
                    $message = "No Device found for this user";
                }
            }

        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }
        return new JsonModel(array("status" => $status, "message" => $message));

    }




    public function setUpperThresholdAction() {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            $this->authService = $this->getServiceInstance('AuthService');
            $data = json_decode(file_get_contents("php://input"), true);
            $deviceId = $data['device_id'];
            $DeviceModel = $this->getServiceInstance('Device');
            $device_Id = $DeviceModel->getDeviceList($user_id,$deviceId);
            if($device_Id) {
                $CommandModel = $this->getServiceInstance('Command');
                $modelCommon = $this->getServiceInstance('Common');
                $currentTS = $modelCommon->getCurrentTimeStamp();
                // $value = $CommandModel->checkCommandTS($deviceId, $currentTS);
                /*if ($value) {
                    $status = true;
                    $response = $value[0]['response'];
                    return new JsonModel(array("status" => $status, "response" => $response));
                } else {*/
                //insert to command table & Config Table
                $deviceConfigModel = $this->getServiceInstance('DeviceConfig');
                $commandEntity = $CommandModel->getEntityInstance();
                $objectManager = $CommandModel->getObjectManager($this->getServiceLocator());
                $commandEntity->setCommandId(htmlspecialchars($data['commandId']));
                $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId);
                $commandEntity->setDeviceIdFK($userProxyInstance);
                $commandEntity->setStatus('Pending'); //0-> InActive Account
                $commandEntity->settimeStamp($currentTS);
                $requestId = $CommandModel->saveCommand($commandEntity);
                $data['requestId'] = $requestId;
                $response = $this->callhttp($data);
                $device_status = json_decode($response->getBody() , true);
                if ($device_status['device_status'] == "open") {
                    $result = $deviceConfigModel->updateUpperThreshold($data);
                    $config = parse_ini_file('config/config.ini');
                    $status = true;
                    $message = "Setting upper Threshold Temperature ";
                    $interval = $config['scan-time'];
                    return new JsonModel(array("status" => $status, "message" => $message, "requestId" => $requestId, "interval" => $interval));
                } else {
                    $status = false;
                    $message = "Device is Not Connected";
                }
                // }
            }
            else{
                $status = false;
                $message = "Device Not Belong to your Account";
            }

        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }


    public function viewUpperThresholdAction()
    {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            $data = json_decode(file_get_contents("php://input"), true);
            $deviceId = $data['device_id'];
            $deviceModel = $this->getServiceInstance('Device');
            $device = $deviceModel->Device_info('deviceId',$deviceId);
            if($device){

                $configModel = $this->getServiceInstance('DeviceConfig');
                $config = $configModel->getDeviceConfig($deviceId);
                if ($config) {
                    $response['status'] = true;
                    $response['response']['temp'] = $config[0]['configUpperThreshold'];
                    $response = $this->getResponseWithHeader()
                        ->setContent(json_encode($response));
                    return $response;
                }
                else{
                    $status = false;
                    $message = "No Device found for this user";
                }
            }

        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }
        return new JsonModel(array("status" => $status, "message" => $message));

    }

    public function currentTempAction() {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            $this->authService = $this->getServiceInstance('AuthService');
            $data = json_decode(file_get_contents("php://input"), true);
            $deviceId = $data['device_id'];
            $DeviceModel = $this->getServiceInstance('Device');
            $device_Id = $DeviceModel->getDeviceList($user_id,$deviceId);
            if($device_Id) {
                $CommandModel = $this->getServiceInstance('Command');
                $modelCommon = $this->getServiceInstance('Common');
                $currentTS = $modelCommon->getCurrentTimeStamp();
                // $value = $CommandModel->checkCommandTS($deviceId, $currentTS);
                /*if ($value) {
                    $status = true;
                    $response = $value[0]['response'];
                    return new JsonModel(array("status" => $status, "response" => $response));
                } else {*/
                //insert to command table
                $commandEntity = $CommandModel->getEntityInstance();
                $objectManager = $CommandModel->getObjectManager($this->getServiceLocator());
                $commandEntity->setCommandId(htmlspecialchars($data['commandId']));
                $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId);
                $commandEntity->setDeviceIdFK($userProxyInstance);
                $commandEntity->setStatus('Pending'); //0-> InActive Account
                $commandEntity->settimeStamp($currentTS);
                $requestId = $CommandModel->saveCommand($commandEntity);
                $data['requestId'] = $requestId;
                $response = $this->callhttp($data);
                $device_status = json_decode($response->getBody() , true);
                if ($device_status['device_status'] == "open") {
                    $config = parse_ini_file('config/config.ini');
                    $status = true;
                    $message = " Getting Temperature";
                    $interval = $config['scan-time'];
                    return new JsonModel(array("status" => $status, "message" => $message, "requestId" => $requestId, "interval" => $interval));
                } else {
                    $status = false;
                    $message = "Device is Not Connected";
                }
                // }
            }
            else{
                $status = false;
                $message = "Device Not Belong to your Account";
            }

        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }

 public function scanAccesspointAction()
    {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            if ($request->isPost()) {
                $data = json_decode(file_get_contents("php://input"), true);
                $deviceId = $data['device_id'];
                $userToken = $data['user_token'];
                if (!empty($deviceId) and !empty($userToken)) {
                    $deviceModel = $this->getServiceInstance('Device');
                    $device = $deviceModel->Device_info('deviceId',$deviceId);
                    if($device){
                        $CommandModel = $this->getServiceInstance('Command');
                        $modelCommon = $this->getServiceInstance('Common');
                        $currentTS = $modelCommon->getCurrentTimeStamp();
                        $value = $CommandModel->checkCommandTS($deviceId,$currentTS);
                        if($value)
                        {
                            $status = true;
                            $response = $value[0]['response'];
                            return new JsonModel(array("status" => $status,"response" => $response));
                        }
                        else {
                            //insert to command table

                            $commandEntity = $CommandModel->getEntityInstance();
                            $objectManager = $CommandModel->getObjectManager($this->getServiceLocator());
                            $commandEntity->setCommandId(htmlspecialchars($data['commandId']));
                            $commandEntity->setUserToken(htmlspecialchars($data['user_token']));
                            $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId);
                            $commandEntity->setDeviceIdFK($userProxyInstance);
                            $commandEntity->setStatus('Pending'); //0-> InActive Account
                            $commandEntity->settimeStamp($currentTS);
                            $requestId = $CommandModel->saveCommand($commandEntity);
                            $data['requestId'] = $requestId;
                            $response = $this->callhttp($data);
                            $device_status = json_decode($response->getBody() , true);
                            if ($device_status['device_status'] == "open") {
                                $config = parse_ini_file('config/config.ini');
                                $status = true;
                                $message = "Accesspoint Scan Started";
                                $interval = $config['scan-time'];
                                return new JsonModel(array("status" => $status, "message" => $message, "requestId" => $requestId, "interval" => $interval));
                            } else {
                                $status = false;
                                $message = "Device is Not Connected";
                            }
                        }

                    }
                    else {
                        $status = false;
                        $message = "device_id is invalid";
                    }

                } else {
                    $status = false;
                    $message = "device_id And user_token can not be null";
                }
            } else {
                $status = false;
                $message = "Fail";
            }
        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }

    public function restoreConfigAction() {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            $data = json_decode(file_get_contents("php://input"), true);
            $deviceId = $data['device_id'];
            $DeviceModel = $this->getServiceInstance('Device');
            $device_Id = $DeviceModel->getDeviceList($user_id,$deviceId);
            if($device_Id) {
                $CommandModel = $this->getServiceInstance('Command');
                $modelCommon = $this->getServiceInstance('Common');
                $currentTS = $modelCommon->getCurrentTimeStamp();
                // $value = $CommandModel->checkCommandTS($deviceId, $currentTS);
                /*if ($value) {
                    $status = true;
                    $response = $value[0]['response'];
                    return new JsonModel(array("status" => $status, "response" => $response));
                } else {*/
                //insert to command table & Config Table
                $deviceConfigModel = $this->getServiceInstance('DeviceConfig');
                $result = $deviceConfigModel->restoreConfig($deviceId);
                $commandEntity = $CommandModel->getEntityInstance();
                $objectManager = $CommandModel->getObjectManager($this->getServiceLocator());
                $commandEntity->setCommandId(htmlspecialchars($data['commandId']));
                $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId);
                $commandEntity->setDeviceIdFK($userProxyInstance);
                $commandEntity->setStatus('Pending'); //0-> InActive Account
                $commandEntity->settimeStamp($currentTS);
		$requestId = $CommandModel->saveCommand($commandEntity);
                $data['requestId'] = $requestId;
                $response = $this->callhttp($data);
                $device_status = json_decode($response->getBody() , true);
                if ($device_status['device_status'] == "open") {
		    $delete = $DeviceModel->deleteDevice($deviceId);
                    $sensorModel = $this->getServiceInstance('Sensor');
                    $sensor_list = $sensorModel->getPairedList($deviceId);
                    $result = $sensorModel->restoreSensor($sensor_list);
		    if($delete){
			$status = true;
                    	$message = "Camera Restored";
		    }
                    else{
			$status = false;
                   	$message = "Camera  is Not Restored";
		    }
                    return new JsonModel(array("status" => $status, "message" => $message));
                } else {
                    $status = false;
                    $message = "Device is Not Connected";
                }
                // }
            }
            else{
                $status = false;
                $message = "Device Not Belong to your Account";
            }

        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }
   

     public function OnDemandRecordingAction()
    {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            $data = json_decode(file_get_contents("php://input"), true);
            $deviceId = $data['device_id'];
            $DeviceModel = $this->getServiceInstance('Device');
            $device_Id = $DeviceModel->getDeviceList($user_id,$deviceId);
            if($device_Id) {
                $CommandModel = $this->getServiceInstance('Command');
                $modelCommon = $this->getServiceInstance('Common');
                $currentTS = $modelCommon->getCurrentTimeStamp();
                // $value = $CommandModel->checkCommandTS($deviceId, $currentTS);
                /*if ($value) {
                    $status = true;
                    $response = $value[0]['response'];
                    return new JsonModel(array("status" => $status, "response" => $response));
                } else {*/
                //insert to command table & Config Table
                $commandEntity = $CommandModel->getEntityInstance();
                $objectManager = $CommandModel->getObjectManager($this->getServiceLocator());
                $commandEntity->setCommandId(htmlspecialchars($data['commandId']));
                $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId);
                $commandEntity->setDeviceIdFK($userProxyInstance);
                $commandEntity->setStatus('Pending'); //0-> InActive Account
                $commandEntity->settimeStamp($currentTS);
                $requestId = $CommandModel->saveCommand($commandEntity);
                $data['requestId'] = $requestId;
                $response = $this->callhttp($data);
                $device_status = json_decode($response->getBody() , true);
                if ($device_status['device_status'] == "open") {
                    $config = parse_ini_file('config/config.ini');
                    $status = true;
                    $message = "Requested to start Video Recording";
                    return new JsonModel(array("status" => $status, "message" => $message));
                } else {
                    $status = false;
                    $message = "Device is Not Connected";
                }
                // }
            }
            else{
                $status = false;
                $message = "Device Not Belong to your Account";
            }

        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }


    public function StopRecordingAction()
    {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            $data = json_decode(file_get_contents("php://input"), true);
            $deviceId = $data['device_id'];
            $DeviceModel = $this->getServiceInstance('Device');
            $device_Id = $DeviceModel->getDeviceList($user_id,$deviceId);
            if($device_Id) {
                $CommandModel = $this->getServiceInstance('Command');
                $modelCommon = $this->getServiceInstance('Common');
                $currentTS = $modelCommon->getCurrentTimeStamp();
                // $value = $CommandModel->checkCommandTS($deviceId, $currentTS);
                /*if ($value) {
                    $status = true;
                    $response = $value[0]['response'];
                    return new JsonModel(array("status" => $status, "response" => $response));
                } else {*/
                //insert to command table & Config Table
                $commandEntity = $CommandModel->getEntityInstance();
                $objectManager = $CommandModel->getObjectManager($this->getServiceLocator());
                $commandEntity->setCommandId(htmlspecialchars($data['commandId']));
                $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId);
                $commandEntity->setDeviceIdFK($userProxyInstance);
                $commandEntity->setStatus('Pending'); //0-> InActive Account
                $commandEntity->settimeStamp($currentTS);
                $requestId = $CommandModel->saveCommand($commandEntity);
                $data['requestId'] = $requestId;
                $response = $this->callhttp($data);
                $device_status = json_decode($response->getBody() , true);
                if ($device_status['device_status'] == "open") {
                    $config = parse_ini_file('config/config.ini');
                    $status = true;
                    $message = "Requested to stop Video Recording";
                    return new JsonModel(array("status" => $status, "message" => $message));
                } else {
                    $status = false;
                    $message = "Device is Not Connected";
                }
                // }
            }
            else{
                $status = false;
                $message = "Device Not Belong to your Account";
            }

        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }



    public function deleteVideoAction()
    {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            $data = json_decode(file_get_contents("php://input"), true);
            $deviceId = $data['device_id'];
            $DeviceModel = $this->getServiceInstance('Device');
            $device_Id = $DeviceModel->getDeviceList($user_id,$deviceId);
            if($device_Id) {
                $CommandModel = $this->getServiceInstance('Command');
                $modelCommon = $this->getServiceInstance('Common');
                $currentTS = $modelCommon->getCurrentTimeStamp();
                // $value = $CommandModel->checkCommandTS($deviceId, $currentTS);
                /*if ($value) {
                    $status = true;
                    $response = $value[0]['response'];
                    return new JsonModel(array("status" => $status, "response" => $response));
                } else {*/
                //insert to command table & Config Table
                $commandEntity = $CommandModel->getEntityInstance();
                $objectManager = $CommandModel->getObjectManager($this->getServiceLocator());
                $commandEntity->setCommandId(htmlspecialchars($data['commandId']));
                $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId);
                $commandEntity->setDeviceIdFK($userProxyInstance);
                $commandEntity->setStatus('Pending'); //0-> InActive Account
                $commandEntity->settimeStamp($currentTS);
                $requestId = $CommandModel->saveCommand($commandEntity);
                $data['requestId'] = $requestId;
                $cubeModel = $this->getServiceInstance('Cube');
                $cubeInfo = $cubeModel->getCubeinfo('cubeCreatedbyFk',$user_id);
		$size =  sizeof( $cubeInfo);
    	    	$last = $size-1;
                $cubeId = "cube_".$cubeInfo[$last]['cubeId'];
                $data['cube_id']= $cubeId;
                $response = $this->callhttp_cube($data);
                $device_status = json_decode($response->getBody() , true);
                if ($device_status['device_status'] == "open") {
                    $config = parse_ini_file('config/config.ini');
		    $interval = $config['scan-time'];
                    $status = true;
                    $message = "Requested to Delete Files";
                    return new JsonModel(array("status" => $status, "message" => $message, "requestId" => $requestId, "interval" => $interval));
                } else {
                    $status = false;
                    $message = "Device is Not Connected";
                }
                // }
            }
            else{
                $status = false;
                $message = "Device Not Belong to your Account";
            }

        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }


    public function startCallAction()
    {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            $data = json_decode(file_get_contents("php://input"), true);
            $deviceIp = $data['ip_address'];
            $DeviceModel = $this->getServiceInstance('Device');
            $deviceId = $DeviceModel->infoFromDeviceIP($deviceIp,$user_id);
            $device_Id = $DeviceModel->getDeviceList($user_id,$deviceId[0]['deviceId']);
            if($device_Id) {
                $CommandModel = $this->getServiceInstance('Command');
                $modelCommon = $this->getServiceInstance('Common');
                $currentTS = $modelCommon->getCurrentTimeStamp();
                // $value = $CommandModel->checkCommandTS($deviceId, $currentTS);
                /*if ($value) {
                    $status = true;
                    $response = $value[0]['response'];
                    return new JsonModel(array("status" => $status, "response" => $response));
                } else {*/
                //insert to command table & Config Table
                $commandEntity = $CommandModel->getEntityInstance();
                $objectManager = $CommandModel->getObjectManager($this->getServiceLocator());
                $commandEntity->setCommandId(htmlspecialchars($data['commandId']));
                $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId[0]['deviceId']);
                $commandEntity->setDeviceIdFK($userProxyInstance);
                $commandEntity->setStatus('Pending'); //0-> InActive Account
                $commandEntity->settimeStamp($currentTS);
                $requestId = $CommandModel->saveCommand($commandEntity);
                $data['requestId'] = $requestId;
	        $data['device_id'] = $deviceId[0]['deviceId'];
                $response = $this->callhttp($data);
                $device_status = json_decode($response->getBody() , true);
                if ($device_status['device_status'] == "open") {
                    $config = parse_ini_file('config/config.ini');
                    $status = true;
                    $message = "Requested to start Call";
                    return new JsonModel(array("status" => $status, "message" => $message));
                } else {
                    $status = false;
                    $message = "Device is Not Connected";
                }
                // }
            }
            else{
                $status = false;
                $message = "Device Not Belong to your Account";
            }

        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }    


    public function endCallAction()
    {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            $data = json_decode(file_get_contents("php://input"), true);
            $deviceIp = $data['ip_address'];
            $DeviceModel = $this->getServiceInstance('Device');
            $deviceId = $DeviceModel->infoFromDeviceIP($deviceIp,$user_id);
            $device_Id = $DeviceModel->getDeviceList($user_id,$deviceId[0]['deviceId']);
            if($device_Id) {
                $CommandModel = $this->getServiceInstance('Command');
                $modelCommon = $this->getServiceInstance('Common');
                $currentTS = $modelCommon->getCurrentTimeStamp();
                // $value = $CommandModel->checkCommandTS($deviceId, $currentTS);
                /*if ($value) {
                    $status = true;
                    $response = $value[0]['response'];
                    return new JsonModel(array("status" => $status, "response" => $response));
                } else {*/
                //insert to command table & Config Table
                $commandEntity = $CommandModel->getEntityInstance();
                $objectManager = $CommandModel->getObjectManager($this->getServiceLocator());
                $commandEntity->setCommandId(htmlspecialchars($data['commandId']));
                $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId[0]['deviceId']);
                $commandEntity->setDeviceIdFK($userProxyInstance);
                $commandEntity->setStatus('Pending'); //0-> InActive Account
                $commandEntity->settimeStamp($currentTS);
                $requestId = $CommandModel->saveCommand($commandEntity);
                $data['requestId'] = $requestId;
                $data['device_id'] = $deviceId[0]['deviceId'];
                $response = $this->callhttp($data);
                $device_status = json_decode($response->getBody() , true);
                if ($device_status['device_status'] == "open") {
                    $config = parse_ini_file('config/config.ini');
                    $status = true;
                    $message = "Requested to end Call";
                    return new JsonModel(array("status" => $status, "message" => $message));
                } else {
                    $status = false;
                    $message = "Device is Not Connected";
                }
                // }
            }
            else{
                $status = false;
                $message = "Device Not Belong to your Account";
            }

        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }


    public function checkCubeStatusAction()
    {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            $data = json_decode(file_get_contents("php://input"), true);
            $cubeModel = $this->getServiceInstance('Cube');
            $cubeInfo = $cubeModel->getCubeinfo('cubeCreatedbyFk',$user_id);
            $cube_Id = $cubeInfo[0]['cubeId'];
            if($cube_Id) {
                $size =  sizeof( $cubeInfo);
                $last = $size-1;
                $data['cube_id'] = "cube_".$cubeInfo[$last]['cubeId'];
                $response = $this->callhttp_cube($data);
                $device_status = json_decode($response->getBody() , true);
//echo $device_status['device_status'];die;

                if ($device_status['device_status'] == "open") {
                    $config = parse_ini_file('config/config.ini');
                    $status = true;
                    $message = "Cube is connected to node";
                    return new JsonModel(array("status" => $status, "message" => $message));
                } else {
                    $status = false;
                    $message = "Cube is Not Connected to Node";
                }
                // }
            }
            else{
                $status = false;
                $message = "Cube Not Belong to your Account";
            }

        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }


     public function setMotionBlockAction() {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            $this->authService = $this->getServiceInstance('AuthService');
            $data = json_decode(file_get_contents("php://input"), true);
            $deviceId = $data['device_id'];
            $DeviceModel = $this->getServiceInstance('Device');
            $device_Id = $DeviceModel->getDeviceList($user_id,$deviceId);
            if($device_Id) {
                $CommandModel = $this->getServiceInstance('Command');
                $modelCommon = $this->getServiceInstance('Common');
                $currentTS = $modelCommon->getCurrentTimeStamp();
                // $value = $CommandModel->checkCommandTS($deviceId, $currentTS);
                /*if ($value) {
                    $status = true;
                    $response = $value[0]['response'];
                    return new JsonModel(array("status" => $status, "response" => $response));
                } else {*/
                //insert to command table & Config Table
                $deviceConfigModel = $this->getServiceInstance('DeviceConfig');
                $commandEntity = $CommandModel->getEntityInstance();
                $objectManager = $CommandModel->getObjectManager($this->getServiceLocator());
                $commandEntity->setCommandId(htmlspecialchars($data['commandId']));
                $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId);
                $commandEntity->setDeviceIdFK($userProxyInstance);
                $commandEntity->setStatus('Pending'); //0-> InActive Account
                $commandEntity->settimeStamp($currentTS);
                $requestId = $CommandModel->saveCommand($commandEntity);
                $data['requestId'] = $requestId;
                $response = $this->callhttp($data);
                $device_status = json_decode($response->getBody() , true);
                if ($device_status['device_status'] == "open") {
                    $result = $deviceConfigModel->updateMotionBlock($data);
                    $config = parse_ini_file('config/config.ini');
                    $status = true;
                    $message = "Setting motion detected block";
                    $interval = $config['scan-time'];
                    return new JsonModel(array("status" => $status, "message" => $message, "requestId" => $requestId, "interval" => $interval));
                } else {
                    $status = false;
                    $message = "Device is Not Connected";
                }
                // }
            }
            else{
                $status = false;
                $message = "Device Not Belong to your Account";
            }

        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }


    public function getMotionBlockAction()
    {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            $data = json_decode(file_get_contents("php://input"), true);
            $deviceId = $data['device_id'];
            $deviceModel = $this->getServiceInstance('Device');
            $device = $deviceModel->Device_info('deviceId',$deviceId);
            if($device){
                $configModel = $this->getServiceInstance('DeviceConfig');
                $config = $configModel->getDeviceConfig($deviceId);
                if ($config) {
                    $response['status'] = true;
                    $response['response']['block'] = $config[0]['configMotionBlock'];
                    $response = $this->getResponseWithHeader()
                        ->setContent(json_encode($response));
                    return $response;
                }
                else{
                    $status = false;
                    $message = "No Device found for this user";
                }
            }

        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }
        return new JsonModel(array("status" => $status, "message" => $message));

    }


     public function setModeAction()
    {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            if ($request->isPost()) {
                $data = json_decode(file_get_contents("php://input"), true);
                $deviceId = $data['device_id'];
                $deviceMode = $data['device_mode'];
                $deviceModel = $this->getServiceInstance('Device');
                $device = $deviceModel->setMode($deviceId,$deviceMode);
                if($device) {
                    $status = true;
                    $message = "Device Mode Configured Successfully";

                }
                else {
                    $status = false;
                    $message = "Device Mode Setting Failed";
                }
            } else {
                $status = false;
                $message = "Fail";
            }
        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }

    public function getModeAction()
    {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            if ($request->isPost()) {
                $data = json_decode(file_get_contents("php://input"), true);
                $deviceId = $data['device_id'];;
                $DeviceModel = $this->getServiceInstance('Device');
                $DeviceMode = $DeviceModel->getMode($deviceId);
                if($DeviceMode) {
                    $status = true;
                    $device_mode = $DeviceMode;
                    return new JsonModel(array("status" => $status, "device_mode" => $device_mode));
                }
                else {
                    $status = false;
                    $message = "Device Mode is not available";
                }
            } else {
                $status = false;
                $message = "Fail";
            }
        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }


     public function getFWVersionAction() {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            $this->authService = $this->getServiceInstance('AuthService');
            $data = json_decode(file_get_contents("php://input"), true);
            $deviceId = $data['device_id'];
            $DeviceModel = $this->getServiceInstance('Device');
            $device_Id = $DeviceModel->getDeviceList($user_id,$deviceId);
            if($device_Id) {
               $deviceData = $DeviceModel->Device_info('deviceId',$deviceId);
		$status = true;
		$fw_version =$deviceData[0]['deviceFW'];
		return new JsonModel(array("status" => $status, "fw_version" => $fw_version));
            }
            else{
                $status = false;
                $message = "Device Not Belong to your Account";
            }

        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }



      public function updateFWAction() {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            $this->authService = $this->getServiceInstance('AuthService');
            $data = json_decode(file_get_contents("php://input"), true);
            $deviceId = $data['device_id'];
            $DeviceModel = $this->getServiceInstance('Device');
            $device_Id = $DeviceModel->getDeviceList($user_id,$deviceId);
            if($device_Id) {
                $CommandModel = $this->getServiceInstance('Command');
                $modelCommon = $this->getServiceInstance('Common');
                $currentTS = $modelCommon->getCurrentTimeStamp();
                // $value = $CommandModel->checkCommandTS($deviceId, $currentTS);
                /*if ($value) {
                    $status = true;
                    $response = $value[0]['response'];
                    return new JsonModel(array("status" => $status, "response" => $response));
                } else {*/
                //insert to command table
                $commandEntity = $CommandModel->getEntityInstance();
                $objectManager = $CommandModel->getObjectManager($this->getServiceLocator());
                $commandEntity->setCommandId(htmlspecialchars($data['commandId']));
                $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId);
                $commandEntity->setDeviceIdFK($userProxyInstance);
                $commandEntity->setStatus('Pending'); //0-> InActive Account
                $commandEntity->settimeStamp($currentTS);
                $requestId = $CommandModel->saveCommand($commandEntity);
                $data['requestId'] = $requestId;
                $config = parse_ini_file('config/config.ini');
		$data['kernel_path'] = $config['kernel-path'];
		$data['fs_path'] = $config['fs-path'];
		$data['kernel_md5'] = $config['kernel-md5'];
		$data['fs_md5'] = $config['fs-md5'];
                $kernel_path = $config['kernel-path'];
                $fs_path = $config['fs-path'];
                $kernel_md5 = $config['kernel-md5'];
                $fs_md5 = $config['fs-md5'];
                //$payload =array("kernel-path" => $kernel_path, "fs-path" => $fs_path, "kernel-md5" => $kernel_md5, "fs-md5" => $fs_md5);
		//$params = json_encode($payload, JSON_UNESCAPED_SLASHES);
                //$data['params'] = $params;
                $response = $this->callhttp_fw($data);
                $device_status = json_decode($response->getBody() , true);
                if ($device_status['device_status'] == "open") {
                    $status = true;
                    $message = "Updating Firmware started";
                    return new JsonModel(array("status" => $status, "message" => $message, "requestId" => $requestId, "interval" => $interval));
                } else {
                    $status = false;
                    $message = "Device is Not Connected";
                }
                // }
            }
            else{
                $status = false;
                $message = "Device Not Belong to your Account";
            }

        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }


public function identifyCameraAction()
    {
        $response = array();
        $request = $this->getRequest();
        
            $data = json_decode(file_get_contents("php://input"), true);
            $mac = $_GET['mac'];
            $DeviceModel = $this->getServiceInstance('Device');
	    $UserModel = $this->getServiceInstance('User');
            $devicData = $DeviceModel->Deviceinfo($mac);
	    $user = $UserModel->selectdata($devicData[0]['device_createdby_fk']);

echo "Device id : "; echo $devicData[0]['deviceId'];echo "<br>";
echo "Device Name : ";echo $devicData[0]['deviceName'];echo "<br>";
echo "Device Mac : ";echo $devicData[0]['deviceMac'];echo "<br>";
echo "Device Token : ";echo $devicData[0]['deviceToken'];echo "<br>";
echo "User : ";echo $user[0][0]['userEmail'];echo "<br>";
die;
        
    }

public function deleteCameraAction()
    {
        $response = array();
        $request = $this->getRequest();
        
            $data = json_decode(file_get_contents("php://input"), true);
            $mac = $_GET['mac'];
            $DeviceModel = $this->getServiceInstance('Device');
	    $UserModel = $this->getServiceInstance('User');
            $devicData = $DeviceModel->Deviceinfo($mac);
                if($devicData) {
                    $delete = $DeviceModel->deleteDevice($devicData[0]['deviceId']);
                    if($delete){
                        $status = true;
                        $message = "Device Deleted";
                    }
		    else{
			$status = false;
                        $message = "Device not Deleted";
			}
                }
		
		else{
			$status = false;
                        $message = "Device not Registered";
		}

echo $status;echo "<br>";
echo $message;echo "<br>";
die;
        
    }



    public function callhttp($data) {
        $config = parse_ini_file('config/config.ini');
        $client = new Client();
        $client->setUri($config['node_ip'] . ":" . $config['node_port']);
        $client->setMethod('POST');
	if($data['command']=="pair-sensor" || $data['command']=="sensor-unpair"){
            $data = array("commandId" => $data['commandId'], "command" => $data['command'],"count" => $data['count'], "device_id" => $data['device_id'],"requestId" => $data['requestId'], "params" => $data['params']);
        }
	else
	{
        $data = array("commandId" => $data['commandId'], "command" => $data['command'], "device_id" => $data['device_id'], "requestId" => $data['requestId'],  "params" => $data['params']);
	}
	
        $asd = json_encode($data);
        $client->setRawBody($asd);
        $response = $client->send();
        return $response;
    }

    
    public function callhttp_fw($data) {
        $config = parse_ini_file('config/config.ini');
        $client = new Client();
        $client->setUri($config['node_ip'] . ":" . $config['node_port']);
        $client->setMethod('POST');
	if($data['command']=="pair-sensor" || $data['command']=="sensor-unpair"){
            $data = array("commandId" => $data['commandId'], "command" => $data['command'],"count" => $data['count'], "device_id" => $data['device_id'],"requestId" => $data['requestId'], "params" => $data['params']);
        }
	else
	{
        $data = array("commandId" => $data['commandId'], "command" => $data['command'], "device_id" => $data['device_id'], "requestId" => $data['requestId'],  "kernel-path" => $data['kernel_path'],  "fs-path" => $data['fs_path'], "kernel-md5" => $data['kernel_md5'], "fs-md5" => $data['fs_md5']);
	}
	
        $asd = json_encode($data);
        $client->setRawBody($asd);
        $response = $client->send();
        return $response;
    }

    
    public function callhttp_cube($data) {
        $config = parse_ini_file('config/config.ini');
        $client = new Client();
        $client->setUri($config['node_ip'] . ":" . $config['node_port']);
        $client->setMethod('POST');
        $data = array("commandId" => $data['commandId'], "command" => $data['command'], "device_id" => $data['cube_id'],"requestId" => $data['requestId'], "params" => $data['params']);
        $asd = json_encode($data);
        $client->setRawBody($asd);
        $response = $client->send();
        return $response;
    }


    public function getResponseWithHeader() {
        $response = $this->getResponse();
        $response->getHeaders()
            //make can accessed by *
            ->addHeaderLine('Access-Control-Allow-Origin', '*')
            //set allow methods
            ->addHeaderLine('Access-Control-Allow-Methods', 'POST PUT DELETE GET');
        return $response;
    }
}
