/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//  <!--  validate signup form on keyup and submit--> 
$(document).ready(function() {

    $('#regForm').validate({
        rules: {
            user_name: {
                required: true,
                minlength: 2
            },
            user_pwd: {
                required: true,
                minlength: 8
            },
            user_confirmpwd: {
                required: true,
                minlength: 8,
                equalTo: "#password"
            },
            user_email: {
                required: true,
                email: true
            }
        },
        messages: {
            user_name: {
                required: "Please enter name",
                minlength: "Name must consist of at least 2 characters"
            },
            user_pwd: {
                required: "Please provide a password",
                minlength: "Password must be at least 8 characters long"
            },
            user_confirmpwd: {
                required: "Please re-enter password",
                minlength: "Password must be at least 8 characters long",
                equalTo: "Password mismatch"
            },
            user_email: "Please enter a valid email address"
        }
    });


    //Login  Form Validation
    $('#loginForm').validate({
        rules: {
            user_pwd: {
                required: true,
                minlength: 8
            },
            user_email: {
                required: true,
                email: true
            }
        },
        messages: {
            user_pwd: {
                required: "Please enter your password"
            },
            user_email: {
                required: "Please enter your email "

            }
        }
    });

    $('#forgot_password').validate({
        rules: {
            currentPassword: {
                required: true
            },
            newPassword: {
                required: true,
                minlength: 8
            },
            confirmPassword: {
                required: true,
                minlength: 8,
                equalTo: "#password"
            }
        },
        messages: {
            currentPassword: {
                required: "Please enter password"
            },
            newPassword: {
                required: "Please provide a password",
                minlength: "Password must be at least 8 characters long"
            },
            confirmPassword: {
                required: "Please re-enter password",
                minlength: "Password must be at least 8 characters long",
                equalTo: "Password mismatch"
            }
        }
    });
    //Reset password Form Validation  
    $('#resetPwdForm').validate({
        rules: {
            newPassword: {
                required: true,
                minlength: 8
            },
            confirmPassword: {
                required: true,
                minlength: 8,
                equalTo: "#newpassword"
            }
        },
        messages: {
            newPassword: {
                required: "Please provide a password",
                minlength: "Password must be at least 8 characters long"
            },
            confirmPassword: {
                required: "Please re-enter password",
                minlength: "Password must be at least 8 characters long",
                equalTo: "Password mismatch"
            }
        }
    });

    $('#email').on('blur', function() {
  
        if ($("#email").hasClass('valid'))
        {

            $.ajax({
                url: siteUrl + "/user/checkuseremail",
                type: "POST",
                dataType: "json",
                data: {
                    'user_email': $('#email').val()
                },
                success: function(result) {
                    if (result.status == "true")
                    {
                        $('#span1 span').html(result.message);
                        $("#span1").show();
                    }
                    else
                    {
                        $('#span2 span').html(result.message);
                        $("#span2").show();
                    }
                },
                error: function(result) {
                    console.log('error');
                }
            });
        }
    });

    $('#email').on('focusin', function() {
        $("#span1").hide();
        $("#span2").hide();
    });

    $('#password').on('keyup', function() {
        var pwd = document.getElementById('password').value;
        if (pwd.length >= 0 && pwd.length < 8) {
            $("#spnPwd").addClass("hidden");
            $("#spnIcon").addClass("hidden");
            // $("#infopwd").addClass("hidden");
        }
    });

    $('#password').on('keyup', function() {
        var strengths = ["Weak", "Medium", "Strong"];
        var colors = ["#b94a48", "#f66606", "#9db36a"];
        var score = 0;
        var regLower = /[a-z]/, regUpper = /[A-Z]/, regNumber = /\d/, regPunctuation = /[.,!@#$%^&*?_~\-£()]/;


        var pwd = document.getElementById('password').value;
//        if (pwd.length <= 7) {
//            score = 0;
//        } else {
        // length is >= 8 in here
        if (pwd.length >= 8) {
            $("#spnPwd").removeClass("hidden");
            $("#spnIcon").removeClass("hidden");
            // $("#infopwd").removeClass("hidden");
            if (regLower.test(pwd) && regNumber.test(pwd) && (regPunctuation.test(pwd) || regUpper.test(pwd)))
            {
                score = 2;
            }
            else if ((regLower.test(pwd) || regUpper.test(pwd)) && regNumber.test(pwd))
            {
                score = 1;
            }
            else
                score = 0;

        }
        var spanPwd = document.getElementById('spnPwd');
        spanPwd.innerHTML = strengths[score];
        spanPwd.style.fontFamily = "Open Sans ,Helvetica Neue, Helvetica, Arial, sans-serif";
        spanPwd.style.color = colors[score];

        var spanIcon = document.getElementById('spnIcon');
        //spanIcon.innerHTML = strengths[score];
        spanIcon.style.color = colors[score];
    });
    /*$("#forgot_password").submit(function(e)
     {
     var postData = $(this).serializeArray();        
     $.ajax(
     {
     url: siteUrl + "/user/resetpassword",
     type: "POST",
     data: postData,
     success: function(data, textStatus, jqXHR)
     {
     alert(JSON.stringify(data));
     },
     error: function(jqXHR, textStatus, errorThrown)
     {
     console.log('error on submit');
     }
     });
     e.preventDefault();	//STOP default action        
     });*/
});

