<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'Device\Controller\Device' => 'Device\Controller\DeviceController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'device-register' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/register',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'register'
                    )
                ),
            ),
	   

	    'check-registration' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/check-registration',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'CheckRegister'
                    )
                ),
            ),

            'check-name' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/check-name',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'checkName'
                    )
                ),
            ),
		
           'device-delete' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/delete',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'delete'
                    )
                ),
            ),

	    'get-status' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/get-status',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'getDeviceStatus'
                    )
                ),
            ),	


            'cube-register' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/cube-register',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'registerCube'
                    )
                ),
            ),

            'cube-update' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/cube-update-config',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'updateCube'
                    )
                ),
            ),

            'device-list' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/list',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'list'
                    )
                ),
            ),


	   'default-device' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/default',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'defaultCamera'
                    )
                ),
            ),

            'device-identity' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/get-device-identity',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'deviceIdentity'
                    )
                ),
            ),

            'scan-sensor' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/scan-sensor',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'scanSensor'
                    )
                ),
            ),

            'check-status' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/check-command-status',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'checkStatus'
                    )
                ),
            ),

            'sensor-result' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/sensor-result',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'scanSensorResult'
                    )
                ),
            ),

            'pair-sensor' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/pair-sensor',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'pairSensor'
                    )
                ),
            ),

	    'unpair-sensor' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/unpair-sensor',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'unpairSensor'
                    )
                ),
            ),

	   'pairedsensor-list' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/list-paired-sensor',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'listPairedSensor'
                    )
                ),
            ),

            'register-sensor' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/register-sensor',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'registerSensor'
                    )
                ),
            ),

            'update-ip' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/update-ip',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'updateIp'
                    )
                ),
            ),

            'edit-device' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/edit-device',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'editDevice'
                    )
                ),
            ),

            'edit-cube' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/edit-cube',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'editCube'
                    )
                ),
            ),


	   'edit-sensor' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/edit-sensor',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'editSensor'
                    )
                ),
            ),

            'upload' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/upload',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'upload'
                    )
                ),
            ),

            'update-config' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/update-config',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'updateConfig'
                    )
                ),
            ),

            'view-config' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/view-config',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'viewConfig'
                    )
                ),
            ),

           'update-lower-threshold' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/update-lower-threshold',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'setLowerThreshold'
                    )
                ),
            ),

            'view-lower-threshold' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/view-lower-threshold',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'viewLowerThreshold'
                    )
                ),
            ),



	  'update-upper-threshold' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/update-upper-threshold',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'setUpperThreshold'
                    )
                ),
            ),

            'view-upper-threshold' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/view-upper-threshold',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'viewUpperThreshold'
                    )
                ),
            ),

	   'current-temp' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/current-temp',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'currentTemp'
                    )
                ),
            ),

           'scan-accesspoint' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/scan-accesspoint',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'scanAccesspoint'
                    )
                ),
            ),
           'factory-reset' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/restore-config',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'restoreConfig'
                    )
                ),
            ),

	   'On-Demand-Recording' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/start-recording',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'OnDemandRecording'
                    )
                ),
            ),


	   'Stop-Recording' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/stop-recording',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'StopRecording'
                    )
                ),
            ),


	   'Delete-video-cube' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/delete-video',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'deleteVideo'
                    )
                ),
            ),

	    'start-call' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/start-call',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'startCall'
                    )
                ),
            ),

            'talkback-polling' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/talkback-polling',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'talkbackPolling'
                    )
                ),
            ),

	    'end-call' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/end-call',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'endCall'
                    )
                ),
            ),

	    'cube-status' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/check-cube-status',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'checkCubeStatus'
                    )
                ),
            ),

           'cube-Ap-Configure' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/check-cube-ap-configure',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'cubeApConfigure'
                    )
                ),
            ),


           'cube-Factory-Reset' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/factory-reset-cube',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'cubeFactoryReset'
                    )
                ),
            ),


           'cube-check-register' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/check-registration-cube',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'CheckCubeRegister'
                    )
                ),
            ),

	    
           'set-motion-block' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/set-motion-block',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'setMotionBlock'
                    )
                ),
            ),

	   'get-motion-block' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/get-motion-block',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'getMotionBlock'
                    )
                ),
            ),


	    'fw-version' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/device/get-fw-version',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'getFWVersion',
                    ),
                ),
            ),


	    'update-fw' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/device/update-fw',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'updateFW',
                    ),
                ),
            ),

            'update-cube-fw' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/device/update-cube-fw',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'updateCubeFW',
                    ),
                ),
            ),
	
	
	    'ignore-doorbell' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/device/ignore-doorbell',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'IgnoreDoorbell',
                    ),
                ),
            ),

	   'stop-alarm' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/device/stop-alarm',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'StopAlarm',
                    ),
                ),
            ),


	 'check-alarm' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/device/check-alarm',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'CheckAlarm',
                    ),
                ),
            ),


	    'get-userinfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/device/get-camerainfo',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'identifyCamera',
                    ),
                ),
            ),

	'delete-cam' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/device/delete-camera',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'deleteCamera',
                    ),
                ),
            ),

         
            'delete-cube' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/device/delete-cube',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'deleteCube',
                    ),
                ),
            ),

	 'start-cronjob' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/device/start-devicepush',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'DevicePush',
                    ),
                ),
            ),

	  'talkback-cronjob' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/device/talkback-cron',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'TalkBackCron',
                    ),
                ),
            ),  


           'reboot-device' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/device/reboot',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'rebootDevice',
                    ),
                ),
            ), 

           'reboot-device-secondary' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/device/reboot-secondary',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'rebootDeviceSecondary',
                    ),
                ),
            ),
     
           'auto-upgrade' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/device/auto-upgrade',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'autoUpgrade',
                    ),
                ),
            ),
       
 

        ),
    ),
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    )
);
