<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Device;

use Device\Model\Device;
use Device\Model\Cube;
use Device\Model\Command;
use Device\Model\DeviceConfig;
use Device\Model\Sensor;
use Device\Model\DevicePush;
use Device\Model\TalkBack;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
use Zend\Authentication\AuthenticationService;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Device' => function($sm) {
                    $device = new Device();
                    $device->setServiceLocator($sm);
                    return $device;
                },
                'Command' => function($sm) {
                    $command = new Command();
                    $command->setServiceLocator($sm);
                    return $command;
                },

                'Cube' => function($sm) {
                    $cube = new Cube();
                    $cube->setServiceLocator($sm);
                    return $cube;
                },

                'DeviceConfig' => function($sm) {
                    $deviceconfig = new DeviceConfig();
                    $deviceconfig->setServiceLocator($sm);
                    return $deviceconfig;
                },

                'Sensor' => function($sm) {
                    $sensor = new Sensor();
                    $sensor->setServiceLocator($sm);
                    return $sensor;
                },

		        'DevicePush' => function($sm) {
                    $devicepush = new DevicePush();
                    $devicepush->setServiceLocator($sm);
                    return $devicepush;
                },

		        'TalkBack' => function($sm) {
                    $talkback = new TalkBack();
                    $talkback->setServiceLocator($sm);
                    return $talkback;
                },

               

                'TenantMsp' => function($sm) {
                    $tm = new TenantMsp();
                    $tm->setServiceLocator($sm);
                    return $tm;
                },
            ),
        );
    }

}

