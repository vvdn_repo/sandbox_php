<?php


namespace Device\Model;

use Application\Entity\SandboxDevicePush;
use Application\Model\AbstractOrmManager;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\ResultSetMapping;
use Zend\Config\Reader\Ini;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\NoResultException;

use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplateMapResolver;
use Zend\Math\Rand;

class DevicePush extends AbstractOrmManager implements ServiceLocatorAwareInterface
{

    public $serviceManager;


    public function getServiceLocator()
    {
        return $this->serviceManager;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceManager = $serviceLocator;
    }

    public function getRepository()
    {
        $repository = $this->getObjectManager($this->serviceManager)->getRepository('Application\Entity\SandboxDevicePush');
        return $repository;
    }

    public function getEntityInstance() {
        if (null === $this->configEntityInstance) {
            $this->configEntityInstance = new SandboxDevicePush();
        }
        return $this->configEntityInstance;
    }



    public function saveDevicePush(SandboxDevicePush &$devicepush) {

        $om = $this->getObjectManager($this->serviceManager);
        $om->persist($devicepush);
        $om->flush();
	$om->clear();
        return $devicepush->getDevicePushId();
    }



    public function InsertDevicePush($pushCommand,$device_id,$user_id,$time_limit,$data){

        $devicepushEntity = $this->getEntityInstance();
        $objectManager = $this->getObjectManager($this->getServiceLocator());
        $deviceProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $device_id);
        $devicepushEntity->setDeviceIdFK($deviceProxyInstance);
        $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxUser', $user_id);
        $devicepushEntity->setDevicePushUserIdFk($userProxyInstance);
        $modelCommon = $this->getServiceLocator()->get('Common');
        $time = $modelCommon->getCurrentTimeStamp();
	$time = $time+$time_limit;
	$devicepushEntity->setDevicePushCommand(htmlspecialchars($pushCommand));
        $devicepushEntity->setTimeStamp(htmlspecialchars($time));
        $devicepushEntity->setDevicePushStatus('pending');
        $devicepushEntity->setStopAlarmStatus(0);
	$devicepushEntity->setDevicePushData($data);
        $devicepushId = $this->saveDevicePush($devicepushEntity);
	return $devicepushId;
    }

      public function InsertDevicePushAlarm($pushCommand,$device_id,$user_id){

        $devicepushEntity = $this->getEntityInstance();
        $objectManager = $this->getObjectManager($this->getServiceLocator());
        $deviceProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $device_id);
        $devicepushEntity->setDeviceIdFK($deviceProxyInstance);
        $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxUser', $user_id);
        $devicepushEntity->setDevicePushUserIdFk($userProxyInstance);
        $modelCommon = $this->getServiceLocator()->get('Common');
        $time = $modelCommon->getCurrentTimeStamp();
        $time = $time+60;
        $devicepushEntity->setDevicePushCommand(htmlspecialchars($pushCommand));
        $devicepushEntity->setTimeStamp(htmlspecialchars($time));
        $devicepushEntity->setDevicePushStatus('pending');
        $devicepushEntity->setStopAlarmStatus(1);
        $devicepushId = $this->saveDevicePush($devicepushEntity);
        return $devicepushId;
    }

    public function InsertDevicePushAutoUpgrade($pushCommand,$device_id,$user_id,$time){

        $devicepushEntity = $this->getEntityInstance();
        $objectManager = $this->getObjectManager($this->getServiceLocator());
        $deviceProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $device_id);
        $devicepushEntity->setDeviceIdFK($deviceProxyInstance);
        $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxUser', $user_id);
        $devicepushEntity->setDevicePushUserIdFk($userProxyInstance);
        $devicepushEntity->setDevicePushCommand(htmlspecialchars($pushCommand));
        $devicepushEntity->setTimeStamp(htmlspecialchars($time));
        $devicepushEntity->setDevicePushStatus('pending');
        $devicepushEntity->setStopAlarmStatus(0);
        $devicepushId = $this->saveDevicePush($devicepushEntity);
        return $devicepushId;
    } 

    public function updateDevicePushId($devicePush_id) {
        $qb = $this->getRepository()->createQueryBuilder("devicepush");
        $query = $qb->update()
            ->set("devicepush.devicepushStatus", "?1")
            ->where("devicepush.devicepushId = ?2")
            ->setParameter(1, "completed")
            ->setParameter(2, $devicePush_id)
            ->getQuery();
        $query->execute();
        return true;

    }

    
    public function updateDevicePush($user_id,$command) {
        $qb = $this->getRepository()->createQueryBuilder("devicepush");
        $query = $qb->update()
            ->set("devicepush.devicepushStatus", "?1")
            ->where("devicepush.userIdFk = ?2")
            ->andWhere("devicepush.devicepushCommand = ?3")
            ->setParameter(1, "completed")
            ->setParameter(2, $user_id)
            ->setParameter(3, $command)
            ->getQuery();
        $query->execute();
        return true;

    }

    public function updateStopAlarm($user_id,$command) {
        $qb = $this->getRepository()->createQueryBuilder("devicepush");
        $query = $qb->update()
            ->set("devicepush.devicepushStatus", "?1")
            ->set("devicepush.stopAlarmStatus", "?3")
            ->where("devicepush.userIdFk = ?2")
            ->andWhere("devicepush.devicepushCommand = ?4")
            ->setParameter(1, "completed")
            ->setParameter(2, $user_id)
            ->setParameter(3, 0)
            ->setParameter(4, $command)
            ->getQuery();
        $query->execute();
        return true;

    }


    public function checkAlarm($user_id) {
        $modelCommon = $this->getServiceLocator()->get('Common');
        $time = $modelCommon->getCurrentTimeStamp();
        $qb = $this->getRepository()->createQueryBuilder("devicepush");
        $qb->where("devicepush.stopAlarmStatus = ?1")
            ->andWhere("devicepush.userIdFk = ?2")
            ->setParameter(1,1)
            ->setParameter(2, $user_id);
        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        return $result;
    }

   public function Device_push() {
        $modelCommon = $this->getServiceLocator()->get('Common');
        $time = $modelCommon->getCurrentTimeStamp();
        $qb = $this->getRepository()->createQueryBuilder("devicepush");
        $qb->where("devicepush.devicepushStatus = :status")
            ->andWhere("devicepush.devicepushTimeStamp < :time")	
            ->setParameter("status","pending")
            ->setParameter("time", $time);
	$qb->groupBy('devicepush.userIdFk');
        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
//print_r($result);die;
        return $result;

    }

  public function Device_push_delete() {
        $modelCommon = $this->getServiceLocator()->get('Common');
        $time = $modelCommon->getCurrentTimeStamp();
        $qb = $this->getRepository()->createQueryBuilder("devicepush");
        $query = $qb->update()
            ->set("devicepush.devicepushStatus", "?1")
            ->where("devicepush.devicepushTimeStamp +4 < :time")
	    //->andWhere("devicepush.devicepushCommand != :command")	
            ->setParameter(1, "completed")
            ->setParameter("time", $time)
	    //->setParameter("command", "upgrade-fw")	
            ->getQuery();
        $query->execute();
        return true;

    }

	




}
