<?php


namespace Device\Model;

use Application\Entity\SandboxSensor;
use Application\Model\AbstractOrmManager;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\ResultSetMapping;
use Zend\Config\Reader\Ini;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\NoResultException;

use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplateMapResolver;
use Zend\Math\Rand;

class Sensor extends AbstractOrmManager implements ServiceLocatorAwareInterface
{

    public $serviceManager;


    public function getServiceLocator()
    {
        return $this->serviceManager;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceManager = $serviceLocator;
    }

    public function getRepository()
    {
        $repository = $this->getObjectManager($this->serviceManager)->getRepository('Application\Entity\SandboxSensor');
        return $repository;
    }

    public function getEntityInstance() {
        if (null === $this->configEntityInstance) {
            $this->configEntityInstance = new SandboxSensor();
        }
        return $this->configEntityInstance;
    }



    public function saveSensor(SandboxSensor &$sensor) {
        $om = $this->getObjectManager($this->serviceManager);
        $om->persist($sensor);
        $om->flush();
        return true;
    }



    public function updateUnpair($data){
 
    for($i=1;$i<=$data['count'];$i++)
	{
          $qb = $this->getRepository()->createQueryBuilder("sensor");
	  $rand_num= Rand::getString(5,'abcdefghijklmnopqrstuvwxyz0123456789', true);
        $query = $qb->update()
            ->set("sensor.sensorMac", "?1")
            ->set("sensor.sensorStatus", "?2")
            ->where("sensor.sensorMac = ?3")
            ->setParameter(1, $rand_num)
            ->setParameter(2, "2")
            ->setParameter(3, $data['params']['sensor-mac'.$i])
            ->getQuery();
        $query->execute();
	}
            return true;

    }


    public function restoreSensor($data){
	$count = sizeof($data);

        for($i=0;$i<$count;$i++)
        {
            $qb = $this->getRepository()->createQueryBuilder("sensor");
            $rand_num= Rand::getString(5,'abcdefghijklmnopqrstuvwxyz0123456789', true);
            $query = $qb->update()
                ->set("sensor.sensorMac", "?1")
                ->set("sensor.sensorStatus", "?2")
                ->where("sensor.sensorId = ?3")
                ->setParameter(1, $rand_num)
                ->setParameter(2, "2")
                ->setParameter(3, $data[$i]['sensorId'])
                ->getQuery();
            $query->execute();
        }
        return true;

    }

   

    public function validateMac($mac){
   
	$qb = $this->getRepository()->createQueryBuilder("sensor");
        $qb->where("sensor.sensorStatus != 2");
        $qb->select($qb->expr()->count('sensor.sensorId') . ' AS sensorCount')
            ->andWhere("sensor.sensorMac = :sensorMac")
            ->setParameter("sensorMac",$mac);
        $result = $qb->getQuery()->getSingleScalarResult();
	if($result){
	return $mac;
	}
            return "true";

    }


     public function getPairedList($user_id) {
        $qb = $this->getRepository()->createQueryBuilder("sensor");
        $qb->where("sensor.sensorUserId = :userId")
            ->andWhere("sensor.sensorStatus != 2")
            ->setParameter("userId",$user_id);
        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        return $result;
    }


    public function editSensor($data) {
        $qb = $this->getRepository()->createQueryBuilder("sensor");
        $query = $qb->update()
            ->set("sensor.sensorName", "?1")
            ->where("sensor.sensorId = ?2")
            ->setParameter(1, $data['sensor_name'])
            ->setParameter(2, $data['sensor_id'])
            ->getQuery();
        $query->execute();
        return true;

    }



}

