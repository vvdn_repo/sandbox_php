<?php


namespace Device\Model;

use Application\Entity\SandboxCommand;
use Application\Model\AbstractOrmManager;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\ResultSetMapping;
use Zend\Config\Reader\Ini;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\NoResultException;

use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplateMapResolver;

class Command extends AbstractOrmManager implements ServiceLocatorAwareInterface
{

    protected $serviceManager;


    public function getServiceLocator()
    {
        return $this->serviceManager;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceManager = $serviceLocator;
    }

    public function getRepository()
    {
        $repository = $this->getObjectManager($this->serviceManager)->getRepository('Application\Entity\SandboxCommand');
        return $repository;
    }

    public function getEntityInstance() {
        if (null === $this->configEntityInstance) {
            $this->configEntityInstance = new SandboxCommand();
        }
        return $this->configEntityInstance;
    }



    public function saveCommand(SandboxCommand &$command) {
        $om = $this->getObjectManager($this->serviceManager);
        $om->persist($command);
        $om->flush();
        return $command->getId();
    }



    public function checkCommandTS($deviceId,$currentTS) {
        $qb = $this->getRepository()->createQueryBuilder("command")
            ->Where("command.deviceIdFK = :device_id")
            ->andWhere($currentTS." - command.timeStamp < 20")
            ->setParameter("device_id", $deviceId);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }


    public function checkStatus($data) {
        $qb = $this->getRepository()->createQueryBuilder("command");
        $qb->where("command.Id = :Id")
            ->andWhere("command.status = :status")
            ->setParameter("Id",$data['requestId'])
            ->setParameter("status","Completed");
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }
    public function updateResult($data){
        if (isset($data['data']) && isset($data['requestId'])) {
            $qb = $this->getRepository()->createQueryBuilder("command");
            $query = $qb->update()
                ->set("command.response", "?1")
                ->set("command.status", "?2")
                ->where("command.Id = ?3")
                ->setParameter(1, json_encode($data['data']))
//->setParameter(1, json_encode($data))
                ->setParameter(2, "Completed")
                ->setParameter(3, $data['requestId'])
                ->getQuery();
            $query->execute();
            return true;
        }
        else{
            return false;
        }

    }



}

