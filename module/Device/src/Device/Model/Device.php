<?php

/**
 * Device
 * @author VVDN Technologies < >
 * @package Device
 * 
 */

namespace Device\model;

use Application\Entity\SandboxDevice;
use Application\Entity\SandboxSensor;
use Application\Entity\SandboxSensorCommand;
use Application\Model\AbstractOrmManager;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\Container as SessionContainer;

use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplateMapResolver;

class Device extends AbstractOrmManager implements ServiceLocatorAwareInterface {

    protected $serviceManager;
    protected $orgEntityInstance;

    public function __construct() {
        
    }
    public function getServiceLocator() {
        return $this->serviceManager;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceManager = $serviceLocator;
    }

    public function getRepository() {
        $repository = $this->getObjectManager($this->serviceManager)->getRepository('Application\Entity\SandboxDevice');
        return $repository;
    }
    public function getRepository1() {
        $repository = $this->getObjectManager($this->serviceManager)->getRepository('Application\Entity\SandboxSensor');
        return $repository;
    }


    public function getEntityInstance() {
        if (null === $this->deviceEntityInstance) {
            $this->deviceEntityInstance = new SandboxDevice();
        }
        return $this->deviceEntityInstance;
    }

    public function getEntityInstance1() {
        if (null === $this->sensorEntityInstance) {
            $this->sensorEntityInstance = new SandboxSensor();
        }
        return $this->sensorEntityInstance;
    }



    public function validateDevicemac($device_mac) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $qb->where("device.deviceStatus != 2");
        $qb->select($qb->expr()->count('device.deviceId') . ' AS deviceCount')
            ->andWhere("device.deviceMac = :deviceMac")
            ->setParameter("deviceMac", $device_mac);
        $result = $qb->getQuery()->getSingleScalarResult();
        return $result;
    }


      public function validateDeviceName($device_name,$user_id) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $qb->where("device.deviceStatus != 2");
        $qb->select($qb->expr()->count('device.deviceId') . ' AS deviceCount')
            ->andWhere("device.deviceName = :deviceName")
            ->andWhere("device.deviceCreatedbyFk = :user_id")
            ->setParameter("deviceName", $device_name)
            ->setParameter("user_id", $user_id);
        $result = $qb->getQuery()->getSingleScalarResult();
        return $result;
    }

    public function Device_info($field,$value) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $qb->where("device.$field = :value")
            ->setParameter("value",$value);
        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        return $result;
    }



    public function saveDevice(SandboxDevice &$device) {
        $om = $this->getObjectManager($this->serviceManager);
        $om->persist($device);
        $om->flush();
        return $device->getDeviceId();
    }


    public function deleteDevice($deviceId){
        $qb = $this->getRepository()->createQueryBuilder("device");
        $query = $qb->update()
            ->set("device.deviceMac", "?1")
            ->set("device.deviceIP", "?4")
            ->set("device.deviceStatus", "?2")
            ->where("device.deviceId = ?3")
            ->setParameter(1, $deviceId)
            ->setParameter(2, "2")
            ->setParameter(3, $deviceId)
            ->setParameter(4, $deviceId)
            ->getQuery();
        $query->execute();
        return true;

    }

    public function getDeviceList($user_id,$deviceList=array()) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $selectColumns =  $selectColumns = array('device.deviceId','device.deviceName','device.deviceType','device.deviceMac','device.deviceSlnumber','device.deviceStatus','device.deviceToken','device.deviceIP','device.deviceFW','device.deviceDefault');
        $qb->select($selectColumns);
        $qb->where("device.deviceStatus != 2")
            ->andWhere("device.deviceCreatedbyFk = :user_id")
            ->setParameter("user_id",$user_id)
	    ->orderBy('device.deviceId', 'DESC');
        if(sizeof($deviceList) > 0){
            $qb->andWhere("device.deviceId IN (:deviceList)")
                ->setParameter("deviceList",$deviceList)
		->orderBy('device.deviceId', 'DESC');
        }
        $result = $qb->getQuery()->getArrayResult();
        /*foreach($result as $key=>$row){
            $result[$key]['deviceRTSP']="rtsp://".$result[$key]['deviceIP']."/PSIA/Streaming/channels/2?videoCodecType=H.264";
        }*/
        return $result;
    }

    public function defaultDevice($user_id,$device_id) {

        $qb = $this->getRepository()->createQueryBuilder("device");
        $query = $qb->update()
            ->set("device.deviceDefault", "?1")
            ->where("device.deviceCreatedbyFk = ?2")
            ->setParameter(1, 0)
            ->setParameter(2, $user_id)
            ->getQuery();
        $query->execute();

        $qb = $this->getRepository()->createQueryBuilder("device");
        $query = $qb->update()
            ->set("device.deviceDefault", "?1")
            ->where("device.deviceId = ?2")
            ->setParameter(1, 1)
            ->setParameter(2, $device_id)
            ->getQuery();
        $query->execute();
        return true;

    }
     
  public function getDeviceIds($user_id) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $selectColumns =  $selectColumns = array('device.deviceId');
        $qb->select($selectColumns);
        $qb->where("device.deviceStatus != 2")
            ->andWhere("device.deviceCreatedbyFk = :user_id")
            ->setParameter("user_id",$user_id);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function validateDevice($device_mac,$user_id) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $qb->addSelect('device.deviceId');
        $qb->where("device.deviceMac = :deviceMac")
            ->andwhere("device.deviceCreatedbyFk = :userId")
            ->setParameter("deviceMac",$device_mac)
            ->setParameter("userId",$user_id);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

     public function validateDevicemacWithUser($device_mac,$user_id) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $selectColumns =  $selectColumns = array('device.deviceId');
        $qb->where("device.deviceStatus != 2")
            ->andWhere("device.deviceMac = :deviceMac")
            ->andWhere("device.deviceCreatedbyFk = :userId")
            ->setParameter("deviceMac", $device_mac)
            ->setParameter("userId", $user_id);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }


    public function infoFromDeviceToken($deviceToken) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $qb->where("device.deviceToken = :deviceToken")
            ->setParameter("deviceToken",$deviceToken);
        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        return $result;
    }

    public function infoFromDeviceIP($deviceIP,$user_id) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $qb->where("device.deviceIP = :deviceIP")
	   ->andwhere("device.deviceCreatedbyFk = :userId")
           ->setParameter("userId",$user_id)
           ->setParameter("deviceIP",$deviceIP);
        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        return $result;
    }


    public function Deviceinfo($mac) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $qb->where("device.deviceMac = :deviceMac")
            ->setParameter("deviceMac",$mac);
        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        return $result;
    }

    public function validateSensormac($sensor_mac) {
        $qb = $this->getRepository1()->createQueryBuilder("sensor");
      //  $qb->where("sensor.sensorStatus != 2");
        $qb->select($qb->expr()->count('sensor.sensorId') . ' AS sensorCount')
            ->andWhere("sensor.sensorMac = :sensorMac")
            ->setParameter("sensorMac", $sensor_mac);
        $result = $qb->getQuery()->getSingleScalarResult();
        return $result;
    }


    public function saveSensor(SandboxSensor &$sensor) {
        $om = $this->getObjectManager($this->serviceManager);
        $om->persist($sensor);
        $om->flush();
        return $sensor->getSensorId();
    }

     public function getSensorinfo($mac) {
        $qb = $this->getRepository1()->createQueryBuilder("sensor");
        $qb->where("sensor.sensorMac = :sensorMac")
            ->setParameter("sensorMac",$mac);
        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        return $result;
    }

   public function validateSensor($mac,$user_id) {
        $qb = $this->getRepository1()->createQueryBuilder("sensor");
        $qb->where("sensor.sensorMac = :sensorMac")
	   ->andWhere("sensor.sensorUserId = :userId")
	    ->setParameter("sensorMac",$mac)
            ->setParameter("userId",$user_id);
        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        return $result;
    }

    public function updateAlarmTS($mac,$alarmTS) {
        $qb = $this->getRepository1()->createQueryBuilder("sensor");
        $query = $qb->update()
            ->set("sensor.sensorAlarmTS", "?1")
            ->where("sensor.sensorMac = ?2")
            ->setParameter(1, $alarmTS)
            ->setParameter(2, $mac)
            ->getQuery();
        $query->execute();
        return true;

    }
  
    public function updateBatteryStatus($mac,$reason) {
        if($reason =="battery-low"){
            $status =0;
        }
        elseif($reason =="Battery-OK"){
            $status =1;
        }
        $qb = $this->getRepository1()->createQueryBuilder("sensor");
        $query = $qb->update()
            ->set("sensor.sensorBatteryStatus", "?1")
            ->where("sensor.sensorMac = ?2")
            ->setParameter(1, $status)
            ->setParameter(2, $mac)
            ->getQuery();
        $query->execute();
        return true;

    }

    public function getActiveDeviceCount($user_id) {

        $qb = $this->getRepository()->createQueryBuilder("device");
        $qb->select($qb->expr()->count('device.deviceId') . ' AS deviceCount')
            ->andWhere("device.deviceCreatedbyFk = :userId")
	    ->andWhere("device.deviceStatus != 2")
            ->setParameter("userId", $user_id);
        $result = $qb->getQuery()->getSingleScalarResult();
        return $result;

    }

    public function getTotalCameracount() {

        $qb = $this->getRepository()->createQueryBuilder("device");
        $qb->select($qb->expr()->count('device.deviceId') . ' AS deviceCount')
            ->andWhere("device.deviceType = :type")
            ->andWhere("device.deviceStatus != 2")
            ->setParameter("type", "camera");
        $result = $qb->getQuery()->getSingleScalarResult();
        return $result;

    }

    public function getTotalDoorbellcount() {

        $qb = $this->getRepository()->createQueryBuilder("device");
        $qb->select($qb->expr()->count('device.deviceId') . ' AS deviceCount')
            ->andWhere("device.deviceType = :type")
            ->andWhere("device.deviceStatus != 2")
            ->setParameter("type", "doorbell");
        $result = $qb->getQuery()->getSingleScalarResult();
        return $result;

    }

    public function updateIP($data) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $query = $qb->update()
            ->set("device.deviceIP", "?1")
            ->where("device.deviceMac = ?2")
            ->setParameter(1, $data['device_ip'])
            ->setParameter(2, $data['device_mac'])
            ->getQuery();
        $query->execute();
        return true;

    }

    public function editDevice($data) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $query = $qb->update()
            ->set("device.deviceName", "?1")
            ->where("device.deviceId = ?2")
            ->setParameter(1, $data['device_name'])
            ->setParameter(2, $data['device_id'])
            ->getQuery();
        $query->execute();
        return true;

    }

    public function updateSensor($data,$devicedata) {
        $qb = $this->getRepository1()->createQueryBuilder("sensor");
        $query = $qb->update()
            ->set("sensor.sensorDeviceId", "?1")
            ->set("sensor.sensorUserId", "?2")
            ->where("sensor.sensorMac = ?3")
            ->setParameter(1, $devicedata[0]['deviceId'])
            ->setParameter(2, $devicedata[0]['device_createdby_fk'])
            ->setParameter(3, $data['sensor_mac'])
            ->getQuery();
        $query->execute();
        return true;

    }

    public function setMode($device_id,$device_mode) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $query = $qb->update()
            ->set("device.deviceMode", "?1")
            ->where("device.deviceId = ?2")
            ->setParameter(1, $device_mode)
            ->setParameter(2, $device_id)
            ->getQuery();
        $query->execute();
        return true;

    }

    public function getMode($device_id) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $qb->addSelect('device.deviceMode');
        $qb->where("device.deviceId = :deviceId")
            ->setParameter("deviceId", $device_id);
        $val = $qb->getQuery()->getArrayResult();
        $result = $val[0][0]['deviceMode'];
        return $result;

    }

    public function updateFwVersion($data) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $query = $qb->update()
            ->set("device.deviceFW", "?1")
            ->where("device.deviceId = ?2")
            ->setParameter(1, $data['value'])
            ->setParameter(2, $data['device_id'])
            ->getQuery();
        $query->execute();
        return true;

    }

   public function updateFwVersionSecondary($data) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $query = $qb->update()
            ->set("device.deviceFWSecondary", "?1")
            ->where("device.deviceId = ?2")
            ->setParameter(1, $data['value'])
            ->setParameter(2, $data['device_id'])
            ->getQuery();
        $query->execute();
        return true;

    }

    public function updateFWTS($device_id,$time) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $query = $qb->update()
            ->set("device.deviceFWTime", "?1")
            ->where("device.deviceId = ?2")
            ->setParameter(1, $time)
            ->setParameter(2,$device_id)
            ->getQuery();
        $query->execute();
        return true;

    }

    
     public function updateDeviceStatus($device_id,$status) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $query = $qb->update()
            ->set("device.deviceStatus", "?1")
            ->where("device.deviceId = ?2")
            ->setParameter(1, $status)
            ->setParameter(2,$device_id)
            ->getQuery();
        $query->execute();
        return true;

    }

    public function updateAutoUpgrade($device_id,$value) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $query = $qb->update()
            ->set("device.deviceAutoUpdate", "?1")
            ->where("device.deviceId = ?2")
            ->setParameter(1, $value)
            ->setParameter(2,$device_id)
            ->getQuery();
        $query->execute();
        return true;

    }


    public function setAutoUpgradeList($type) {
        //$DeviceModel = $this->getServiceInstance('Device');
        $upgradeList = $this->getAutoUpgradeList($type);
        $devicePushModel = $this->getServiceLocator()->get('DevicePush');
        $pushCommand = "upgrade-fw";
        $start=$upgradeList[0]['device_createdby_fk'];
        $modelCommon = $this->getServiceLocator()->get('Common');
        $time = $modelCommon->getCurrentTimeStamp();
        $start_time = $time;
        $size = sizeof($upgradeList);
        for($i=0;$i<$size;$i++)
        {
            if($upgradeList[$i]['device_createdby_fk'] == $start)
            {
                $time = $time+30;
                $DevicePushId = $devicePushModel->InsertDevicePushAutoUpgrade($pushCommand,$upgradeList[$i]['deviceId'],$upgradeList[$i]['device_createdby_fk'],$time);

            }

            else
            {
                $time =$start_time ;
                $DevicePushId = $devicePushModel->InsertDevicePushAutoUpgrade($pushCommand,$upgradeList[$i]['deviceId'],$upgradeList[$i]['device_createdby_fk'],$time);
                $start = $upgradeList[$i]['device_createdby_fk'];

            }


        }
        return true;
    }

    public function getAutoUpgradeList($type) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $qb->where("device.deviceAutoUpdate = :value")
	        ->andWhere("device.deviceStatus != :status")
            ->andWhere("device.deviceType = :type")
            ->setParameter("value",1)
	        ->setParameter("status",2)
            ->setParameter("type",$type);
        $qb->orderBy('device.deviceCreatedbyFk', 'ASC');
        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();

        return $result;

    }


}
