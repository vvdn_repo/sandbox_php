<?php


namespace Device\Model;

use Application\Entity\SandboxTalkBack;
use Application\Model\AbstractOrmManager;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\ResultSetMapping;
use Zend\Config\Reader\Ini;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\NoResultException;

use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplateMapResolver;
use Zend\Math\Rand;

class TalkBack extends AbstractOrmManager implements ServiceLocatorAwareInterface
{

    public $serviceManager;


    public function getServiceLocator()
    {
        return $this->serviceManager;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceManager = $serviceLocator;
    }

    public function getRepository()
    {
        $repository = $this->getObjectManager($this->serviceManager)->getRepository('Application\Entity\SandboxTalkBack');
        return $repository;
    }

    public function getEntityInstance() {
        if (null === $this->configEntityInstance) {
            $this->configEntityInstance = new SandboxTalkBack();
        }
        return $this->configEntityInstance;
    }



    public function saveTalkBack(SandboxTalkBack &$talkback) {

        $om = $this->getObjectManager($this->serviceManager);
        $om->persist($talkback);
        $om->flush();
	    $om->clear();
        return $talkback->getTalkBackId();
    }

    public function InsertPush($pushCommand,$device_id,$user_id,$time_limit,$data){

        $devicepushEntity = $this->getEntityInstance();
        $objectManager = $this->getObjectManager($this->getServiceLocator());
        $deviceProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $device_id);
        $devicepushEntity->setTalkbackDeviceIdFK($deviceProxyInstance);
        $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxUser', $user_id);
        $devicepushEntity->setTalkbackUserIdFk($userProxyInstance);
        $modelCommon = $this->getServiceLocator()->get('Common');
        $time = $modelCommon->getCurrentTimeStamp();
        $time = $time+$time_limit;
        $devicepushEntity->setTalkbackCommand(htmlspecialchars($pushCommand));
        $devicepushEntity->setTalkbackTimeStamp(htmlspecialchars($time));
        $devicepushEntity->setTalkbackStatus('pending');
        $devicepushId = $this->saveTalkBack($devicepushEntity);
        return $devicepushId;
    }

    public function checkTalkBack($device_id) {
        $qb = $this->getRepository()->createQueryBuilder("talkback");
        $qb->where("talkback.talkbackStatus = :status")
            ->andWhere("talkback.deviceIdFK = :device")
	    ->andWhere("talkback.talkbackCommand = ?1")	
            ->setParameter("status","Pending")
            ->setParameter("device", $device_id)
	    ->setParameter(1, "end-talkback");
        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        return $result;

    }

    public function updateTalkBack($device_id) {
        $modelCommon = $this->getServiceLocator()->get('Common');
        $time = $modelCommon->getCurrentTimeStamp();
        $qb = $this->getRepository()->createQueryBuilder("talkback");
        $query = $qb->update()
            ->set("talkback.talkbackTimeStamp", "?1")
            ->where("talkback.deviceIdFK = ?2")
            ->andWhere("talkback.talkbackStatus = ?3")
	    ->andWhere("talkback.talkbackCommand = ?4")	
            ->setParameter(1, $time)
            ->setParameter(2, $device_id)
            ->setParameter(3, "Pending")
	    ->setParameter(4, "end-talkback")	
            ->getQuery();
        $query->execute();
        return true;

    }

    public function updateEndTalkBack($device_id) {
        $qb = $this->getRepository()->createQueryBuilder("talkback");
        $query = $qb->update()
            ->set("talkback.talkbackStatus", "?1")
            ->where("talkback.deviceIdFK = ?2")
	    ->andWhere("talkback.talkbackCommand = ?3")	
            ->setParameter(1, "Completed")
            ->setParameter(2, $device_id)
	    ->setParameter(3, "end-talkback")	
            ->getQuery();
        $query->execute();
        return true;

    }



   public function getTalkBackList() {
        $modelCommon = $this->getServiceLocator()->get('Common');
        $time = $modelCommon->getCurrentTimeStamp();
        $qb = $this->getRepository()->createQueryBuilder("talkback");
        $qb->where("talkback.talkbackStatus = :status")
            ->andWhere("talkback.talkbackTimeStamp +20 < :time")
            ->setParameter("status","pending")
            ->setParameter("time", $time);
        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();

        return $result;

    }


    public function updateEndTalkBackCron($talkback_id) {
        $qb = $this->getRepository()->createQueryBuilder("talkback");
        $query = $qb->update()
            ->set("talkback.talkbackStatus", "?1")
            ->where("talkback.talkbackId = ?2")
            ->setParameter(1, "Completed")
            ->setParameter(2, $talkback_id)
            ->getQuery();
        $query->execute();
        return true;

    }

    public function updateOffline($device_id) {
        $qb = $this->getRepository()->createQueryBuilder("talkback");
        $query = $qb->update()
            ->set("talkback.talkbackStatus", "?1")
            ->where("talkback.deviceIdFK = ?2")
            ->setParameter(1, "Completed")
            ->setParameter(2, $device_id)
            ->getQuery();
        $query->execute();
        return true;

    }

  public function Talk_push_delete() {
        $modelCommon = $this->getServiceLocator()->get('Common');
        $time = $modelCommon->getCurrentTimeStamp();
        $qb = $this->getRepository()->createQueryBuilder("talkback");
        $query = $qb->update()
            ->set("talkback.talkbackStatus", "?1")
            ->where("talkback.talkbackTimeStamp +20 < :time")
            ->setParameter(1, "completed")
            ->setParameter("time", $time)
            ->getQuery();
        $query->execute();
        return true;

    }

	




}
