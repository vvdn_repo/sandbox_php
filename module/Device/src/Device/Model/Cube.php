<?php
/**
 * User: vvdnlt168
 * Date: 3/6/15
 * Time: 10:19 AM
 */

namespace Device\model;

use Application\Entity\SandboxCube;
use Application\Model\AbstractOrmManager;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\Container as SessionContainer;

use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplateMapResolver;

class Cube extends AbstractOrmManager implements ServiceLocatorAwareInterface
{

    protected $serviceManager;
    protected $orgEntityInstance;

    public function __construct()
    {

    }

    public function getServiceLocator()
    {
        return $this->serviceManager;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceManager = $serviceLocator;
    }

    public function getRepository()
    {
        $repository = $this->getObjectManager($this->serviceManager)->getRepository('Application\Entity\SandboxCube');
        return $repository;
    }

    public function getEntityInstance()
    {
        if (null === $this->deviceEntityInstance) {
            $this->deviceEntityInstance = new SandboxCube();
        }
        return $this->deviceEntityInstance;
    }


    public function validateCubemac($cube_mac) {
        $qb = $this->getRepository()->createQueryBuilder("cube");
        $qb->where("cube.cubeStatus != 2");
        $qb->select($qb->expr()->count('cube.cubeId') . ' AS cubeCount')
            ->andWhere("cube.cubeMac = :cubeMac")
            ->setParameter("cubeMac", $cube_mac);
        $result = $qb->getQuery()->getSingleScalarResult();
        return $result;
    }

    public function validateCube($field1,$value1,$field2,$value2) {
        $qb = $this->getRepository()->createQueryBuilder("cube");
        $qb->where("cube.$field1 = :value1")
            ->andWhere("cube.$field2 = :value2")
            ->setParameter("value1",$value1)
            ->setParameter("value2",$value2);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

     public function validateCubemacWithUser($cube_mac,$user_id) {
        $qb = $this->getRepository()->createQueryBuilder("cube");
        $selectColumns =  $selectColumns = array('cube.deviceId');
        $qb->where("cube.cubeStatus != 2")
            ->andWhere("cube.cubeMac = :cubeMac")
            ->andWhere("cube.cubeCreatedbyFk = :userId")
            ->setParameter("cubeMac", $cube_mac)
            ->setParameter("userId", $user_id);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function getCubeinfo($field,$value) {
        $qb = $this->getRepository()->createQueryBuilder("cube");
        $qb->where("cube.$field = :value")
            ->andWhere("cube.cubeStatus != 2")
            ->setParameter("value",$value);
        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        return $result;
    }



    public function saveCube(SandboxCube &$cube) {
        $om = $this->getObjectManager($this->serviceManager);
        $om->persist($cube);
        $om->flush();
        return $cube->getCubeId();
    }


    public function editCube($data) {
        $qb = $this->getRepository()->createQueryBuilder("cube");
        $query = $qb->update()
            ->set("cube.cubeName", "?1")
            ->where("cube.cubeId = ?2")
            ->setParameter(1, $data['cube_name'])
            ->setParameter(2, $data['cube_id'])
            ->getQuery();
        $query->execute();
        return true;

    }

    public function deleteCube($cubeId){
        $qb = $this->getRepository()->createQueryBuilder("cube");
        $query = $qb->update()
            ->set("cube.cubeMac", "?1")
            ->set("cube.cubePublicIP", "?2")
            ->set("cube.cubeLanIP", "?3")
            ->set("cube.cubeStatus", "?4")
            ->where("cube.cubeId = ?5")
            ->setParameter(1, $cubeId)
            ->setParameter(2, $cubeId)
            ->setParameter(3, $cubeId)
            ->setParameter(4, "2")
            ->setParameter(5, $cubeId)
            ->getQuery();
        $query->execute();
        return true;

    }

    public function getCubeList($user_id,$cubeList=array()) {
        $qb = $this->getRepository()->createQueryBuilder("cube");
        $selectColumns =  $selectColumns = array('cube.cubeId','cube.cubeName','cube.cubeMac','cube.cubeStatus','cube.cubeToken','cube.cubePublicIP','cube.cubeLanIP','cube.cubePath');
        $qb->select($selectColumns);
        $qb->where("cube.cubeStatus != 2")
            ->andWhere("cube.cubeCreatedbyFk = :user_id")
            ->setParameter("user_id",$user_id);
        if(sizeof($cubeList) > 0){
            $qb->andWhere("cube.cubeId IN (:cubeList)")
                ->setParameter("cubeList",$cubeList);
        }
        $result = $qb->getQuery()->getArrayResult();
        /*foreach($result as $key=>$row){
            $result[$key]['deviceRTSP']="rtsp://".$result[$key]['deviceIP']."/PSIA/Streaming/channels/2?videoCodecType=H.264";
        }*/
        return $result;
    }

    public function updateCubeResult($data,$user_id){
        if (isset($user_id)) {
            $qb = $this->getRepository()->createQueryBuilder("cube");
            $query = $qb->update()
                ->set("cube.cubePublicIP", "?1")
                ->set("cube.cubeLanIP", "?2")
                ->where("cube.cubeCreatedbyFk = ?3")
                ->andWhere("cube.cubeMac = ?4")
                ->setParameter(1, $data['params']['cube_publicip'])
                ->setParameter(2, $data['params']['cube_lanip'])
                ->setParameter(3, $user_id)
                ->setParameter(4, $data['params']['cube_mac'])
                ->getQuery();
            $query->execute();


            return true;
        }
        else{
            return false;
        }

    }

    public function updateFwVersion($data) {
        $qb = $this->getRepository()->createQueryBuilder("cube");
        $cube_id = explode("cube_", $data['cube_id']);
        $query = $qb->update()
            ->set("cube.cubeFW", "?1")
            ->where("cube.cubeId = ?2")
            ->setParameter(1, $data['value'])
            ->setParameter(2, $cube_id[1])
            ->getQuery();
        $query->execute();
        return true;
    }


    public function updateCubeStatus($cube_id,$status) {
        $qb = $this->getRepository()->createQueryBuilder("cube");
        $query = $qb->update()
            ->set("cube.cubeStatus", "?1")
            ->where("cube.cubeId = ?2")
            ->setParameter(1, $status)
            ->setParameter(2,$cube_id)
            ->getQuery();
        $query->execute();
        return true;

    }
}


