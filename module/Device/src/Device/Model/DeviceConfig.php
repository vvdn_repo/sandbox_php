<?php

namespace Device\Model;

use Application\Entity\SandboxConfig;
use Application\Model\AbstractOrmManager;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\ResultSetMapping;
use Zend\Config\Reader\Ini;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\NoResultException;

use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplateMapResolver;

class DeviceConfig extends AbstractOrmManager implements ServiceLocatorAwareInterface
{

    protected $serviceManager;


    public function getServiceLocator()
    {
        return $this->serviceManager;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceManager = $serviceLocator;
    }

    public function getRepository()
    {
        $repository = $this->getObjectManager($this->serviceManager)->getRepository('Application\Entity\SandboxConfig');
        return $repository;
    }

    public function getEntityInstance() {
        if (null === $this->configEntityInstance) {
            $this->configEntityInstance = new SandboxConfig();
        }
        return $this->configEntityInstance;
    }

    public function saveConfig(SandboxConfig &$config) {
        $om = $this->getObjectManager($this->serviceManager);
        $om->persist($config);
        $om->flush();
        return $config->getConfigId();
    }



    
    public function updateConfigResult($data){

        if (isset($data['device_id'])) {
            $qb = $this->getRepository()->createQueryBuilder("config");
            $query = $qb->update()
                ->set("config.configMotionDetection", "?1")
                ->set("config.configCubeRecording", "?2")
                ->set("config.configCloudRecording", "?3")
                ->set("config.configVideoQuality", "?4")
                ->set("config.configAudioEnable", "?5")
                ->set("config.configVoiceMail", "?6")
                ->set("config.configLowerThreshold", "?7")
                ->set("config.configUpperThreshold", "?8")
                ->set("config.configLowerThresholdAlert", "?9")
                ->set("config.configUpperThresholdAlert", "?10")
               // ->set("config.configAutoUpgrade", "?11")
                ->where("config.configDeviceId = ?12")
                ->setParameter(1, $data['data']['motion-detection'])
                ->setParameter(2, $data['data']['cube-recording'])
                ->setParameter(3, $data['data']['cloud-recording'])
                ->setParameter(4, $data['data']['hd-enable'])
                ->setParameter(5, $data['data']['audio-enable'])
                ->setParameter(6, $data['data']['voice-mail'])
                ->setParameter(7, $data['data']['lower-threshold'])
                ->setParameter(8, $data['data']['upper-threshold'])
                ->setParameter(9, $data['data']['lower-threshold-alert'])
                ->setParameter(10, $data['data']['upper-threshold-alert'])
               // ->setParameter(11, $data['data']['auto-upgrade'])
                ->setParameter(12, $data['device_id'])
                ->getQuery();
            $query->execute();


            return true;
        }
        else{
         return false;
       }

    }

    public function updateConfig($data){

        if (isset($data['device_id'])) {
            $qb = $this->getRepository()->createQueryBuilder("config");
            $query = $qb->update()
                ->set("config.configMotionDetection", "?1")
                ->set("config.configCubeRecording", "?2")
                ->set("config.configCloudRecording", "?3")
                ->set("config.configVideoQuality", "?4")
                ->set("config.configAudioEnable", "?5")
                ->set("config.configVoiceMail", "?6")
                ->set("config.configLowerThreshold", "?7")
                ->set("config.configUpperThreshold", "?8")
                ->set("config.configLowerThresholdAlert", "?9")
                ->set("config.configUpperThresholdAlert", "?10")
                ->set("config.configAutoUpgrade", "?11")
                ->where("config.configDeviceId = ?12")
                ->setParameter(1, $data['params']['motion-detection'])
                ->setParameter(2, $data['params']['cube-recording'])
                ->setParameter(3, $data['params']['cloud-recording'])
                ->setParameter(4, $data['params']['hd-enable'])
                ->setParameter(5, $data['params']['audio-enable'])
                ->setParameter(6, $data['params']['voice-mail'])
                ->setParameter(7, $data['params']['lower-threshold'])
                ->setParameter(8, $data['params']['upper-threshold'])
                ->setParameter(9, $data['params']['lower-threshold-alert'])
                ->setParameter(10, $data['params']['upper-threshold-alert'])
                ->setParameter(11, $data['params']['auto-upgrade'])
                ->setParameter(12, $data['device_id'])
                ->getQuery();
            $query->execute();


            return true;
        }
        else{
            return false;
        }

    }
    public function restoreConfig($deviceId){

        if ($deviceId) {
            $qb = $this->getRepository()->createQueryBuilder("config");
            $query = $qb->update()
                ->set("config.configMotionDetection", "?1")
                ->set("config.configCubeRecording", "?2")
                ->set("config.configCloudRecording", "?3")
                ->set("config.configVideoQuality", "?4")
                ->set("config.configAudioEnable", "?5")
                ->set("config.configVoiceMail", "?6")
                ->where("config.configDeviceId = ?7")
                ->setParameter(1, "0")
                ->setParameter(2, "0")
                ->setParameter(3, "0")
                ->setParameter(4, "1")
		        ->setParameter(5, "1")
                ->setParameter(6, "0")
                ->setParameter(7, $deviceId)
                ->getQuery();
            $query->execute();


            return true;
        }
        else{
            return false;
        }

    }


     public function updateLowerThreshold($data){
        if (isset($data['device_id'])) {
            $qb = $this->getRepository()->createQueryBuilder("config");
            $query = $qb->update()
                ->set("config.configLowerThreshold", "?1")
                ->where("config.configDeviceId = ?2")
                ->setParameter(1, $data['params']['lower-threshold'])
                ->setParameter(2, $data['device_id'])
                ->getQuery();
            $query->execute();

            return true;
        }
        else{
            return false;
        }

    }

    public function updateLowerThresholdResult($data){

        if (isset($data['requestId']) && isset($data['device_id'])) {
            $qb = $this->getRepository()->createQueryBuilder("config");
            $query = $qb->update()
                ->set("config.configLowerThreshold", "?1")
                ->where("config.configDeviceId = ?2")
                ->setParameter(1, $data['data']['lower-threshold'])
                ->setParameter(2, $data['device_id'])
                ->getQuery();
            $query->execute();


            return true;
        }
        else{
            return false;
        }

    }


    

    public function updateUpperThreshold($data){
        if (isset($data['device_id'])) {
            $qb = $this->getRepository()->createQueryBuilder("config");
            $query = $qb->update()
                ->set("config.configUpperThreshold", "?1")
                ->where("config.configDeviceId = ?2")
                ->setParameter(1, $data['params']['upper-threshold'])
                ->setParameter(2, $data['device_id'])
                ->getQuery();
            $query->execute();

            return true;
        }
        else{
            return false;
        }

    }

    public function updateUpperThresholdResult($data){

        if (isset($data['requestId']) && isset($data['device_id'])) {
            $qb = $this->getRepository()->createQueryBuilder("config");
            $query = $qb->update()
                ->set("config.configUpperThreshold", "?1")
                ->where("config.configDeviceId = ?2")
                ->setParameter(1, $data['data']['upper-threshold'])
                ->setParameter(2, $data['device_id'])
                ->getQuery();
            $query->execute();


            return true;
        }
        else{
            return false;
        }

    }


     public function updateMotionBlock($data){
        if (isset($data['device_id'])) {
            $qb = $this->getRepository()->createQueryBuilder("config");
            $query = $qb->update()
                ->set("config.configMotionBlock", "?1")
                ->where("config.configDeviceId = ?2")
                ->setParameter(1, $data['params']['block-size'])
                ->setParameter(2, $data['device_id'])
                ->getQuery();
            $query->execute();

            return true;
        }
        else{
            return false;
        }

    }

    public function updateMotionBlockResult($data){

        if (isset($data['requestId']) && isset($data['device_id'])) {
            $qb = $this->getRepository()->createQueryBuilder("config");
            $query = $qb->update()
                ->set("config.configMotionBlock", "?1")
                ->where("config.configDeviceId = ?2")
                ->setParameter(1, $data['data']['block-size'])
                ->setParameter(2, $data['device_id'])
                ->getQuery();
            $query->execute();


            return true;
        }
        else{
            return false;
        }

    }

    public function getDeviceConfig($device_id) {
        $qb = $this->getRepository()->createQueryBuilder("config");
        $qb->Where("config.configDeviceId = :device_id")
            ->setParameter("device_id",$device_id);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }
}

