<?php

/**
 * module.config.php handles the routes and paths for the admin module 
 * @package Application
 * @author VVDN Technologies < >
 */
return array(
    'controllers' => array(
        'invokables' => array(
            'Admin\Controller\Admin' => 'Admin\Controller\AdminController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'register-admin' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/admin/register',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Admin',
                        'action' => 'register',
                    ),
                ),
            ),
            'login-admin' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/admin/login',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Admin',
                        'action' => 'login',
                    ),
                ),
            ),

            'logout-admin' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/admin/logout',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Admin',
                        'action' => 'logout',
                    ),
                ),
            ),

	        'admin-home' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/admin/home',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Admin',
                        'action' => 'adminHome',
                    ),
                ),
            ),

            'dashboard' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/admin/dashboard',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Admin',
                        'action' => 'adminDashboard',
                    ),
                ),
            ),
	    
	     'listUser' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/admin/users',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Admin',
                        'action' => 'listUsers',
                    ),
                ),
            ),

            'getUser' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/admin/getusers',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Admin',
                        'action' => 'getUsers',
                    ),
                ),
            ),

            'listDevices' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/devices[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Admin',
                        'action' => 'listDevices',
                    ),
                ),
            ),

            'getDevice' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/getdevices[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Admin',
                        'action' => 'getDevices',
                    ),
                ),
            ),

            'edit-device-admin' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/edit-device',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Admin',
                        'action' => 'editDevice'
                    )
                ),
            ),

            'view-config-admin' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/view-config',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Admin',
                        'action' => 'viewConfig'
                    )
                ),
            ),

            'update-config-admin' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/update-config',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Admin',
                        'action' => 'updateConfig'
                    )
                ),
            ),

            'factory-reset-admin' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/restore-config',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Admin',
                        'action' => 'restoreConfig'
                    )
                ),
            ),


            'update-fw-admin' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/update-fw',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Admin',
                        'action' => 'updateFW',
                    ),
                ),
            ),

	   'upload-fw-view-camera' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/upload-fw-camera',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Admin',
                        'action' => 'uploadFwViewCamera'
                    )
                ),
            ),

           'upload-fw-view-doorbell' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/upload-fw-doorbell',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Admin',
                        'action' => 'uploadFwViewDoorbell'
                    )
                ),
            ),

            'upload-fw-view-cube' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/upload-fw-cube',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Admin',
                        'action' => 'uploadFwViewCube'
                    )
                ),
            ),

            'get-versions' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/get-versions',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Admin',
                        'action' => 'getVersions'
                    )
                ),
            ),

           


        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            'layout/register' => __DIR__ . '/../view/layout/register_login.phtml',
        ),
        'template_path_stack' => array(
            'admin' => __DIR__ . '/../view',
        ),
    ),
    'email_whitelist' => array(
        'domains' => array(
        ),
        'patterns' => array(
            'mclouddev#range#@mailinator.com' => array(
                'startLimit' => 1,
                'endLimit' => 200
            ),
            'mcloudtest#range#@mailinator.com' => array(
                'startLimit' => 1,
                'endLimit' => 200
            )
        ),
        'emailids' => array(
        )
    ),
    'email_blacklist' => array(
        'domains' => array(
        ),
        'emailids' => array(
        )
    ),
    'email_validate_check' => array(
        'whitelist' => true,
        'blacklist' => true
    ),
);
