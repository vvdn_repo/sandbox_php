<?php

/**
 * This class contains getAutoloaderConfig,onBootstrap functions   
 * @package Admin
 * @author VVDNTechnologies 
 */

namespace Admin;

use Admin\Model\TenantMsp;
use Admin\Model\Admin;
use Admin\Model\Firmware;
use Admin\Model\SandboxAdminTable;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
use Zend\Authentication\AuthenticationService;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module implements AutoloaderProviderInterface {

    public function onBootstrap(MvcEvent $e) {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'SandboxAdminTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new SandboxAdminTable($dbAdapter, $sm);
                    return $table;
                },
                'Admin' => function($sm) {
                    $Admin = new Admin();
                    $Admin->setServiceLocator($sm);
                    return $Admin;
                },
                'Firmware' => function($sm) {
                    $Firmware = new Firmware();
                    $Firmware->setServiceLocator($sm);
                    return $Firmware;
                }
            ),
        );
    }

}
