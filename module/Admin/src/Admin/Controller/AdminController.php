<?php

/**
 * AdminController handles all the actions comming from routes
 * for the Application module
 * @package Application
 * @author VVDN Technologies < >
 */

namespace Admin\Controller;

require VENDOR_PATH . '/aws/aws-autoloader.php';
use Admin\Form\AdminFormFilter;
use Admin\Form\AdminLoginForm;
use Admin\Form\AdminRegisterForm;
use Zend\Http\Header\SetCookie;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\Http\Response;
use Zend\View\Model\ViewModel;
use Zend\Config\Reader\Ini;
use Zend\Session\Container;
use Zend\Session\Container as SessionContainer;
use Zend\Math\Rand;
use Zend\Http\Client;
use Aws\Common\Aws;
use Aws\S3\S3Client;
use Aws\Common\Credentials\Credentials;
use Zend\Config\Reader\Json;
use Zend\Serializer\Serializer;




class AdminController extends AbstractActionController {

    protected $sessionStorage;
    protected $authService;

    /*
     * This function is used to get specified service instance
     * @param service type
     * @return Object Instance of required ZF2 service
     */

    protected function getServiceInstance($service) {
        return $this->getServiceLocator()->get($service);
    }

    /*
     * This function will used to register admins in the system
     * and to populate registration form
     * @return Array of registration status with admin info
     */


    public function getResponseWithHeader() {
        $response = $this->getResponse();
        $response->getHeaders()
            //make can accessed by *
            ->addHeaderLine('Access-Control-Allow-Origin', '*')
            //set allow methods
            ->addHeaderLine('Access-Control-Allow-Methods', 'POST PUT DELETE GET');
        return $response;
    }

    public function registerAction() {
        $response = array();
        $adminModel = $this->getServiceInstance('ApiOperations');
        $formData = array();
        $dbAdapter = $this->getServiceInstance('Zend\Db\Adapter\Adapter');
        //$data = $this->params()->fromPost();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = json_decode (file_get_contents ("php://input"), true);

            $formData = $data;
            //Validate Email from whitelist
            $isValid = $this->validateEmailStatus($data['admin_email']);
            $adminModel = $this->getServiceInstance('Admin');
            $isUnique =$adminModel->validateEmailUnique($data['admin_email']);
            if ("true" === $isValid ) {
                if ("true" === $isUnique ) {
                    // BEGIN-Create Admin
                    $adminEntity = $adminModel->getEntityInstance();
                    $modelCommon = $this->getServiceInstance('Common');
                    $time = $modelCommon->getCurrentTimeStamp();
                    // Create Activation Key
                    $activationKey = $adminModel->generateActivationKey($data);
                    $objectManager = $adminModel->getObjectManager($this->getServiceLocator());
                    $adminEntity->setAdminName(htmlspecialchars($data['admin_name']));
                    $enc_pwd = array();
                    $enc_pwd = $adminModel->encryptPassword($data['admin_pwd']);
                    $adminEntity->setAdminPwd($enc_pwd['key']);
                    $adminEntity->setAdminPwdsalt($enc_pwd['salt']);
                    $adminEntity->setAdminEmail(htmlspecialchars($data['admin_email']));
                    $adminEntity->setAdminMobile(htmlspecialchars($data['admin_mobile']));
                    //$adminEntity->setemailConfirmation($activationKey);
                    $adminEntity->setAdminStatus(0); //0-> InActive Account
                    $adminModel->saveAdmin($adminEntity);

                    $info["adminId"] = $adminEntity->getAdminId();
                    $info["adminName"] = $adminEntity->getAdminName();
                    $info["adminEmail"] = $adminEntity->getAdminEmail();
                    $info["toEmail"] = $adminEntity->getAdminEmail();
                    $info["activationKey"] = $adminModel->generateActivationKey($info);

                    $login_time=time();
                    $rand_num= Rand::getString(5,'abcdefghijklmnopqrstuvwxyz0123456789', true);
                    $admin_tocken="sb_".$info["adminId"]."_".$rand_num;
                    $adminEntity->setAdminToken($admin_tocken);
                    $adminModel->saveAdmin($adminEntity);
                    $adminModel->sendEmail("registration", $info);
                    $status =true;
                    $message = "Account Created Successfully";
                }
                else{
                    $status =false;
                    $message = "The email id is already registerd";
                }
            }
            else {
                $status =false;
                $message = "Failed";
            }
        }
        $response['status'] = $status;
        $response['message'] = $message;
        return new JsonModel($response);
    }


    /*
     * This function will used to login admins in the system
     * and to populate login form
     * @return Array of login status with login form
     */

    public function loginAction() {
        $view = $this->getServiceInstance('ViewRenderer');
        //$eventlogInstance = $this->getServiceInstance('Eventlog');
        $view->headTitle(PAGE_TITLE . " Login");
//        $this->layout('layout/register_login');
        $this->layout('layout/register_login');
        $adminLoginForm = new AdminLoginForm();
        $adminModel = $this->getServiceInstance('Admin');
        $request = $this->getRequest();
        $this->authService = $this->getServiceInstance('AuthService');
        $formData=Array();
        if ($request->isPost()) {
            $dbAdapter = $this->getServiceInstance('Zend\Db\Adapter\Adapter');
            $adminFormFilter = new AdminFormFilter();
            $adminFormFilter->setDbAdapter($dbAdapter);
            $adminFormFilter->setFormType('login');
            $adminLoginForm->setInputFilter($adminFormFilter->getInputFilter());
            $adminLoginForm->setData($request->getPost());
            $data = $this->params()->fromPost();
            $adminEntity = $adminModel->getEntityInstance();
            $adminData=$adminModel->validateLogin($data);
            $adminDataOtp=$adminModel->validateLoginOtp($data);
            if($adminData)
            {
                if($adminData[0][0]['adminStatus'] == 0) {
                    $this->flashMessenger()->addErrorMessage("Account is Not Activated");
                }
                else if($adminData[0][0]['adminStatus'] == 1) {
                    $session = new SessionContainer('base');
                    // $session->setExpirationSeconds('10');
                    /*$sessionConfig = new SessionConfig();
                    $sessionConfig->setOptions(array(
                        'use_cookies' => true,
                        'cookie_httponly' => true,
                        'gc_maxlifetime' => '10',
                        'cookie_lifetime' => '10'
                    ));
                    $manager = new SessionManager($sessionConfig);*/
                    // printf("cookie: %s, gc: %s", ini_get('session.cookie_lifetime'), ini_get('session.gc_maxlifetime'));die;
                    $session->offsetSet('admin_id',$adminData[0][0][adminId]);
                    $session->offsetSet('admin_email',$adminData[0][0][adminEmail]);

                    $this->redirect()->toUrl(BASE_PATH . '/admin/home');
                }

            }
            if($adminDataOtp)
            {
                if($adminDataOtp[0][0]['adminStatus'] == 1) {
                    $session = new Container('base');
                    $session->offsetSet('admin_id',$adminDataOtp[0][0][adminId]);
                    $this->redirect()->toUrl(BASE_PATH . '/admin/resetpwd');
                }
            }
            else
            {
                $this->flashMessenger()->addErrorMessage("Incorrect admin name or password!");
            }
        }

    }


    public function adminHomeAction() {


        $session = new SessionContainer('base');
        $val = $session->offsetGet('admin_id');

        if($val)
        {
            $view = $this->getServiceInstance('ViewRenderer');
            $view->headTitle(PAGE_TITLE . " | Home");
            $this->layout('layout/admin');

        }
        else{
            $this->redirect()->toUrl(BASE_PATH . '/admin/login');
        }


    }

    public function adminDashboardAction() {


        $session = new SessionContainer('base');
        $val = $session->offsetGet('admin_id');

        if($val)
        {
            $userModel = $this->getServiceInstance('User');
            $totalCount=$userModel->getTotalUsercount();
            $activeCount=$userModel->getActiveUsercount();
            $deviceModel = $this->getServiceInstance('Device');
            $totalCamera=$deviceModel->getTotalCameracount();
            $toatalDoorbell=$deviceModel->getTotalDoorbellcount();
            $firmwareModel = $this->getServiceInstance('Firmware');
            $versions = $firmwareModel->getFirmwareVersion();
            $camera = $versions[0]['cameraPrimaryVersion'];
            $doorbell = $versions[0]['doorbellPrimaryVersion'];
            $android = $versions[0]['androidVersion'];
            $ios = $versions[0]['iosVersion'];
            $cloud = $versions[0]['cloudVersion'];
            $secondary=$versions[0]['firmwareSecondaryVersion'];

            /* $config = parse_ini_file('config/config.ini');
             $android=$config['android-version'];
             $ios=$config['ios-version'];
             $primary=$config['fw-version'];
             $secondary=$config['secondary-version'];
             $cloud=$config['cloud_version'];*/


            $status = true;
            $message = "Your Session Expired";

            return new JsonModel(array("status" => $status,"message" => $message,"total_count" => $totalCount,"active_count" => $activeCount,"camera_count" => $totalCamera,"doorbell_count" => $toatalDoorbell,"android" => $android,"ios" => $ios,"camera" => $camera,"doorbell" => $doorbell,"secondary" => $secondary,"cloud" => $cloud));

        }
        else{
            $this->redirect()->toUrl(BASE_PATH . '/admin/login');
        }


    }




    public function getUsersAction()
    {
        //$session = new Container('base');
        //$admin_id = $session->offsetGet('admin_id');
        $admin_id =1;
        if ($admin_id) {
            $userModel = $this->getServiceInstance('User');
            $user_list = $userModel->getUserList();

            if ($user_list) {
                $response['status'] = true;
                $response['data'] = $user_list;
                return new JsonModel(array("status" => true,"data" =>$response['data']));
            }
            else{
                $status = false;
                $message = "No user found";
            }
        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status,"message" => $message));
    }



    public function listUsersAction(){

        $session = new SessionContainer('base');
        $val = $session->offsetGet('admin_id');

        if($val)
        {
            $view = $this->getServiceInstance('ViewRenderer');
            $view->headTitle(PAGE_TITLE . " | Users");
            $this->layout('layout/admin');

        }
        else{
            $this->redirect()->toUrl(BASE_PATH . '/user/login');
        }
    }

    public function listDevicesAction(){
        $session = new SessionContainer('base');
        $val = $session->offsetGet('admin_id');

        if($val)
        {
            $view = $this->getServiceInstance('ViewRenderer');
            $view->headTitle(PAGE_TITLE . " | Admin");
            $this->layout('layout/admin');

        }
        else{
            $this->redirect()->toUrl(BASE_PATH . '/user/login');
        }
    }


    public function getDevicesAction()
    {
        $session = new Container('base');
        $admin_id = $session->offsetGet('admin_id');
        if ($admin_id) {
            $data = json_decode (file_get_contents ("php://input"), true);
            $user_id = $this->params('id');
            $deviceModel = $this->getServiceInstance('Device');
            $device_list = $deviceModel->getDeviceList($user_id);
            if ($device_list) {
                $response['status'] = true;
                $response['data'] = $device_list;
                return new JsonModel(array("status" => true,"data" =>$response['data']));
            }
            else{
                $status = false;
                $message = "No Device found";
                $data = $device_list;
            }
        }
        else{
            $status = false;
            $message = "Your Session Expired";
            $data ="";
        }

        return new JsonModel(array("status" => $status,"message" => $message,"data" =>$data));
    }

    public function editDeviceAction() {
        $session = new Container('base');
        $admin_id = $session->offsetGet('admin_id');
        if ($admin_id) {
            $response = array();
            $request = $this->getRequest();
            if ($request->isPost()) {
                $data = json_decode(file_get_contents("php://input"), true);
                $deviceId = $data['device_id'];
                $deviceModel = $this->getServiceInstance('Device');
                $deviceData = $deviceModel->Device_info('deviceId',$deviceId);
                if ($deviceData) {
                    $objectManager = $deviceModel->getObjectManager($this->getServiceLocator());
                    $result = $deviceModel->editDevice($data);
                    if($result){
                        $status = true;
                        $message = "Device Edited Successfully";
                        return new JsonModel(array("status" => $status, "message" => $message));
                    }
                }
                else{
                    $status = false;
                    $message = "No device found for given device_id";
                }
            }
            else{
                $status = false;
                $message = "Fail";
            }
        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }
        return new JsonModel(array("status" => $status, "message" => $message));

    }

    public function viewConfigAction()
    {
        $session = new Container('base');
        $admin_id = $session->offsetGet('admin_id');
        if ($admin_id) {
            $response = array();
            $request = $this->getRequest();
            $data = json_decode(file_get_contents("php://input"), true);
            $deviceId = $data['device_id'];
            $deviceModel = $this->getServiceInstance('Device');
            $device = $deviceModel->Device_info('deviceId',$deviceId);
            if($device){
                $configModel = $this->getServiceInstance('DeviceConfig');
                $config = $configModel->getDeviceConfig($deviceId);
                if ($config) {
                    $response['status'] = true;
                    $response['response']['motion-detection'] = $config[0]['configMotionDetection'];
                    $response['response']['cube-recording'] = $config[0]['configCubeRecording'];
                    $response['response']['cloud-recording'] = $config[0]['configCloudRecording'];
                    $response['response']['audio-enable'] = $config[0]['configAudioEnable'];
                    $response['response']['hd-enable'] = $config[0]['configVideoQuality'];
                    $response['response']['voice-mail'] = $config[0]['configVoiceMail'];
                    $response['response']['lower-threshold'] = $config[0]['configLowerThreshold'];
                    $response['response']['upper-threshold'] = $config[0]['configUpperThreshold'];
                    $response['response']['lower-threshold-alert'] = $config[0]['configLowerThresholdAlert'];
                    $response['response']['upper-threshold-alert'] = $config[0]['configUpperThresholdAlert'];
                    $response['response']['auto-upgrade'] = $config[0]['configAutoUpgrade'];
                    $response = $this->getResponseWithHeader()
                        ->setContent(json_encode($response));
                    return $response;
                }
                else{
                    $status = false;
                    $message = "Configuration not found";
                }
            }

            else{
                $status = false;
                $message = "No Device found";
            }

        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }
        return new JsonModel(array("status" => $status, "message" => $message));

    }

    public function updateConfigAction() {
        $session = new Container('base');
        $admin_id = $session->offsetGet('admin_id');
        if ($admin_id) {
            $response = array();
            $request = $this->getRequest();
            $this->authService = $this->getServiceInstance('AuthService');
            $data = json_decode(file_get_contents("php://input"), true);
            $deviceId = $data['device_id'];
            $DeviceModel = $this->getServiceInstance('Device');
            if($deviceId) {
                $deviceConfigModel = $this->getServiceInstance('DeviceConfig');
                $CommandModel = $this->getServiceInstance('Command');
                $modelCommon = $this->getServiceInstance('Common');
                $currentTS = $modelCommon->getCurrentTimeStamp();
                // $value = $CommandModel->checkCommandTS($deviceId, $currentTS);
                /*if ($value) {
                    $status = true;
                    $response = $value[0]['response'];
                    return new JsonModel(array("status" => $status, "response" => $response));
                } else {*/
                //insert to command table
                $commandEntity = $CommandModel->getEntityInstance();
                $objectManager = $CommandModel->getObjectManager($this->getServiceLocator());
                $commandEntity->setCommandId(htmlspecialchars($data['commandId']));
                $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId);
                $commandEntity->setDeviceIdFK($userProxyInstance);
                $commandEntity->setStatus('Pending'); //0-> InActive Account
                $commandEntity->settimeStamp($currentTS);
                $requestId = $CommandModel->saveCommand($commandEntity);
                $data['requestId'] = $requestId;
                $response = $this->callhttp($data);
                $device_status = json_decode($response->getBody() , true);
                if ($device_status['device_status'] == "open") {
                    $result = $deviceConfigModel->updateConfig($data);
                    $config = parse_ini_file('config/config.ini');
                    $status = true;
                    $message = "Configuring Settings";
                    $interval = $config['scan-time'];
                    return new JsonModel(array("status" => $status, "message" => $message, "requestId" => $requestId, "interval" => $interval));
                } else {
                    $status = false;
                    $message = "Device is Not Connected";
                }
                // }
            }
            else{
                $status = false;
                $message = "Device Not Belong to your Account";
            }

        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }


    public function restoreConfigAction() {
        $session = new Container('base');
        $admin_id = $session->offsetGet('admin_id');
        if ($admin_id) {
            $response = array();
            $request = $this->getRequest();
            $data = json_decode(file_get_contents("php://input"), true);
            $deviceId = $data['device_id'];
            $DeviceModel = $this->getServiceInstance('Device');
            if($deviceId) {
                $CommandModel = $this->getServiceInstance('Command');
                $modelCommon = $this->getServiceInstance('Common');
                $currentTS = $modelCommon->getCurrentTimeStamp();
                // $value = $CommandModel->checkCommandTS($deviceId, $currentTS);
                /*if ($value) {
                    $status = true;
                    $response = $value[0]['response'];
                    return new JsonModel(array("status" => $status, "response" => $response));
                } else {*/
                //insert to command table & Config Table
                $deviceConfigModel = $this->getServiceInstance('DeviceConfig');
                $result = $deviceConfigModel->restoreConfig($deviceId);
                $commandEntity = $CommandModel->getEntityInstance();
                $objectManager = $CommandModel->getObjectManager($this->getServiceLocator());
                $commandEntity->setCommandId(htmlspecialchars($data['commandId']));
                $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId);
                $commandEntity->setDeviceIdFK($userProxyInstance);
                $commandEntity->setStatus('Pending'); //0-> InActive Account
                $commandEntity->settimeStamp($currentTS);
                $requestId = $CommandModel->saveCommand($commandEntity);
                $data['requestId'] = $requestId;
                $response = $this->callhttp($data);
                $device_status = json_decode($response->getBody() , true);
                if ($device_status['device_status'] == "open") {
                    $delete = $DeviceModel->deleteDevice($deviceId);
                    $sensorModel = $this->getServiceInstance('Sensor');
                    $sensor_list = $sensorModel->getPairedList($deviceId);
                    $result = $sensorModel->restoreSensor($sensor_list);
                    if($delete){
                        $status = true;
                        $message = "Device Restored";
                    }
                    else{
                        $status = false;
                        $message = "Device  is Not Restored";
                    }
                    return new JsonModel(array("status" => $status, "message" => $message));
                } else {
                    $status = false;
                    $message = "Device is Not Connected";
                }
                // }
            }
            else{
                $status = false;
                $message = "Device Not Belong to your Account";
            }

        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }


    public function updateFWAction() {
        $session = new Container('base');
        $admin_id = $session->offsetGet('admin_id');
        if ($admin_id) {
            $response = array();
            $request = $this->getRequest();
            $this->authService = $this->getServiceInstance('AuthService');
            $data = json_decode(file_get_contents("php://input"), true);
            $deviceId = $data['device_id'];
            $DeviceModel = $this->getServiceInstance('Device');
            $device_Id = 1;
            if($device_Id) {
                $CommandModel = $this->getServiceInstance('Command');
                $modelCommon = $this->getServiceInstance('Common');
                $currentTS = $modelCommon->getCurrentTimeStamp();
                // $value = $CommandModel->checkCommandTS($deviceId, $currentTS);
                /*if ($value) {
                    $status = true;
                    $response = $value[0]['response'];
                    return new JsonModel(array("status" => $status, "response" => $response));
                } else {*/
                //insert to command table
                $commandEntity = $CommandModel->getEntityInstance();
                $objectManager = $CommandModel->getObjectManager($this->getServiceLocator());
                $commandEntity->setCommandId(htmlspecialchars($data['commandId']));
                $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId);
                $commandEntity->setDeviceIdFK($userProxyInstance);
                $commandEntity->setStatus('Pending'); //0-> InActive Account
                $commandEntity->settimeStamp($currentTS);
                $requestId = $CommandModel->saveCommand($commandEntity);
                $data['requestId'] = $requestId;
                $config = parse_ini_file('config/config.ini');
                $data['kernel_path'] = $config['kernel-path'];
                $data['fs_path'] = $config['fs-path'];
                $data['kernel_md5'] = $config['kernel-md5'];
                $data['fs_md5'] = $config['fs-md5'];
                $kernel_path = $config['kernel-path'];
                $fs_path = $config['fs-path'];
                $kernel_md5 = $config['kernel-md5'];
                $fs_md5 = $config['fs-md5'];
                //$payload =array("kernel-path" => $kernel_path, "fs-path" => $fs_path, "kernel-md5" => $kernel_md5, "fs-md5" => $fs_md5);
                //$params = json_encode($payload, JSON_UNESCAPED_SLASHES);
                //$data['params'] = $params;
                $response = $this->callhttp_fw($data);
                $device_status = json_decode($response->getBody() , true);
                if ($device_status['device_status'] == "open") {
                    $status = true;
                    $message = "Updating Firmware started";
                    return new JsonModel(array("status" => $status, "message" => $message, "requestId" => $requestId, "interval" => $interval));
                } else {
                    $status = false;
                    $message = "Device is Not Connected";
                }
                // }
            }
            else{
                $status = false;
                $message = "Device Not Belong to your Account";
            }

        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }



    public function callhttp($data) {
        $config = parse_ini_file('config/config.ini');
        $client = new Client();
        $client->setUri($config['node_ip'] . ":" . $config['node_port']);
        $client->setMethod('POST');
        if($data['command']=="pair-sensor" || $data['command']=="sensor-unpair"){
            $data = array("commandId" => $data['commandId'], "command" => $data['command'],"count" => $data['count'], "device_id" => $data['device_id'],"requestId" => $data['requestId'], "params" => $data['params']);
        }

        elseif($data['command']=="set-mode")
        {
            $data = array("commandId" => $data['commandId'], "command" => $data['command'],"device_id" => $data['device_id'],"requestId" => $data['requestId'], "userMode" => $data['userMode']);

        }

        else
        {
            $data = array("commandId" => $data['commandId'], "command" => $data['command'], "device_id" => $data['device_id'], "requestId" => $data['requestId'],  "params" => $data['params']);
        }

        $asd = json_encode($data);
        $client->setRawBody($asd);
        $response = $client->send();
        return $response;
    }

    public function callhttp_fw($data) {
        $config = parse_ini_file('config/config.ini');
        $client = new Client();
        $client->setUri($config['node_ip'] . ":" . $config['node_port']);
        $client->setMethod('POST');
        if($data['command']=="pair-sensor" || $data['command']=="sensor-unpair"){
            $data = array("commandId" => $data['commandId'], "command" => $data['command'],"count" => $data['count'], "device_id" => $data['device_id'],"requestId" => $data['requestId'], "params" => $data['params']);
        }
        else
        {
            $data = array("commandId" => $data['commandId'], "command" => $data['command'], "device_id" => $data['device_id'], "requestId" => $data['requestId'],  "kernel-path" => $data['kernel_path'],  "fs-path" => $data['fs_path'], "kernel-md5" => $data['kernel_md5'], "fs-md5" => $data['fs_md5']);
        }

        $asd = json_encode($data);
        $client->setRawBody($asd);
        $response = $client->send();
        return $response;
    }


    /*
     * This function will be used to check login form message
     * @return true or fale
     */

    protected function checkLoginFormMessages($adminLoginForm) {
        $messages = $adminLoginForm->getMessages();
        if (empty($messages)) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * This function will be used to logout a logged in admin
     * @return viewModel
     */

    public function logoutAction() {
        $session_admin = new Container('base');
        $session_admin->getManager()->getStorage()->clear();
        return $this->redirect()->toRoute('login');
    }


    /*
     * This action function will be used to display Terms Of Service
     * todo definition yet to be defined
     */

    public function termsOfServicesAction() {
        $view = $this->getServiceInstance('ViewRenderer');
        $view->headTitle(PAGE_TITLE . " | Terms Of Services");
        $this->layout('layout/termspolicyLayout');
        // Get Organisation Data
        $mspOrgData = $this->organizationBrandingData('terms.html');
        $this->_view = new ViewModel();
        $this->layout()->setVariable('orgLogo', $mspOrgData['orgLogo']);
        $this->layout()->setVariable('mspOrgName', $mspOrgData['orgName']);
        return new ViewModel(array(
            'name' => $mspOrgData['fileContent']
        ));
    }

    public function privacyPolicyAction() {
        $view = $this->getServiceInstance('ViewRenderer');
        $view->headTitle(PAGE_TITLE . " | Privacy Policy");
        $this->layout('layout/termspolicyLayout');
        // Get Organisation Data
        $mspOrgData = $this->organizationBrandingData('policy.html');
        $this->_view = new ViewModel();
        $this->layout()->setVariable('orgLogo', $mspOrgData['orgLogo']);
        $this->layout()->setVariable('mspOrgName', $mspOrgData['orgName']);
        return new ViewModel(array(
            'name' => $mspOrgData['fileContent'],
        ));
    }

    public function contactUsAction() {
        $view = $this->getServiceInstance('ViewRenderer');
        $view->headTitle(PAGE_TITLE . " | Contact Us");
        $this->layout('layout/termspolicyLayout');
        // Get Organisation Data
        $mspOrgData = $this->organizationBrandingData('contact.html');
        $this->_view = new ViewModel();
        $this->layout()->setVariable('orgLogo', $mspOrgData['orgLogo']);
        $this->layout()->setVariable('mspOrgName', $mspOrgData['orgName']);
        return new ViewModel(array(
            'name' => $mspOrgData['fileContent'],
        ));
    }



    public function activateAccountAction() {
        $encodedkey = $this->params('id');
        $now = time();
        $past = strtotime('-1 days', $now);
        $adminData = array('adminStatus' => 0);
        //$eventlogInstance = $this->getServiceInstance('Eventlog');
        if (!empty($encodedkey)) {
            //$adminId = base64_decode($encodedAdminId);
            $adminModel = $this->getServiceInstance('Admin');
            $data = $adminModel->decodeActivationKey($encodedkey);
            if ($data['adminEmail'] != "" && $data['adminId'] != "") {
                $admin = $adminModel->loadByAdminEmail($data['adminEmail']);
                $this->authService = $this->getServiceInstance('AuthService');
                $this->authService->clearIdentity();
                if ($admin['adminId'] == $data['adminId'] && $admin['adminStatus'] == 0) {

                    if ($data['time'] > $past) {
                        $adminData['adminStatus'] = 1;
                        $adminModel->updateAdmin($adminData, array('adminId' => $admin['adminId']));
                        $this->flashMessenger()->addSuccessMessage("Account Activated Successfully");
                        $this->authService = $this->getServiceInstance('AuthService');
//                        $this->initializeAdmin($data['adminEmail']);
                        $logMessage = array('%EMAIL%' => $data['adminEmail']);
                        // $eventlogInstance->createLog('admin', 'activate', $admin['adminId'], $admin['adminName'], 'success', "", "", 0, 0, $logMessage);
                        return $this->redirect()->toRoute('login');
                    } else {
                        // start: to get the expire link status in session
                        $session = new SessionContainer('expire_link');
                        $session->expire_link = 1;
                        // end
                        $this->authService = $this->getServiceInstance('AuthService');
                        $this->initializeAdmin($data['adminEmail']);
                        $this->redirect()->toRoute('landing-page');
                    }
                } else if ($admin['adminStatus'] == 1) {
                    return $this->redirect()->toRoute('login');
                }
            }
        }
        if ($adminData['adminStatus'] == 1) {
            return $this->redirect()->toRoute('login');
        } else {
            return $this->redirect()->toRoute('login');
        }
    }


    public function generatecaptchaAction() {
        $response = array();
        $response['status'] = "success";
        $adminModel = $this->getServiceInstance('Admin');
        $response['captchaId'] = $adminModel->generateCaptcha();
        return new JsonModel($response);
    }

    public function checkadminemailAction() {

        $request = $this->getRequest();
        $email = $request->getPost('admin_email');
        $response = array();
        $response['availability'] = "0";
        $response['message'] = "Already Used";
        $response['status'] = "false";
        $response['email'] = $email;
        $eventlogInstance = $this->getServiceInstance('Eventlog');
        $adminModel = $this->getServiceInstance('Admin');
        $adminCount = $adminModel->getEmailCount($email);
        if ($adminCount == 0) {
            $response['availability'] = "1";
            $response['message'] = "Available";
            $response['status'] = $this->validateEmailStatus($email);
            if ("false" === $response['status']) {
                $response['message'] = "Not Allowed";
            }
        }
        else {
            //$logMessage = array("%EMAIL%" => $email);
            //$eventlogInstance->createLog('admin', 'verify-email', 0, "", 'failure', "", "", 0, 0, $logMessage);
        }

        return new JsonModel($response);
    }


    public function forgotpasswordAction() {
        $view = $this->getServiceInstance('ViewRenderer');
        $view->headTitle(PAGE_TITLE . " | Reset Password");
        $this->layout('layout/register_login');
        $request = $this->getRequest();
        $response = array();
        $response['status'] = 'failure';
        $response['email'] = '';
        if ($request->isPost()) {
            $email = htmlspecialchars($request->getPost('admin_email'));
            $response['email'] = $email;
            $adminModel = $this->getServiceInstance('Admin');
            $adminCount = $adminModel->getEmailCount($email);
            if ($adminCount == 1) {
                $newPass = rand(78900000, 98700023);
                // $encPass = $adminModel->encryptPassword($newPass);
                //echo $newPass . "  -->" . $encPass;
                $modelCommon = $this->getServiceInstance('Common');
                $currentTS = $modelCommon->getCurrentTimeStamp();
                $status = $adminModel->resetPassword($email, $newPass, 1, $currentTS);
                if ($status) {
                    $response['status'] = 'success';
                    $admin = $adminModel->loadByAdminEmail($email);
                    $data['toEmail'] = $email;
                    $data['adminPwd'] = $newPass;
                    $data['adminName'] = $admin['adminName'];
                    $adminModel->sendEmail('passwordreset', $data);
                    //$logIpAddress = $_SERVER['REMOTE_ADDR'];
                    // $eventLog = $this->getServiceInstance('Eventlog');
                    //$logMessage = array('%IPADDRESS%' => $logIpAddress);
                    // $eventLog->createLog('admin', 'otp', $admin['adminId'], $admin['adminName'], 'success', 'Password Reset', "", 0, 0, $logMessage);
                }
            }
        } else
            $response['status'] = 'loadpage';
        return $response;
    }

    /**
     * Admin can reset the password using current password
     * @return string
     */
    public function resetpwdAction()
    {
        $view = $this->getServiceInstance('ViewRenderer');
        $view->headTitle("Reset Password");
        $this->layout('layout/register_login');
        $request = $this->getRequest();
        $this->authService = $this->getServiceInstance('AuthService');

        if ($request->isPost()) {
            $adminModel = $this->getServiceInstance('Admin');
            $npwd = $request->getPost('newPassword');
            $session = new Container('base');
            $id = $session->offsetGet('admin_id');
            if($id){
                $encPass = $adminModel->encryptPassword($npwd);
                $data = $adminModel->selectdata($id);
                $modelCommon = $this->getServiceInstance('Common');
                $currentTS = $modelCommon->getCurrentTimeStamp();
                $time_elapsed = $currentTS - $data[0][0]['adminOtpTimestamp'];
                if ($time_elapsed > 86400) {
                    $this->flashMessenger()->addErrorMessage("OTP password has expired!");
                    return $this->redirect()->toRoute('resetpwd');
                }
                else {
                    $status = $adminModel->resetpwd($data[0][0]['adminId'], $encPass['key'],$encPass['salt']);
                    if ($status) {
                        //$this->authService->clearIdentity();
                        $this->flashMessenger()->addErrorMessage("Your password has been updated successfully. Please login with new Password");

                        return $this->redirect()->toRoute('login');
                    }
                    else {
                        $this->flashMessenger()->addErrorMessage("Failed to update the password!");
                        $response['message'] = "Failed to update the password!";
                    }
                }
            }
            else{
                $this->flashMessenger()->addErrorMessage("Session expired!");
                return $this->redirect()->toRoute('resetpwd');
            }

        }
    }

    public function resetpasswordAction() {
        $view = $this->getServiceInstance('ViewRenderer');
        $eventLog = $this->getServiceInstance('Eventlog');
        $view->headTitle("Reset Password");
        $this->layout('layout/register_login');
        $request = $this->getRequest();
        $response = array();
        $response['status'] = 0;
        $response['message'] = '';
        $ajax = false;

        if ($request->isPost()) {
            $cpwd = $request->getPost('currentPassword');
            $npwd = $request->getPost('newPassword');

            if (!$cpwd && !$npwd) {
                $ajax = true;
                $data = json_decode(file_get_contents("php://input"));
                $cpwd = $data->currentPassword;
                $npwd = $data->newPassword;
            }
            $adminModel = $this->getServiceInstance('Admin');
            $this->authService = $this->getServiceInstance('AuthService');
            $adminAuth = $this->authService->getStorage()->read();

            $adminId = $adminAuth['admin_id'];
            $admin = $adminModel->getAdminById($adminId);
            $encPass = $adminModel->encryptPassword($cpwd);
            $encCurrentPass = $adminModel->encryptPassword($npwd);
            if ($adminAuth['adminOtpstatus'] == 1) {
                if ($cpwd == $npwd) {
                    $this->flashMessenger()->addErrorMessage("New password cannot be the same as current password!");
                    return $this->redirect()->toRoute('resetpassword');
                } else if ($adminAuth['adminPwd'] == $encCurrentPass) {
                    $this->flashMessenger()->addErrorMessage("New password cannot be the same as current password!");
                    return $this->redirect()->toRoute('resetpassword');
                } else {
                    if ($adminAuth['adminOtp'] == $encPass || $adminAuth['adminPwd'] == $encPass) {

                        $modelCommon = $this->getServiceInstance('Common');
                        $currentTS = $modelCommon->getCurrentTimeStamp();
                        $time_elapsed = $currentTS - $adminAuth['adminOtpTimestamp'];

                        if ($adminAuth['adminOtp'] == $encPass && $time_elapsed > 3600) {
                            $this->flashMessenger()->addErrorMessage("OTP password has expired!");
                            return $this->redirect()->toRoute('resetpassword');
                        } else {
                            $encPass = $adminModel->encryptPassword($npwd);
                            $status = $adminModel->resetPassword($admin['adminEmail'], $encPass);
                            if ($status) {
                                $this->authService->clearIdentity();
                                $this->flashMessenger()->addSuccessMessage("Your password has been updated successfully");
                                $eventLog->createLog('admin', 'reset-password-otp', $admin['adminId'], $admin['adminName'], 'success', 'Password Reset');
                                return $this->redirect()->toRoute('login');
                            } else
                                $response['message'] = "Failed to update the password!";
                        }
                    } else
                        $this->flashMessenger()->addErrorMessage("Current password is not matching!");
                    return $this->redirect()->toRoute('resetpassword');
                }
            }
            else {

                if ($cpwd == $npwd) {
                    //$this->authService->clearIdentity();
                    if (!$ajax) {
                        $this->flashMessenger()->addErrorMessage("New password cannot be the same as current password!");
                        return $this->redirect()->toRoute('resetpassword');
                    } else {
                        $response['message'] = "New password cannot be the same as current password!";
                    }
                } else {
                    if ($encPass == $admin['adminPwd']) {
                        $encPass = $adminModel->encryptPassword($npwd);
                        $status = $adminModel->resetPassword($admin['adminEmail'], $encPass);
                        if ($status) {
                            $eventLog->createLog('admin', 'reset-password', $admin['adminId'], $admin['adminName'], 'success', 'Password Reset');
                            if (!$ajax) {
                                $this->authService->clearIdentity();
                                $this->flashMessenger()->addSuccessMessage("Your password has been updated successfully");
                                //$this->initializeAdmin($admin['adminEmail']);
                                return $this->redirect()->toRoute('login');
                            } else {
                                $response['status'] = 1;
                                $response['message'] = "Your password had been updated successfully!";
                            }
                        } else
                            $response['message'] = "Failed to update the password!";
                    }
                    else {
                        if (!$ajax) {
                            $this->flashMessenger()->addErrorMessage("Current password is not matching!");
                            return $this->redirect()->toRoute('resetpassword');
                        } else {
                            $response['message'] = "Current password is not matching!";
                        }
                    }
                }
            }
        }
        if ($ajax)
            return new JsonModel($response);
        else
            return $response;
    }


    public function deviceAction(){

        $session = new SessionContainer('base');
        $val = $session->offsetGet('admin_id');

        if($val)
        {
            $view = $this->getServiceInstance('ViewRenderer');
            $view->headTitle(PAGE_TITLE . " | Home");
            $this->layout('layout/home');

        }
        else{
            $this->redirect()->toUrl(BASE_PATH . '/admin/login');
        }
    }

    public function listAlarmAction(){

        $session = new SessionContainer('base');
        $val = $session->offsetGet('admin_id');

        if($val)
        {
            $view = $this->getServiceInstance('ViewRenderer');
            $view->headTitle(PAGE_TITLE . " | Home");
            $this->layout('layout/home');

        }
        else{
            $this->redirect()->toUrl(BASE_PATH . '/admin/login');
        }
    }


    public function uploadFwViewCameraAction(){

        $session = new SessionContainer('base');
        $val = $session->offsetGet('admin_id');

        if($val)
        {
            $view = $this->getServiceInstance('ViewRenderer');

            $view->headTitle(PAGE_TITLE . " | Upload Firmware");
            $this->layout('layout/admin');
            $firmwareModel = $this->getServiceInstance('Firmware');
            $admin = $firmwareModel->getFirmwareVersion();
            $request = $this->getRequest();
            if (isset($_POST['submit'])) {
                if(empty($_FILES["fileToUpload_fs"]["name"]) || empty($_FILES["fileToUpload_kernal"]["name"]))
                {
                    $this->flashMessenger()->addErrorMessage("Select Firmwares to Upload");
                    die;
                }
                $fw_version = $_POST["fw_version"];
                $ios_version = $_POST["ios_version"];
                $android_version = $_POST["android_version"];
                $cloud_version = $_POST["cloud_version"];
                $md5_fs = md5_file($_FILES["fileToUpload_fs"]["tmp_name"]);
                $md5_kernal = md5_file($_FILES["fileToUpload_kernal"]["tmp_name"]);
                $config = $this->getServiceLocator()->get('config');
                $bucket = $config['s3-bucket-fw'];
                $client = S3Client::factory(array(
                    'key' => $config['s3-key'],
                    'secret' => $config['s3-secret']
                ));

                //$folderId ="sbxu-wcnd-fw";
                $fileName_fs=$_FILES["fileToUpload_fs"]["name"];
                $fileName_kernal=$_FILES["fileToUpload_kernal"]["name"];
                $uploadfile_fs =$_FILES["fileToUpload_fs"]["tmp_name"];
                $uploadfile_kernal =$_FILES["fileToUpload_kernal"]["tmp_name"];
                $fileSize_fs = filesize($uploadfile_fs);
                $fileSize_kernal = filesize($uploadfile_kernal);

                if ($client) {

                    $result = $client->putObject(array(
                        'Bucket' => $bucket,
                        'Key' => $fileName_fs,
                        'SourceFile' => $uploadfile_fs,
                        'ACL' => 'public-read',
                        'Metadata' => array(
                            'name' => $fileName_fs,
                            'bytes' => $fileSize_fs
                        )
                    ));


                    $result = $client->putObject(array(
                        'Bucket' => $bucket,
                        'Key' => $fileName_kernal,
                        'SourceFile' => $uploadfile_kernal,
                        'ACL' => 'public-read',
                        'Metadata' => array(
                            'name' => $fileName_kernal,
                            'bytes' => $fileSize_kernal
                        )
                    ));
                }
                $result_fs = $client->getObject(array(
                    'Bucket' => $bucket,
                    'Key'    =>$fileName_fs
                ));

                $result_kernal = $client->getObject(array(
                    'Bucket' => $bucket,
                    'Key'    =>$fileName_kernal
                ));
                if($result_fs['Metadata']['bytes'] == $fileSize_fs && $result_kernal['Metadata']['bytes'] == $fileSize_kernal)
                {
                    $fs_path="https://s3-us-west-2.amazonaws.com/sbxu-wcnd-fw/".$fileName_fs;
                    $kernal_path ="https://s3-us-west-2.amazonaws.com/sbxu-wcnd-fw/".$fileName_kernal;
                    $admin = $firmwareModel->updateCameraFirmwareVersion($fw_version,$md5_fs,$md5_kernal,$ios_version,$android_version,$cloud_version,$fs_path,$kernal_path);
                    $deviceModel = $this->getServiceInstance('Device');
                    $type="camera";
                    $update = $deviceModel->setAutoUpgradeList($type);
                    $this->flashMessenger()->addErrorMessage("New Firmware Uploaded Successfully");
                }
            }

        }
        else{
            $this->redirect()->toUrl(BASE_PATH . '/admin/login');
        }

    }


    public function uploadFwViewDoorbellAction(){

        $session = new SessionContainer('base');
        $val = $session->offsetGet('admin_id');

        if($val)
        {
            $view = $this->getServiceInstance('ViewRenderer');

            $view->headTitle(PAGE_TITLE . " | Upload Firmware");
            $this->layout('layout/admin');
            $firmwareModel = $this->getServiceInstance('Firmware');
            $admin = $firmwareModel->getFirmwareVersion();
            $request = $this->getRequest();
            if (isset($_POST['submit'])) {
                if(empty($_FILES["fileToUpload_fs"]["name"]) || empty($_FILES["fileToUpload_kernal"]["name"]))
                {
                    $this->flashMessenger()->addErrorMessage("Select Firmwares to Upload");
                    die;
                }
                $fw_version = $_POST["fw_version"];
                $ios_version = $_POST["ios_version"];
                $android_version = $_POST["android_version"];
                $cloud_version = $_POST["cloud_version"];
                $md5_fs = md5_file($_FILES["fileToUpload_fs"]["tmp_name"]);
                $md5_kernal = md5_file($_FILES["fileToUpload_kernal"]["tmp_name"]);
                $config = $this->getServiceLocator()->get('config');
                $bucket = $config['s3-bucket-fw'];
                $client = S3Client::factory(array(
                    'key' => $config['s3-key'],
                    'secret' => $config['s3-secret']
                ));

                //$folderId ="sbxu-wcnd-fw";
                $fileName_fs=$_FILES["fileToUpload_fs"]["name"];
                $fileName_kernal=$_FILES["fileToUpload_kernal"]["name"];
                $uploadfile_fs =$_FILES["fileToUpload_fs"]["tmp_name"];
                $uploadfile_kernal =$_FILES["fileToUpload_kernal"]["tmp_name"];
                $fileSize_fs = filesize($uploadfile_fs);
                $fileSize_kernal = filesize($uploadfile_kernal);

                if ($client) {

                    $result = $client->putObject(array(
                        'Bucket' => $bucket,
                        'Key' => $fileName_fs,
                        'SourceFile' => $uploadfile_fs,
                        'ACL' => 'public-read',
                        'Metadata' => array(
                            'name' => $fileName_fs,
                            'bytes' => $fileSize_fs
                        )
                    ));


                    $result = $client->putObject(array(
                        'Bucket' => $bucket,
                        'Key' => $fileName_kernal,
                        'SourceFile' => $uploadfile_kernal,
                        'ACL' => 'public-read',
                        'Metadata' => array(
                            'name' => $fileName_kernal,
                            'bytes' => $fileSize_kernal
                        )
                    ));
                }
                $result_fs = $client->getObject(array(
                    'Bucket' => $bucket,
                    'Key'    =>$fileName_fs
                ));

                $result_kernal = $client->getObject(array(
                    'Bucket' => $bucket,
                    'Key'    =>$fileName_kernal
                ));
                if($result_fs['Metadata']['bytes'] == $fileSize_fs && $result_kernal['Metadata']['bytes'] == $fileSize_kernal)
                {
                    $fs_path="https://s3-us-west-2.amazonaws.com/sbxu-wcnd-fw/".$fileName_fs;
                    $kernal_path ="https://s3-us-west-2.amazonaws.com/sbxu-wcnd-fw/".$fileName_kernal;
                    $admin = $firmwareModel->updateDoorbellFirmwareVersion($fw_version,$md5_fs,$md5_kernal,$ios_version,$android_version,$cloud_version,$fs_path,$kernal_path);
                    $deviceModel = $this->getServiceInstance('Device');
                    $type="doorbell";
                    $update = $deviceModel->setAutoUpgradeList($type);
                    $this->flashMessenger()->addErrorMessage("New Firmware Uploaded Successfully");
                }
            }

        }
        else{
            $this->redirect()->toUrl(BASE_PATH . '/admin/login');
        }

    }


    public function uploadFwViewCubeAction(){

        $session = new SessionContainer('base');
        $val = $session->offsetGet('admin_id');

        if($val)
        {
            $view = $this->getServiceInstance('ViewRenderer');

            $view->headTitle(PAGE_TITLE . " | Upload Firmware");
            $this->layout('layout/admin');
            $firmwareModel = $this->getServiceInstance('Firmware');
            $admin = $firmwareModel->getFirmwareVersion();
            $request = $this->getRequest();
            if (isset($_POST['submit'])) {
                if(empty($_FILES["fileToUpload"]["name"]))
                {
                    $this->flashMessenger()->addErrorMessage("Select Firmwares to Upload");
                    die;
                }
                $cube_version = $_POST["cube_version"];
                $md5 = md5_file($_FILES["fileToUpload"]["tmp_name"]);
                $config = $this->getServiceLocator()->get('config');
                $bucket = $config['s3-bucket-fw'];
                $client = S3Client::factory(array(
                    'key' => $config['s3-key'],
                    'secret' => $config['s3-secret']
                ));

                //$folderId ="sbxu-wcnd-fw";
                $fileName=$_FILES["fileToUpload"]["name"];
                $uploadfile =$_FILES["fileToUpload"]["tmp_name"];
                $fileSize = filesize($uploadfile);

                if ($client) {

                    $result = $client->putObject(array(
                        'Bucket' => $bucket,
                        'Key' => $fileName,
                        'SourceFile' => $uploadfile,
                        'ACL' => 'public-read',
                        'Metadata' => array(
                            'name' => $fileName,
                            'bytes' => $fileSize
                        )
                    ));

                }
                $result = $client->getObject(array(
                    'Bucket' => $bucket,
                    'Key'    =>$fileName
                ));

                if($result['Metadata']['bytes'] == $fileSize)
                {
                    $path="https://s3-us-west-2.amazonaws.com/sbxu-wcnd-fw/".$fileName;
                    $admin = $firmwareModel->updateCubeFirmwareVersion($cube_version,$md5,$path);
                    //$deviceModel = $this->getServiceInstance('Device');
                    //$update = $deviceModel->setAutoUpgradeList();
                    $this->flashMessenger()->addErrorMessage("New Firmware Uploaded Successfully");
                }
            }

        }
        else{
            $this->redirect()->toUrl(BASE_PATH . '/admin/login');
        }

    }

    public function getVersionsAction(){
        $session = new SessionContainer('base');
        $val = $session->offsetGet('admin_id');

        if($val) {
            $firmwareModel = $this->getServiceInstance('Firmware');
            $versions = $firmwareModel->getFirmwareVersion();
            $status = true;
            $camra_version = $versions[0]['cameraPrimaryVersion'];
            $doorbell_version = $versions[0]['doorbellPrimaryVersion'];
            $cube_version = $versions[0]['cubeVersion'];
            $android_version = $versions[0]['androidVersion'];
            $ios_version = $versions[0]['iosVersion'];
            $cloud_version = $versions[0]['cloudVersion'];
        }
        return new JsonModel(array("status" => $status, "camera_version" => $camra_version,"doorbell_version" => $doorbell_version,"cube_version" => $cube_version,"android_version" => $android_version,"ios_version" => $ios_version,"cloud_version" => $cloud_version));


    }


    public function load_testAction(){


        $view = $this->getServiceInstance('ViewRenderer');
        $view->headTitle(PAGE_TITLE . " | Home");
        $this->layout('layout/home');


    }

    /*
     * Check whether the Email address is already registered or not
     */

    public function verifyEmailAction() {
        $status = array();
        $request = $this->getRequest();
        $eventlogInstance = $this->getServiceInstance('Eventlog');
        $status['status'] = true;
        if ($request->isPost()) {
            $data = json_decode(file_get_contents("php://input"), true);
            $status['adminId'] = $data['adminId'];
            $status['adminEmail'] = $data['adminEmail'];
            $adminModel = $this->getServiceInstance('Admin');
            $adminCount = $adminModel->getEmailCount($data['adminEmail'], $data['adminId']);
            if ($adminCount < 1) {
                $status['available'] = 1;
            } else {
                $status['available'] = 0;
                $status['message'] = "Email address already registered!!";
                $logMessage = array("%EMAIL%" => $data['adminEmail']);
                $eventlogInstance->createLog('admin', 'duplicate-email', 0, "", 'failure', "", "", 0, 0, $logMessage);
            }
        }
        return new JsonModel($status);
    }

    public function validateEmailStatus($email) {
        $isValid = "false";
        $config = $this->getServiceInstance('config');
        // Allowed
        $allowedDomains = $config['email_whitelist']['domains'];
        $allowedEmails = $config['email_whitelist']['emailids'];
        // Not Allowed
        $notAllowedDomains = $config['email_blacklist']['domains'];
        $notAllowedEmails = $config['email_blacklist']['emailids'];
        // Validation Status
        $validateStatus = $config['email_validate_check'];
        $emailDomain = substr(strrchr($email, "@"), 1);
        // Check for whitelisted data
        if ($validateStatus['whitelist']) {
            if (in_array($emailDomain, $allowedDomains)) {
                return $isValid = "true";
            } elseif (in_array($email, $config['email_whitelist']['emailids'])) {
                return  $isValid = "true";
            } elseif (empty($allowedDomains)) {
                return  $isValid = "true";
            } else {
                foreach ($config['email_whitelist']['patterns'] as $pattern => $patternLimit) {
                    $emailParts = explode("#range#", $pattern);
                    $isMatch = preg_match('/^' . $emailParts[0] . '(\d+)' . $emailParts[1] . '$/', $email, $matches);
                    if ($isMatch && $matches[1] >= $patternLimit['startLimit'] && $matches[1] <= $patternLimit['endLimit']) {
                        return $isValid = "true";
                        break;
                    }
                }
            }
        }

    }


}