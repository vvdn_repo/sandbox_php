<?php

/**
 * MeruAdminTable model
 * @package admin
 * @author VVDN Technologies 
 */

namespace Admin\Model;

use Exception;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\ServiceManager\ServiceManager;

class SandboxAdminTable extends AbstractTableGateway {

    /**
     * Allowed fields
     *
     * @var array
     */
    protected $_fields = array(
        'admin_id',
        'admin_orgid_fk',
        'admin_name',
        'admin_email'
    );

    /**
     * Admindata
     *
     * @var array
     */
    protected $_data = array();
    protected $table = 'sandbox_admin';
    protected static $staticTableName = 'sandbox_admin';
    protected $serviceManager;
    protected $adapter;

    public function __construct(Adapter $adapter, ServiceManager 
            $serviceManager) {
        $this->adapter = $adapter;
        $this->initialize();
        $this->resultSetPrototype = new HydratingResultSet();
        $this->serviceManager = $serviceManager;
    }

    public static function getTableName() {
        return self::$staticTableName;
    }

    /**
     * Setting the needed data
     *
     * @param array $data
     * @return Admin_Model_SandboxAdminTable
     */
    public function setData(array $data) {
        if (!empty($this->_data)) {
            throw new Auth_Model_Exception(
            'Data already set'
            );
        }
        foreach ($data as $name => $value) {
            if (in_array($name, $this->_fields)) {
                $this->_data[$name] = $value;
            }
        }
        return $this;
    }

    /**
     * Getting the data
     *
     * @param string $key
     * @return mixed
     */
    protected function _getData($key) {
        if (!in_array($key, $this->_fields)) {
            throw new Auth_Model_Exception(
            'Given data is not available: ' . $key
            );
        }
        if (!isset($this->_data[$key])) {
            return null;
        }
        return $this->_data[$key];
    }

    /**
     * Get the admin id
     *
     * @return int
     */
    public function getId() {
        return $this->_getData('admin_id');
    }

    /**
     * Get adminemail
     *
     * @return string
     */
    public function getAdminemail() {
        return $this->_getData('admin_email');
    }

    /**
     * Get adminorg id
     *
     * @return string
     */
    public function getAdminOrgId() {
        return $this->_getData('admin_orgid_fk');
    }

    /**
     * Static factory method to load the admin by adminname.
     *
     * @param string $adminname
     * @return Admin_Model_SandboxAdminTable|null
     */
    public function loadByAdminEmail($adminemail) {
        $result = $this->select(function (Select $select) use ($adminemail) {
            $select->where(array('admin_email' => $adminemail));
        });

        if (!$result) {
            throw new Exception("Could not find row admin email " . $adminemail);
        }
        return $result->toArray();
    }

    public function fetchAll() {
        $result = $this->select();
        return $result->toArray();
    }

    public function getAdmin($id) {
        $result = $this->select(function (Select $select) use ($id) {
            $select->where(array('admin_id' => $id));
        });
        return $result->toArray();
    }

    public function saveAdmin($admin) {
        if (!isset($admin['admin_id'])) {
            $this->insert($admin);
            $lastInsertId = $this->adapter->getDriver()->getLastGeneratedValue();
            return $lastInsertId;
        } else {
            if ($this->getAdmin($admin['admin_id'])) {
                $this->update($admin, array('admin_id' => $admin['admin_id']));
            } else {
                throw new Exception('Admin id does not exist');
            }
        }
    }

    public function getAdminCreatedBy($created_by) {
        $result = $this->select(function (Select $select) use ($created_by) {
            $select->columns(array('admin_name', 'admin_email', 'admin_mobile'));
            $select->join('sandbox_org', 'sandbox_org.org_id = sandbox_admin.admin_orgid_fk', 
                    array('org_name'), 'left');
            $select->where(array('admin_createdby_fk' => $created_by));
        });
        return $result->toArray();
    }

    public function encryptPassword($password = "") {
        if (!empty($password)) {
            $config = $this->serviceManager->get('config');
            $password = md5($password . $config['password_hash_salt']);
            return $password;
        }
    }

    public function updateAdmin($data, $where) {
        $this->update($data, $where);
    }

}
