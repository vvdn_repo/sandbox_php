<?php

/**
 * Firmware Model
 *
 * @package Firmware
 * @author VVDN Technologies < >
 */

namespace Admin\Model;

use Application\Entity\SandboxFirmware;
use Application\Model\AbstractOrmManager;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;
use Zend\Captcha\Image as ZendcaptchaImage;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\Container as SessionContainer;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplateMapResolver;
use Zend\Crypt\Key\Derivation\Pbkdf2;
use Zend\Math\Rand;

class Firmware extends AbstractOrmManager implements ServiceLocatorAwareInterface {

    protected $serviceManager;
    protected $firmwareEntityInstance;

    public function getServiceLocator() {
        return $this->serviceManager;
    }


    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceManager = $serviceLocator;
    }

    public function getRepository() {
        $repository = $this->getObjectManager($this->serviceManager)->getRepository('Application\Entity\SandboxFirmware');
        return $repository;
    }

    public function getEntityInstance() {
        if (null === $this->firmwareEntityInstance) {
            $this->firmwareEntityInstance = new SandboxFirmware();
        }
        return $this->firmwareEntityInstance;
    }


    public function saveAlarm(SandboxFirmware &$alarm)
    {
        $om = $this->getObjectManager($this->serviceManager);
        $om->persist($alarm);
        $om->flush();
        return $alarm->getFirmwareId();
    }


    public function updateCameraFirmwareVersion($fw_version,$md5_fs,$md5_kernal,$ios_version,$android_version,$cloud_version,$primary_fs_path,$primary_kernal_path){
        $qb = $this->getRepository()->createQueryBuilder("Firmware");
        $query = $qb->update()
            ->set("Firmware.cameraPrimaryVersion", "?1")
            ->set("Firmware.cameraPrimaryFsMd5", "?2")
            ->set("Firmware.cameraPrimaryKernalMd5", "?3")
            ->set("Firmware.iosVersion", "?4")
            ->set("Firmware.androidVersion", "?5")
            ->set("Firmware.cloudVersion", "?6")
	    ->set("Firmware.cameraPrimaryFsPath", "?7")
            ->set("Firmware.cameraPrimaryKernalPath", "?8")
            ->setParameter(1, $fw_version)
            ->setParameter(2, $md5_fs)
            ->setParameter(3, $md5_kernal)
            ->setParameter(4, $ios_version)
            ->setParameter(5, $android_version)
            ->setParameter(6, $cloud_version)
	    ->setParameter(7, $primary_fs_path)
            ->setParameter(8, $primary_kernal_path)

            ->getQuery();
        $query->execute();
        return true;
    }

    public function updateDoorbellFirmwareVersion($fw_version,$md5_fs,$md5_kernal,$ios_version,$android_version,$cloud_version,$primary_fs_path,$primary_kernal_path){
        $qb = $this->getRepository()->createQueryBuilder("Firmware");
        $query = $qb->update()
            ->set("Firmware.doorbellPrimaryVersion", "?1")
            ->set("Firmware.doorbellPrimaryFsMd5", "?2")
            ->set("Firmware.doorbellPrimaryKernalMd5", "?3")
            ->set("Firmware.iosVersion", "?4")
            ->set("Firmware.androidVersion", "?5")
            ->set("Firmware.cloudVersion", "?6")
            ->set("Firmware.doorbellPrimaryFsPath", "?7")
            ->set("Firmware.doorbellPrimaryKernalPath", "?8")
            ->setParameter(1, $fw_version)
            ->setParameter(2, $md5_fs)
            ->setParameter(3, $md5_kernal)
            ->setParameter(4, $ios_version)
            ->setParameter(5, $android_version)
            ->setParameter(6, $cloud_version)
            ->setParameter(7, $primary_fs_path)
            ->setParameter(8, $primary_kernal_path)

            ->getQuery();
        $query->execute();
        return true;
    }

    public function updateCubeFirmwareVersion($cube_version,$md5,$path){
        $qb = $this->getRepository()->createQueryBuilder("Firmware");
        $query = $qb->update()
            ->set("Firmware.cubeVersion", "?1")
            ->set("Firmware.cubeMd5", "?2")
            ->set("Firmware.cubePath", "?3")
            ->setParameter(1, $cube_version)
            ->setParameter(2, $md5)
            ->setParameter(3, $path)

            ->getQuery();
        $query->execute();
        return true;
    }

    public function getFirmwareVersion($uid,$devicelist){

        $qb = $this->getRepository()->createQueryBuilder("Firmware");
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }
    

}
