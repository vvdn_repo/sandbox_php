<?php

/**
 * Admin Model
 * 
 * @package Admin
 * @author VVDN Technologies < >
 */

namespace Admin\Model;

use Application\Entity\SandboxAdmin;
use Application\Model\AbstractOrmManager;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;
use Zend\Captcha\Image as ZendcaptchaImage;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\Container as SessionContainer;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplateMapResolver;
use Zend\Crypt\Key\Derivation\Pbkdf2;
use Zend\Math\Rand;

class Admin extends AbstractOrmManager implements ServiceLocatorAwareInterface {

    protected $serviceManager;
    protected $adminEntityInstance;

    public function getServiceLocator() {
        return $this->serviceManager;
    }


    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceManager = $serviceLocator;
    }

    public function getRepository() {
        $repository = $this->getObjectManager($this->serviceManager)->getRepository('Application\Entity\SandboxAdmin');
        return $repository;
    }

    public function getEntityInstance() {
        if (null === $this->adminEntityInstance) {
            $this->adminEntityInstance = new SandboxAdmin();
        }
        return $this->adminEntityInstance;
    }

    public function sendRegistrationSuccessEmail(MeruAdmin $admin) {
        $view = new PhpRenderer();
        $resolver = new TemplateMapResolver();
        $resolver->setMap(array(
            'registerMailTemplate' => MODULE_APPLICATION_PATH
            . '/view/email/templates/register_success.phtml'
        ));
        $view->setResolver($resolver);
        $viewModel = new ViewModel();
        $viewModel->setTemplate('registerMailTemplate')
                ->setVariables(array(
                    'admin' => $admin
        ));
        $content = $view->render($viewModel);
        //Set Html Mail Type
        $text = new MimePart('');
        $text->type = "text/plain";

        $html = new MimePart($content);
        $html->type = "text/html";

        $body = new MimeMessage();
        $body->setParts(array($html));
        //$transport = new SendmailTransport();
        $transport = new SmtpTransport();
        $config = $this->serviceManager->get('config');
        $options = new SmtpOptions($config['smtp_options']);
        $transport->setOptions($options);
        $message = new Message();
        $message->addTo($admin->getAdminEmail())
                ->addFrom($config['sender_email'])
                ->setEncoding('UTF-8');
        $message->setSubject("Welcome to Meru Cloud")
                ->setBody($body);
        $transport->send($message);
    }

    /*
     * Load the admin by adminemail.
     *
     * @param string $adminemail
     * @return Array $result
     */


    public function createAdminSession($admin = array()) {
        $admin['admin_orgid_fk'] = 1;
        $admin['admin_id'] = 2;
        $this->authService = $this->serviceManager->get('AuthService');
        $this->authService->getStorage()->write($admin);
    }



    public function validateLogin($admin) {
        $enc_pwd = $this->decryptPassword($admin);
        $qb = $this->getRepository()->createQueryBuilder("admin");
        $qb->addSelect('admin.adminEmail');
        $qb->where("admin.adminEmail = :adminEmail")
            ->andwhere("admin.adminPwd = :adminPwd")
            ->setParameter("adminEmail",$admin['admin_email'])
            ->setParameter("adminPwd",$enc_pwd);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }
    public function validateLoginOtp($admin) {
       // $enc_pwd = $this->decryptPassword($admin);
        $qb = $this->getRepository()->createQueryBuilder("admin");
        $qb->addSelect('admin.adminEmail');
        $qb->where("admin.adminEmail = :adminEmail")
            ->andwhere("admin.adminOtp = :adminOtp")
            ->setParameter("adminEmail",$admin['admin_email'])
            ->setParameter("adminOtp",$admin['admin_pwd']);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }
    public function selectdata($id) {
        $qb = $this->getRepository()->createQueryBuilder("admin");
        $qb->addSelect('admin.adminId');
        $qb->where("admin.adminId = :adminId")
            ->setParameter("adminId",$id);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }
    public function encryptPassword($pass = "") {
        if (!empty($pass)) {
            $enc_pwd = array();
            $enc_pwd['salt'] = Rand::getString(strlen($pass),'abcdefghijklmnopqrstuvwxyz0123456789', true);
            $enc_pwd['key'] = Pbkdf2::calc('sha512', $pass,$enc_pwd['salt'], 10000, strlen($pass)*2);
            return $enc_pwd;
        }
    }
    public function decryptPassword($admin) {
        if (!empty($admin)) {
            $qb = $this->getRepository()->createQueryBuilder("admin");
            $qb->addSelect('admin.adminEmail');
            $qb->where("admin.adminEmail = :adminEmail")
                ->setParameter("adminEmail",$admin['admin_email']);
            $result = $qb->getQuery()->getArrayResult();
            $salt = $result[0][0][adminPwdsalt];
            $enc_pwd = Pbkdf2::calc('sha512', $admin['admin_pwd'],$salt, 10000, strlen($admin['admin_pwd'])*2);
            return $enc_pwd;
        }
    }

    public function validateEmailUnique($email) {
        $isUnique = "false";
        $qb = $this->getRepository()->createQueryBuilder("admin");
        $qb->addSelect('admin.adminEmail');
        $qb->where("admin.adminEmail = :adminEmail")
            ->setParameter("adminEmail",$email);
        $result = $qb->getQuery()->getArrayResult();

        if (empty($result)) {
            $isUnique = "true";
        } else {
            $isUnique = "false";
        }
        return $isUnique;
    }


    public function loadByAdminEmail($admin) {
        $qb = $this->getRepository()->createQueryBuilder("admin");
        $qb->addSelect('admin.adminEmail');
        $qb->where("admin.adminEmail = :adminEmail")
            ->setParameter("adminEmail",$admin);
        $val = $qb->getQuery()->getArrayResult();
        $result=$val[0][0];

        return $result;
    }

    public function getAdminById($id) {
        $qb = $this->getRepository()->createQueryBuilder("admin");
        $qb->addSelect('org');
        $qb->leftJoin("admin.adminOrgidFk", "org");
        $qb->where("admin.adminId = :adminId")
                ->setParameter("adminId", $id);

        $result = $qb->getQuery()->getSingleResult(AbstractQuery::HYDRATE_ARRAY);
        return $result;
    }

    /*
     * Get admin email by id
     */

    public function getEmailById($id) {
        $qb = $this->getRepository()->createQueryBuilder("admin");
        $qb->addSelect('admin.adminEmail', 'admin.adminName');
        $qb->where("admin.adminId = :adminId")
                ->setParameter("adminId", $id);

        $result = $qb->getQuery()->getSingleResult();
        return $result;
    }

    public function getUidfromtoken($token) {
        $qb = $this->getRepository()->createQueryBuilder("admin");
        $qb->addSelect('admin.adminId');
        $qb->where("admin.adminToken = :adminToken")
            ->setParameter("adminToken", $token);
        $val = $qb->getQuery()->getArrayResult();
        $result = $val[0][0]['adminId'];
        return $result;


    }

    public function saveAdmin(SandboxAdmin &$admin) {
        $om = $this->getObjectManager($this->serviceManager);
        $om->persist($admin);
        $om->flush();
        return $admin->getAdminId();
    }

    public function updateAlarmConfig($admin_id,$time) {
        $qb = $this->getRepository()->createQueryBuilder("admin");
        $query = $qb->update()
            ->set("admin.adminAlarmTimestamp", "?1")
            ->where("admin.adminId = ?2")
            ->setParameter(1, $time)
            ->setParameter(2, $admin_id)
            ->getQuery();
        $query->execute();
        return true;

    }

    public function getAlarmConfig($admin_id) {
        $qb = $this->getRepository()->createQueryBuilder("admin");
        $qb->addSelect('admin.adminAlarmTimestamp');
        $qb->where("admin.adminId = :adminId")
            ->setParameter("adminId", $admin_id);
        $val = $qb->getQuery()->getArrayResult();
        $result = $val[0][0]['adminAlarmTimestamp'];
        return $result;


    }



    public function getAdminsList($filterData, $start, $limit, $count, $sortBy = array()) {
        $qb = $this->getRepository()->createQueryBuilder("admin");
        $selectColumns = array($qb->expr()->count('admin.adminId') . ' AS totalAdmins');
        $qb->where("admin.adminStatus != 2");
        if ($count != 1) {
            $selectColumns = array('admin.adminId', 'admin.adminName', 'admin.adminStatus', 'admin.adminEmail', 'admin.adminPhone', 'org.orgName', 'org.orgId', 'org.orgType');
        }
        $qb->select($selectColumns)
                ->leftJoin("admin.adminOrgidFk", "org");
        if (!empty($filterData['admin_id'])) {
            $qb->AndWhere("admin.adminCreatedbyFk = :createdBy")
                    ->setParameter('createdBy', $filterData['admin_id']);
        }
        if (empty($filterData['globalSearch'])) {
            if (!empty($filterData['org_id'])) {
                $qb->andWhere("admin.adminOrgidFk = :orgId")
                        ->setParameter('orgId', $filterData['org_id']);
            }
        } else {
            if ($filterData['orgType'] != 'csp') {
                $qb->andwhere("admin.adminOrgidFk IN (:orgIds)")
                        ->setParameter("orgIds", $filterData['OrgIds']);
            }
        }
        //role check in case of csp
        if (!empty($filterData['adminRoleCheck'])) {
            if ($filterData['adminRoleCheck'] == 'admin') {
                $qb->andwhere("admin.adminRole = 'admin' OR admin.adminRole = 'superadmin'");
            } else {
                $qb->andwhere("admin.adminRole = '" . $filterData['adminRoleCheck'] . "' OR admin.adminRole = '" . $filterData['adminRoleCheck'] . "-superadmin'");
            }
        }
        if ($filterData['admin_name'] !== "") {
            $qb->andWhere($qb->expr()->like('admin.adminName', $qb->expr()->literal('%' . $filterData['admin_name'] . '%'))
                    . ' OR ' . $qb->expr()->like('admin.adminEmail', $qb->expr()->literal('%' . $filterData['admin_name'] . '%'))
                    . ' OR ' . $qb->expr()->like('admin.adminPhone', $qb->expr()->literal('%' . $filterData['admin_name'] . '%'))
                    . ' OR ' . $qb->expr()->like('admin.adminStatus', $qb->expr()->literal('%' . $filterData['admin_name'] . '%')));
        }
        if (!empty($limit)) {
            $qb->setFirstResult($start);
            $qb->setMaxResults($limit);
        }
        foreach ($sortBy as $column => $sortDir) {
            $qb->orderBy('admin.' . $column, $sortDir);
        }
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }



    public function updateAdmin($data, $where) {
        $qb = $this->getRepository()->createQueryBuilder("admin");
        $qb = $qb->update();
        $count = 1;
        foreach ($data as $key => $value) {
            $qb->set("admin." . $key, "?" . $count)
                    ->setParameter($count, $value);
            $count++;
        }
        $query = $qb->where("admin.adminId = :adminId")
                ->setParameter("adminId", $where['adminId'])
                ->getQuery();
        $query->execute();
    }

    public function updateAdminsByIds($data, $adminIds) {
        $qb = $this->getRepository()->createQueryBuilder("admin");
        $qb = $qb->update()
                ->set("admin.adminStatus", "?1")
                ->set("admin.adminPwd", "?2")
                ->set("admin.adminModifiedbyFk ", "?3")
                ->set("admin.adminModifiedon", "?4")
                ->set("admin.adminEmail ", $qb->expr()->concat($qb->expr()->concat("admin.adminId", $qb->expr()->literal("-")), "admin.adminEmail"))
                ->setParameter(1, $data['adminStatus'])
                ->setParameter(2, $data['adminPwd'])
                ->setParameter(3, $data['adminModifiedbyFk'])
                ->setParameter(4, $data['adminModifiedon']);


        $query = $qb->where("admin.adminId IN (:adminIds)")
                ->setParameter("adminIds", $adminIds)
                ->getQuery();

        $query->execute();
    }

    public function updateLastlogin(MeruAdmin $admin, $adminId) {
        $qb = $this->getRepository()->createQueryBuilder("admin");
        $query = $qb->update()
                ->set("admin.adminLastlogin", "?1")
                ->where("admin.adminId = ?2")
                ->setParameter(1, $admin->getAdminLastlogin())
                ->setParameter(2, $adminId)
                ->getQuery();
        $query->execute();
    }

    /**
     * Generate captcha and return the Captcha Id.
     * This will save the captcha word in session
     */
    public function generateCaptcha() {
        ZendcaptchaImage::$CN = ZendcaptchaImage::$C = ZendcaptchaImage::$VN = ZendcaptchaImage::$V = array("2", "3", "4", "5", "6", "7", "9");
        $captcha = new ZendcaptchaImage();
        $captcha->setWordLen('4')
                ->setHeight('60')
                ->setFont('public/sandboxapp/fonts/ariali.ttf')
                ->setImgDir('public/sandboxapp/img/captcha')
                ->setDotNoiseLevel('5')
                ->setLineNoiseLevel('5');
        $captcha->generate();
        $captchaId = $captcha->getId();
        $container = new SessionContainer('zend_form_captcha' . $captchaId);
        $container->captcha_word = $captcha->getWord();
        return $captchaId;
    }

    /**
     * Validate the Submitted captcha against the captcha word in session
     */
    public function validateCaptcha($captcha) {
        $captchaId = $captcha['id'];
        $captchaInput = $captcha['input'];
        $session = new SessionContainer('zend_form_captcha' . $captchaId);
        $captchaWord = $session->captcha_word;
        if ($captchaWord) {
            if ($captchaInput != $captchaWord) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public function getEmailCount($adminemail, $adminId = 0) {
        $qb = $this->getRepository()->createQueryBuilder("admin");
        $qb->where("admin.adminStatus != 2");
        $qb->select($qb->expr()->count('admin.adminId') . ' AS adminCount')
                ->andWhere("admin.adminEmail = :adminEmail")
                ->setParameter("adminEmail", $adminemail);
        if ($adminId) {
            $qb->andWhere("admin.adminId != :adminId")
                    ->setParameter("adminId", $adminId);
        }
        $result = $qb->getQuery()->getSingleScalarResult();
        return $result;
    }

    public function resetpwd($id, $password = "" , $salt){
        if (isset($id) && isset($password)) {
            $null = "";
            $qb = $this->getRepository()->createQueryBuilder("admin");
            $query = $qb->update()
                ->set("admin.adminPwd", "?1")
                ->set("admin.adminPwdsalt", "?2")
                ->set("admin.adminOtp", "?4")
                ->set("admin.adminOtpstatus", "?4")
                ->set("admin.adminOtpTimestamp", "?4")
                ->where("admin.adminId = ?3")
                ->setParameter(1, $password)
                ->setParameter(2, $salt)
                ->setParameter(3, $id)
                ->setParameter(4, $null)
                ->getQuery();
            $query->execute();
            return true;
        }

    }

    public function resetPassword($email, $password = "", $otp = 0, $currentTS = "") {
        if (isset($email) && isset($password)) {
            $qb = $this->getRepository()->createQueryBuilder("admin");
            if ($otp == 1) {

                $query = $qb->update()
                        ->set("admin.adminOtp", "?1")
                        ->set("admin.adminOtpstatus", "?2")
                        ->set("admin.adminOtpTimestamp", "?3")
                        ->where("admin.adminEmail = ?4")
                        ->setParameter(1, $password)
                        ->setParameter(2, $otp)
                        ->setParameter(3, $currentTS)
                        ->setParameter(4, $email)
                        ->getQuery();
            } else {
                $query = $qb->update()
                        ->set("admin.adminPwd", "?1")
                        ->set("admin.adminOtpstatus", "?2")
                        ->set("admin.adminOtp", "?3")
                        ->where("admin.adminEmail = ?4")
                        ->setParameter(1, $password)
                        ->setParameter(2, $otp)
                        ->setParameter(3, '')
                        ->setParameter(4, $email)
                        ->getQuery();
            }
            $query->execute();
            return true;
        }
        return false;
    }

    public function sendEmail($type = "", $data) {

        $view = new PhpRenderer();
        $resolver = new TemplateMapResolver();
        $templatefile = "";
        switch ($type) {
            case 'registration':
                $templatefile = "register_activate.phtml";
                $subject = "Welcome to Sandbox - Activation Email";
                break;
            case 'passwordreset':
                $templatefile = "password_reset.phtml";
                $subject = "Sandbox Cloud - Reset Password";
                break;
            case 'emptyhtml':
                $templatefile = "empty_html.phtml";
                $subject = $data['subject'];
                break;
            case 'passwordmodified':
                $templatefile = "password_changed.phtml";
                $subject = $data['subject'];
                break;
        }
        if (isset($templatefile) && $data['toEmail']) {
            $resolver->setMap(array(
                'mailTemplate' => MODULE_APPLICATION_PATH
                . '/view/email/templates/' . $templatefile
            ));
            $view->setResolver($resolver);
            $viewModel = new ViewModel();
            $viewModel->setTemplate('mailTemplate')
                    ->setVariables(array(
                        'data' => $data
            ));
            $content = $view->render($viewModel);
            //Set Html Mail Type
            $text = new MimePart('');
            $text->type = "text/plain";

            $html = new MimePart($content);
            $html->type = "text/html";

            $body = new MimeMessage();
            $body->setParts(array($html));
            //$transport = new SendmailTransport();
            $transport = new SmtpTransport();
            $config = $this->serviceManager->get('config');
            $options = new SmtpOptions($config['smtp_options']);
            $transport->setOptions($options);
            $message = new Message();
            $message->addTo($data['toEmail'])
                    ->addFrom($config['sender_email'])
                    ->setEncoding('UTF-8');
            $message->setSubject($subject)
                    ->setBody($body);
            $transport->send($message);
        }
    }

    public function generateActivationKey($data) {
        $ts = time();
        $adminInfo = $data['adminId'] . ":" . $data["adminEmail"] . ":" . $ts;
        $key = base64_encode($adminInfo);
        return $key;
    }

    public function decodeActivationKey($key) {
        $adminInfo = base64_decode($key);
        $info = explode(":", $adminInfo);
        $data['adminId'] = $info["0"];
        $data['adminEmail'] = $info["1"];
        $data['time'] = $info["2"];
        return $data;
    }

    /**
     * Get the count of Active Admins in an organization
     * @param type $orgId
     * @return count of active admins
     */
    public function getActiveAdmincount($orgId) {
        $qb = $this->getRepository()->createQueryBuilder("admin");
        $qb->select($qb->expr()->countDistinct('admin.adminId') . 'as adminCount');
        $qb->Where("admin.adminStatus = 1");
        $qb->andWhere("admin.adminOrgidFk = ?1")
                ->setParameter(1, $orgId);
        $query = $qb->getQuery();
        $result = $query->getScalarResult();
        return $result[0]['adminCount'];
    }

    public function setAndSaveAdmin(&$adminEntity, $data, $adminId, $role = "admin") {
        $objectManager = $this->getObjectManager($this->getServiceLocator());
        $organizationModel = $this->getServiceLocator()->get('Organization');
        $org = $organizationModel->getActiveOrganization();
        $orgId = $org['orgId'];
        $orgProxyInstance = $objectManager->getReference('Application\Entity\MeruOrg', $orgId);
        // change newline to comma address
        $commonModel = $this->getServiceLocator()->get('Common');
        $data->address = $commonModel->changeNl2Comma($data->address);

        $adminEntity->setAdminName($data->name);
        $adminEntity->setAdminPwd($this->encryptPassword($data->password));
        $adminEntity->setAdminEmail($data->email);
        $adminEntity->setAdminAddress($data->address);
        $adminEntity->setAdminOrgidFk($orgProxyInstance);
        $adminEntity->setAdminPhone($data->phone);
        $adminEntity->setAdminOtpstatus($data->otpStatus);
        $adminEntity->setAdminCreatedbyFk($adminId);
        $adminEntity->setAdminStatus(0);
        $adminEntity->setAdminLastlogin(0);
        $adminEntity->setAdminRole($role);
        $this->saveAdmin($adminEntity);
    }

    public function getAdminList($filterData, $start, $limit, $count, $sortBy = array()) {
        $qb = $this->getRepository()->createQueryBuilder("admin");
        $selectColumns = array($qb->expr()->count('admin.adminId') . ' AS totalAdmins');
        $qb->where("admin.adminStatus != 2");
        if ($count != 1) {
            $selectColumns = array('admin.adminId', 'admin.adminName', 'admin.adminStatus', 'admin.adminEmail', 'admin.adminPhone', 'admin.adminRole');
            $qb->leftJoin('admin.meruTenantAdmin', 'tenantmsp');
            $selectColumns[] = $qb->expr()->countDistinct('tenantmsp.tmspTenantid') . 'as tenantCount';
        }
        $qb->select($selectColumns);
        $qb->addSelect('partial admin.{adminId,adminOrgidFk}');
        if (!empty($filterData['orgList'])) {
            $qb->andWhere("admin.adminOrgidFk IN (:orgList)")
                    ->setParameter('orgList', $filterData['orgList']);
        }
        if ($filterData['admin_name'] !== "") {
            $qb->andWhere($qb->expr()->like('admin.adminName', $qb->expr()->literal('%' . $filterData['admin_name'] . '%'))
                    . ' OR ' . $qb->expr()->like('admin.adminEmail', $qb->expr()->literal('%' . $filterData['admin_name'] . '%'))
                    . ' OR ' . $qb->expr()->like('admin.adminPhone', $qb->expr()->literal('%' . $filterData['admin_name'] . '%')));
        }
//        if (isset($filterData['filter_superadmin'])) {
//            $qb->andWhere("admin.adminRole != 'superadmin' ");
//        }

        if ($filterData['sorting_column'] !== "") {
            
        }
        if (!empty($limit)) {
            $qb->setFirstResult($start);
            $qb->setMaxResults($limit);
        }

        foreach ($sortBy as $column => $sortDir) {
            $alias = "admin";
            $qb->orderBy($alias . "." . $column, $sortDir);
        }
        $result = $qb->groupBy('admin.adminId')->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        return $result;
    }

    public function getOrgSuperAdmin($orgId) {
        $qb = $this->getRepository()->createQueryBuilder("admin");
        $qb->select('admin');
        $qb->where("admin.adminOrgidFk = :orgId")
                ->setParameter("orgId", $orgId);
        $qb->andWhere("admin.adminRole = :role")
                ->setParameter("role", "superadmin");
        try {
            $result = $qb->getQuery()->getSingleResult(AbstractQuery::HYDRATE_ARRAY);
        } catch (NoResultException $ex) {
            $result = array();
        }

        return $result;
    }

    public function getOrgAdmins($orgId) {
        $qb = $this->getRepository()->createQueryBuilder("admin");
        $qb->select('admin');
        $qb->where("admin.adminOrgidFk = :orgId")
                ->setParameter("orgId", $orgId);
        try {
            $result = $qb->getQuery()->getResult(AbstractQuery::HYDRATE_ARRAY);
        } catch (NoResultException $ex) {
            $result = array();
        }
        return $result;
    }

    public function getOrgAdminAndSuperAdmin($orgId) {
        $filterData['roles'][] = 'superadmin';
        $filterData['roles'][] = 'admin';
        $qb = $this->getRepository()->createQueryBuilder("admin");
        $qb->select('admin');
        $qb->where("admin.adminOrgidFk = :orgId")
                ->setParameter("orgId", $orgId);
        $qb->andwhere($qb->expr()->in('admin.adminRole', ':role'))
                ->setParameter('role', $filterData['roles']);
        try {
            $result = $qb->getQuery()->getResult(AbstractQuery::HYDRATE_ARRAY);
        } catch (NoResultException $ex) {
            $result = array();
        }
        return $result;
    }

    public function getCSPDetails() {
        $filterData['search'] = 'csp';
        $qb = $this->getRepository()->createQueryBuilder("admin");
        $qb->leftJoin("admin.adminOrgidFk", "org");
        $selectColumns = array('partial admin.{adminId , '
            . 'adminEmail,adminStatus,adminRole}');
        $qb->select($selectColumns);
        $qb->addSelect("partial admin.{ adminId,adminOrgidFk }");
        $qb->addSelect('partial org.{ orgId,orgName,orgType }');
        $qb->where("admin.adminStatus = 1");
        $qb->andWhere($qb->expr()->like('org.orgType', $qb->expr()->literal('%' . $filterData['search'] . '%')));
        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)
                ->getArrayResult();
        return $result;
    }

    public function generateAlphaNumericPassword() {
        $length = 8;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
