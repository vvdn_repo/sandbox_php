<?php
/**
 * Admin Login Form
 * @package Admin
 * @author VVDN Technologies < >
 */

namespace Admin\Form;
use Zend\Form\Form;
class AdminLoginForm extends Form
{
    public function __construct()
    {
        parent::__construct('admin_login');

        $this->add(array(
            'name' => 'admin_email',
            'type' => 'Text',            
            'options' => array(
                'label' => 'Email Address',
            ),
        ));
        $this->add(array(
            'name' => 'admin_pwd',
            'type' => 'Password',
            'options' => array(
                'label' => 'Password',
            ),
        ));
        
        $this->add(array(
            'name' => 'remember_me',
            'type' => 'CheckBox',
            'options' => array(
                'label' => 'Remember Me',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Submit',
                'id' => 'submit_login',
            ),
        ));
    }
}
