<?php

/**
 * Admin Registration Form
 * @package Admin
 * @author VVDN Technologies < >
 */

namespace Admin\Form;

use Zend\Form\Form;

class AdminRegisterForm extends Form {

    public function __construct() {
        parent::__construct('admin_register');

        $this->add(array(
            'name' => 'admin_email',
            'type' => 'Text',
            'options' => array(
                'label' => 'Email Address',
            ),
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Email ID'
            ),
        ));
        $this->add(array(
            'name' => 'admin_name',
            'type' => 'Text',
            'options' => array(
                'label' => 'Full Name',
            ),
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Full Name'
            )
        ));
        $this->add(array(
            'name' => 'admin_pwd',
            'type' => 'Password',
            'options' => array(
                'label' => 'Password',
            ),
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Password'
            )
        ));

        $this->add(array(
            'name' => 'admin_confirmpwd',
            'type' => 'Password',
            'options' => array(
                'label' => 'Confirm Password',
            ),
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Confirm Placeholder'
            )
        ));

        $this->add(array(
            'name' => 'admin_mobile',
            'type' => 'Text',
            'options' => array(
                'label' => 'Mobile Number',
            ),
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Mobile Number'
            )
        ));
        $this->add(array(
            'name' => 'agree_terms',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'label' => 'I agree to the Terms of Service and Privacy Policy',
                'use_hidden_element' => true
            ),
            'attributes' => array(
                'class' => 'form-control'
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Sign Up',
                'id' => 'submit_register'
            ),
        ));
    }

}
