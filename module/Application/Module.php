<?php

/**
 * This class contains getAutoloaderConfig,onBootstrap functions   
 * @package Alarm
 * @author VVDNTechnologies < >
 */

namespace Application;

use Application\Model\Client;
use Application\Model\Common;
use Application\Model\DeviceSignal;
use Application\Model\DeviceThroughput;
use Application\Model\DeviceUpgrade;
use Application\Model\EntityAction;
use Application\Model\DeviceState;
use Application\Model\Eventlog;
use Application\Model\Report;
use Application\Model\ReportGroup;
use Application\Model\ReportType;
use Exception;
use Application\Model\ReportSchedule;
use Application\model\Notification;
use Application\model\MeruNotificationLastSend;
use Application\Model\ReportPdfGenerate;
use Application\Model\ReportJPGraphGenerate;
use Zend\Console\Request as ConsoleRequest;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ConsoleUsageProviderInterface;
use Zend\Mvc\Controller\Plugin\FlashMessenger;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Console\Adapter\AdapterInterface as Console;
use Application\Model\MongoModel;

class Module implements
AutoloaderProviderInterface, ConfigProviderInterface, ConsoleUsageProviderInterface {

    public function onBootstrap(MvcEvent $e) {
        $request = $e->getRequest();
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $this->serviceManager = $e->getApplication()->getServiceManager();
        $this->authService = $this->serviceManager->get('AuthService');
        $userAuth = $this->authService->getStorage()->read();

        /**
         * Attach Flash Messages Listener
         */
        $eventManager->attach(MvcEvent::EVENT_RENDER, function($e) {
            $flashMessenger = new FlashMessenger();
            if ($flashMessenger->hasErrorMessages()) {
                $e->getViewModel()->setVariable('errorMessages', $flashMessenger->getErrorMessages());
            }
            if ($flashMessenger->hasSuccessMessages()) {
                $e->getViewModel()->setVariable('successMessages', $flashMessenger->getSuccessMessages());
            }
        });

        /**
         * Disable Error Page Layout
         */
        $eventManager->attach(MvcEvent::EVENT_DISPATCH_ERROR, function($e) {
            $result = $e->getResult();
            $result->setTerminal(TRUE);
        });


        if (!$request instanceof ConsoleRequest) {

            //Temporary Check for tenant.To disable Customer Navigation Link.
            //@todo Need to rework on this.            
            if (!empty($userAuth)) {
                if ($userAuth['user_id'] > 0) {
                    $viewModel = $e->getApplication()->getMvcEvent()->getViewModel();
                    $viewModel->removeCustomerNav = 1;
                    $isInactive = $this->checkIfUserInActive($e, $userAuth);

                    if ($isInactive == "true") {
                        $this->redirectToLandingPage($e);
                    } else {
                        $isOtpUsed = $this->checkOtpstatus($e, $userAuth);

                        if (!($isOtpUsed == "false")) {

                            $this->redirectToResetPassword($e);
                        } else {
                            try {
                                $path = $_SERVER['HTTP_REFERER'];
                            } catch (Exception $ex) {
                                
                            }
                            if ($userAuth['userLastlogin'] == 0 && strpos($path, "login") !== false) {
                                if (!(($userAuth['userOrgidFk']['orgType'] == 'csp') || ($userAuth['userOrgidFk']['orgType'] == 'msp') || ($userAuth['userOrgidFk']['orgType'] == 'xsp-advance'))) {
                                    $this->redirectToLandingPage($e);
                                }
                            }
                        }
                    }
                }
            } else {
                if ($request->isXmlHttpRequest()) {
                    $eventManager = $e->getApplication()->getEventManager();
                    $eventManager->attach(MvcEvent::EVENT_DISPATCH, function($event) {
                        $routeMatch = $event->getRouteMatch();
                        $action = $routeMatch->getParam('action');
                        if ($action != "generatecaptcha" and $action != "checkuseremail") {
                            $module = new Module();
                            $module->unauthorizedJson($event);
                        }
                    }, 100);
                }
                //Forward Request to Login if User Session does not exist
                else{}
                 //  $this->redirectToLogin($e);
            }

            /**
             * Set Callback on event dispatch
             */
            $eventManager->attach(MvcEvent::EVENT_DISPATCH, function($e) {
                $routeMatch = $e->getRouteMatch();
                $viewModel = $e->getViewModel();
                $viewModel->setVariable('controller', $routeMatch->getParam('controller'));
                $viewModel->setVariable('action', $routeMatch->getParam('action'));
            }, 100);

            $view = $this->serviceManager->get('ViewRenderer');
            $view->headScript()->prependScript('var siteUrl = "' . BASE_PATH . '"');
        }
    }

    public function getConfig() {
        $appModuleConfig = include __DIR__ . '/config/module.config.php';
        $appConfig = include __DIR__ . '/../../config/application.config.php';
        $appModuleConfig = array_merge($appModuleConfig, $appConfig['env_config']);
        return $appModuleConfig;
    }

    public function getAutoloaderConfig() {
        $vendorPath = $this->getConfig();
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                   // 'PDFLibrary' => $vendorPath['vendor_path'] . '/PDFLibrary',
                   // 'GraphLibrary' => $vendorPath['vendor_path'] . '/GraphLibrary',
                ),
            ),
        );
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Common' => function($sm) {
            $model = new Common();
            $model->setServiceLocator($sm);
            return $model;
        },
                'ReportGroup' => function($sm) {
            $model = new ReportGroup();
            $model->setServiceLocator($sm);
            return $model;
        },
                'ReportType' => function($sm) {
            $model = new ReportType();
            $model->setServiceLocator($sm);
            return $model;
        },
                'Report' => function($sm) {
            $model = new Report();
            $model->setServiceLocator($sm);
            return $model;
        },
                'ReportSchedule' => function($sm) {
            $model = new ReportSchedule();
            $model->setServiceLocator($sm);
            return $model;
        },
                'DeviceThroughput' => function($sm) {
            $model = new DeviceThroughput();
            $model->setServiceLocator($sm);
            return $model;
        },
                'DeviceUpgrade' => function($sm) {
            $model = new DeviceUpgrade();
            $model->setServiceLocator($sm);
            return $model;
        },                
                'ReportPdfGenerate' => function($sm) {
            $model = new ReportPdfGenerate();
            $model->setServiceLocator($sm);
            return $model;
        },
                'ReportJPGraphGenerate' => function($sm) {
            $model = new ReportJPGraphGenerate();
            $model->setServiceLocator($sm);
            return $model;
        },
                'DeviceSignal' => function($sm) {
            $model = new DeviceSignal();
            $model->setServiceLocator($sm);
            return $model;
        },
                'DeviceState' => function($sm) {
            $model = new DeviceState();
            $model->setServiceLocator($sm);
            return $model;
        },
                'Client' => function($sm) {
            $model = new Client();
            $model->setServiceLocator($sm);
            return $model;
        },
                'EntityAction' => function($sm) {
            $model = new EntityAction();
            $model->setServiceLocator($sm);
            return $model;
        },
                'Eventlog' => function($sm) {
            $model = new Eventlog();
            $model->setServiceLocator($sm);
            return $model;
        },
                'Feedback' => function($sm) {
            $model = new Model\Feedback();
            $model->setServiceLocator($sm);
            return $model;
        },
                'Notification' => function($sm) {
            $model = new Model\Notification();
            $model->setServiceLocator($sm);
            return $model;
        },
                'MeruNotificationLastSend' => function($sm) {
            $model = new Model\MeruNotificationLastSend();
            $model->setServiceLocator($sm);
            return $model;
        },
                'MongoModel' => function($sm) {
            $model = new Model\MongoModel();
            $model->setServiceLocator($sm);
            return $model;
        },

            ),
        );
    }

    public function getGuestPages() {
        $guestPages = array();
        $guestPages[0]['controller'] = 'Application\Controller\Index';
        $guestPages[0]['action'] = 'index';

        $guestPages[1]['controller'] = 'User\Controller\User';
        $guestPages[1]['action'] = 'register';

        $guestPages[2]['controller'] = 'User\Controller\User';
        $guestPages[2]['action'] = 'forgotpassword';

        $guestPages[3]['controller'] = 'User\Controller\User';
        $guestPages[3]['action'] = 'activateAccount';

        $guestPages[4]['controller'] = 'User\Controller\User';
        $guestPages[4]['action'] = 'termsOfServices';

        $guestPages[5]['controller'] = 'User\Controller\User';
        $guestPages[5]['action'] = 'privacyPolicy';

        $guestPages[6]['controller'] = 'User\Controller\User';
        $guestPages[6]['action'] = 'contactUs';

        $guestPages[7]['controller'] = 'Application\Controller\Index';
        $guestPages[7]['action'] = 'landingPage';

        $guestPages[8]['controller'] = 'Application\Controller\Index';
        $guestPages[8]['action'] = 'main';

        $guestPages[9]['controller'] = 'Application\Controller\Index';
        $guestPages[9]['action'] = 'userhome';

        $guestPages[10]['controller'] = 'User\Controller\User';
        $guestPages[10]['action'] = 'resetpwd';

        $guestPages[11]['controller'] = 'Application\Controller\Index';
        $guestPages[11]['action'] = 'landing';


        return $guestPages;
    }

    public function unauthorizedJson(MvcEvent $event) {
       /* $response = $event->getResponse();
        $response->setStatusCode(403);
        $response->sendHeaders();
        $event->stopPropagation(true);
        echo json_encode(array('success' => false, 'message' => 'Authorization required. Please log in.'));
        die;*/
    }

    private function redirectToLogin(MvcEvent $event) {
        $eventManager = $event->getApplication()->getEventManager();
        $eventManager->attach(MvcEvent::EVENT_DISPATCH, function($event) {
            $routeMatch = $event->getRouteMatch();
            $redirectToLogin = true;
            $controller = $routeMatch->getParam('controller');
            $action = $routeMatch->getParam('action');
            $module = new Module();
            $guestPages = $module->getGuestPages();

            //Do Not redirect to login if request is for Api Module
            if (stripos($controller, "Api\Controller") !== false) {
                $redirectToLogin = false;
            } else {
                foreach ($guestPages as $page) {
                    if ($page['controller'] == $controller and $page['action'] == $action) {
                        $redirectToLogin = false;

                        break;
                    }
                }
            }

            if ($redirectToLogin) {
                $routeMatch->setParam('controller', 'User\Controller\User');
                $routeMatch->setParam('action', 'login');
            }
        }, 100);
    }

    /**
     * Check if userStatus is Inactive in Session,
     * If Yes, Get User details from DB and check if userStatus has changed.
     *      If yes,then remove user's previous session and Create new Session
     * @param MvcEvent $event
     * @param type $userAuth
     * @param MvcEvent $event
     * @return boolean
     */
    private function checkIfUserInActive(MvcEvent $event, $userAuth) {
        $isInactive = "false";
        $this->serviceManager = $event->getApplication()->getServiceManager();
        $config = $this->serviceManager->get('config');

        if ($userAuth['userStatus'] === $config['status_code']['inactive']) {
            $this->serviceManager = $event->getApplication()->getServiceManager();
            $userModel = $this->serviceManager->get('User');
            $user = $userModel->loadByUserEmail($userAuth['userEmail']);

            if ($user['userStatus'] !== $config['status_code']['inactive']) {
                $this->authService = $this->serviceManager->get('AuthService');
                $this->authService->clearIdentity();
                $userModel->createUserSession($user);
            } else {
                $isInactive = "true";
            }
        }

        return $isInactive;
    }

    private function checkOtpStatus(MvcEvent $event, $userAuth) {
        $isOtpUsed = "false";
        if ($userAuth['userOtpstatus'] == 1) {//Check If OTP not used i.e value = 1.
            $this->serviceManager = $event->getApplication()->getServiceManager();
            $userModel = $this->serviceManager->get('User');
            $user = $userModel->loadByUserEmail($userAuth['userEmail']);

            if (!($user['userOtpstatus'] == 0)) {
                $this->authService = $this->serviceManager->get('AuthService');
                $this->authService->clearIdentity();
                $userModel->createUserSession($user);
                $isOtpUsed = "true";
            }
        }

        return $isOtpUsed;
    }

    private function redirectToLandingPage(MvcEvent $event) {
//        Redirect To Landing-page
        $eventManager = $event->getApplication()->getEventManager();
        $eventManager->attach(MvcEvent::EVENT_DISPATCH, function($event) {
            $routeMatch = $event->getRouteMatch();
            $action = $routeMatch->getParam('action');
            if ($action !== "landingPage" and $action !== "activateAccount" and $action !== "logout" and
                    $action !== "contactUs" and $action !== "termsOfServices" and $action !== "privacyPolicy") {
                $url = $event->getRouter()->assemble(array(), array('name' => 'landing-page'));
                $response = $event->getResponse();
                $response->setHeaders($response->getHeaders()->addHeaderLine('Location', $url));
                $response->setStatusCode(302);
                $response->sendHeaders();
                exit();
            }
        }, 100);
    }

    private function redirectToResetPassword(MvcEvent $event) {
        //Redirect To ResetPassword-page
        $eventManager = $event->getApplication()->getEventManager();
        $eventManager->attach(MvcEvent::EVENT_DISPATCH, function($event) {
            $routeMatch = $event->getRouteMatch();
            $action = $routeMatch->getParam('action');

            if ($action !== "resetpassword" and $action !== "logout" and $action !== "activateAccount") {
                $url = $event->getRouter()->assemble(array(), array('name' => 'resetpassword'));
                $response = $event->getResponse();
                $response->setHeaders($response->getHeaders()->addHeaderLine('Location', $url));
                $response->setStatusCode(302);
                $response->sendHeaders();
                exit();
            }
        }, 100);
    }

    public function getConsoleUsage(Console $console) {
        return array(
            // Describe available commands
            'entity-operation' => 'Entity Operation Cron');
    }

}
