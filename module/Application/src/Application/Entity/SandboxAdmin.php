<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SandboxAdmin
 *
 * @ORM\Table(name="sandbox_admin", uniqueConstraints={@ORM\UniqueConstraint(name="admin_email_UNIQUE", columns={"admin_email"})})
 * @ORM\Entity
 */
class SandboxAdmin{

    /**
     * @var integer
     *
     * @ORM\Column(name="admin_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $adminId;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_name", type="string", length=45, nullable=true)
     */
    private $adminName;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_email", type="string", length=100, nullable=true)
     */
    private $adminEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_pwd", type="string", length=100, nullable=true)
     */
    private $adminPwd;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_pwdsalt", type="string", length=500, nullable=true)
     */
    private $adminPwdsalt;

    /**
     * @var integer
     *
     * @ORM\Column(name="admin_createdby_fk", type="integer", nullable=true)
     */
    private $adminCreatedbyFk;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_mobile", type="string", length=20, nullable=true)
     */
    private $adminMobile;


    /**
     * @var integer
     *
     * @ORM\Column(name="admin_status", type="integer", nullable=true)
     */
    private $adminStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="admin_token", type="string",length=100, nullable=true)
     */
    private $adminToken;
    /**
     * @var boolean
     *
     * @ORM\Column(name="admin_otpstatus", type="integer", nullable=true)
     */
    private $adminOtpstatus;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_otp", type="string", length=100, nullable=true)
     */
    private $adminOtp;

    /**
     * @var integer
     *
     * @ORM\Column(name="admin_otptimestamp", type="integer", nullable=true)
     */
    private $adminOtpTimestamp;




    /**
     * Get adminId
     *
     * @return integer 
     */
    public function getAdminId() {
        return $this->adminId;
    }

    /**
     * Set adminName
     *
     * @param string $adminName
     * @return integer
     */
    public function setAdminName($adminName) {
        $this->adminName = $adminName;

        return $this;
    }

    /**
     * Get adminName
     *
     * @return string 
     */
    public function getAdminName() {
        return $this->adminName;
    }

    /**
     * Set adminEmail
     *
     * @param string $adminEmail
     * @return string
     */
    public function setAdminEmail($adminEmail) {
        $this->adminEmail = $adminEmail;

        return $this;
    }

    /**
     * Get adminEmail
     *
     * @return string 
     */
    public function getAdminEmail() {
        return $this->adminEmail;
    }

    /**
     * Set adminPwd
     *
     * @param string $adminPwd
     * @return string
     */
    public function setAdminPwd($adminPwd) {
        $this->adminPwd = $adminPwd;

        return $this;
    }

    /**
     * Get adminPwd
     *
     * @return string 
     */
    public function getAdminPwd() {
        return $this->adminPwd;
    }


    /**
     * Set adminPwdsalt
     *
     * @param string $adminPwdsalt
     * @return string
     */
    public function setAdminPwdsalt($adminPwdsalt) {
        $this->adminPwdsalt = $adminPwdsalt;

        return $this;
    }

    /**
     * Get adminPwdsalt
     *
     * @return string
     */
    public function getAdminPwdsalt() {
        return $this->adminPwdsalt;
    }



    /**
     * Set adminCreatedbyFk
     *
     * @param integer $adminCreatedbyFk
     * @return string
     */
    public function setAdminCreatedbyFk($adminCreatedbyFk) {
        $this->adminCreatedbyFk = $adminCreatedbyFk;

        return $this;
    }

    /**
     * Get adminCreatedbyFk
     *
     * @return integer 
     */
    public function getAdminCreatedbyFk() {
        return $this->adminCreatedbyFk;
    }


    /**
     * Set adminMobile
     *
     * @param string $adminMobile
     * @return string
     */
    public function setAdminMobile($adminMobile) {
        $this->adminMobile = $adminMobile;

        return $this;
    }

    /**
     * Get adminMobile
     *
     * @return string 
     */
    public function getAdminMobile() {
        return $this->adminMobile;
    }

    /**
     * Set adminStatus
     *
     * @param integer $adminStatus
     * @return string
     */
    public function setAdminStatus($adminStatus) {
        $this->adminStatus = $adminStatus;

        return $this;
    }

    /**
     * Get adminStatus
     *
     * @return integer
     */
    public function getAdminStatus() {
        return $this->adminStatus;
    }



     /**
 * Set adminToken
 *
 * @param integer $adminToken
 * @return string
 */
    public function setAdminToken($adminTocken) {
        $this->adminToken = $adminTocken;

        return $this;
    }

    /**
     * Get adminToken
     *
     * @return string
     */
    public function getAdminToken() {
        return $this->adminToken;
    }
    

 /**
     * Set adminOtpstatus
     *
     * @param boolean $adminOtpstatus
     * @return SandboxAdmin
     */
    public function setAdminOtpstatus($adminOtpstatus) {
        $this->adminOtpstatus = $adminOtpstatus;

        return $this;
    }

    /**
     * Get adminOtpstatus
     *
     * @return boolean 
     */
    public function getAdminOtpstatus() {
        return $this->adminOtpstatus;
    }

    /**
     * Set adminOtp
     *
     * @param string $adminOtp
     * @return SandboxAdmin
     */
    public function setAdminOtp($adminOtp) {
        $this->adminOtp = $adminOtp;

        return $this;
    }

    /**
     * Get adminOtp
     *
     * @return string 
     */
    public function getAdminOtp() {
        return $this->adminOtp;
    }

    /**
     * Set adminOtpTimestamp
     *
     * @param integer $adminOtpTimestamp
     * @return SandboxAdmin
     */
    public function setAdminOtpTimestamp($adminOtpTimestamp) {
        $this->adminOtpTimestamp = $adminOtpTimestamp;

        return $this;
    }

    /**
     * Get adminOtpTimestamp
     *
     * @return integer 
     */
    public function getAdminOtpTimestamp() {
        return $this->adminOtpTimestamp;
    }

}
