<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SandboxLoad
 *
 * @ORM\Table(name="sandbox_Load")
 * @ORM\Entity
 */
class SandboxLoad{

    /**
     * @var integer
     *
     * @ORM\Column(name="Load_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $LoadId;

    /**
     * @var string
     *
     * @ORM\Column(name="Load_name", type="string", length=45, nullable=true)
     */
    private $LoadName;

    /**
     * @var string
     *
     * @ORM\Column(name="Load_time", type="string", length=100, nullable=true)
     */
    private $LoadTime;




    /**
     * Get LoadId
     *
     * @return integer 
     */
    public function getLoadId() {
        return $this->LoadId;
    }

    /**
     * Set LoadName
     *
     * @param string $LoadName
     * @return integer
     */
    public function setLoadName($LoadName) {
        $this->LoadName = $LoadName;

        return $this;
    }

    /**
     * Get LoadName
     *
     * @return string 
     */
    public function getLoadName() {
        return $this->LoadName;
    }

    /**
     * Set LoadTime
     *
     * @param string $LoadTime
     * @return string
     */
    public function setLoadTime($LoadTime) {
        $this->LoadTime = $LoadTime;

        return $this;
    }

    /**
     * Get LoadTime
     *
     * @return string 
     */
    public function getLoadTime() {
        return $this->LoadTime;
    }

}
