<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * sandboxDevice
 *
 * @ORM\Table(name="sandbox_device", indexes={@ORM\Index(name="device_createdby", columns={"device_createdby_fk"})})
 * @ORM\Entity
 */
class sandboxDevice {

    /**
     * @var integer
     *
     * @ORM\Column(name="device_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $deviceId;

    /**
     * @var string
     *
     * @ORM\Column(name="device_name", type="string", length=100, nullable=true)
     */
    private $deviceName;


    /**
     * @var string
     *
     * @ORM\Column(name="device_type", type="string",length=100, nullable=true)
     */
    private $deviceType;

    /**
     * @var string
     *
     * @ORM\Column(name="device_mac", type="string", length=45, nullable=true)
     */
    private $deviceMac;


    /**
     * @var integer
     *
     * @ORM\Column(name="device_createdon", type="integer", nullable=true)
     */
    private $deviceCreatedon;

    /**
     * @var integer
     *
     * @ORM\Column(name="device_status", type="integer", nullable=true)
     */
    private $deviceStatus = '1';


    /**
     * @var string
     *
     * @ORM\Column(name="device_slnumber", type="string", length=45, nullable=true)
     */
    private $deviceSlnumber;

    

    /**
     * @var \Application\Entity\SandboxUser
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\SandboxUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="device_createdby_fk", referencedColumnName="user_id", nullable=true)
     * })
     */
    private $deviceCreatedbyFk;

    /**
     * @var string
     *
     * @ORM\Column(name="device_token", type="string", length=45, nullable=true)
     */
    private $deviceToken;


    /**
     * @var string
     *
     * @ORM\Column(name="device_IP", type="string", length=100, nullable=true)
     */
    private $deviceIP;



    /**
     * @var integer
     *
     * @ORM\Column(name="device_default", type="integer", options={"default" = 0})
     */
    private $deviceDefault;

    /**
     * @var string
     *
     * @ORM\Column(name="device_FW", type="string",length=200,nullable=true,options={"default"="2.1.3.423"})
     */
    private $deviceFW;

    /**
     * @var string
     *
     * @ORM\Column(name="device_FWSecondary", type="string",length=200,nullable=true,options={"default"="2.1.3.423"})
     */
    private $deviceFWSecondary;


    /**
     * @var string
     *
     * @ORM\Column(name="device_FW_time", type="string",length=200,nullable=true)
     */
    private $deviceFWTime;
    
    /**
     * @var string
     *
     * @ORM\Column(name="device_autoupdate", type="integer",nullable=true, options={"default" = 1})
     */
    private $deviceAutoUpdate;


    /**
     * Constructor
     */
    public function __construct() {
        $this->sandboxAp = new \Doctrine\Common\Collections\ArrayCollection();
        $this->sandboxDeviceSite = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get deviceId
     *
     * @return integer
     */
    public function getDeviceId() {
        return $this->deviceId;
    }

    /**
     * Set deviceName
     *
     * @param string $deviceName
     * @return sandboxDevice
     */
    public function setDeviceName($deviceName) {
        $this->deviceName = $deviceName;

        return $this;
    }

    /**
     * Get deviceName
     *
     * @return string
     */
    public function getDeviceName() {
        return $this->deviceName;
    }


    /**
     * Set deviceType
     *
     * @param string $deviceType
     * @return sandboxDevice
     */
    public function setDeviceType($deviceType) {
        $this->deviceType = $deviceType;

        return $this;
    }

    /**
     * Get deviceType
     *
     * @return string
     */
    public function getDeviceType() {
        return $this->deviceType;
    }

    /**
     * Set deviceMac
     *
     * @param string $deviceMac
     * @return sandboxDevice
     */
    public function setDeviceMac($deviceMac) {
        $this->deviceMac = $deviceMac;

        return $this;
    }

    /**
     * Get deviceMac
     *
     * @return string
     */
    public function getDeviceMac() {
        return $this->deviceMac;
    }

    /**
     * Set deviceCreatedon
     *
     * @param integer $deviceCreatedon
     * @return sandboxDevice
     */
    public function setDeviceCreatedon($deviceCreatedon) {
        $this->deviceCreatedon = $deviceCreatedon;

        return $this;
    }

    /**
     * Get deviceCreatedon
     *
     * @return integer
     */
    public function getDeviceCreatedon() {
        return $this->deviceCreatedon;
    }


    /**
     * Set deviceStatus
     *
     * @param boolean $deviceStatus
     * @return sandboxDevice
     */
    public function setDeviceStatus($deviceStatus) {
        $this->deviceStatus = $deviceStatus;

        return $this;
    }

    /**
     * Get deviceStatus
     *
     * @return boolean
     */
    public function getDeviceStatus() {
        return $this->deviceStatus;
    }

    /**
     * Set deviceSlnumber
     *
     * @param string $deviceSlnumber
     * @return sandboxDevice
     */
    public function setDeviceSlnumber($deviceSlnumber) {
        $this->deviceSlnumber = $deviceSlnumber;

        return $this;
    }

    /**
     * Get deviceSlnumber
     *
     * @return string
     */
    public function getDeviceSlnumber() {
        return $this->deviceSlnumber;
    }



    /**
     * Set deviceCreatedbyFk
     *
     * @param \Application\Entity\sandboxDevice $deviceCreatedbyFk
     * @return sandboxDevice
     */
    public function setDeviceCreatedbyFk(\Application\Entity\SandboxUser $deviceCreatedbyFk = null) {
        $this->deviceCreatedbyFk = $deviceCreatedbyFk;

        return $this;
    }

    /**
     * Get deviceCreatedbyFk
     *
     * @return \Application\Entity\sandboxDevice
     */
    public function getDeviceCreatedbyFk() {
        return $this->deviceCreatedbyFk;
    }

    /**
     * Set deviceToken
     *
     * @param string $deviceToken
     * @return sandboxDevice
     */
    public function setDeviceToken($deviceToken) {
        $this->deviceToken = $deviceToken;

        return $this;
    }

    /**
     * Get deviceToken
     *
     * @return string
     */
    public function getDeviceToken() {
        return $this->deviceToken;
    }


    /**
     * Set deviceIP
     *
     * @param string $deviceIP
     * @return sandboxDevice
     */
    public function setDeviceIP($deviceIP) {
        $this->deviceIP = $deviceIP;

        return $this;
    }

    /**
     * Get deviceIP
     *
     * @return string
     */
    public function getDeviceIP() {
        return $this->deviceIP;
    }


    /**
     * Set deviceDefault
     *
     * @param integer $deviceDefault
     * @return SandboxDevice
     */
    public function setDeviceDefault($deviceDefault) {
        $this->deviceDefault = $deviceDefault;

        return $this;
    }

    /**
     * Get deviceDefault
     *
     * @return integer
     */
    public function getdeviceDefault() {
        return $this->deviceDefault;
    }


    /**
     * Set deviceFW
     *
     * @param string $deviceFW
     * @return sandboxDevice
     */
    public function setDeviceFW($deviceFW) {
        $this->deviceFW = $deviceFW;

        return $this;
    }

    /**
     * Get deviceFW
     *
     * @return string
     */
    public function getDeviceFW() {
        return $this->deviceFW;
    }

    
    /**
     * Set deviceFWSecondary
     *
     * @param string $deviceFWSecondary
     * @return sandboxDevice
     */
    public function setDeviceFWSecondary($deviceFWSecondary) {
        $this->deviceFWSecondary = $deviceFWSecondary;

        return $this;
    }

    /**
     * Get deviceFWSecondary
     *
     * @return string
     */
    public function getDeviceFWSecondary() {
        return $this->deviceFWSecondary;
    }

    /**
     * Set deviceFWTime
     *
     * @param string $deviceFWTime
     * @return sandboxDevice
     */
    public function setDeviceFWTime($deviceFWTime) {
        $this->deviceFWTime = $deviceFWTime;

        return $this;
    }

    /**
     * Get deviceFWTime
     *
     * @return string
     */
    public function getDeviceFWTime() {
        return $this->deviceFWTime;
    }

     /**
     * Set deviceAutoUpdate
     *
     * @param string $deviceAutoUpdate
     * @return sandboxDevice
     */
    public function setDeviceAutoUpdate($deviceAutoUpdate) {
        $this->deviceAutoUpdate = $deviceAutoUpdate;

        return $this;
    }

    /**
     * Get deviceAutoUpdate
     *
     * @return string
     */
    public function getDeviceAutoUpdate() {
        return $this->deviceAutoUpdate;
    }

}
