<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * sandboxConfig
 *
 * @ORM\Table(name="sandbox_config", indexes={@ORM\Index(name="config_deviceid", columns={"config_deviceid"})})
 * @ORM\Entity
 */
class sandboxConfig {

    /**
     * @var integer
     *
     * @ORM\Column(name="config_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $configId;


    /**
     * @var \Application\Entity\SandboxDevice
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\SandboxDevice")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="config_deviceid", referencedColumnName="device_id", nullable=true)
     * })
     */
    private $configDeviceId;

    /**
     * @var integer
     *
     * @ORM\Column(name="config_createdon", type="integer", nullable=true)
     */
    private $configCreatedon;


    /**
     * @var integer
     *
     * @ORM\Column(name="config_updatedon", type="integer", nullable=true)
     */
    private $configUpdatedon;



    /**
     * @var integer
     *
     * @ORM\Column(name="config_motiondetection", type="integer", nullable=true,options={"default" = 0})
     */
    private $configMotionDetection;

    /**
     * @var integer
     *
     * @ORM\Column(name="config_cuberecording", type="integer", nullable=true,options={"default" = 0})
     */
    private $configCubeRecording;


    /**
     * @var integer
     *
     * @ORM\Column(name="config_cloudrecording", type="integer", nullable=true,options={"default" = 0})
     */
    private $configCloudRecording;

    /**
     * @var integer
     *
     * @ORM\Column(name="config_videoquality", type="integer", nullable=true,options={"default" = 1})
     */
    private $configVideoQuality;

    /**
     * @var integer
     *
     * @ORM\Column(name="config_audioenable", type="integer", nullable=true,options={"default" = 1})
     */
    private $configAudioEnable;


    /**
     * @var integer
     *
     * @ORM\Column(name="config_voicemail", type="integer", nullable=true,options={"default" = 0})
     */
    private $configVoiceMail;


    /**
     * @var integer
     *
     * @ORM\Column(name="config_lower_threshold", type="integer", nullable=true,options={"default" = 0})
     */
    private $configLowerThreshold;


    /**
     * @var integer
     *
     * @ORM\Column(name="config_upper_threshold", type="integer", nullable=true,options={"default" = 0})
     */
    private $configUpperThreshold;


    /**
     * @var integer
     *
     * @ORM\Column(name="config_lower_threshold_alert", type="integer", nullable=true,options={"default" = 1})
     */
    private $configLowerThresholdAlert;

    /**
     * @var integer
     *
     * @ORM\Column(name="config_upper_threshold_alert", type="integer", nullable=true,options={"default" = 1})
     */
    private $configUpperThresholdAlert;

    /**
     * @var integer
     *
     * @ORM\Column(name="config_motionblock", type="string",length=20, nullable=true,options={"default" = "FFF"})
     */
    private $configMotionBlock;


    /**
     * @var integer
     *
     * @ORM\Column(name="config_autoupgrade", type="integer", nullable=true,options={"default" = 1})
     */
    private $configAutoUpgrade;


    /**
     * Constructor
     */
    public function __construct() {
        $this->sandboxAp = new \Doctrine\Common\Collections\ArrayCollection();
        $this->sandboxConfigSite = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get configId
     *
     * @return integer
     */
    public function getConfigId() {
        return $this->configId;
    }

    /**
     * Set configDeviceId
     *
     * @param \Application\Entity\sandboxConfig $configDeviceId
     * @return sandboxConfig
     */
    public function setConfigDeviceId(\Application\Entity\SandboxDevice $configDeviceId = null) {
        $this->configDeviceId = $configDeviceId;

        return $this;
    }

    /**
     * Get configDeviceId
     *
     * @return \Application\Entity\sandboxConfig
     */
    public function getConfigDeviceId() {
        return $this->configDeviceId;
    }


    /**
     * Set configmotiondetection
     *
     * @param string $configMotionDetection
     * @return sandboxConfig
     */
    public function setConfigMotionDetection($configMotionDetection) {
        $this->configMotionDetection = $configMotionDetection;

        return $this;
    }

    /**
     * Get configmotiondetection
     *
     * @return string
     */
    public function getConfigMotionDetection() {
        return $this->configMotionDetection;
    }


    /**
     * Set configcuberecording
     *
     * @param string $configCubeRecording
     * @return sandboxConfig
     */
    public function setConfigCubeRecording($configCubeRecording) {
        $this->configCubeRecording = $configCubeRecording;

        return $this;
    }

    /**
     * Get configcuberecording
     *
     * @return string
     */
    public function getConfigCubeRecording() {
        return $this->configCubeRecording;
    }


    /**
     * Set configcloudrecording
     *
     * @param string $configCloudRecording
     * @return sandboxConfig
     */
    public function setConfigCloudRecording($configCloudRecording) {
        $this->configCloudRecording = $configCloudRecording;

        return $this;
    }

    /**
     * Get configcloudrecording
     *
     * @return string
     */
    public function getConfigCloudRecording() {
        return $this->configCloudRecording;
    }

    /**
     * Set configvideoquality
     *
     * @param string $configVideoQuality
     * @return sandboxConfig
     */
    public function setConfigVideoQuality($configVideoQuality) {
        $this->configVideoQuality = $configVideoQuality;

        return $this;
    }

    /**
     * Get configvideoquality
     *
     * @return string
     */
    public function getConfigVideoQuality() {
        return $this->configVideoQuality;
    }

    /**
     * Set configaudioenable
     *
     * @param string $configAudioEnable
     * @return sandboxConfig
     */
    public function setConfigAudioEnable($configAudioEnable) {
        $this->configAudioEnable = $configAudioEnable;

        return $this;
    }

    /**
     * Get configaudioenable
     *
     * @return string
     */
    public function getConfigAudioEnable() {
        return $this->configAudioEnable;
    }


    /**
     * Set configlowerthreshold
     *
     * @param string $configLowerThreshold
     * @return sandboxConfig
     */
    public function setConfigLowerThreshold($configLowerThreshold) {
        $this->configLowerThreshold = $configLowerThreshold;

        return $this;
    }

    /**
     * Get configlowerthreshold
     *
     * @return string
     */
    public function getConfigLowerThreshold() {
        return $this->configLowerThreshold;
    }


    /**
     * Set configUpperthreshold
     *
     * @param string $configupperThreshold
     * @return sandboxConfig
     */
    public function setConfigUpperThreshold($configUpperThreshold) {
        $this->configUpperThreshold = $configUpperThreshold;

        return $this;
    }

    /**
     * Get configupperthreshold
     *
     * @return string
     */
    public function getConfigUpperThreshold() {
        return $this->configUpperThreshold;
    }


    /**
     * Set configlowerthresholdAlert
     *
     * @param string $configLowerThresholdAlert
     * @return sandboxConfig
     */
    public function setConfigLowerThresholdAlert($configLowerThresholdAlert) {
        $this->configLowerThresholdAlert = $configLowerThresholdAlert;

        return $this;
    }

    /**
     * Get configlowerthresholdAlert
     *
     * @return string
     */
    public function getConfigLowerThresholdAlert() {
        return $this->configLowerThresholdAlert;
    }


    /**
     * Set configUpperthresholdAlert
     *
     * @param string $configUpperThresholdAlert
     * @return sandboxConfig
     */
    public function setConfigUpperThresholdAlert($configUpperThresholdAlert) {
        $this->configUpperThresholdAlert = $configUpperThresholdAlert;

        return $this;
    }

    /**
     * Get configupperthresholdAlert
     *
     * @return string
     */
    public function getConfigUpperThresholdAlert() {
        return $this->configUpperThresholdAlert;
    }


    /**
     * Set configMotionBlock
     *
     * @param integer $configMotionBlock
     * @return sandboxConfig
     */
    public function setConfigMotionBlock($configMotionBlock) {
        $this->configMotionBlock = $configMotionBlock;

        return $this;
    }

    /**
     * Get configMotionBlock
     *
     * @return integer
     */
    public function getConfigMotionBlock() {
        return $this->configMotionBlock;
    }


    /**
     * Set configVoiceMail
     *
     * @param integer $configVoiceMail
     * @return sandboxConfig
     */
    public function setConfigVoiceMail($configVoiceMail) {
        $this->configVoiceMail = $configVoiceMail;

        return $this;
    }

    /**
     * Get configVoiceMail
     *
     * @return integer
     */
    public function getConfigVoiceMail() {
        return $this->configVoiceMail;
    }



    /**
     * Set configCreatedon
     *
     * @param integer $configCreatedon
     * @return sandboxConfig
     */
    public function setConfigCreatedon($configCreatedon) {
        $this->configCreatedon = $configCreatedon;

        return $this;
    }

    /**
     * Get configCreatedon
     *
     * @return integer
     */
    public function getConfigCreatedon() {
        return $this->configCreatedon;
    }

    /**
     * Set configUpdatedon
     *
     * @param integer $configUpdatedon
     * @return sandboxConfig
     */
    public function setConfigUpdatedon($configUpdatedon) {
        $this->configCreatedon = $configUpdatedon;

        return $this;
    }

    /**
     * Get configUpdatedon
     *
     * @return integer
     */
    public function getConfigUpdatedon() {
        return $this->configUpdatedon;
    }

     /**
     * Set configAutoUpdate
     *
     * @param integer $configUpdatedon
     * @return sandboxConfig
     */
    public function setConfigAutoUpdate($configAutoUpgrade) {
        $this->configAutoUpgrade = $configAutoUpgrade;

        return $this;
    }

    /**
     * Get configAutoUpdate
     *
     * @return integer
     */
    public function getConfigAutoUpdate() {
        return $this->configAutoUpgrade;
    }


}

