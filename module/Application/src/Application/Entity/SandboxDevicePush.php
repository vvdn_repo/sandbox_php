<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * sandboxDevicePush
 *
 * @ORM\Table(name="sandbox_devicepush")
 * @ORM\Entity
 */
class sandboxDevicePush {

    /**
     * @var integer
     *
     * @ORM\Column(name="devicepush_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $devicepushId;


    /**
     * @var \Application\Entity\SandboxDevice
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\SandboxDevice")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="device_IdFK", referencedColumnName="device_id", nullable=true)
     * })
     */

    private $deviceIdFK;



    /**
     * @var string
     *
     * @ORM\Column(name="devicepush_timeStamp", type="string",length=30, nullable=true)
     */
    private $devicepushTimeStamp;

    /**
     * @var string
     *
     * @ORM\Column(name="devicepush_comand", type="string",length=30, nullable=true)
     */
    private $devicepushCommand;


    /**
     * @var string
     *
     * @ORM\Column(name="devicepush_status", type="string",length=30, nullable=true)
     */
    private $devicepushStatus;

   

    /**
     * @var \Application\Entity\SandboxUser
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\SandboxUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_IdFK", referencedColumnName="user_id", nullable=true)
     * })
     */
    private $userIdFk;


    /**
     * @var string
     *
     * @ORM\Column(name="stopalarm_status", type="integer", nullable=false)
     */
    private $stopAlarmStatus;

     /**
     * @var string
     *
     * @ORM\Column(name="devicepush_data", type="string",length=300, nullable=true)
     */
    private $devicepushData;

    /**
     * Constructor
     */
    public function __construct() {
        $this->sandboxAp = new \Doctrine\Common\Collections\ArrayCollection();
        $this->sandboxCommandSite = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get Id
     *
     * @return integer
     */
    public function getDevicePushId() {
        return $this->devicepushId;
    }

    /**
     * Set deviceIdFK
     *
     * @param string $deviceIdFK
     * @return sandboxCommand
     */
    public function setDeviceIdFK($deviceIdFK) {
        $this->deviceIdFK = $deviceIdFK;

        return $this;
    }

    /**
     * Get deviceIdFK
     *
     * @return string
     */
    public function getDeviceIdFK() {
        return $this->deviceIdFK;
    }


    /**
     * Set timeStamp
     *
     * @param string $timeStamp
     * @return sandboxCommand
     */
    public function setTimeStamp($timeStamp) {
        $this->devicepushTimeStamp = $timeStamp;

        return $this;
    }

    /**
     * Get timeStamp
     *
     * @return string
     */
    public function getTimeStamp() {
        return $this->devicepushTimeStamp;
    }

    /**
     * Set devicepushCommand
     *
     * @param string $devicepushCommand
     * @return sandboxCommand
     */
    public function setDevicePushCommand($devicepushCommand) {
        $this->devicepushCommand = $devicepushCommand;

        return $this;
    }

    /**
     * Get $devicepushCommand
     *
     * @return string
     */
    public function getDevicePushCommand() {
        return $this->devicepushCommand;
    }

    /**
     * Set devicepushStatus
     *
     * @param string $devicepushStatus
     * @return sandboxCommand
     */
    public function setDevicePushStatus($devicepushStatus) {
        $this->devicepushStatus = $devicepushStatus;

        return $this;
    }

    /**
     * Get $devicepushStatus
     *
     * @return string
     */
    public function getDevicePushStatus() {
        return $this->devicepushStatus;
    }

    /**
     * Set devicepushUserIdFk
     *
     * @param string $devicepushUserIdFk
     * @return sandboxCommand
     */
    public function setDevicePushUserIdFk($devicepushUserIdFk) {
        $this->userIdFk = $devicepushUserIdFk;

        return $this;
    }

    /**
     * Get devicepushUserIdFk
     *
     * @return string
     */
    public function getDevicePushUserIdFk() {
        return $this->userIdFk;
    }


    /**
     * Set stopAlarmStatus
     *
     * @param string $stopAlarmStatus
     * @return sandboxCommand
     */
    public function setStopAlarmStatus($stopAlarmStatus) {
        $this->stopAlarmStatus = $stopAlarmStatus;

        return $this;
    }

    /**
     * Get stopAlarmStatus
     *
     * @return string
     */
    public function getStopAlarmStatus() {
        return $this->stopAlarmStatus;
    }


    /**
     * Set devicepushData
     *
     * @param string $devicepushData
     * @return sandboxCommand
     */
    public function setDevicePushData($devicepushData) {
        $this->devicepushData = $devicepushData;

        return $this;
    }

    /**
     * Get devicepushData
     *
     * @return string
     */
    public function getDevicePushData() {
        return $this->devicepushData;
    }

}

