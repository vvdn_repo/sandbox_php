<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SandboxUser
 *
 * @ORM\Table(name="sandbox_user", uniqueConstraints={@ORM\UniqueConstraint(name="user_email_UNIQUE", columns={"user_email"})})
 * @ORM\Entity
 */
class SandboxUser{

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="user_name", type="string", length=45, nullable=true)
     */
    private $userName;

    /**
     * @var string
     *
     * @ORM\Column(name="user_email", type="string", length=100, nullable=true)
     */
    private $userEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="user_pwd", type="string", length=100, nullable=true)
     */
    private $userPwd;

    /**
     * @var string
     *
     * @ORM\Column(name="user_pwdsalt", type="string", length=500, nullable=true)
     */
    private $userPwdsalt;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_createdby_fk", type="integer", nullable=true)
     */
    private $userCreatedbyFk;

    /**
     * @var string
     *
     * @ORM\Column(name="user_mobile", type="string", length=20, nullable=true)
     */
    private $userMobile;


    /**
     * @var integer
     *
     * @ORM\Column(name="user_status", type="integer", nullable=true)
     */
    private $userStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_token", type="string",length=100, nullable=true)
     */
    private $userToken;
    /**
     * @var boolean
     *
     * @ORM\Column(name="user_otpstatus", type="integer", nullable=true)
     */
    private $userOtpstatus;

    /**
     * @var string
     *
     * @ORM\Column(name="user_otp", type="string", length=100, nullable=true)
     */
    private $userOtp;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_otptimestamp", type="integer", nullable=true)
     */
    private $userOtpTimestamp;


    /**
     * @var integer
     *
     * @ORM\Column(name="user_alarmtimestamp", type="integer", nullable=true)
     */
    private $userAlarmTimestamp;


    /**
     * @var integer
     *
     * @ORM\Column(name="user_awsstatus", type="integer", options={"default" = 0})
     */
    private $userAwsStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_awsbucket", type="string", length=100, nullable=true)
     */
    private $userAwsBucket;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_awsaccesskey", type="string", length=100, nullable=true)
     */
    private $userAwsAccessKey;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_awssecretkey", type="string", length=100, nullable=true)
     */
    private $userAwsSecretKey;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_mode", type="integer", options={"default" = 1})
     */
    private $userMode;



    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId() {
        return $this->userId;
    }

    /**
     * Set userName
     *
     * @param string $userName
     * @return integer
     */
    public function setUserName($userName) {
        $this->userName = $userName;

        return $this;
    }

    /**
     * Get userName
     *
     * @return string 
     */
    public function getUserName() {
        return $this->userName;
    }

    /**
     * Set userEmail
     *
     * @param string $userEmail
     * @return string
     */
    public function setUserEmail($userEmail) {
        $this->userEmail = $userEmail;

        return $this;
    }

    /**
     * Get userEmail
     *
     * @return string 
     */
    public function getUserEmail() {
        return $this->userEmail;
    }

    /**
     * Set userPwd
     *
     * @param string $userPwd
     * @return string
     */
    public function setUserPwd($userPwd) {
        $this->userPwd = $userPwd;

        return $this;
    }

    /**
     * Get userPwd
     *
     * @return string 
     */
    public function getUserPwd() {
        return $this->userPwd;
    }


    /**
     * Set userPwdsalt
     *
     * @param string $userPwdsalt
     * @return string
     */
    public function setUserPwdsalt($userPwdsalt) {
        $this->userPwdsalt = $userPwdsalt;

        return $this;
    }

    /**
     * Get userPwdsalt
     *
     * @return string
     */
    public function getUserPwdsalt() {
        return $this->userPwdsalt;
    }



    /**
     * Set userCreatedbyFk
     *
     * @param integer $userCreatedbyFk
     * @return string
     */
    public function setUserCreatedbyFk($userCreatedbyFk) {
        $this->userCreatedbyFk = $userCreatedbyFk;

        return $this;
    }

    /**
     * Get userCreatedbyFk
     *
     * @return integer 
     */
    public function getUserCreatedbyFk() {
        return $this->userCreatedbyFk;
    }


    /**
     * Set userMobile
     *
     * @param string $userMobile
     * @return string
     */
    public function setUserMobile($userMobile) {
        $this->userMobile = $userMobile;

        return $this;
    }

    /**
     * Get userMobile
     *
     * @return string 
     */
    public function getUserMobile() {
        return $this->userMobile;
    }

    /**
     * Set userStatus
     *
     * @param integer $userStatus
     * @return string
     */
    public function setUserStatus($userStatus) {
        $this->userStatus = $userStatus;

        return $this;
    }

    /**
     * Get userStatus
     *
     * @return integer
     */
    public function getUserStatus() {
        return $this->userStatus;
    }



     /**
 * Set userToken
 *
 * @param integer $userToken
 * @return string
 */
    public function setUserToken($userTocken) {
        $this->userToken = $userTocken;

        return $this;
    }

    /**
     * Get userToken
     *
     * @return string
     */
    public function getUserToken() {
        return $this->userToken;
    }
    

 /**
     * Set userOtpstatus
     *
     * @param boolean $userOtpstatus
     * @return SandboxUser
     */
    public function setUserOtpstatus($userOtpstatus) {
        $this->userOtpstatus = $userOtpstatus;

        return $this;
    }

    /**
     * Get userOtpstatus
     *
     * @return boolean 
     */
    public function getUserOtpstatus() {
        return $this->userOtpstatus;
    }

    /**
     * Set userOtp
     *
     * @param string $userOtp
     * @return SandboxUser
     */
    public function setUserOtp($userOtp) {
        $this->userOtp = $userOtp;

        return $this;
    }

    /**
     * Get userOtp
     *
     * @return string 
     */
    public function getUserOtp() {
        return $this->userOtp;
    }

    /**
     * Set userOtpTimestamp
     *
     * @param integer $userOtpTimestamp
     * @return SandboxUser
     */
    public function setUserOtpTimestamp($userOtpTimestamp) {
        $this->userOtpTimestamp = $userOtpTimestamp;

        return $this;
    }

    /**
     * Get userOtpTimestamp
     *
     * @return integer 
     */
    public function getUserOtpTimestamp() {
        return $this->userOtpTimestamp;
    }

    /**
     * Set userAlarmTimestamp
     *
     * @param integer $userAlarmTimestamp
     * @return SandboxUser
     */
    public function setUserAlarmTimestamp($userAlarmTimestamp) {
        $this->userAlarmTimestamp = $userAlarmTimestamp;

        return $this;
    }

    /**
     * Get userAlarmTimestamp
     *
     * @return integer
     */
    public function getUserAlarmTimestamp() {
        return $this->userAlarmTimestamp;
    }

    /**
     * Set userAwsStatus
     *
     * @param integer $userAwsStatus
     * @return SandboxUser
     */
    public function setUserAwsStatus($userAwsStatus) {
        $this->userAwsStatus = $userAwsStatus;

        return $this;
    }

    /**
     * Get userAwsStatus
     *
     * @return integer
     */
    public function getUserAwsStatus() {
        return $this->userAwsStatus;
    }

    /**
     * Set userAwsBucket
     *
     * @param integer $userAwsBucket
     * @return SandboxUser
     */
    public function setUserAwsBucket($userAwsBucket) {
        $this->userAwsBucket = $userAwsBucket;

        return $this;
    }

    /**
     * Get userAwsBucket
     *
     * @return integer
     */
    public function getUserAwsBucket() {
        return $this->userAwsBucket;
    }


    /**
     * Set userAwsAccessKey
     *
     * @param integer $userAwsAccessKey
     * @return SandboxUser
     */
    public function setUserAwsAccessKey($userAwsAccessKey) {
        $this->userAwsAccessKey = $userAwsAccessKey;

        return $this;
    }

    /**
     * Get userAwsAccessKey
     *
     * @return integer
     */
    public function getUserAwsAccessKey() {
        return $this->userAwsAccessKey;
    }


    /**
     * Set userAwsSecretKey
     *
     * @param integer $userAwsSecretKey
     * @return SandboxUser
     */
    public function setUserAwsSecretKey($userAwsSecretKey) {
        $this->userAwsSecretKey = $userAwsSecretKey;

        return $this;
    }

    /**
     * Get userAwsSecretKey
     *
     * @return integer
     */
    public function getUserAwsSecretKey() {
        return $this->userAwsSecretKey;
    }

    /**
     * Set userMode
     *
     * @param integer $userMode
     * @return SandboxUser
     */
   public function setUserMode($userMode) {
        $this->userMode = $userMode;

        return $this;
    }

    /**
     * Get userMode
     *
     * @return integer
     */
    public function getUserMode() {
        return $this->userMode;
    }

}
