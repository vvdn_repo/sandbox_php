<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * sandboxAlarm
 *
 * @ORM\Table(name="sandbox_alarm")
 * @ORM\Entity
 */
class sandboxAlarm {

    /**
     * @var integer
     *
     * @ORM\Column(name="alarm_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $alarmId;


    /**
     * @var string
     *
     * @ORM\Column(name="device_IdFK", type="text", nullable=true)
     */


    private $deviceIdFK;



    /**
     * @var string
     *
     * @ORM\Column(name="alarm_type", type="text", nullable=true)
     */
    private $alarmType;

     /**
     * @var string
     *
     * @ORM\Column(name="alarm_typeName", type="text", nullable=true)
     */
    private $alarmTypeName;

    /**
     * @var string
     *
     * @ORM\Column(name="alarm_timeStamp", type="string",length=30, nullable=true)
     */
    private $alarmTimeStamp;

    /**
     * @var string
     *
     * @ORM\Column(name="alarm_filename", type="string",length=30, nullable=true)
     */
    private $alarmFileName;

    /**
     * @var string
     *
     * @ORM\Column(name="alarm_thumbnail",  type="text", nullable=true)
     */
    private $alarmThumbnail;

    /**
     * @var string
     *
     * @ORM\Column(name="alarm_readStatus",  type="integer", nullable=true,options={"default" = 0})
     */
    private $alarmReadStatus;



    /**
     * Constructor
     */
    public function __construct() {
        $this->sandboxAp = new \Doctrine\Common\Collections\ArrayCollection();
        $this->sandboxCommandSite = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get Id
     *
     * @return integer
     */
    public function getalarmId() {
        return $this->alarmId;
    }

    /**
     * Set deviceIdFK
     *
     * @param string $deviceIdFK
     * @return sandboxCommand
     */
    public function setDeviceIdFK($deviceIdFK) {
        $this->deviceIdFK = $deviceIdFK;

        return $this;
    }

    /**
     * Get deviceIdFK
     *
     * @return string
     */
    public function getDeviceIdFK() {
        return $this->deviceIdFK;
    }


    /**
     * Set alarmType
     *
     * @param string $alarmtype
     * @return sandboxCommand
     */
    public function setAlarmType($alarmtype) {
        $this->alarmType = $alarmtype;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getAlarmType() {
        return $this->alarmType;
    }


    /**
     * Set alarmTypeName
     *
     * @param string $alarmtypeNamee
     * @return sandboxCommand
     */
    public function setAlarmTypeName($alarmtypename) {
        $this->alarmTypeName = $alarmtypename;

        return $this;
    }

    /**
     * Get alarmTypeName
     *
     * @return string
     */
    public function getAlarmTypeName() {
        return $this->alarmTypeName;
    }

    /**
     * Set fileName
     *
     * @param string $fileName
     * @return sandboxCommand
     */
    public function setFileName($fileName) {
        $this->alarmFileName = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return string
     */
    public function getFileName() {
        return $this->alarmFileName;
    }

    /**
     * Set thumbnail
     *
     * @param string $thumbnail
     * @return sandboxCommand
     */
    public function setThumbnail($thumbnail) {
        $this->alarmThumbnail = $thumbnail;

        return $this;
    }

    /**
     * Get thumbnail
     *
     * @return string
     */
    public function getThumbnail() {
        return $this->alarmThumbnail;
    }


    /**
     * Set timeStamp
     *
     * @param string $timeStamp
     * @return sandboxCommand
     */
    public function setTimeStamp($timeStamp) {
        $this->alarmTimeStamp = $timeStamp;

        return $this;
    }

    /**
     * Get timeStamp
     *
     * @return string
     */
    public function getTimeStamp() {
        return $this->alarmTimeStamp;
    }

    
     /**
     * Set alarmReadStatus
     *
     * @param string $alarmReadStatus
     * @return sandboxAlarm
     */
    public function setReadStatus($alarmReadStatus) {
        $this->alarmReadStatus = $alarmReadStatus;
        return $this;
    }

    /**
     * Get alarmReadStatus
     *
     * @return string
     */
    public function getReadStatus() {
        return $this->alarmReadStatus;
    }

}

