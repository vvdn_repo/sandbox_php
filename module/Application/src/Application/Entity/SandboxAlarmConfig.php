<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * sandboxAlarmConfig
 *
 * @ORM\Table(name="sandbox_alarmConfig",indexes={@ORM\Index(name="alarmConfig_user", columns={"alarmConfig_user"})})
 * @ORM\Entity
 */
class sandboxAlarmConfig {

    /**
     * @var integer
     *
     * @ORM\Column(name="alarmConfig_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $alarmConfigId;

    /**
     * @var \Application\Entity\SandboxUser
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\SandboxUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="alarmConfig_user", referencedColumnName="user_id", nullable=true)
     * })
     */
    private $alarmConfigUser;


    /**
     * @var string
     *
     * @ORM\Column(name="alarmConfig_timestamp", type="string",length=100, nullable=true)
     */
    private $alarmConfigTimestamp;

    /**
     * Constructor
     */
    public function __construct() {
        $this->sandboxAp = new \Doctrine\Common\Collections\ArrayCollection();
        $this->sandboxAlarmConfigSite = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get alarmConfigId
     *
     * @return integer
     */
    public function getAlarmConfigId() {
        return $this->alarmConfigId;
    }

    /**
     * Set alarmConfigUser
     *
     * @param \Application\Entity\sandboxAlarmConfig $alarmConfigUser
     * @return sandboxAlarmConfig
     */
    public function setAlarmConfigUser(\Application\Entity\SandboxUser $alarmConfigUser = null) {
        $this->alarmConfigUser = $alarmConfigUser;

        return $this;
    }

    /**
     * Get alarmConfigUser
     *
     * @return \Application\Entity\sandboxAlarmConfig
     */
    public function getAlarmConfigUser() {
        return $this->alarmConfigUser;
    }


    /**
     * Set alarmConfigTimestamp
     *
     * @param string $alarmConfigTimestamp
     * @return sandboxAlarmConfig
     */
    public function setAlarmConfigTimestamp($alarmConfigTimestamp) {
        $this->alarmConfigTimestamp = $alarmConfigTimestamp;

        return $this;
    }

    /**
     * Get alarmConfigTimestamp
     *
     * @return string
     */
    public function getAlarmConfigTimestamp() {
        return $this->alarmConfigTimestamp;
    }


}

