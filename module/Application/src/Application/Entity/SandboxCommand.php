<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * sandboxCommand
 *
 * @ORM\Table(name="sandbox_command")
 * @ORM\Entity
 */
class sandboxCommand {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $Id;

     /**
     * @var integer
     *
     * @ORM\Column(name="command_id", type="integer", nullable=false)
     */
    private $commandId;

    /**
     * @var string
     *
     * @ORM\Column(name="user_token", type="string", length=30, nullable=true)
     */
    private $userToken;


   /**
     * @var \Application\Entity\SandboxDevice
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\SandboxDevice")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="device_IdFK", referencedColumnName="device_id", nullable=true)
     * })
     */
     
    private $deviceIdFK;


    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string",length=30, nullable=true)
     */
    private $status;


    /**
     * @var string
     *
     * @ORM\Column(name="response", type="text", nullable=true)
     */
    private $response;

    /**
     * @var string
     *
     * @ORM\Column(name="timeStamp", type="string",length=30, nullable=true)
     */
    private $timeStamp;



    /**
     * Constructor
     */
    public function __construct() {
        $this->sandboxAp = new \Doctrine\Common\Collections\ArrayCollection();
        $this->sandboxCommandSite = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get Id
     *
     * @return integer
     */
    public function getId() {
        return $this->Id;
    }


    /**
     * Set commandId
     *
     * @param string $commandId
     * @return sandboxCommand
     */
    public function setCommandId($commandId) {
        $this->commandId = $commandId;

        return $this;
    }

    /**
     * Get commandId
     *
     * @return string
     */
    public function getCommandId() {
        return $this->commandId;
    }

    /**
     * Set userToken
     *
     * @param string $userToken
     * @return sandboxCommand
     */
    public function setUserToken($userToken) {
        $this->userToken = $userToken;

        return $this;
    }

    /**
     * Get userToken
     *
     * @return string
     */
    public function getUserToken() {
        return $this->userToken;
    }

 /**
     * Set deviceIdFK
     *
     * @param string $deviceIdFK
     * @return sandboxCommand
     */
    public function setDeviceIdFK($deviceIdFK) {
        $this->deviceIdFK = $deviceIdFK;

        return $this;
    }

    /**
     * Get deviceIdFK
     *
     * @return string
     */
    public function getDeviceIdFK() {
        return $this->deviceIdFK;
    }


    /**
     * Set status
     *
     * @param string $status
     * @return sandboxCommand
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus() {
        return $this->status;
    }


    /**
     * Set response
     *
     * @param string $response
     * @return sandboxCommand
     */
    public function setResponse($response) {
        $this->response = $response;

        return $this;
    }

    /**
     * Get response
     *
     * @return string
     */
    public function getResponse() {
        return $this->response;
    }


    /**
     * Set timeStamp
     *
     * @param string $timeStamp
     * @return sandboxCommand
     */
    public function setTimeStamp($timeStamp) {
        $this->timeStamp = $timeStamp;

        return $this;
    }

    /**
     * Get timeStamp
     *
     * @return string
     */
    public function getTimeStamp() {
        return $this->response;
    }

    

}
