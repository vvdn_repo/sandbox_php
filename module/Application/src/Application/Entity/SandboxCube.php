<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * sandboxCube
 *
 * @ORM\Table(name="sandbox_cube", indexes={@ORM\Index(name="cube_createdby", columns={"cube_createdby_fk"})})
 * @ORM\Entity
 */
class sandboxCube {

    /**
     * @var integer
     *
     * @ORM\Column(name="cube_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $cubeId;

    /**
     * @var string
     *
     * @ORM\Column(name="cube_name", type="string", length=100, nullable=true)
     */
    private $cubeName;


    /**
     * @var string
     *
     * @ORM\Column(name="cube_mac", type="string", length=45, nullable=true)
     */
    private $cubeMac;


    /**
     * @var integer
     *
     * @ORM\Column(name="cube_createdon", type="integer", nullable=true)
     */
    private $cubeCreatedon;

    /**
     * @var integer
     *
     * @ORM\Column(name="cube_status", type="integer", nullable=true)
     */
    private $cubeStatus = '1';


    /**
     * @var \Application\Entity\SandboxUser
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\SandboxUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cube_createdby_fk", referencedColumnName="user_id", nullable=true)
     * })
     */
    private $cubeCreatedbyFk;

    /**
     * @var string
     *
     * @ORM\Column(name="cube_token", type="string", length=45, nullable=true)
     */
    private $cubeToken;


    /**
     * @var string
     *
     * @ORM\Column(name="cube_publicIP", type="string", length=100, nullable=true)
     */
    private $cubePublicIP;

    /**
     * @var string
     *
     * @ORM\Column(name="cube_lanIP", type="string", length=100, nullable=true)
     */
    private $cubeLanIP;

    /**
     * @var string
     *
     * @ORM\Column(name="cube_path", type="string", length=100, nullable=true)
     */
    private $cubePath;

    /**
     * @var string
     *
     * @ORM\Column(name="cube_FW", type="string", length=100, nullable=true)
     */
    private $cubeFW;



    /**
     * Constructor
     */
    public function __construct() {
        $this->sandboxAp = new \Doctrine\Common\Collections\ArrayCollection();
        $this->sandboxCubeSite = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get cubeId
     *
     * @return integer 
     */
    public function getCubeId() {
        return $this->cubeId;
    }

    /**
     * Set cubeName
     *
     * @param string $cubeName
     * @return sandboxCube
     */
    public function setCubeName($cubeName) {
        $this->cubeName = $cubeName;

        return $this;
    }

    /**
     * Get cubeName
     *
     * @return string 
     */
    public function getCubeName() {
        return $this->cubeName;
    }


    /**
     * Set cubeMac
     *
     * @param string $cubeMac
     * @return sandboxCube
     */
    public function setCubeMac($cubeMac) {
        $this->cubeMac = $cubeMac;

        return $this;
    }

    /**
     * Get cubeMac
     *
     * @return string 
     */
    public function getCubeMac() {
        return $this->cubeMac;
    }

    /**
     * Set cubeCreatedon
     *
     * @param integer $cubeCreatedon
     * @return sandboxCube
     */
    public function setCubeCreatedon($cubeCreatedon) {
        $this->cubeCreatedon = $cubeCreatedon;

        return $this;
    }

    /**
     * Get cubeCreatedon
     *
     * @return integer 
     */
    public function getCubeCreatedon() {
        return $this->cubeCreatedon;
    }


    /**
     * Set cubeStatus
     *
     * @param boolean $cubeStatus
     * @return sandboxCube
     */
    public function setCubeStatus($cubeStatus) {
        $this->cubeStatus = $cubeStatus;

        return $this;
    }

    /**
     * Get cubeStatus
     *
     * @return boolean 
     */
    public function getCubeStatus() {
        return $this->cubeStatus;
    }

    /**
     * Set cubeCreatedbyFk
     *
     * @param \Application\Entity\sandboxCube $cubeCreatedbyFk
     * @return sandboxCube
     */
    public function setCubeCreatedbyFk(\Application\Entity\SandboxUser $cubeCreatedbyFk = null) {
        $this->cubeCreatedbyFk = $cubeCreatedbyFk;

        return $this;
    }

    /**
     * Get cubeCreatedbyFk
     *
     * @return \Application\Entity\sandboxCube
     */
    public function getCubeCreatedbyFk() {
        return $this->cubeCreatedbyFk;
    }

    /**
     * Set cubeToken
     *
     * @param string $cubeToken
     * @return sandboxCube
     */
    public function setCubeToken($cubeToken) {
        $this->cubeToken = $cubeToken;

        return $this;
    }

    /**
     * Get cubeToken
     *
     * @return string
     */
    public function getCubeToken() {
        return $this->cubeToken;
    }


    /**
     * Set cubepublicIP
     *
     * @param string $cubepublicIP
     * @return sandboxCube
     */
    public function setCubePublicIP($cubepublicIP) {
        $this->cubePublicIP = $cubepublicIP;

        return $this;
    }

    /**
     * Get cubepublicIP
     *
     * @return string
     */
    public function getCubePublicIP() {
        return $this->cubePublicIP;
    }

    /**
     * Set cubelanIP
     *
     * @param string $cubelanIP
     * @return sandboxCube
     */
    public function setCubeLanIP($cubelanIP) {
        $this->cubeLanIP = $cubelanIP;

        return $this;
    }

    /**
     * Get cubelanIP
     *
     * @return string
     */
    public function getCubeLanIP() {
        return $this->cubeLanIP;
    }

    /**
     * Set cubepath
     *
     * @param string $cubepath
     * @return sandboxCube
     */
    public function setCubePath($cubepath) {
        $this->cubePath = $cubepath;

        return $this;
    }

    /**
     * Get cubepath
     *
     * @return string
     */
    public function getCubePath() {
        return $this->cubePath;
    }

    /**
     * Set cubefw
     *
     * @param string $cubefw
     * @return sandboxCube
     */
    public function setCubeFW($cubefw) {
        $this->cubeFW = $cubefw;

        return $this;
    }

    /**
     * Get cubefw
     *
     * @return string
     */
    public function getCubeFW() {
        return $this->cubeFW;
    }


}
