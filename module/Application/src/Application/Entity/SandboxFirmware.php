<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * sandboxFirmware
 *
 * @ORM\Table(name="sandbox_firmware")
 * @ORM\Entity
 */
class SandboxFirmware {

    /**
     * @var integer
     *
     * @ORM\Column(name="firmware_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $firmwareId;

    /**
     * @var string
     *
     * @ORM\Column(name="camera_primaryversion", type="string",length=30, nullable=true)
     */
    private $cameraPrimaryVersion;

    /**
     * @var string
     *
     * @ORM\Column(name="doorbell_primaryversion", type="string",length=30, nullable=true)
     */
    private $doorbellPrimaryVersion;

    /**
     * @var string
     *
     * @ORM\Column(name="firmware_secondaryversion", type="string",length=30, nullable=true)
     */
    private $firmwareSecondaryVersion;

    /**
     * @var string
     *
     * @ORM\Column(name="camera_primaryfspath", type="string",length=200, nullable=true)
     */
    private $cameraPrimaryFsPath;

    /**
     * @var string
     *
     * @ORM\Column(name="doorbell_primaryfspath", type="string",length=200, nullable=true)
     */
    private $doorbellPrimaryFsPath;

    /**
     * @var string
     *
     * @ORM\Column(name="camera_primarykernalpath", type="string",length=200, nullable=true)
     */
    private $cameraPrimaryKernalPath;

    /**
     * @var string
     *
     * @ORM\Column(name="doorbell_primarykernalpath", type="string",length=200, nullable=true)
     */
    private $doorbellPrimaryKernalPath;

    /**
     * @var string
     *
     * @ORM\Column(name="camera_primaryfsmd5", type="string",length=60, nullable=true)
     */
    private $cameraPrimaryFsMd5;

    /**
     * @var string
     *
     * @ORM\Column(name="doorbell_primaryfsmd5", type="string",length=60, nullable=true)
     */
    private $doorbellPrimaryFsMd5;


    /**
     * @var string
     *
     * @ORM\Column(name="camera_primarykernalmd5", type="string",length=60, nullable=true)
     */
    private $cameraPrimaryKernalMd5;


    /**
     * @var string
     *
     * @ORM\Column(name="doorbell_primarykernalmd5", type="string",length=60, nullable=true)
     */
    private $doorbellPrimaryKernalMd5;


    /**
     * @var string
     *
     * @ORM\Column(name="cloud_version", type="string",length=30, nullable=true)
     */
    private $cloudVersion;

    /**
     * @var string
     *
     * @ORM\Column(name="android_version", type="string",length=30, nullable=true)
     */
    private $androidVersion;


    /**
     * @var string
     *
     * @ORM\Column(name="ios_version", type="string",length=30, nullable=true)
     */
    private $iosVersion;

    /**
     * @var string
     *
     * @ORM\Column(name="cube_version", type="string",length=30, nullable=true)
     */
    private $cubeVersion;

    /**
     * @var string
     *
     * @ORM\Column(name="cube_md5", type="string",length=60, nullable=true)
     */
    private $cubeMd5;

    /**
     * @var string
     *
     * @ORM\Column(name="cube_path", type="string",length=200, nullable=true)
     */
    private $cubePath;


    /**
     * Constructor
     */
    public function __construct() {
        $this->sandboxAp = new \Doctrine\Common\Collections\ArrayCollection();
        $this->sandboxFirmwareSite = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get firmwareId
     *
     * @return integer
     */
    public function getFirmwareId() {
        return $this->firmwareId;
    }

    /**
     * Set cameraPrimaryVersion
     *
     * @param string $cameraPrimaryVersion
     * @return sandboxFirmware
     */
    public function setCameraPrimaryVersion($cameraPrimaryVersion) {
        $this->cameraPrimaryVersion = $cameraPrimaryVersion;

        return $this;
    }

    /**
     * Get cameraPrimaryVersion
     *
     * @return string
     */
    public function getCameraPrimaryVersion() {
        return $this->cameraPrimaryVersion;
    }

    /**
     * Set doorbellPrimaryVersion
     *
     * @param string $doorbellPrimaryVersion
     * @return sandboxFirmware
     */
    public function setDoorbellPrimaryVersion($doorbellPrimaryVersion) {
        $this->doorbellPrimaryVersion = $doorbellPrimaryVersion;

        return $this;
    }

    /**
     * Get doorbellPrimaryVersion
     *
     * @return string
     */
    public function getDoorbellPrimaryVersion() {
        return $this->doorbellPrimaryVersion;
    }

    /**
     * Set cameraPrimaryFsPath
     *
     * @param string $cameraPrimaryFsPath
     * @return sandboxFirmware
     */
    public function setCameraPrimaryFsPath($cameraPrimaryFsPath) {
        $this->cameraPrimaryFsPath = $cameraPrimaryFsPath;

        return $this;
    }

    /**
     * Get cameraPrimaryFsPath
     *
     * @return string
     */
    public function getCameraPrimaryFsPath() {
        return $this->cameraPrimaryFsPath;
    }

    /**
     * Set doorbellPrimaryFsPath
     *
     * @param string $doorbellPrimaryFsPath
     * @return sandboxFirmware
     */
    public function setDoorbellPrimaryFsPath($doorbellPrimaryFsPath) {
        $this->doorbellPrimaryFsPath = $doorbellPrimaryFsPath;

        return $this;
    }

    /**
     * Get doorbellPrimaryFsPath
     *
     * @return string
     */
    public function getDoorbellPrimaryFsPath() {
        return $this->DoorbellPrimaryFsPath;
    }


    /**
     * Set cameraPrimaryKernalPath
     *
     * @param string $cameraPrimaryKernalPath
     * @return sandboxFirmware
     */
    public function setCameraPrimaryKernalPath($cameraPrimaryKernalPath) {
        $this->cameraPrimaryKernalPath = $cameraPrimaryKernalPath;

        return $this;
    }

    /**
     * Get cameraPrimaryKernalPath
     *
     * @return string
     */
    public function getCameraPrimaryKernalPath() {
        return $this->cameraPrimaryKernalPath;
    }

    /**
     * Set doorbellPrimaryKernalPath
     *
     * @param string $doorbellPrimaryKernalPath
     * @return sandboxFirmware
     */
    public function setDoorbellPrimaryKernalPath($doorbellPrimaryKernalPath) {
        $this->doorbellPrimaryKernalPath = $doorbellPrimaryKernalPath;

        return $this;
    }

    /**
     * Get doorbellPrimaryKernalPath
     *
     * @return string
     */
    public function getDoorbellPrimaryKernalPath() {
        return $this->doorbellPrimaryKernalPath;
    }

    /**
     * Set firmwareSecondaryVersion
     *
     * @param string $firmwareSecondaryVersion
     * @return sandboxFirmware
     */
    public function setFirmwareSecondaryVersion($secondaryVersion) {
        $this->firmwareSecondaryVersion = $secondaryVersion;

        return $this;
    }

    /**
     * Get firmwareSecondaryVersion
     *
     * @return string
     */
    public function getFirmwareSecondaryVersion() {
        return $this->firmwareSecondaryVersion;
    }


    /**
     * Set cameraPrimaryFsMd5
     *
     * @param string $cameraPrimaryFsMd5
     * @return sandboxFirmware
     */
    public function setCameraPrimaryFsMd5($cameraPrimaryFsMd5) {
        $this->cameraPrimaryFsMd5 = $cameraPrimaryFsMd5;

        return $this;
    }

    /**
     * Get cameraPrimaryFsMd5
     *
     * @return string
     */
    public function getCameraPrimaryFsMd5() {
        return $this->cameraPrimaryFsMd5;
    }

    /**
     * Set doorbellPrimaryFsMd5
     *
     * @param string $doorbellPrimaryFsMd5
     * @return sandboxFirmware
     */
    public function setDoorbellPrimaryFsMd5($doorbellPrimaryFsMd5) {
        $this->doorbellPrimaryFsMd5 = $doorbellPrimaryFsMd5;

        return $this;
    }

    /**
     * Get doorbellPrimaryFsMd5
     *
     * @return string
     */
    public function getDoorbellPrimaryFsMd5() {
        return $this->doorbellPrimaryFsMd5;
    }

    /**
     * Set cameraPrimaryFsMd5
     *
     * @param string $cameraPrimaryFsMd5
     * @return sandboxFirmware
     */
    public function setCameraPrimaryKernalMd5($cameraPrimaryKernalMd5) {
        $this->cameraPrimaryKernalMd5 = $cameraPrimaryKernalMd5;

        return $this;
    }

    /**
     * Get cameraPrimaryFsMd5
     *
     * @return string
     */
    public function getCameraPrimaryKernalMd5() {
        return $this->cameraPrimaryKernalMd5;
    }

    /**
     * Set doorbellPrimaryFsMd5
     *
     * @param string $doorbellPrimaryFsMd5
     * @return sandboxFirmware
     */
    public function setDoorbellPrimaryKernalMd5($doorbellPrimaryKernalMd5) {
        $this->doorbellPrimaryKernalMd5 = $doorbellPrimaryKernalMd5;

        return $this;
    }

    /**
     * Get doorbellPrimaryFsMd5
     *
     * @return string
     */
    public function getDoorbellPrimaryKernalMd5() {
        return $this->doorbellPrimaryKernalMd5;
    }

    /**
     * Set firmwareSecondaryFsMd5
     *
     * @param string $firmwareSecondaryFsMd5
     * @return sandboxFirmware
     */
    public function setFirmwareSecondaryFsMd5($firmwareSecondaryFsMd5) {
        $this->firmwareSecondaryFsMd5 = $firmwareSecondaryFsMd5;

        return $this;
    }

    /**
     * Get firmwareSecondaryFsMd5
     *
     * @return string
     */
    public function getFirmwareSecondaryFsMd5() {
        return $this->firmwareSecondaryFsMd5;
    }

    /**
     * Set firmwareSecondaryFsMd5
     *
     * @param string $firmwareSecondaryFsMd5
     * @return sandboxFirmware
     */
    public function setFirmwareSecondaryKernalMd5($firmwareSecondaryKernalMd5) {
        $this->firmwareSecondaryKernalMd5 = $firmwareSecondaryKernalMd5;

        return $this;
    }

    /**
     * Get firmwareSecondaryFsMd5
     *
     * @return string
     */
    public function getFirmwareSecondaryKernalMd5() {
        return $this->firmwareSecondaryKernalMd5;
    }

    /**
     * Set cloudVersion
     *
     * @param string $cloudVersion
     * @return sandboxFirmware
     */
    public function setCloudVersion($cloudVersion) {
        $this->cloudVersion = $cloudVersion;

        return $this;
    }

    /**
     * Get cloudVersion
     *
     * @return string
     */
    public function getCloudVersion() {
        return $this->cloudVersion;
    }

    /**
     * Set androidVersion
     *
     * @param string $cloudVersion
     * @return sandboxFirmware
     */
    public function setAndroidVersion($androidVersion) {
        $this->androidVersion = $androidVersion;

        return $this;
    }

    /**
     * Get androidVersion
     *
     * @return string
     */
    public function getAndroidVersion() {
        return $this->androidVersion;
    }

    /**
     * Set iosVersion
     *
     * @param string $iosVersion
     * @return sandboxFirmware
     */
    public function setIosVersion($iosVersion) {
        $this->iosVersion = $iosVersion;

        return $this;
    }

    /**
     * Get iosVersion
     *
     * @return string
     */
    public function getIosVersion() {
        return $this->iosVersion;
    }

    /**
     * Set cubeVersion
     *
     * @param string $cubeVersion
     * @return sandboxFirmware
     */
    public function setCubeVersion($cubeVersion) {
        $this->cubeVersion = $cubeVersion;

        return $this;
    }

    /**
     * Get cubeVersion
     *
     * @return string
     */
    public function getCubeVersion() {
        return $this->cubeVersion;
    }

    /**
     * Set cubeMD5
     *
     * @param string $cubeMD5
     * @return sandboxFirmware
     */
    public function setCubeMD5($cubeMD5) {
        $this->cubeMd5 = $cubeMD5;

        return $this;
    }

    /**
     * Get cubeVersion
     *
     * @return string
     */
    public function getCubeMD5() {
        return $this->cubeMd5;
    }

    /**
     * Set cubePath
     *
     * @param string $cubePath
     * @return sandboxFirmware
     */
    public function setCubePath($cubePath) {
        $this->cubePath = $cubePath;

        return $this;
    }

    /**
     * Get cubePath
     *
     * @return string
     */
    public function getCubePath() {
        return $this->cubePath;
    }

}
