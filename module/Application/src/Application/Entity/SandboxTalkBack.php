<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * sandboxTalkBack
 *
 * @ORM\Table(name="sandbox_talkback")
 * @ORM\Entity
 */
class sandboxTalkBack {

    /**
     * @var integer
     *
     * @ORM\Column(name="talkback_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $talkbackId;


    /**
     * @var \Application\Entity\SandboxDevice
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\SandboxDevice")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="device_IdFK", referencedColumnName="device_id", nullable=true)
     * })
     */

    private $deviceIdFK;



    /**
     * @var string
     *
     * @ORM\Column(name="talkback_timeStamp", type="string",length=30, nullable=true)
     */
    private $talkbackTimeStamp;

    /**
     * @var string
     *
     * @ORM\Column(name="talkback_command", type="string",length=30, nullable=true)
     */
    private $talkbackCommand;


    /**
     * @var string
     *
     * @ORM\Column(name="talkback_status", type="string",length=30, nullable=true)
     */
    private $talkbackStatus;

    /**
     * @var \Application\Entity\SandboxUser
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\SandboxUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_IdFK", referencedColumnName="user_id", nullable=true)
     * })
     */
    private $userIdFk;


    /**
     * Constructor
     */
    public function __construct() {
        $this->sandboxAp = new \Doctrine\Common\Collections\ArrayCollection();
        $this->sandboxCommandSite = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get Id
     *
     * @return integer
     */
    public function getTalkbackId() {
        return $this->talkbackId;
    }

    /**
     * Set deviceIdFK
     *
     * @param string $deviceIdFK
     * @return sandboxCommand
     */
    public function setTalkbackDeviceIdFK($deviceIdFK) {
        $this->deviceIdFK = $deviceIdFK;

        return $this;
    }

    /**
     * Get deviceIdFK
     *
     * @return string
     */
    public function getTalkbackDeviceIdFK() {
        return $this->deviceIdFK;
    }


    /**
     * Set timeStamp
     *
     * @param string $timeStamp
     * @return sandboxCommand
     */
    public function setTalkbackTimeStamp($timeStamp) {
        $this->talkbackTimeStamp = $timeStamp;

        return $this;
    }

    /**
     * Get timeStamp
     *
     * @return string
     */
    public function getTalkbackTimeStamp() {
        return $this->talkbackTimeStamp;
    }

    /**
     * Set talkbackCommand
     *
     * @param string $talkbackCommand
     * @return sandboxCommand
     */
    public function setTalkbackCommand($talkbackCommand) {
        $this->talkbackCommand = $talkbackCommand;

        return $this;
    }

    /**
     * Get $talkbackCommand
     *
     * @return string
     */
    public function getTalkbackCommand() {
        return $this->talkbackCommand;
    }

    /**
     * Set talkbackStatus
     *
     * @param string $talkbackStatus
     * @return sandboxCommand
     */
    public function setTalkbackStatus($talkbackStatus) {
        $this->talkbackStatus = $talkbackStatus;

        return $this;
    }

    /**
     * Get $talkbackStatus
     *
     * @return string
     */
    public function getTalkbackStatus() {
        return $this->talkbackStatus;
    }

    /**
     * Set talkbackUserIdFk
     *
     * @param string $talkbackUserIdFk
     * @return sandboxCommand
     */
    public function setTalkbackUserIdFk($talkbackUserIdFk) {
        $this->userIdFk = $talkbackUserIdFk;

        return $this;
    }

    /**
     * Get $talkbackUserIdFk
     *
     * @return string
     */
    public function getTalkbackUserIdFk() {
        return $this->userIdFk;
    }



}
