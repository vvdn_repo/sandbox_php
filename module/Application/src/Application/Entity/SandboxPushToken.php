<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * sandboxPushToken
 *
 * @ORM\Table(name="sandbox_pushtoken")
 * @ORM\Entity
 */
class sandboxPushToken {

    /**
     * @var integer
     *
     * @ORM\Column(name="token_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tokenId;


    /**
     * @var \Application\Entity\SandboxUser
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\SandboxUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="token_userIdFK", referencedColumnName="user_id", nullable=true)
     * })
     */

    private $tokenUserIdFK;

    /**
     * @var string
     *
     * @ORM\Column(name="token_appIdentifier", type="string", nullable=true)
     */
    private $tokenAppIdentifier;


    /**
     * @var string
     *
     * @ORM\Column(name="token_value", type="string", nullable=true)
     */
    private $tokenValue;

	/**
     * @var string
     *
     * @ORM\Column(name="token_platform", type="string", nullable=true)
     */
    private $tokenPlatform;

	
    /**
     * @var string
     *
     * @ORM\Column(name="token_status", type="string",length=30, nullable=true)
     */
    private $tokenStatus;

	/**
     * @var string
     *
     * @ORM\Column(name="token_awsendpt", type="string",length=255, nullable=true)
     */
    private $tokenEndpoint;

	
    /**
     * Constructor
     */
    public function __construct() {
        $this->sandboxAp = new \Doctrine\Common\Collections\ArrayCollection();
        $this->sandboxCommandSite = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get Id
     *
     * @return integer
     */
    public function getokenId() {
        return $this->tokenId;
    }

    /**
     * Set tokenUserIdFK
     *
     * @param string $tokenUserIdFK
     * @return sandboxPushToken
     */
    public function setUserIdFK($tokenUserIdFK) {
        $this->tokenUserIdFK = $tokenUserIdFK;

        return $this;
    }

    /**
     * Get tokenUserIdFK
     *
     * @return integer
     */
    public function getUserIdFK() {
        return $this->tokenUserIdFK;
    }

    /**
     * Set tokenAppIdentifier
     *
     * @param string $appIdentifier
     * @return sandboxPushToken
     */
    public function setTokenAppIdentifier($appIdentifier) {
        $this->tokenAppIdentifier = $appIdentifier;
        return $this;
    }

    /**
     * Get tokenAppIdentifier
     *
     * @return integer
     */
    public function getTokenAppIdentifier() {
        return $this->tokenAppIdentifier;
    }

    /**
     * Set tokenStatus
     *
     * @param string $tokenStatus
     * @return sandboxPushToken
     */
    public function setTokenStatus($tokenStatus) {
        $this->tokenStatus =$tokenStatus ;
        return $this;
    }

    /**
     * Get tokenStatus
     *
     * @return integer
     */
    public function getTokenStatus() {
        return $this->tokenStatus;
    }
    /**
     * Set tokenValue
     *
     * @param string $tokenValue
     * @return sandboxPushToken
     */
    public function setTokenValue($tokenValue) {
        $this->tokenValue = $tokenValue;
        return $this;
    }

    /**
     * Get tokenValue
     *
     * @return integer
     */
    public function getTokenValue() {
        return $this->tokenValue;
    }
	
	/**
     * Set tokenPlatform
     *
     * @param string $tokenPlatform
     * @return sandboxPushToken
     */
    public function setTokenPlatform($tokenPlatform) {
        $this->tokenPlatform = $tokenPlatform;
        return $this;
    }

    /**
     * Get tokenPlatform
     *
     * @return integer
     */
    public function getTokenPlatform() {
        return $this->tokenPlatform;
    }
	/**
     * Set tokenEndpoint
     *
     * @param string $tokenEndpoint
     * @return sandboxPushToken
     */
    public function setTokenEndpt($tokenEndpoint) {
        $this->tokenEndpoint = $tokenEndpoint;
        return $this;
    }

    /**
     * Get tokenEndpoint
     *
     * @return string
     */
    public function getTokenEndpt() {
        return $this->tokenEndpoint;
    }
    

}
