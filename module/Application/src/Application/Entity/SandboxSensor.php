<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * sandboxSensor
 *
 * @ORM\Table(name="sandbox_sensor",uniqueConstraints={@ORM\UniqueConstraint(name="sensor_mac_UNIQUE", columns={"sensor_mac"})})
 * @ORM\Entity
 */
class sandboxSensor {

    /**
     * @var integer
     *
     * @ORM\Column(name="sensor_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $sensorId;

    /**
     * @var string
     *
     * @ORM\Column(name="sensor_name", type="string",length=30, nullable=true)
     */
    private $sensorName;


    /**
     * @var string
     *
     * @ORM\Column(name="sensor_mac", type="string",length=30, nullable=true)
     */
    private $sensorMac;


    /**
     * @var \Application\Entity\SandboxUser
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\SandboxUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sensor_userid", referencedColumnName="user_id", nullable=true)
     * })
     */
    private $sensorUserId;


    /**
     * @var integer
     *
     * @ORM\Column(name="sensor_status", type="integer", nullable=true)
     */
    private $sensorStatus = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="sensor_batterystatus", type="integer", nullable=true)
     */
    private $sensorBatteryStatus = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="sensor_alarmTS", type="string",length=30,  nullable=true)
     */
    private $sensorAlarmTS;


    /**
     * Constructor
     */
    public function __construct() {
        $this->sandboxAp = new \Doctrine\Common\Collections\ArrayCollection();
        $this->sandboxSensorSite = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get sensorId
     *
     * @return integer
     */
    public function getSensorId() {
        return $this->sensorId;
    }

    /**
     * Set sensorName
     *
     * @param string $sensorName
     * @return sandboxSensor
     */
    public function setSensorName($sensorName) {
        $this->sensorName = $sensorName;

        return $this;
    }

    /**
     * Get sensorName
     *
     * @return string
     */
    public function getSensorName() {
        return $this->sensorName;
    }



    /**
     * Set sensorMac
     *
     * @param string $sensorMac
     * @return sandboxSensor
     */
    public function setSensorMac($sensorMac) {
        $this->sensorMac = $sensorMac;

        return $this;
    }

    /**
     * Get sensorMac
     *
     * @return string
     */
    public function getSensorMac() {
        return $this->sensorMac;
    }

    /**
     * Set sensorUserId
     *
     * @param integer $sensorUserId
     * @return sandboxSensor
     */
    public function setSensorUserId($sensorUserId) {
        $this->sensorUserId = $sensorUserId;

        return $this;
    }

    /**
     * Get sensorUserId
     *
     * @return integer
     */
    public function getSensorUserId() {
        return $this->sensorUserId;
    }


    /**
     * Set sensorStatus
     *
     * @param boolean $sensorStatus
     * @return sandboxSensor
     */
    public function setSensorStatus($sensorStatus) {
        $this->sensorStatus = $sensorStatus;

        return $this;
    }

    /**
     * Get sensorStatus
     *
     * @return boolean
     */
    public function getSensorStatus() {
        return $this->sensorStatus;
    }

     /**
     * Set sensorBatteryStatus
     *
     * @param boolean $sensorBatteryStatus
     * @return sandboxSensor
     */
    public function setSensorBatteryStatus($sensorBatteryStatus) {
        $this->sensorBatteryStatus = $sensorBatteryStatus;

        return $this;
    }

    /**
     * Get sensorBatteryStatus
     *
     * @return boolean
     */
    public function getSensorBatteryStatus() {
        return $this->sensorBatteryStatus;
    }
 

    /**
     * Set sensorAlarmTS
     *
     * @param boolean $sensorAlarmTS
     * @return sandboxSensor
     */
    public function setSensorAlarmTS($sensorAlarmTS) {
        $this->sensorAlarmTS = $sensorAlarmTS;

        return $this;
    }

    /**
     * Get sensorAlarmTS
     *
     * @return boolean
     */
    public function getSensorAlarmTS() {
        return $this->sensorAlarmTS;
    }

}
