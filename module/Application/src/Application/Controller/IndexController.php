<?php

/**
 * IndexController handles all the action comming from routes 
 * for the Application module  
 * @package Application
 * @author VVDN Technologies < >
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Zend\Config\Reader\Ini;
use Zend\Session\Container as SessionContainer;

class IndexController extends AbstractActionController {

    protected function getServiceInstance($service) {
        return $this->getServiceLocator()->get($service);
    }

    /*
     * This action function will populate the index page of the application 
     * @return Object Instance of ViewModel of index page
     */

    public function indexAction() {
        $view = $this->getServiceInstance('ViewRenderer');
        $this->layout('layout/landing');
    }

    public function mainAction() {
        $view = $this->getServiceInstance('ViewRenderer');
        $view->headTitle(PAGE_TITLE);
        $config = $this->getServiceInstance('config');
        $orgLogo = ANGULAR_APP_PATH . $config['default_logo_path'];
        $viewModel = new ViewModel(array(
            'orgLogo' => $orgLogo,
            'login' => BASE_PATH . "/user/login"
        ));
        $viewModel->setTerminal(true);
        return $viewModel;
    }

    /*
     * This action function will populate the landing page of the 
     * application after first time login 
     * @return Object Instance of ViewModel of landing page
     */

    public function landingAction() {



    }


    public function userhomeAction() {

        $session = new SessionContainer('base');
        $val = $session->offsetGet('user_id');

        if($val)
        {
            $view = $this->getServiceInstance('ViewRenderer');
            $view->headTitle(PAGE_TITLE . " | Home");
            $this->layout('layout/home');

        }
        else{
            $this->redirect()->toUrl(BASE_PATH . '/user/login');
        }


    }

    public function sendActivationEmailAction() {
        $userId = $this->getRequest()->getQuery('id');

        $userModel = $this->getServiceInstance('User');
        $eventlogInstance = $this->getServiceInstance('Eventlog');
        $user = $userModel->getEmailById($userId);
        $response = array('status' => false);
        if (!empty($user)) {
            //ReSend Verification
            $info["userId"] = $userId;
            $info["userName"] = $user['userName'];
            $info["userEmail"] = $user['userEmail'];
            $info["toEmail"] = $user['userEmail'];
            $info["activationKey"] = $userModel->generateActivationKey($info);
            $userModel->sendEmail("registration", $info);
            $logMessage = array("%EMAIL%" => $user['userEmail']);
            $eventlogInstance->createLog('user', 'resend-activation-email', $userId, $user['userName'], 'success', "", "", 0, 0, $logMessage);
            $response['status'] = true;
            $response['message'] = "Activation Email Sent Successfully.";
        }
        echo json_encode($response);
        die;
    }

    /*
     * This action function will populate the home page of the 
     * application after login
     * @return Object Instance of ViewModel of home page
     */

    
    

}
