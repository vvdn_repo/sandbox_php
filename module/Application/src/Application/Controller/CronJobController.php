<?php

/**
 * IndexController handles all the action comming from routes 
 * for the Application module  
 * @package Application
 * @author VVDN Technologies < >
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController,
    Zend\Console\Request as ConsoleRequest;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Zend\Config\Reader\Ini;

class CronJobController extends AbstractActionController {

    private $message = "";

    protected function getServiceInstance($service) {
        return $this->getServiceLocator()->get($service);
    }

    /**
     * Run cron for data set once
     * 
     * @param:no
     * @return JsonModel
     */
    public function cronScheduleReportOnceAction() {
        // Load Generate Modal
        $graphGenerateModel = $this->getServiceInstance('ReportJPGraphGenerate');
        // User Modal
        $userModel = $this->getServiceInstance('User');
        // Load PDF generate modal and generate report pdf
        $pdfGenerateModel = $this->getServiceInstance('ReportPdfGenerate');
        // Load report generate modal
        $reportScheduleModel = $this->getServiceInstance('ReportSchedule');
        // Load Organization Model
        $organizationModel = $this->getServiceInstance('Organization');
        $rsReportsList = $reportScheduleModel->cronScheduledOnceList();
        // Load Comman Modal
        $modelCommon = $this->getServiceInstance('Common');
        // Loop through the scheduled report from Report Scheduling table 
        foreach ($rsReportsList as $key => $val) {
            $rscheduleEmails = explode(',', $val['rscheduleEmails']);
            $orgId = $rsReportsList[$key][0]['rschedule_orgid_fk'];
            $usrId = $rsReportsList[$key][0]['rschedule_userid_fk'];
            $rscheduleId = $rsReportsList[$key][0]['rscheduleId'];
            // Email Signature
            $orgEmailSignature = $organizationModel->getOrgEmailSignature($orgId);
            $data['emailSignature'] = $orgEmailSignature['emailSignature'];
            // Check report type
            if ($val['rscheduleReporttype'] == 'executive-summary-report') {
                // Graph Image Generate
                $graphGenerateModel->graphTenantsWithMostAps($orgId, $val['rscheduleReporttype']);
                $graphGenerateModel->graphTenantsWithMostClients($orgId, $val['rscheduleReporttype']);
                $graphGenerateModel->graphTenantsWithMostCriticalAlarms($orgId, $val['rscheduleReporttype']);
                // Get user email by id
                $userEmail = $userModel->getEmailById($usrId);
                // PDF
                $genratePdf = $pdfGenerateModel->generatePDF($orgId, $userEmail['userEmail'], $val['rscheduleReporttype']);
                // Send Emails
                foreach ($rscheduleEmails as $eValue) {
                    $data['toEmail'] = $eValue;
                    $data['subject'] = $val['rscheduleSubject'];
                    // Send PDF Report Notification Mail
                    $reportScheduleModel->sendPDFReportNotification($data);
                    //--
                    $rsReportsListMsz['data'][$val['rscheduleReporttype']][] = $data;
                    $rsReportsListMsz['status'] = 'Success';
                }
            } elseif ($val['rscheduleReporttype'] == 'my-access-points-summary-report') {
                // Graph Generate Report
                $graphGenerateModel->graphModelWithMostAps($orgId, $val['rscheduleReporttype']);
                $graphGenerateModel->graphFirmwareWithMostAps($orgId, $val['rscheduleReporttype']);
                $graphGenerateModel->graphConfiguredUnconfiguredAps($orgId, $val['rscheduleReporttype']);
                $order = 'DESC';
                $graphGenerateModel->graphApbythroughput($orgId, $order, 4, $val['rscheduleReporttype']);
                $order = 'ASC';
                $graphGenerateModel->graphApbythroughput($orgId, $order, 5, $val['rscheduleReporttype']);
                $order = 'DESC';
                $graphGenerateModel->graphApbyclient($orgId, $order, 6, $val['rscheduleReporttype']);
                $order = 'ASC';
                $graphGenerateModel->graphApbyclient($orgId, $order, 7, $val['rscheduleReporttype']);
                // Get user email by id
                $userEmail = $userModel->getEmailById($usrId);
                // PDF
                $genratePdf = $pdfGenerateModel->generatePDF($orgId, $userEmail['userEmail'], $val['rscheduleReporttype']);

                // Send Emails
                foreach ($rscheduleEmails as $eValue) {
                    $data['toEmail'] = $eValue;
                    $data['subject'] = $val['rscheduleSubject'];
                    // Send PDF Report Notification Mail
                    $reportScheduleModel->sendPDFReportNotification($data);
                    $rsReportsListMsz['data'][$val['rscheduleReporttype']][] = $data;
                    $rsReportsListMsz['status'] = 'Success';
                }
            } elseif ($val['rscheduleReporttype'] == 'client-summary-report') {
                // Graph Generate Report
                $graphGenerateModel->graphClientThroughput($orgId, $order, 1, $val['rscheduleReporttype']);
                // Get user email by id
                $userEmail = $userModel->getEmailById($usrId);
                // PDF
                $genratePdf = $pdfGenerateModel->generatePDF($orgId, $userEmail['userEmail'], $val['rscheduleReporttype']);

                // Send Emails
                foreach ($rscheduleEmails as $eValue) {
                    $data['toEmail'] = $eValue;
                    $data['subject'] = $val['rscheduleSubject'];
                    // Send PDF Report Notification Mail
                    $reportScheduleModel->sendPDFReportNotification($data);
                    $rsReportsListMsz['data'][$val['rscheduleReporttype']][] = $data;
                    $rsReportsListMsz['status'] = 'Success';
                }
            } elseif ($val['rscheduleReporttype'] == 'access-point-summary-report') {
                $order = 'DESC';
                $graphGenerateModel->graphApbythroughput($orgId, $order, 1, $val['rscheduleReporttype']);
                $order = 'ASC';
                $graphGenerateModel->graphApbythroughput($orgId, $order, 2, $val['rscheduleReporttype']);
                $order = 'DESC';
                $graphGenerateModel->graphApbyclient($orgId, $order, 3, $val['rscheduleReporttype']);
                $order = 'ASC';
                $graphGenerateModel->graphApbyclient($orgId, $order, 4, $val['rscheduleReporttype']);
                // Get user email by id
                $userEmail = $userModel->getEmailById($usrId);
                // PDF
                $genratePdf = $pdfGenerateModel->generatePDF($orgId, $userEmail['userEmail'], $val['rscheduleReporttype']);
                // Send Emails
                foreach ($rscheduleEmails as $eValue) {
                    $data['toEmail'] = $eValue;
                    $data['subject'] = $val['rscheduleSubject'];
                    // Send PDF Report Notification Mail
                    $reportScheduleModel->sendPDFReportNotification($data);
                    $rsReportsListMsz['data'][$val['rscheduleReporttype']][] = $data;
                    $rsReportsListMsz['status'] = 'Success';
                }
            } elseif ($val['rscheduleReporttype'] == 'ssid-summary-report') {
                // Graph Generate Report                
                $order = 'DESC';
                $graphGenerateModel->graphSsidbythroughput($orgId, $order, 1, $val['rscheduleReporttype']);
                $order = 'ASC';
                $graphGenerateModel->graphSsidbythroughput($orgId, $order, 2, $val['rscheduleReporttype']);
                $order = 'DESC';
                $graphGenerateModel->graphSsidbyclient($orgId, $order, 3, $val['rscheduleReporttype']);
                $order = 'ASC';
                $graphGenerateModel->graphSsidbyclient($orgId, $order, 4, $val['rscheduleReporttype']);
                // Get user email by id
                $userEmail = $userModel->getEmailById($usrId);
                // PDF
                $genratePdf = $pdfGenerateModel->generatePDF($orgId, $userEmail['userEmail'], $val['rscheduleReporttype']);
                // Send Emails
                foreach ($rscheduleEmails as $eValue) {
                    $data['toEmail'] = $eValue;
                    $data['subject'] = $val['rscheduleSubject'];
                    // Send PDF Report Notification Mail
                    $reportScheduleModel->sendPDFReportNotification($data);
                    $rsReportsListMsz['data'][$val['rscheduleReporttype']][] = $data;
                    $rsReportsListMsz['status'] = 'Success';
                }
            } elseif ($val['rscheduleReporttype'] == 'alarm-summary-report') {
                // Graph Generate Report                
                $graphGenerateModel->graphGetTenantSummary($orgId, 1, $val['rscheduleReporttype'], 0, 'sitewithalarm', 'Sites With Alarm');
                $graphGenerateModel->graphGetTenantSummary($orgId, 2, $val['rscheduleReporttype'], 'critical', 'sitewithalarm', 'Sites With Critical Alarm');
                $graphGenerateModel->graphGetTenantSummary($orgId, 3, $val['rscheduleReporttype'], 0, 'sitewithalarmseveritytime', 'Critical Alarms Timeline');
                // Get user email by id
                $userEmail = $userModel->getEmailById($usrId);
                // PDF
                $genratePdf = $pdfGenerateModel->generatePDF($orgId, $userEmail['userEmail'], $val['rscheduleReporttype']);
                // Send Emails
                foreach ($rscheduleEmails as $eValue) {
                    $data['toEmail'] = $eValue;
                    $data['subject'] = $val['rscheduleSubject'];
                    // Send PDF Report Notification Mail
                    $reportScheduleModel->sendPDFReportNotification($data);
                    $rsReportsListMsz['data'][$val['rscheduleReporttype']][] = $data;
                    $rsReportsListMsz['status'] = 'Success';
                }
            }
            // Update Status
            $rsStatusUpdt = $reportScheduleModel->statusUpdateReportScheduleOther($rscheduleId, $orgId);
        }
        // To check data in console
        print_r($rsReportsListMsz);
        die();
    }

    /**
     * Run cron for data set multiple
     * 
     * @param:no
     * @return JsonModel
     */
    public function cronScheduleReportAction() {
        // Load Generate Modal
        $graphGenerateModel = $this->getServiceInstance('ReportJPGraphGenerate');
        // User Modal
        $userModel = $this->getServiceInstance('User');
        // Load PDF generate modal and generate report pdf
        $pdfGenerateModel = $this->getServiceInstance('ReportPdfGenerate');
        // Load report generate modal
        $reportScheduleModel = $this->getServiceInstance('ReportSchedule');
        // Load Organization Model
        $organizationModel = $this->getServiceInstance('Organization');
        $rsReportsList = $reportScheduleModel->cronScheduledList();
        // Load Comman Modal
        $modelCommon = $this->getServiceInstance('Common');
        // Loop through the scheduled report from Report Scheduling table
        foreach ($rsReportsList as $key => $val) {
            $rscheduleEmails = explode(',', $val['rscheduleEmails']);
            $orgId = $rsReportsList[$key][0]['rschedule_orgid_fk'];
            $usrId = $rsReportsList[$key][0]['rschedule_userid_fk'];
            $rscheduleId = $rsReportsList[$key][0]['rscheduleId'];
            // Email Signature
            $orgEmailSignature = $organizationModel->getOrgEmailSignature($orgId);
            $data['emailSignature'] = $orgEmailSignature['emailSignature'];
            // Check report type
            if ($val['rscheduleReporttype'] == 'executive-summary-report') {
                // Graph Image Generate
                $graphGenerateModel->graphTenantsWithMostAps($orgId, $val['rscheduleReporttype']);
                $graphGenerateModel->graphTenantsWithMostClients($orgId, $val['rscheduleReporttype']);
                $graphGenerateModel->graphTenantsWithMostCriticalAlarms($orgId, $val['rscheduleReporttype']);
                // Get user email by id
                $userEmail = $userModel->getEmailById($usrId);
                // PDF
                $genratePdf = $pdfGenerateModel->generatePDF($orgId, $userEmail['userEmail'], $val['rscheduleReporttype']);
                // Send Emails
                foreach ($rscheduleEmails as $eValue) {
                    $data['toEmail'] = $eValue;
                    $data['subject'] = $val['rscheduleSubject'];
                    // Send PDF Report Notification Mail
                    $reportScheduleModel->sendPDFReportNotification($data);
                    //--
                    $rsReportsListMsz['data'][$val['rscheduleReporttype']][] = $data;
                    $rsReportsListMsz['status'] = 'Success';
                }
            } elseif ($val['rscheduleReporttype'] == 'my-access-points-summary-report') {
                // Graph Generate Report
                $graphGenerateModel->graphModelWithMostAps($orgId, $val['rscheduleReporttype']);
                $graphGenerateModel->graphFirmwareWithMostAps($orgId, $val['rscheduleReporttype']);
                $graphGenerateModel->graphConfiguredUnconfiguredAps($orgId, $val['rscheduleReporttype']);
                $order = 'DESC';
                $graphGenerateModel->graphApbythroughput($orgId, $order, 4, $val['rscheduleReporttype']);
                $order = 'ASC';
                $graphGenerateModel->graphApbythroughput($orgId, $order, 5, $val['rscheduleReporttype']);
                $order = 'DESC';
                $graphGenerateModel->graphApbyclient($orgId, $order, 6, $val['rscheduleReporttype']);
                $order = 'ASC';
                $graphGenerateModel->graphApbyclient($orgId, $order, 7, $val['rscheduleReporttype']);
                // Get user email by id
                $userEmail = $userModel->getEmailById($usrId);
                // PDF
                $genratePdf = $pdfGenerateModel->generatePDF($orgId, $userEmail['userEmail'], $val['rscheduleReporttype']);

                // Send Emails
                foreach ($rscheduleEmails as $eValue) {
                    $data['toEmail'] = $eValue;
                    $data['subject'] = $val['rscheduleSubject'];
                    // Send PDF Report Notification Mail
                    $reportScheduleModel->sendPDFReportNotification($data);
                    $rsReportsListMsz['data'][$val['rscheduleReporttype']][] = $data;
                    $rsReportsListMsz['status'] = 'Success';
                }
            } elseif ($val['rscheduleReporttype'] == 'client-summary-report') {
                // Graph Generate Report
                $graphGenerateModel->graphClientThroughput($orgId, $order, 1, $val['rscheduleReporttype']);
                // Get user email by id
                $userEmail = $userModel->getEmailById($usrId);
                // PDF
                $genratePdf = $pdfGenerateModel->generatePDF($orgId, $userEmail['userEmail'], $val['rscheduleReporttype']);

                // Send Emails
                foreach ($rscheduleEmails as $eValue) {
                    $data['toEmail'] = $eValue;
                    $data['subject'] = $val['rscheduleSubject'];
                    // Send PDF Report Notification Mail
                    $reportScheduleModel->sendPDFReportNotification($data);
                    $rsReportsListMsz['data'][$val['rscheduleReporttype']][] = $data;
                    $rsReportsListMsz['status'] = 'Success';
                }
            } elseif ($val['rscheduleReporttype'] == 'access-point-summary-report') {
                $order = 'DESC';
                $graphGenerateModel->graphApbythroughput($orgId, $order, 1, $val['rscheduleReporttype']);
                $order = 'ASC';
                $graphGenerateModel->graphApbythroughput($orgId, $order, 2, $val['rscheduleReporttype']);
                $order = 'DESC';
                $graphGenerateModel->graphApbyclient($orgId, $order, 3, $val['rscheduleReporttype']);
                $order = 'ASC';
                $graphGenerateModel->graphApbyclient($orgId, $order, 4, $val['rscheduleReporttype']);
                // Get user email by id
                $userEmail = $userModel->getEmailById($usrId);
                // PDF
                $genratePdf = $pdfGenerateModel->generatePDF($orgId, $userEmail['userEmail'], $val['rscheduleReporttype']);
                // Send Emails
                foreach ($rscheduleEmails as $eValue) {
                    $data['toEmail'] = $eValue;
                    $data['subject'] = $val['rscheduleSubject'];
                    // Send PDF Report Notification Mail
                    $reportScheduleModel->sendPDFReportNotification($data);
                    $rsReportsListMsz['data'][$val['rscheduleReporttype']][] = $data;
                    $rsReportsListMsz['status'] = 'Success';
                }
            } elseif ($val['rscheduleReporttype'] == 'ssid-summary-report') {
                // Graph Generate Report                
                $order = 'DESC';
                $graphGenerateModel->graphSsidbythroughput($orgId, $order, 1, $val['rscheduleReporttype']);
                $order = 'ASC';
                $graphGenerateModel->graphSsidbythroughput($orgId, $order, 2, $val['rscheduleReporttype']);
                $order = 'DESC';
                $graphGenerateModel->graphSsidbyclient($orgId, $order, 3, $val['rscheduleReporttype']);
                $order = 'ASC';
                $graphGenerateModel->graphSsidbyclient($orgId, $order, 4, $val['rscheduleReporttype']);
                // Get user email by id
                $userEmail = $userModel->getEmailById($usrId);
                // PDF
                $genratePdf = $pdfGenerateModel->generatePDF($orgId, $userEmail['userEmail'], $val['rscheduleReporttype']);
                // Send Emails
                foreach ($rscheduleEmails as $eValue) {
                    $data['toEmail'] = $eValue;
                    $data['subject'] = $val['rscheduleSubject'];
                    // Send PDF Report Notification Mail
                    $reportScheduleModel->sendPDFReportNotification($data);
                    $rsReportsListMsz['data'][$val['rscheduleReporttype']][] = $data;
                    $rsReportsListMsz['status'] = 'Success';
                }
            } elseif ($val['rscheduleReporttype'] == 'alarm-summary-report') {
                // Graph Generate Report                
                $graphGenerateModel->graphGetTenantSummary($orgId, 1, $val['rscheduleReporttype'], 0, 'sitewithalarm', 'Sites With Alarm');
                $graphGenerateModel->graphGetTenantSummary($orgId, 2, $val['rscheduleReporttype'], 'critical', 'sitewithalarm', 'Sites With Critical Alarm');
                $graphGenerateModel->graphGetTenantSummary($orgId, 3, $val['rscheduleReporttype'], 0, 'sitewithalarmseveritytime', 'Critical Alarms Timeline');
                // Get user email by id
                $userEmail = $userModel->getEmailById($usrId);
                // PDF
                $genratePdf = $pdfGenerateModel->generatePDF($orgId, $userEmail['userEmail'], $val['rscheduleReporttype']);
                // Send Emails
                foreach ($rscheduleEmails as $eValue) {
                    $data['toEmail'] = $eValue;
                    $data['subject'] = $val['rscheduleSubject'];
                    // Send PDF Report Notification Mail
                    $reportScheduleModel->sendPDFReportNotification($data);
                    $rsReportsListMsz['data'][$val['rscheduleReporttype']][] = $data;
                    $rsReportsListMsz['status'] = 'Success';
                }
            }
            // Update Status
            $rsStatusUpdt = $reportScheduleModel->statusUpdateReportScheduleOther($rscheduleId, $orgId);
        }
        // To check data in console
        print_r($rsReportsListMsz);
        die();
    }

    /**
     * Cron to Peform Action like ENable/Disable 
     * on entities like org/sitegroup/site/service/device
     */
    public function entityOperationAction() {
        $this->message = "################################################################ \n";
        $currentTimestamp = time();
        $config = $this->getServiceInstance('config');
        $eaModel = $this->getServiceInstance('EntityAction');
        $filterData = array('status' => $config['status_code']['active']);
        $filterData['actionAt'] = $currentTimestamp;
        $result = $eaModel->getData($filterData);
        $this->message .= "List of Queued Operations to be performed on Entities:- \n" . json_encode($result) . "\n";
        $entityActionIds = array();
        foreach ($result as $data) {
            $this->message .= "************************************** \n";
            $entityType = $data['entity'];
            $action = $data['action'];
            $method = $action . ucfirst($entityType) . "Actions"; //For eg. disableOrgActions/enableSitegroupActions
            $this->$method($data['entityId'], $data['actionBy']);
            $entityActionIds[] = $data['entityActionId'];
            $this->message .= "************************************** \n";
        }
        $updateData = array('status' => $config['status_code']['inactive']);
        $where = array('entityActionId' => $entityActionIds);
        $eaModel->updateEntityAction($updateData, $where);
        $this->message .= "\n";
        $this->message .= "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ \n";
        $this->message .= "Date:-" . date('Y-m-d H:i:s') . "\n";
        $this->message .= "################################################################ \n";
        echo $this->message;
    }

    /**
     * Disable Organization and all entities coming under it.
     * @param int $orgId
     * @param string $actionBy
     */
    private function disableOrgActions($orgId, $actionBy) {
        $config = $this->getServiceInstance('config');
        $orgModel = $this->getServiceInstance('Organization');
        $filterData = array('parent_id' => $orgId);

        //Get Active Tenants in MSP
        $tenantResult = $orgModel->getChildOrganizations("tenant", $filterData);
        $tenantsIds = array();
        // Get All tenants ids for this organization
        foreach ($tenantResult as $key => $tenant) {
            $tenantsIds[] = $tenant['orgId'];
        }
        if (!empty($tenantsIds)) {
            //Disable All Tenants
            $updateData = array('orgStatus' => $config['status_code']['inactive']);
            $updateData['orgDisabledBy'] = $actionBy;
            $updateData['orgDisabledat'] = time();
            $where = array('orgIds' => $tenantsIds);
            $orgModel->updateOrganizations($updateData, $where);
        }
        $orgIds = $tenantsIds;
        $orgIds[] = $orgId;
        //Disable All Sitegroups present in Orgids
        $sitegroupModel = $this->getServiceInstance('Sitegroup');
        $updateData = array('sitegroupDisabledEntity' => 'org');
        $updateData['sitegroupDisabledEntityBy'] = $actionBy;
        $updateData['sitegroupStatus'] = $config['status_code']['inactive'];
        $where = array('orgIds' => $orgIds);
        $where['status'] = $config['status_code']['active'];
        $sitegroupModel->updateSitegroupData($updateData, $where);

        //Disable All Sites present in Orgids
        $siteModel = $this->getServiceInstance('Site');
        $updateData = array('siteDisabledEntity' => 'org');
        $updateData['siteDisabledEntityBy'] = $actionBy;
        $updateData['siteStatus'] = $config['status_code']['inactive'];
        $where = array('orgIds' => $orgIds);
        $where['status'] = $config['status_code']['active'];
        $siteModel->updateSiteData($updateData, $where);

        //Disable All Services present in Orgids
        $serviceModel = $this->getServiceInstance('Service');
        $updateData = array('serviceDisabledEntity' => 'org');
        $updateData['serviceDisabledEntityBy'] = $actionBy;
        $updateData['serviceStatus'] = "disable";
        $where = array('orgIds' => $orgIds);
        $where['status'] = $config['status_code']['active'];
        $serviceModel->updateServiceData($updateData, $where);

        //Get Devices present in Orgids
        $filterData['status'] = $config['status_code']['active'];
        $filterData['orgIds'] = $orgIds;
        $deviceModel = $this->getServiceInstance('Device');
        $devices = $deviceModel->getDevicesByOrg($filterData, 0, 0, 0);
        $commandModel = $this->getServiceInstance('Command');
        //Send Status"Down" command to devices
        $deviceIds = array();
        foreach ($devices as $device) {
            $command = $commandModel->generateEditMsoloCommand(array('deviceStatus' => "down"), $device['deviceMac']);
            $commandEntity = $commandModel->getEntityInstance(true);
            $commandEntity->setCommandMac($device['deviceMac']);
            $commandEntity->setCommandJson(json_encode($command));
            $commandEntity->setCommandCreatedOn(time());
            $commandEntity->setCommandStatus(1);
            $commandModel->createCommand($commandEntity);
            $deviceIds[] = $device['deviceId'];
        }

        //Disable all deviceIds
        $updateData = array('deviceDisabledEntity' => 'org');
        $updateData['deviceDisabledEntityBy'] = $actionBy;
        $updateData['deviceStatus'] = $config['status_code']['inactive'];

        $deviceModel->updateDevice($updateData, $deviceIds, $multipleDevices = true);

        //Notify Org of Deactivation
        $emailData = array('subject' => 'Your Organization has been Deactivated');
        $this->notifyOrg($orgId, $emailData, "deactivate_subscription.phtml");
    }

    private function enableOrgActions($orgId, $actionBy) {
        $config = $this->getServiceInstance('config');
        $orgModel = $this->getServiceInstance('Organization');
        $filterData = array('parent_id' => $orgId);
        $filterData['disabledBy'] = $actionBy;

        $tenantResult = $orgModel->getChildOrganizations("tenant", $filterData);
        // Get All tenants ids for this organization
        foreach ($tenantResult as $key => $tenant) {
            $tenantsIds[] = $tenant['orgId'];
        }
        if (!empty($tenantsIds)) {
            //Activate All Tenants
            $updateData = array('orgStatus' => $config['status_code']['active']);
            $updateData['orgDisabledBy'] = "none";
            $updateData['orgDisabledat'] = 0;
            $where = array('orgIds' => $tenantsIds);
            $orgModel->updateOrganizations($updateData, $where);
        }
        $orgIds = $tenantsIds;
        $orgIds[] = $orgId;
        //Activate All Sitegroups present in Orgids
        $sitegroupModel = $this->getServiceInstance('Sitegroup');
        $updateData = array('sitegroupDisabledEntity' => 'none');
        $updateData['sitegroupDisabledEntityBy'] = "none";
        $updateData['sitegroupStatus'] = $config['status_code']['active'];
        $where = array('orgIds' => $orgIds);
        $where['disabledEntity'] = "org";
        $where['disabledEntityBy'] = $actionBy;
        $sitegroupModel->updateSitegroupData($updateData, $where);

        //Activate All Sites present in Orgids
        $siteModel = $this->getServiceInstance('Site');
        $updateData = array('siteDisabledEntity' => 'none');
        $updateData['siteDisabledEntityBy'] = "none";
        $updateData['siteStatus'] = $config['status_code']['active'];
        $where = array('orgIds' => $orgIds);
        $where['disabledEntity'] = "org";
        $where['disabledEntityBy'] = $actionBy;
        $siteModel->updateSiteData($updateData, $where);

        //Activate All Services present in Orgids
        $serviceModel = $this->getServiceInstance('Service');
        $updateData = array('serviceDisabledEntity' => 'none');
        $updateData['serviceDisabledEntityBy'] = "none";
        $updateData['serviceStatus'] = "enable";
        $where = array('orgIds' => $orgIds);
        $where['disabledEntity'] = "org";
        $where['disabledEntityBy'] = $actionBy;
        $serviceModel->updateServiceData($updateData, $where);

        //Get Devices Disabled by $actionBy in Orgids
        $filterData['orgIds'] = $orgIds;
        $filterData['disabledEntity'] = "org";
        $filterData['disabledEntityBy'] = $actionBy;
        $deviceModel = $this->getServiceInstance('Device');
        $devices = $deviceModel->getDevicesByOrg($filterData, 0, 0, 0);
        $commandModel = $this->getServiceInstance('Command');
        //Send Status "Up" command to devices
        $deviceIds = array();
        foreach ($devices as $device) {
            $command = $commandModel->generateEditMsoloCommand(array('deviceStatus' => "up"), $device['deviceMac']);
            $commandEntity = $commandModel->getEntityInstance(true);
            $commandEntity->setCommandMac($device['deviceMac']);
            $commandEntity->setCommandJson(json_encode($command));
            $commandEntity->setCommandCreatedOn(time());
            $commandEntity->setCommandStatus(1);
            $commandModel->createCommand($commandEntity);
            $deviceIds[] = $device['deviceId'];
        }

        //Enable all deviceIds
        $updateData = array('deviceDisabledEntity' => 'none');
        $updateData['deviceDisabledEntityBy'] = "none";
        $updateData['deviceStatus'] = $config['status_code']['active'];

        $deviceModel->updateDevice($updateData, $deviceIds, $multipleDevices = true);
        //Notify Org of Activation
        $emailData = array('subject' => 'Your Organization has been Activated');
        $this->notifyOrg($orgId, $emailData, "activate_subscription.phtml");
    }

    private function disableSitegroupActions($sitegroupId, $actionBy) {
        $config = $this->getServiceInstance('config');
        //Disable All Sites present in SitegroupId
        $siteModel = $this->getServiceInstance('Site');
        $updateData = array('siteDisabledEntity' => 'sitegroup');
        $updateData['siteDisabledEntityBy'] = $actionBy;
        $updateData['siteStatus'] = $config['status_code']['inactive'];
        $where = array('sitegroupIds' => array($sitegroupId));
        $where['status'] = $config['status_code']['active'];
        $siteModel->updateSiteData($updateData, $where);
        //get sites in sitegroup 
        $siteResult = $siteModel->getSitesBySitegroupId($sitegroupId);
        $siteIds = array();
        foreach ($siteResult as $key => $siteData) {
            $siteIds[] = $siteData['siteId'];
        }
        //Get Services present in Sitegroupid
        $serviceIds = array();
        $scModel = $this->getServiceInstance('ServiceContext');
        $serviceContextRows = $scModel->getServiceContextByContextType("sitegroup", $sitegroupId);
        foreach ($serviceContextRows as $serviceContextRow) {
            $serviceIds[] = $serviceContextRow['service_context_sid_fk'];
        }
        //disable services inside site of given sitegroup
        $serviceContextRowsSite = $scModel->getServiceContextByContextType("site", $siteIds);
        foreach ($serviceContextRowsSite as $serviceContextRow) {
            $serviceIds[] = $serviceContextRow['service_context_sid_fk'];
        }
        //Disable All Services present in Orgids
        $serviceModel = $this->getServiceInstance('Service');
        $updateData = array('serviceDisabledEntity' => 'sitegroup');
        $updateData['serviceDisabledEntityBy'] = $actionBy;
        $updateData['serviceStatus'] = "disable";
        $where = array('serviceIds' => $serviceIds);
        $where['status'] = $config['status_code']['active'];
        $serviceModel->updateServiceData($updateData, $where);

        //Get Devices present in SitegroupId
        $filterData['activeDevices'] = $config['status_code']['active'];
        $filterData['sitegroupIds'] = array($sitegroupId);

        $deviceSiteRows = $siteModel->getDevicesBySitegroupId($filterData);
        $commandModel = $this->getServiceInstance('Command');

        //Send Status"Down" command to devices
        $deviceIds = array();
        foreach ($deviceSiteRows as $deviceSiteRow) {
            foreach ($deviceSiteRow['meruDeviceSite'] as $device) {
                $deviceMac = $device['devicesiteDeviceidFk']['deviceMac'];
                if (!empty($deviceMac)) {
                    $command = $commandModel->generateEditMsoloCommand(array('deviceStatus' => "down"), $deviceMac);
                    $commandEntity = $commandModel->getEntityInstance(true);
                    $commandEntity->setCommandMac($deviceMac);
                    $commandEntity->setCommandJson(json_encode($command));
                    $commandEntity->setCommandCreatedOn(time());
                    $commandEntity->setCommandStatus(1);
                    $commandModel->createCommand($commandEntity);
                    $deviceIds[] = $device['devicesiteDeviceidFk']['deviceId'];
                }
            }
        }

        //Disable all deviceIds
        $deviceModel = $this->getServiceInstance('Device');
        $updateData = array('deviceDisabledEntity' => 'sitegroup');
        $updateData['deviceDisabledEntityBy'] = $actionBy;
        $updateData['deviceStatus'] = $config['status_code']['inactive'];

        $deviceModel->updateDevice($updateData, $deviceIds, $multipleDevices = true);
    }

    private function enableSitegroupActions($sitegroupId, $actionBy) {
        $config = $this->getServiceInstance('config');

        //Activate All Sites present in SitegroupId
        $siteModel = $this->getServiceInstance('Site');
        $updateData = array('siteDisabledEntity' => 'none');
        $updateData['siteDisabledEntityBy'] = "none";
        $updateData['siteStatus'] = $config['status_code']['active'];
        $where = array('sitegroupIds' => array($sitegroupId));
        $where['disabledEntity'] = "sitegroup";
        $where['disabledEntityBy'] = $actionBy;
        $siteModel->updateSiteData($updateData, $where);
        //get sites in sitegroup 
        $siteResult = $siteModel->getSitesBySitegroupId($sitegroupId);
        $siteIds = array();
        foreach ($siteResult as $key => $siteData) {
            $siteIds[] = $siteData['siteId'];
        }
        //Get Services present in Sitegroupid
        $serviceIds = array();
        $scModel = $this->getServiceInstance('ServiceContext');
        $serviceContextRows = $scModel->getServiceContextByContextType("sitegroup", $sitegroupId);
        foreach ($serviceContextRows as $serviceContextRow) {
            $serviceIds[] = $serviceContextRow['service_context_sid_fk'];
        }
        //disable services inside site of given sitegroup
        $serviceContextRowsSite = $scModel->getServiceContextByContextType("site", $siteIds);
        foreach ($serviceContextRowsSite as $serviceContextRow) {
            $serviceIds[] = $serviceContextRow['service_context_sid_fk'];
        }
        //Activate All Services present in SitegroupId
        $serviceModel = $this->getServiceInstance('Service');
        $updateData = array('serviceDisabledEntity' => 'none');
        $updateData['serviceDisabledEntityBy'] = "none";
        $updateData['serviceStatus'] = "enable";
        $where = array('serviceIds' => $serviceIds);
        $where['disabledEntity'] = "sitegroup";
        $where['disabledEntityBy'] = $actionBy;
        $serviceModel->updateServiceData($updateData, $where);

        //Get Devices present in SitegroupId
        $filterData = array();
        $filterData['deviceDisabledEntity'] = "sitegroup";
        $filterData['deviceDisabledEntityBy'] = $actionBy;
        $filterData['sitegroupIds'] = array($sitegroupId);

        $deviceSiteRows = $siteModel->getDevicesBySitegroupId($filterData);
        $commandModel = $this->getServiceInstance('Command');
        //Send Status"Down" command to devices
        $deviceIds = array();
        foreach ($deviceSiteRows as $deviceSiteRow) {
            foreach ($deviceSiteRow['meruDeviceSite'] as $device) {
                if (!empty($device['devicesiteDeviceidFk']['deviceMac'])) {
                    $deviceMac = $device['devicesiteDeviceidFk']['deviceMac'];
                    $command = $commandModel->generateEditMsoloCommand(array('deviceStatus' => "up"), $deviceMac);
                    $commandEntity = $commandModel->getEntityInstance(true);
                    $commandEntity->setCommandMac($deviceMac);
                    $commandEntity->setCommandJson(json_encode($command));
                    $commandEntity->setCommandCreatedOn(time());
                    $commandEntity->setCommandStatus(1);
                    $commandModel->createCommand($commandEntity);
                    $deviceIds[] = $device['devicesiteDeviceidFk']['deviceId'];
                }
            }
        }

        //Enable all deviceIds
        $deviceModel = $this->getServiceInstance('Device');
        $updateData = array('deviceDisabledEntity' => 'none');
        $updateData['deviceDisabledEntityBy'] = "none";
        $updateData['deviceStatus'] = $config['status_code']['active'];
        $deviceModel->updateDevice($updateData, $deviceIds, $multipleDevices = true);
    }

    private function disableSiteActions($siteId, $actionBy) {
        $config = $this->getServiceInstance('config');

        //Get Services present in Siteid
        $serviceIds = array();
        $scModel = $this->getServiceInstance('ServiceContext');
        $serviceContextRows = $scModel->getServiceContextByContextType("site", $siteId);
        foreach ($serviceContextRows as $serviceContextRow) {
            $serviceIds[] = $serviceContextRow['service_context_sid_fk'];
        }
        //Disable All Services with $serviceIds
        $serviceModel = $this->getServiceInstance('Service');
        $updateData = array('serviceDisabledEntity' => 'site');
        $updateData['serviceDisabledEntityBy'] = $actionBy;
        $updateData['serviceStatus'] = "disable";
        $where = array('serviceIds' => $serviceIds);
        $where['status'] = $config['status_code']['active'];
        $serviceModel->updateServiceData($updateData, $where);

        //Get Devices present in SiteId
        $params = array();
        $params['activeDevices'] = $config['status_code']['active'];
        $deviceSiteModel = $this->getServiceInstance('DeviceSite');
        $deviceSiteRows = $deviceSiteModel->getDeviceInSite($siteId, 0, $params);

        $commandModel = $this->getServiceInstance('Command');
        //Send Status"Down" command to devices
        $deviceIds = array();
        foreach ($deviceSiteRows as $device) {
            $deviceMac = $device['deviceMac'];
            $command = $commandModel->generateEditMsoloCommand(array('deviceStatus' => "down"), $deviceMac);
            $commandEntity = $commandModel->getEntityInstance(true);
            $commandEntity->setCommandMac($deviceMac);
            $commandEntity->setCommandJson(json_encode($command));
            $commandEntity->setCommandCreatedOn(time());
            $commandEntity->setCommandStatus(1);
            $commandModel->createCommand($commandEntity);
            $deviceIds[] = $device['deviceId'];
        }

        //Disable all deviceIds
        $deviceModel = $this->getServiceInstance('Device');
        $updateData = array('deviceDisabledEntity' => 'site');
        $updateData['deviceDisabledEntityBy'] = $actionBy;
        $updateData['deviceStatus'] = $config['status_code']['inactive'];

        $deviceModel->updateDevice($updateData, $deviceIds, $multipleDevices = true);
    }

    private function enableSiteActions($siteId, $actionBy) {
        $config = $this->getServiceInstance('config');

        //Get Services present in Sitegroupid
        $serviceIds = array();
        $scModel = $this->getServiceInstance('ServiceContext');
        $serviceContextRows = $scModel->getServiceContextByContextType("site", $siteId);
        foreach ($serviceContextRows as $serviceContextRow) {
            $serviceIds[] = $serviceContextRow['service_context_sid_fk'];
        }
        //Activate All Services present in SiteId
        $serviceModel = $this->getServiceInstance('Service');
        $updateData = array('serviceDisabledEntity' => 'none');
        $updateData['serviceDisabledEntityBy'] = "none";
        $updateData['serviceStatus'] = "enable";
        $where = array('serviceIds' => $serviceIds);
        $where['disabledEntity'] = "site";
        $where['disabledEntityBy'] = $actionBy;
        $serviceModel->updateServiceData($updateData, $where);

        //Get Devices present in SiteId
        $params = array();
        $params['deviceDisabledEntity'] = "site";
        $params['deviceDisabledEntityBy'] = $actionBy;
        $deviceSiteModel = $this->getServiceInstance('DeviceSite');
        $deviceSiteRows = $deviceSiteModel->getDeviceInSite($siteId, 0, $params);

        $commandModel = $this->getServiceInstance('Command');
        //Send Status"Down" command to devices
        $deviceIds = array();
        foreach ($deviceSiteRows as $device) {
            $deviceMac = $device['deviceMac'];
            $command = $commandModel->generateEditMsoloCommand(array('deviceStatus' => "up"), $deviceMac);
            $commandEntity = $commandModel->getEntityInstance(true);
            $commandEntity->setCommandMac($deviceMac);
            $commandEntity->setCommandJson(json_encode($command));
            $commandEntity->setCommandCreatedOn(time());
            $commandEntity->setCommandStatus(1);
            $commandModel->createCommand($commandEntity);
            $deviceIds[] = $device['deviceId'];
        }

        //Enable all deviceIds
        $deviceModel = $this->getServiceInstance('Device');
        $updateData = array('deviceDisabledEntity' => 'none');
        $updateData['deviceDisabledEntityBy'] = "none";
        $updateData['deviceStatus'] = $config['status_code']['active'];

        $deviceModel->updateDevice($updateData, $deviceIds, $multipleDevices = true);
    }

    public function suspendOrgNotificationAction() {
        $this->message = "################################################################ \n";
        $currentTimestamp = time();
        $config = $this->getServiceInstance('config');
        $eaModel = $this->getServiceInstance('EntityAction');
        $filterData = array('status' => $config['status_code']['active']);
        $filterData['entity'] = "org";
        $filterData['action'] = "disable";

        $filterData['suspendNotify'] = $currentTimestamp;
        $result = $eaModel->getData($filterData);

        $this->message .= "List to be Suspended:- \n" . json_encode($result) . "\n";
        foreach ($result as $data) {
            $this->message .= "************************************** \n";
            $emailData = array();
            $emailData['suspendDays'] = ceil(($data['actionAt'] - $currentTimestamp) / 60 / 60 / 24);
            $emailData['subject'] = "Account will get suspended after " . $emailData['suspendDays'] . "days";

            $this->notifyOrg($data['entityId'], $emailData, "suspend_account_notify.phtml");
            $this->message .= "************************************** \n";
        }
        $this->message .= "\n";
        $this->message .= "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ \n";
        $this->message .= "Date:-" . date('Y-m-d H:i:s') . "\n";
        $this->message .= "################################################################ \n";
        echo $this->message;
    }

    /**
     * Send Notification to Organization
     * 
     * @param int $orgId
     * @param array $emailData
     * @param string $template
     */
    public function notifyOrg($orgId, $emailData, $template) {
        $orgModel = $this->getServiceInstance('Organization');
        $orgUsers = $orgModel->getOrgUsers($orgId);
        $commonModel = $this->getServiceInstance('Common');
        $this->message .= "Org Details and Its Users:- \n" . json_encode($orgUsers) . "\n";

        foreach ($orgUsers[0]['meruUser'] as $user) {
            if (!empty($user['userEmail'])) {
                $emailData['toEmail'] = $user['userEmail'];
                $emailData['userName'] = $user['userName'];
                $emailData['orgName'] = $orgUsers[0]['orgName'];

                $commonModel->sendEmail($emailData, $template);
            }
        }
    }

    /**
     * Initiate Upgrade Commands for devices
     * Release will be scheduledby CSP admin. 
     * Based on the schedule the upgrade will be happening in the schedules time
     * Window of 1hr. Upgrade will be happening on LocalTimeZone of AP
     */
    public function scheduledFirmwareUpgradeAction() {
        echo "########## Scheduled Firmware Upgrade Started \n";
        $firmwareReleaseModel = $this->getServiceInstance('FirmwareRelease');
        $filterData['status'] = 'scheduled';                
        $dateList = array();
        $time = time();        
        $filterData['today'] = date("Y-m-d", $time);
        //Get any Scheduled releases for today
        $scheduledReleases = $firmwareReleaseModel->getFwReleases($filterData);
        print_r($scheduledReleases);
        $releaseIdList = array();
        foreach ($scheduledReleases as $release) {
            echo "#### Version: " . $version['firmwareVersion'];
            $releaseId = $release['releaseId'];
            $date = $release['releaseDate'];
            $time = $release['releaseTime'];
            $fId = $release['firmwareId'];
            $siteList = $release['releaseSiteids'];
            $deviceState = $release['releaseDeviceState'];
            $orgId = $release['orgId'];
            $response = $this->generateAPReleases($orgId,$siteList,$date,$time,$fId,$releaseId,$deviceState);       
            $releaseIdList[] = $releaseId;
            //print_r($response);
            //Generate Upgrade commands from Ap Releases
        }
        //Mark Selected Releases as processed
        if(!empty($releaseIdList)){
            $filterData['status'] = 'completed';
            $firmwareReleaseModel->updateReleaseStatus($releaseIdList,$filterData);
        }
        //Generate Commands for AP
        $this->processAPReleases($time);
        
    }
    
    public function generateAPReleases($orgId,$siteList,$date,$time,$fId,$releaseId,$deviceState){
        echo "Generating AP Release for ORG:".$orgId." SiteList:".$siteList." Date/Time:".$date."/".$time;                
        $siteFilter = false;
        $deviceList = array();
        if($siteList !=""){
              $releaseSiteids = explode(',', $siteList);
	      if (! empty($releaseSiteids)) {
                  echo "+++Site Filter ###";
                     $siteFilter = true;
                     $deviceSiteModel = $this->getServiceInstance('DeviceSite');           
	             $deviceSiteList = $deviceSiteModel->getDevicesInSites($releaseSiteids);         
                     foreach($deviceSiteList as $ds){
                         $device = array();
                         $device['deviceId'] = $ds['devicesiteDeviceidFk']['deviceId'];
                         $device['deviceSlnumber'] = $ds['devicesiteDeviceidFk']['deviceSlnumber'];                         
                         $device['deviceMac'] = $ds['devicesiteDeviceidFk']['deviceMac'];                         
                         $device['deviceCountryCode'] = $ds['devicesiteDeviceidFk']['deviceCountryCode'];
                         $device['deviceTimezone'] = $ds['devicesiteDeviceidFk']['deviceTimezone'];
                         $deviceList [$ds['devicesiteDeviceidFk']['deviceId']] = $device;
                     }
              }
        }
        if(!$siteFilter || $deviceState!=1 ){
            //If a Site filter is not added and Device state is not set to configured only
            echo "--No Site Filter";
            $deviceModel = $this->getServiceInstance('Device');           
            $filterData = array();
            $filterData['notDeleted'] = true;
            if($deviceState==2){
                $filterData['unconfigure'] = true;
            }           
            if($deviceState==1){
                $filterData['configure'] = true;
            }           
            $filterData['org_id'] = $orgId;
            echo "GGGGGGGGGGGXXXXXXXXXXXXX";
            print_r($filterData);            
            echo "GGGGGGGGGGGXXXXXXXXXXXXX";
            $tempdeviceList = $deviceModel->getDevicesByOrg($filterData, 0, 0, 0);
            foreach($tempdeviceList as $dInfo){                
                $deviceList[$dInfo['deviceId']] = $dInfo;
            }
        }
        echo "#######";
        print_r($deviceList);
        echo "###<><><><><>###";
        $now = time();
        $commonModel = $this->getServiceInstance('Common');           
        $utcTimeStamp = strtotime($date." ".$time);        
        $reader = new Ini();
        $zoneMapper = $reader->fromFile(MODULE_PATH . '/Application/config/timeZonemap.ini');        
        $timeZones = $commonModel->timezones();                
        $deviceUpgradeModel = $this->getServiceInstance('DeviceUpgrade');
        foreach ($deviceList as $dev) {            
            $zoneText = trim (str_replace (array('(', ')'), '_', $dev['deviceTimezone']));
            if ($zoneMapper['timeZoneMap'][$zoneText] != '') {
                $zoneCode       = $zoneMapper['timeZoneMap'][$zoneText];
                $offset         = $timeZones[$zoneCode];
                $localTimeStamp = $utcTimeStamp - ($offset * 3600);
            }
            else
                $localTimeStamp = $utcTimeStamp; //UTC

            if ($now > $localTimeStamp) {
                echo "\n##Schedule for tomorrow##".$zoneText;
                $localTimeStamp += 24 * 3600;
            }
            //Create Entry in Device_Release            
            $deviceUpgrade = $deviceUpgradeModel->getEntityInstance(true);
            $objectManager = $deviceUpgradeModel->getObjectManager($this->getServiceLocator());
            $orgProxyInstance = $objectManager->getReference('Application\Entity\MeruOrg', $orgId);
            $deviceProxyInstance = $objectManager->getReference('Application\Entity\MeruDevice', $dev['deviceId']);
            $firmwareProxyInstance = $objectManager->getReference('Application\Entity\MeruFirmware', $fId);
            $releaseProxyInstance = $objectManager->getReference('Application\Entity\MeruFirmwareRelease', $releaseId);
            $deviceUpgrade->setUpgradeOrgid($orgProxyInstance);
            $deviceUpgrade->setUpgradeDid($deviceProxyInstance);
            $deviceUpgrade->setUpgradeFwid($firmwareProxyInstance);
            $deviceUpgrade->setUpgradeReleaseid($releaseProxyInstance);
            $deviceUpgrade->setUpgradeMac($dev['deviceMac']);
            $deviceUpgrade->setUpgradeSln($dev['deviceSlnumber']);
            $deviceUpgrade->setUpgradeTs($localTimeStamp);
            $deviceUpgrade->setUpgradeStatus(0);
            $deviceUpgradeModel->saveDeviceUpgrade($deviceUpgrade);
            echo "\n".$dev['deviceMac']."\n";                  
        }
    }
    
    /**
     * Read all pending upgrades during the given time period and process
     * Upgrade.
     */
    public function processAPReleases($time){ 
        //Generate commands for Upgrade, matches the current timestamp window
        echo "######Processing AP Releases";
        $startTime = $time - (1.5 * 60 * 60);
        $deviceUpgradeModel = $this->getServiceInstance('DeviceUpgrade');        
        $firmwareModel = $this->getServiceInstance('Firmware');        
        $commandModel = $this->getServiceInstance('command');        
        $apReleases = $deviceUpgradeModel->activeDeviceUpgrades($startTime,$time,0);
        $config = $this->getServiceInstance('config');
        $s3Path = $config['firmware_path'];
        $apMacList = array();
        $apSlnList = array();
        $upgradeIdList = array();
        $logData = array();
        $logData[] = array('SNo.', 'Serial Number', 'MAC Address','Firmware Version');
        $Si = 1;
        foreach($apReleases as $apRelease){
            $fwData = array();
            $fwData['url'] = $s3Path . "/" . $apRelease['upgradeFwid']['firmwareVersion'];
            $fwData['file'] = $apRelease['upgradeFwid']['firmwareFilename'];
            $command = $commandModel->generateUpgradeCommand($fwData, $apRelease['upgradeMac']);
            $commandModel->insertCommand ($command);            
            $upgradeIdList[] = $apRelease['upgradeId'];
            $apSlnList[] = $apRelease['upgradeSln'];
            $apMacList[] = $apRelease['upgradeMac'];            
            $logData[] = array($Si++, $apRelease['upgradeSln'], $apRelease['upgradeMac'],$apRelease['upgradeFwid']['firmwareVersion']); 
        }
        
        $apCount = sizeof($apMacList);
        if($apCount > 0){
           $commandModel->doFactoryReset ($apMacList);
            $eventlogInstance                = $this->getServiceInstance ('Eventlog');
            $logMessage                      = "Scheduler Initiated Firmware Upgrade command for " . $apCount . " APs";
            //Need to add Event Logs
            //$eventlogInstance->createLog('firmware-upgrade', 'release-now', $firmwareId, '', 'success', "", "", 0, 0, $logMessage, $logData);
            
            //Update Upgrades as processed
            $filterData                      = array();
            $filterData['upgradeReleasedOn'] = $time;
            $filterData['status']            = 1;
            $deviceUpgradeModel->updateDeviceUpgrade ($upgradeIdList, $filterData);
        }        

    }

    /**
     * 
     * @param type $fwId
     * Process Upgrade related to one version of Firmware
     */
    public function processBatchUpgrade($fwId = 0) {
        echo "########## Batch Firmware Upgrade Started for Firmware Id" . $fwId . "\n";
        $organizationModel = $this->getServiceInstance('Organization');
        $org = $organizationModel->getActiveOrganization();
        $upgradeSameVersionList = array();
        $upgradeSuccessList = array();
        $expiredReleases = array();
        $activeReleases = array();
        $response['status'] = false;
        $response['message'] = "No Releases scheduled!!";
        $time = time();
        $response['startTime'] = $time;
        $minDate = date("Y-m-d", strtotime("-2 days", $time));
        $config = $this->getServiceInstance('config');
        $s3Path = $config['firmware_path'];
        $orgIdList = array();
        $mspList = array();
        $tenantList = array();
        //Read the scheduled Releases
        $firmwareReleaseModel = $this->getServiceInstance('FirmwareRelease');
        $filterData['status'] = 'scheduled';
        $filterData['fwId'] = $fwId;
        $releases = $firmwareReleaseModel->getFwReleases($filterData, 0, 0);
        foreach ($releases as $release) {
            //Capture the expired Schedules                       
            $relDate = date("Y-m-d", strtotime($release['releaseDate']));
            if ($relDate < $minDate) {
                $expiredReleases[] = $release['releaseId'];
            } else {
                $activeReleases [] = $release;
                $orgIdList[] = $release['orgId'];
                /*if ($release['orgType'] == 'msp') {
                    $mspList[] = $release['orgId'];
                }*/
            }
        }

        //Get the Child Organizations  
        /*if (sizeof($mspList) > 0) {
            $orgModel = $this->getServiceInstance('Organization');
            $tenantList = $orgModel->getChildOrgByParenetList($mspList);
            foreach ($tenantList as $tenant) {
                $orgIdList[] = $tenant['orgId'];
            }
        }*/

        if (sizeof($orgIdList) > 0) {
            //Select Timezones available in Devices            
            $deviceModel = $this->getServiceInstance('device');
            $commonModel = $this->getServiceInstance('common');
            $commandModel = $this->getServiceInstance('command');
            $deviceSiteModel = $this->getServiceInstance('DeviceSite');
            $timeZones = $deviceModel->getDevicesTimezones($orgIdList);
            $dateformat = 'Y-m-d H:i:s';
            if (sizeof($timeZones) > 0) {
                $reader = new Ini();
                $zoneMapper = $reader->fromFile(MODULE_PATH . '/Application/config/timeZonemap.ini');
                foreach ($timeZones as $zone) {
                    $zoneText = trim(str_replace(array('(', ')'), '_', $zone['deviceTimezone']));
                    if ($zoneMapper['timeZoneMap'][$zoneText] != '')
                        $localZone = $zoneMapper['timeZoneMap'][$zoneText];
                    else
                        $localZone = 'UTC';
                    //echo $zone['deviceTimezone']."  Zone-- ".$localZone."\n"; 
                    $localtimeStamp = $commonModel->utc_to_local($time, $localZone, false);
                    $maxUpgradetimeStamp = $localtimeStamp + (60 * 60);
                    $minupgrade_date_time = date($dateformat, $localtimeStamp);
                    $maxupgrade_date_time = date($dateformat, $maxUpgradetimeStamp);
                    foreach ($activeReleases as $release) {
                        $fwData['url'] = $s3Path . "/" . $release['firmwareVersion'];
                        $fwData['file'] = $release['firmwareFilename'];
                        $fwModel = $release['firmwareModel'];
                        $fwReleaseSiteids = explode(',', $release['releaseSiteids']);
                        $filterArray = array();
                        //$filterArray = array('firmwareModel' => $fwModel); -- Removing the Model number cross check
                        $scheduledDateTime = $release['releaseDate'] . " " . $release['releaseTime'];
                        if (($minupgrade_date_time <= $scheduledDateTime) && ($scheduledDateTime <= $maxupgrade_date_time)) {
                            //Get DeviceDetails in ORG with this TimeZone
                            echo "Time Matches.. " . $localZone . "-->" . $zone['deviceTimezone'] . "\n";
                            if (empty($fwReleaseSiteids)) {
                                $scheduledDevices = $deviceModel->getDevicesByTimezone($orgIdList, $zone['deviceTimezone'], $filterArray);
                            } else {
                                $filterArray['timeZone'] = $zone['deviceTimezone'];
                                $deviceSiteList = $deviceSiteModel->getDevicesInSites($fwReleaseSiteids, $filterArray);
                                foreach ($deviceSiteList as $deviceSite) {
                                    $scheduledDevices[] = $deviceSite['devicesiteDeviceidFk'];
                                }
                            }
                            //print_r($scheduledDevices);
                            foreach ($scheduledDevices as $device) {
                                if ($device['deviceSwversion'] != $release['firmwareVersion']) {                                    
                                    //echo "Sending Upgrade Command for ". $device['deviceMac'];                                    
                                    if (stristr($device['deviceSlnumber'], 'xp8i') !== FALSE) {
                                        //Check for REV2 AP - Temporary fix
                                        $command = $commandModel->generateUpgradeCommand($fwData, $device['deviceMac']);
                                        $commandModel->insertCommand($command);
                                        $commandModel->doFactoryReset(array($device['deviceMac']));
                                        $upgradeSuccessList[] = $device['deviceSlnumber'];
                                    }
                                } else {
                                    $upgradeSameVersionList[] = $device['deviceSlnumber'];
                                }
                            }
                            //Update the Schedule as complete
                        }
                        else{
                             $bufferMinDate = date("Y-m-d H:i:s", strtotime("-1 days", strtotime($minupgrade_date_time)));
                             $bufferMaxDate = date("Y-m-d H:i:s", strtotime("-1 days", strtotime($maxupgrade_date_time)));
                             
                            
                        }
                    }
                }
            }
        }
        $response['endTime'] = time();
        $response['status'] = true;
        $response['message'] = "Execution Complete!!";
        $response['successList'] = $upgradeSuccessList;
        $response['sameVersion'] = $upgradeSameVersionList;
        $response['expiredRelease'] = $expiredReleases;
        $response['activeRelease'] = $activeReleases;
        if (sizeof($expiredReleases) > 0)
            $firmwareReleaseModel->removeExpiredReleases($expiredReleases);
        return $response;
    }

}
