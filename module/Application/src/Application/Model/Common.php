<?php

/**
 * This model is used to handle Application module related functions  
 * @package Alarm
 * @author VVDNTechnologies < >
 */

namespace Application\Model;

use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplateMapResolver;

class Common {

    protected $serviceManager;

    public function getServiceLocator() {
        return $this->serviceManager;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceManager = $serviceLocator;
    }

    /**
     * This function will return current timestamp
     */
    public function getCurrentTimeStamp() {
        return time();
    }

    public function makeHttpRequest($url) {
        /* cURL Resource */
        $ch = curl_init();
        /* Set URL */
        curl_setopt($ch, CURLOPT_URL, $url);

        /* Tell cURL to return the output */
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        /* Tell cURL NOT to return the headers */
        curl_setopt($ch, CURLOPT_HEADER, false);

        /* Execute cURL, Return Data */
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    public function makePostHttpRequest($url, $data) {

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data))
        );

        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    /**
     * Send Email With Attachment OR Without Attachment
     * @param: array $emailData
     * @param: string $templatefile
     * @param: string $filename
     * @param: string $attachment
     * @return send email
     */
    public function sendEmail($emailData, $templatefile, $filename, $attachment = '') {

        $view = new PhpRenderer();
        $resolver = new TemplateMapResolver();
        $resolver->setMap(array(
            'mailTemplate' => MODULE_APPLICATION_PATH
            . '/view/email/templates/' . $templatefile
        ));
        $view->setResolver($resolver);
        $viewModel = new ViewModel();
        $viewModel->setTemplate('mailTemplate')
                ->setVariables(array(
                    'data' => $emailData
        ));
        $content = $view->render($viewModel);
        //Set Html Mail Type
        $text = new MimePart('');
        $text->type = "text/plain";

        $html = new MimePart($content);
        $html->type = "text/html";

        // Check if mail type is attachment image
        if ((isset($attachment)) && ($filename != '')) {
            $content = new MimeMessage();
            $content->setParts(array($text, $html));
            $contentPart = new MimePart($content->generateMessage());
            $contentPart->type = 'multipart/alternative;' . PHP_EOL . ' boundary="' . $content->getMime()->boundary() . '"';
            // Check Attachment Type      
            if ($attachment == 'image') {
                $ext = $this->getFileExtension($filename);
                $fileName = PUBLIC_PATH . "/feedback-assets/" . $filename;
                $attachment = new MimePart(fopen($fileName, 'r'));
                if ($ext == 'pdf') {
                    $attachment->type = 'application/pdf';
                } else {
                    $attachment->type = 'image/jpg';
                }
                $explodeFileName = explode('*_*', $filename);
                $attachment->filename = $explodeFileName[1];
            } elseif ($attachment == 'pdf') {
                $fileName = PUBLIC_PATH . "/report-pdf/" . $filename;
                $attachment = new MimePart(fopen($fileName, 'r'));
                $attachment->type = 'application/pdf';
                $attachment->filename = $filename;
            }
            $attachment->encoding = 'base64';
            $attachment->disposition = 'attachment';
        }

        $body = new MimeMessage();
        // Check if mail type is attachment
        if ((isset($attachment)) && ($filename != '')) {
            $body->setParts(array($contentPart, $attachment));
        } else {
            $body->setParts(array($html));
        }

        $transport = new SmtpTransport();
        $config = $this->getServiceLocator()->get('config');
        $options = new SmtpOptions($config['smtp_options']);
        $transport->setOptions($options);
        $message = new Message();
        $message->addTo($emailData['toEmail'])
                ->addFrom($config['sender_email'])
                ->setEncoding('UTF-8');
        if (!empty($emailData['ccEmail'])) {
            $message->addCc($emailData['ccEmail']);
        }
        if (!empty($emailData['bccEmail'])) {
            $message->addBcc($emailData['bccEmail']);
        }
        $message->setSubject($emailData['subject'])
                ->setBody($body);
        $transport->send($message);
    }

    /*
     * Remove white spaces
     */

    public function removeWhiteSpaces($string) {
        $result = preg_replace('/\s+/', '', $string);
        return $result;
    }

    /*
     * Get file extension
     */

    public function getFileExtension($filename) {
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        return $ext;
    }

    /**
     * File Download
     * @Param: $fileName -> Name of the file to be downloaded 
     * @Param: $fileNameDownload -> File name that shows in download
     * @Return: set headers
     */
    public function fileDownload($fileName, $fileNameDownload) {
        $response = new \Zend\Http\Response\Stream();
        $response->setStream(fopen($fileName, 'r'));
        $response->setStatusCode(200);
        $headers = new \Zend\Http\Headers();
        $headers->addHeaderLine('Content-Type', 'Meru Firmware File Download')
                ->addHeaderLine('Content-Disposition', 'attachment; filename="' . $fileNameDownload . '"')
                ->addHeaderLine('Content-Length', filesize($fileName));
        $response->setHeaders($headers);
        return $response;
    }

    /*
     * Change nl to comma(,)
     */

    public function changeNl2Comma($content) {
        $str = preg_replace("/((\r)|(\r?))/", '', trim($content));
        $strstr = preg_replace('#\n+#', ', ', $str);
        return $strstr;
    }

    /**
     * 
     * @param type $time - TimeStamp
     * @param type $timezone - OffsetTimezone(UM3,UP10 etc)
     * @param type $dst- Daylight Savings
     * @return int - timeStamp value curresponding to localTimeZone
     */
    public function utc_to_local($time = '', $timezone = 'UTC', $dst = FALSE) {
        if ($time == '') {
            return time();
        }
        $offset = $this->timezones($timezone);
        $time += $offset * 3600;
        if ($dst == TRUE) {
            $time += 3600;
        }
        return $time;
    }

    public function timezones($tz = '') {
        // Note: Don't change the order of these even though
        // some items appear to be in the wrong order
        $zones = array(
            'UM12' => -12,
            'UM11' => -11,
            'UM10' => -10,
            'UM95' => -9.5,
            'UM9' => -9,
            'UM8' => -8,
            'UM7' => -7,
            'UM6' => -6,
            'UM5' => -5,
            'UM45' => -4.5,
            'UM4' => -4,
            'UM35' => -3.5,
            'UM3' => -3,
            'UM2' => -2,
            'UM1' => -1,
            'UTC' => 0,
            'UP1' => +1,
            'UP2' => +2,
            'UP3' => +3,
            'UP35' => +3.5,
            'UP4' => +4,
            'UP45' => +4.5,
            'UP5' => +5,
            'UP55' => +5.5,
            'UP575' => +5.75,
            'UP6' => +6,
            'UP65' => +6.5,
            'UP7' => +7,
            'UP8' => +8,
            'UP875' => +8.75,
            'UP9' => +9,
            'UP95' => +9.5,
            'UP10' => +10,
            'UP105' => +10.5,
            'UP11' => +11,
            'UP115' => +11.5,
            'UP12' => +12,
            'UP1275' => +12.75,
            'UP13' => +13,
            'UP14' => +14
        );

        if ($tz == '') {
            return $zones;
        }

        if ($tz == 'GMT')
            $tz = 'UTC';

        return (!isset($zones[$tz])) ? 0 : $zones[$tz];
    }

    /**
     * Calculate Time From Timestamp
     * @param integer $timestamp  
     */
    public function convertTime($timestamp) {
        if ($timestamp != 0 && $timestamp != '') {
            $otherTime = date('Y-m-d h:i:s', $timestamp);
            $currentTime = date('Y-m-d h:i:s', time());
            $date1 = new \DateTime($otherTime);
            $date2 = new \DateTime($currentTime);
            $diff = $date2->diff($date1);
            $minutes = $diff->format('%i');
            $hours = $diff->format('%h');
            $days = $diff->format('%d');
            $months = $diff->format('%m');
            $years = $diff->format('%y');
            if ($years == 0) {
                if ($months == 0) {
                    if ($days == 0) {
                        if ($hours == 0) {
                            if ($minutes == 0) {
                                echo "";
                            } else {
                                if ($minutes > 1) {
                                    $response = $minutes . ' minutes';
                                } else {
                                    $response = $minutes . ' minute';
                                }
                            }
                        } else {
                            if ($hours > 1) {
                                $response = $hours . ' hours';
                            } else {
                                $response = $hours . ' hour';
                            }
                        }
                    } else {
                        if ($days > 1) {
                            $response = $days . ' days';
                        } else {
                            $response = $days . ' day';
                        }
                    }
                } else {
                    if ($months > 1) {
                        $response = $months . ' months';
                    } else {
                        $response = $months . ' month';
                    }
                }
            } else {
                if ($years > 1) {
                    $response = $years . ' years';
                } else {
                    $response = $years . ' year';
                }
            }
        } else {
            $response = "";
        }
        return $response;
    }

}
