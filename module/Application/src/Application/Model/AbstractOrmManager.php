<?php

namespace Application\Model;

use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\ServiceLocatorInterface;

abstract class AbstractOrmManager {

    protected $objectManager;

    public function setObjectManager(EntityManager $em) {
        $this->objectManager = $em;
        return $this;
    }

    /**
     * Returns the ObjectManager
     *
     * Fetches the ObjectManager from ServiceLocator if it has not been initiated
     * and then returns it
     *
     * @access public
     * @param ServiceLocatorInterface $serviceLocator
     * @return EntityManager
     */
    public function getObjectManager(ServiceLocatorInterface $serviceLocator) {
        if (null === $this->objectManager) {
            //$this->setObjectManager($serviceLocator->get('Doctrine\Common\Persistence\ObjectManager'));
            $this->setObjectManager($serviceLocator->get('Doctrine\ORM\EntityManager'));
        }
        return $this->objectManager;
    }

    abstract protected function getRepository();
    
    abstract protected function getEntityInstance();
}
