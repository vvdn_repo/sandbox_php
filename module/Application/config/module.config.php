<?php

/**
 * module.config.php handles the routes and paths for the application module 
 * @package Application
 * @author VVDN Technologies < >
 */

namespace Application;

return array(
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' => 'Application\Controller\IndexController',

        ),
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
        ),
    ),
    'router' => array(
        'routes' => array(
            'index' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action' => 'index',
                    ),
                ),
            ),
            'home' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/home[/][:flag]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action' => 'home',
                    ),
                ),
            ),
            'landing-page' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/userhome',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action' => 'userhome',
                    ),
                ),
            )  ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'view_manager' => array(
         'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(
            'sandbox/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index'
            . '/index.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
            'home/userhome' => __DIR__ . '/../view/partials'
                . '/userhome.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
                'entity-operation' => array(
                    'options' => array(
                        // add [ and ] if optional ( ex : [<doname>] )
                        'route' => 'entity-operation',
                        'defaults' => array(
                            'controller' => 'Application\Controller\CronJob',
                            'action' => 'entityOperation'
                        ),
                    ),
                ),
                
                'cron-schedule-report' => array(
                    'options' => array(
                        'route' => 'cron-schedule-report',
                        'defaults' => array(
                            'controller' => 'Application/Controller/CronJob',
                            'action' => 'cronScheduleReport'
                        ),
                    ),
                ),
            )
        )
    ),
    // SMTP mail settings

   'smtp_options' => array(
       'host' => 'smtp.gmail.com',
        'connection_class' => 'login',
        'connection_config' => array(
            'ssl' => 'tls',
            'username' => 'sandbox661@gmail.com',
            'password' => 'appu1234'
        ),
        'port' => 587
    ),
   
    // Paths for the controller APIs
    'sender_email' => 'sandbox661@gmail.com',

    's3-bucket-fw' => 'sbxu-wcnd-fw',
    's3-key' => 'AKIAIYJIG2ARNJUEEQ5Q',
    's3-secret' => '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', 
);
