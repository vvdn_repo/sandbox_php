<?php


namespace Api\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Zend\Math\Rand;
use Zend\Session\Container;
use Zend\Http\Client;
use Zend\Http\Header\SetCookie;

class DeviceApiController extends AbstractRestfulController
{


    /**
     * This function will be called in case of
     * GET request from client
     *
     * @return Response Json
     */
    protected function getServiceInstance($service)
    {
        return $this->getServiceLocator()->get($service);
    }

    public  function deviceResponseAction()
    {
        $response = array();
        $request = $this->getRequest();
        $status = false;
        $message = "No data Found";
        if ($request->isPost()) {
            $data = json_decode(file_get_contents("php://input"), true);
            $command = $data['command'];
	    if($command =="raise-alarm" && $data['reason'] =="device-disconnected" || $data['reason'] =="device-connected"){
		$deviceModel = $this->getServiceInstance('Device');
		$device_id = $deviceModel->Device_info('deviceMac',$data['device_mac']);
		$deviceId = $device_id[0]['deviceId'];
		if(empty($deviceId))    {
         	   $cubeModel = $this->getServiceInstance('Cube');
           	   $cubeInfo = $cubeModel->getCubeinfo('cubeMac',$data['device_mac']);
        	   $cube_Id = $cubeInfo[0]['cubeId'];
          	   $deviceId = $cube_Id;
          	   $data['reason']=$data['reason']."-cube";
          	   $data['type']="cube";
   		   }
		}
	    else{
		$deviceId = $this->params('deviceId');
		}
            $action= $this->processCommand($command,$data,$deviceId);

	    if($command == "keep-alive")
            {
                $status = true;$commandId = "95"; $command = "keep-alive"; $device_id = $data['device_id']; $reason = "Device id ".$device_id." is alive";
                return new JsonModel(array("status" => $status,"commandId" => $commandId, "command" => $command,"device_id" => $device_id, "reason" => $reason));
            }

            $status = true;
            $message = "Command Processed";
        }
       return new JsonModel(array("status" => $status, "message" => $message, "command" => $command));

    }

 public function processCommand($command,$data,$deviceId)
 {

     switch($command){
         case 'scan-sensor' :{
             $commandModel = $this->getServiceInstance('Command');
             $value = $commandModel->updateResult($data);
               return $value;

         }

         case 'pair-sensor' :{
             $commandModel = $this->getServiceInstance('Command');
             $value = $commandModel->updateResult($data);
             return $value;

         }

	case 'sensor-unpair' :{
             $commandModel = $this->getServiceInstance('Command');
             $value = $commandModel->updateResult($data);
             return $value;

         }

         case 'scan-ssid' :{
             $commandModel = $this->getServiceInstance('Command');
             $value = $commandModel->updateResult($data);
             return $value;

         }

         case 'update-config' :{
	     $deviceConfigModel = $this->getServiceInstance('DeviceConfig');
             $result = $deviceConfigModel->updateConfigResult($data);	
             $commandModel = $this->getServiceInstance('Command');
             $value = $commandModel->updateResult($data);
             return $value;

         }

          case 'raise-alarm' :{

             $alarmModel = $this->getServiceInstance('Alarm');

             $deviceModel = $this->getServiceInstance('Device');
             $device_info = $deviceModel->Device_info('deviceId',$deviceId);
             $user_id = $device_info[0]['device_createdby_fk'];
	         $userModel = $this->getServiceInstance('User');
             $userMode = $userModel->getMode($user_id);
           
	     if($data['reason'] =="device-connected"){
                  $command= "set-mode";
                  $data['user_mode'] = $userMode;
                  $DevicePushModel = $this->getServiceInstance('DevicePush');
                  $time_limit = 2;
                  $DevicePushModel->InsertDevicePush($command,$deviceId,$user_id,$time_limit,$userMode);
              }	
	     if($data['type'] =="cube"){
                  $value = $alarmModel->createAlarmCube($data,$deviceId,$userMode);
              }
              else{
                  $value = $alarmModel->createAlarm($data,$deviceId,$userMode);
              }
             if($value){

                // $action= $this->getThumbnail($user_id,$value,$data['file'],$deviceId);
             }
             return $value;

         }

         case 'doorbell-press' :{
             $alarmModel = $this->getServiceInstance('Alarm');
             $value = $alarmModel->createAlarm($data,$deviceId);
             return $value;

         }

	case 'temp-threshold' :{
             $commandModel = $this->getServiceInstance('Command');
             $value = $commandModel->updateResult($data);
             $deviceConfigModel = $this->getServiceInstance('DeviceConfig');
             $result = $deviceConfigModel->updateTempResult($data);
             return $value;
         }

 	case 'set-motion-block' :{
             $commandModel = $this->getServiceInstance('Command');
             $value = $commandModel->updateResult($data);
             $deviceConfigModel = $this->getServiceInstance('DeviceConfig');
             $result = $deviceConfigModel->updateMotionBlockResult($data);
             return $value;
         }
	

	case 'current-temp' :{
             $commandModel = $this->getServiceInstance('Command');
             $value = $commandModel->updateResult($data);
             return $value;

         }

	case 'fw-version' :{
             $deviceModel = $this->getServiceInstance('Device');
             $value = $deviceModel->updateFwVersion($data);
             $firmwareModel = $this->getServiceInstance('Firmware');
             $versions = $firmwareModel->getFirmwareVersion();
             $fw= $data['value'];
             $device_info = $deviceModel->Device_info('deviceId',$deviceId);
             if($device_info[0]['deviceType'] =="camera"){
                 $fw1 = $versions[0]['cameraPrimaryVersion'];
             }
             elseif($device_info[0]['deviceType'] =="doorbell"){
                 $fw1 = $versions[0]['doorbellPrimaryVersion'];
             }
             $split1 = explode('.',$fw,4);
             $split2 = explode('.',$fw1,4);
             for($i=0;$i<=3;$i++)
             {
                 if($split1[$i] < $split2[$i])
                 {
                     if($device_info[0]['deviceAutoUpdate'] ==1)
                     {

                         $devicePushModel = $this->getServiceLocator()->get('DevicePush');
                         $pushCommand = "upgrade-fw";
                         $modelCommon = $this->getServiceLocator()->get('Common');
                         $time = $modelCommon->getCurrentTimeStamp();
                         $start_time = $time+90;
                         $DevicePushId = $devicePushModel->InsertDevicePushAutoUpgrade($pushCommand,$deviceId,$device_info[0]['device_createdby_fk'],$start_time);

                     }
                     else{
                         $alarmModel = $this->getServiceInstance('Alarm');
                         $data['reason'] ="fw-update";
                         $user_id = $device_info[0]['deviceCreatedbyFk'];
                         $userModel = $this->getServiceInstance('User');
                         $userMode = $userModel->getMode($user_id);
                         $value = $alarmModel->createAlarm($data,$deviceId,$userMode);
                     }
                 }
             }
             return $value;

         }

       case 'fw-version-cube' :{
             $cubeModel = $this->getServiceInstance('Cube');
             $value = $cubeModel->updateFwVersion($data);
             $firmwareModel = $this->getServiceInstance('Firmware');
             $versions = $firmwareModel->getFirmwareVersion();
             $fw= $data['value'];
             $cubeId = explode("cube_", $data['cube_id']);
             $cube_info = $cubeModel->getCubeinfo('cubeId',$cubeId[1]);
             $fw1 = $versions[0]['cubeVersion'];
             $split1 = explode('.',$fw,4);
             $split2 = explode('.',$fw1,4);
             for($i=0;$i<=3;$i++)
             {
                 if($split1[$i] < $split2[$i])
                 {

                     /*if($device_info[0]['deviceAutoUpdate'] ==1)
                     {

                         $devicePushModel = $this->getServiceLocator()->get('DevicePush');
                         $pushCommand = "upgrade-fw";
                         $modelCommon = $this->getServiceLocator()->get('Common');
                         $time = $modelCommon->getCurrentTimeStamp();
                         $start_time = $time+90;
                         $DevicePushId = $devicePushModel->InsertDevicePushAutoUpgrade($pushCommand,$deviceId,$device_info[0]['device_createdby_fk'],$start_time);

                     }
                     else{*/
                         $alarmModel = $this->getServiceInstance('Alarm');
                         $data['reason'] ="fw-update-cube";
                         //$user_id = $cube_info[0]['cubeCreatedbyFk'];
                         //$userModel = $this->getServiceInstance('User');
                         $userMode = 1;//$userModel->getMode($user_id);
                         $value = $alarmModel->createAlarmCube($data,$cubeId[1],$userMode);
			 return $value;	

                    // }
                 }
             }
             return $value;

         }
       case 'fw-version-secondary' :{
             $deviceModel = $this->getServiceInstance('Device');
             $value = $deviceModel->updateFwVersionSecondary($data);
             /*$config = parse_ini_file('config/config.ini');
             $fw= $data['value'];
             $fw1 = $config['fw-version'];
             $split1 = explode('.',$fw,4);
             $split2 = explode('.',$fw1,4);
             for($i=0;$i<=3;$i++)
             {
                 if($split1[$i] < $split2[$i])
                 {
                     $alarmModel = $this->getServiceInstance('Alarm');
                     $device_info = $deviceModel->Device_info('deviceId',$deviceId);
                     $data['reason'] ="fw-update";
                     $user_id = $device_info[0]['device_createdby_fk'];
                     $userModel = $this->getServiceInstance('User');
                     $userMode = $userModel->getMode($user_id);
                     $value = $alarmModel->createAlarm($data,$deviceId,$userMode);
                 }
             }*/
             return $value;

         }

       case 'save-thumbnail' :{
             $alarmModel = $this->getServiceInstance('Alarm');
             $value = $alarmModel->updateThubnail($data);
             return $value;

         }

	 case 'delete-video-response' :{
             $commandModel = $this->getServiceInstance('Command');
             $value = $commandModel->updateResult($data);
             return $value;
         }

	case 'on-demand-record-response' :{
             $commandModel = $this->getServiceInstance('Command');
             $value = $commandModel->updateResult($data);
             return $value;
         }
     }

 }
 

 public function getThumbnail($userId,$alarmId,$thumbnail,$deviceId)
    {
        $cubeModel = $this->getServiceInstance('Cube');
        $cubeInfo = $cubeModel->getCubeinfo('cubeCreatedbyFk',$userId);
	$size =  sizeof( $cubeInfo);
	$last = $size-1;

        $data = array();
        $data['commandId'] = 20;
        $data['command'] = "get-thumbnail";
        $thumbnail_name = explode(".",$thumbnail);
        $data['cube_id'] = "cube_".$cubeInfo[$last]['cubeId'];
	$data['thumbnail_name'] = "device_".$deviceId."/".$thumbnail_name[0].".jpg";

        $data['alarm_id'] = $alarmId;

        $response = $this->callhttp($data);
        $device_status = json_decode($response->getBody() , true);
        if ($device_status['device_status'] == "open") {
            $status = true;
            $message = "Receiving Image";
            return new JsonModel(array("status" => $status, "message" => $message));
        } else {
            $status = false;
            $message = "Cube is Not Connected";
            return new JsonModel(array("status" => $status, "message" => $message));
        }


    }


    public function callhttp($data) {
        $config = parse_ini_file('config/config.ini');
        $client = new Client();
        $client->setUri($config['node_ip'] . ":" . $config['node_port']);
        $client->setMethod('POST');
        $data = array("commandId" => $data['commandId'], "command" => $data['command'], "device_id" => $data['cube_id'],"thumbnail_name" => $data['thumbnail_name'],"alarmId" => $data['alarm_id']);
        $asd = json_encode($data);
        $client->setRawBody($asd);
        $response = $client->send();
        return $response;
    }

}
