<?php

/**
 * Contains Api related Operations 
 * @package Api
 * @author VVDN Technologies < >
 */

namespace Api\Model;

use Application\Entity\SandboxUser;
use Application\Model\AbstractOrmManager;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\Container as SessionContainer;

use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplateMapResolver;

class ApiOperations extends AbstractOrmManager implements ServiceLocatorAwareInterface{
    protected $serviceManager;
    protected $userEntityInstance;

    public function getServiceLocator() {
        return $this->serviceManager;
    }
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceManager = $serviceLocator;
    }

    public function getRepository() {
        $repository = $this->getObjectManager($this->serviceManager)->getRepository('Application\Entity\SandboxUser');
        return $repository;
    }

    public function getEntityInstance() {
        if (null === $this->userEntityInstance) {
            $this->userEntityInstance = new SandboxUser();
        }
        return $this->userEntityInstance;
    }


    /**
     * Parse Data and create associative array 
     * 
     * @param Array $orgData
     * @param Array $dataRequiredOf 
     * @return Array Associative array
     */
    public function LoginResponse($userData = array()) {
    $user_email=$userData['user_email'];
    $user_pwd=$userData['user_pwd'];
	$qb = $this->getRepository()->createQueryBuilder("user");
      $qb->addSelect('user.userEmail');
      $qb->where("user.userEmail = :userEmail")
        ->andwhere("user.userPwd = :userPwd")
        ->andwhere("user.userStatus = :userStatus")
            ->setParameter("userEmail",$user_email)
            ->setParameter("userPwd",$user_pwd)
            ->setParameter("userStatus",1);
      $result = $qb->getQuery()->getArrayResult();
      return $result;
    }

    
    public function generateActivationKey($data) {
        $ts = time();
        $userInfo = $data['userId'] . ":" . $data["userEmail"] . ":" . $ts;
        $key = base64_encode($userInfo);
        return $key;
    }

    public function decodeActivationKey($key) {
        $userInfo = base64_decode($key);
        $info = explode(":", $userInfo);
        $data['userId'] = $info["0"];
        $data['userEmail'] = $info["1"];
        $data['time'] = $info["2"];
        return $data;
    }
 public function encryptPassword($password = "") {
        if (!empty($password)) {
            $config = $this->serviceManager->get('config');
            $password = md5($password . $config['password_hash_salt']);
            return $password;
        }
    }
     public function saveUser(SandboxUser &$user) {

        $om = $this->getObjectManager($this->serviceManager);
        $om->persist($user);
        $om->flush();
        return $user->getUserId();
    }
    
    public function sendEmail($type = "", $data) {

        $view = new PhpRenderer();
        $resolver = new TemplateMapResolver();
        $templatefile = "";
        switch ($type) {
            case 'registration':
                $templatefile = "register_activate.phtml";
                $subject = "Welcome to Sandbox Cloud - Activation Email";
                break;
            case 'passwordreset':
                $templatefile = "password_reset.phtml";
                $subject = "Sandbox Cloud - Reset Password";
                break;
            case 'emptyhtml':
                $templatefile = "empty_html.phtml";
                $subject = $data['subject'];
                break;
            case 'passwordmodified':
                $templatefile = "password_changed.phtml";
                $subject = $data['subject'];
                break;
        }
        if (isset($templatefile) && $data['toEmail']) {
            $resolver->setMap(array(
                'mailTemplate' => MODULE_APPLICATION_PATH
                . '/view/email/templates/' . $templatefile
            ));
            $view->setResolver($resolver);
            $viewModel = new ViewModel();
            $viewModel->setTemplate('mailTemplate')
                    ->setVariables(array(
                        'data' => $data
            ));
            $content = $view->render($viewModel);
            //Set Html Mail Type
            $text = new MimePart('');
            $text->type = "text/plain";

            $html = new MimePart($content);
            $html->type = "text/html";

            $body = new MimeMessage();
            $body->setParts(array($html));
            //$transport = new SendmailTransport();
            $transport = new SmtpTransport();
            $config = $this->serviceManager->get('config');
            $options = new SmtpOptions($config['smtp_options']);
            $transport->setOptions($options);
            $message = new Message();
            $message->addTo($data['toEmail'])
                    ->addFrom($config['sender_email'])
                    ->setEncoding('UTF-8');
            $message->setSubject($subject)
                    ->setBody($body);
            $transport->send($message);
        }
    }
}
