<?php
namespace Api\Controller;

use Application\Entity\SandboxDevice;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;


class DeviceCommands extends AbstractRestfulController
{


    /**
     * This function will be called in case of
     * GET request from client
     *
     * @return Response Json
     */
    protected function getServiceInstance($service)
    {
        return $this->getServiceLocator()->get($service);
    }



}