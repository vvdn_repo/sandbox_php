<?php

return array(
    'controllers' => array(
        'invokables' => array(
          
            'Api\Controller\UserApi' => 'Api\Controller\UserApiController',
            'Api\Controller\DeviceApi' => 'Api\Controller\DeviceApiController',
        ),
    ),
    'router' => array(
        'routes' => array(
           
            'api-login' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/api/login',
                    'defaults' => array(
                        'controller' => 'Api\Controller\UserApi',
                        'action' => 'login'
                    )
                ),
            ),
            'api-register' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/api/register',
                    'defaults' => array(
                        'controller' => 'Api\Controller\UserApi',
                        'action' => 'register'
                    )
                ),
            ),

	     'api-update-token' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/api/updatetoken',
                    'defaults' => array(
                        'controller' => 'Api\Controller\UserApi',
                        'action' => 'updatepushToken'
                    )
                ),
            ),

	    'api-update-token-IOS' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/api/updatetokenios',
                    'defaults' => array(
                        'controller' => 'Api\Controller\UserApi',
                        'action' => 'updatepushTokenIOS'
                    )
                ),
            ),

            'api-forgotpassword' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/api/forgotpassword',
                    'defaults' => array(
                        'controller' => 'Api\Controller\UserApi',
                        'action' => 'Forgotpassword'
                    )
                ),
            ),

	    
           'reset-pwd' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/api/reset-password',
                    'defaults' => array(
                        'controller' => 'Api\Controller\UserApi',
                        'action' => 'resetpwd'
                    )
                ),
            ),


            'api-logout' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/api/logout',
                    'defaults' => array(
                        'controller' => 'Api\Controller\UserApi',
                        'action' => 'logout'
                    )
                ),
            ),
            'device-response' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/api/device-response[/][:deviceId]',
                    'defaults' => array(
                        'controller' => 'Api\Controller\DeviceApi',
                        'action' => 'deviceResponse'
                    )
                ),
            ),

        ),
    ),
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    )
);
