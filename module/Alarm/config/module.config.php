<?php
/**
 * module.config.php handles the routes and paths for the Alarm module 
 * @package Alarm
 * @author VVDNTechnologies 
 */
return array(
    'controllers' => array(
        'invokables' => array(
            'Alarm\Controller\Alarm' => 'Alarm\Controller\AlarmController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'list-alarms' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/alarm/list-alarms',
                    'defaults' => array(
                        'controller' => 'Alarm\Controller\Alarm',
                        'action' => 'listAlarms'
                    )
                ),
            ),

            'list-voicemail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/alarm/list-voicemail',
                    'defaults' => array(
                        'controller' => 'Alarm\Controller\Alarm',
                        'action' => 'listVoiceMail'
                    )
                ),
            ),

	'list-all-alarms' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/alarm/list-all-alarms',
                    'defaults' => array(
                        'controller' => 'Alarm\Controller\Alarm',
                        'action' => 'listAllAlarms'
                    )
                ),
            ),

      'clear-alarms' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/alarm/clear-alarms',
                    'defaults' => array(
                        'controller' => 'Alarm\Controller\Alarm',
                        'action' => 'clearAlarms'
                    )
                ),
            ),

      'clear-voicemail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/alarm/clear-voicemail',
                    'defaults' => array(
                        'controller' => 'Alarm\Controller\Alarm',
                        'action' => 'clearVoiceMail'
                    )
                ),
            ),       

       'update-alarm-read' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/alarm/update-read-alarms',
                    'defaults' => array(
                        'controller' => 'Alarm\Controller\Alarm',
                        'action' => 'updateRead'
                    )
                ),
            ),

	'get-unread-count' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/alarm/get-unread-count',
                    'defaults' => array(
                        'controller' => 'Alarm\Controller\Alarm',
                        'action' => 'getUnreadCount'
                    )
                ),
            ),



        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
     'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
     's3-bucket' => 'sbxu-wcnd',
    's3-key' => 'AKIAIYJIG2ARNJUEEQ5Q',
    's3-secret' => '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',
);
