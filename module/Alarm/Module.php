<?php
/**
 * This class contains getAutoloaderConfig,onBootstrap functions   
 * @package Alarm
 * @author VVDNTechnologies
 */

namespace Alarm;

use Alarm\Model\Alarm;
use Alarm\Model\AlarmConfig;
use Alarm\Model\PushNotification;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module {

    public function onBootstrap(MvcEvent $e) {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(

                'Alarm' => function($sm) {
                    $alarm = new Alarm();
                    $alarm->setServiceLocator($sm);
                    return $alarm;
                },

		'AlarmConfig' => function($sm) {
                    $alarmConfig = new AlarmConfig();
                    $alarmConfig->setServiceLocator($sm);
                    return $alarmConfig;
                },
                    'PushNotification' => function($sm) {
                    $push = new PushNotification();
                    $push->setServiceLocator($sm);
                    return $push;
                },
            ),
        );
    }

}
