<?php

/**
 * AlarmController handles Alarm related views  
 * @package Alarm
 * @author VVDNTechnologies <meru_cnms@vvdntech.com> 
 */

namespace Alarm\Controller;

require VENDOR_PATH . '/aws/aws-autoloader.php';
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\Config\Reader\Ini;
use Zend\Session\Container;
use Zend\Http\Client;
use Aws\Common\Aws;
use Aws\S3\S3Client;
use Aws\Common\Credentials\Credentials;


class AlarmController extends AbstractActionController {

    protected function getServiceInstance($service) {
        return $this->getServiceLocator()->get($service);
    }

    /*
     * This function is used to populate the alarm page description
     * @return Object Instance of ViewModel of alarm page
     */
    public function listAlarmsAction(){
        $deviceId = false;
        $status = false;
        $message = "Your Session Expired";
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if (!$user_id) {
            return new JsonModel(array("status" => $status, "message" => $message));
        } 
        $data = array();
        $request = $this->getRequest();
       if ($request->isPost()) {            
            $data = json_decode(file_get_contents("php://input"), true);
            $deviceId = $data['device_id'];
	    $pageNumber = $data['page_number'];
        }
        if($deviceId){
             $alarmModel = $this->getServiceInstance('Alarm');
            $userModel = $this->getServiceInstance('User');
            $time = $userModel->getAlarmConfig($user_id);
            $alarmList = $alarmModel->getAlarmList($user_id,$deviceId,$time,$pageNumber);
        }
        else{
            $devieModel = $this->getServiceInstance('Device');
            $devieList = $devieModel->getDeviceIds($user_id);
            $device_list=array();
            foreach ($devieList as $value) {
                array_push($device_list,$value['deviceId']);
            }
	    $CubeModel = $this->getServiceInstance('Cube');
            $cubeInfo = $CubeModel->getCubeinfo('cubeCreatedbyFk',$user_id);
            if($cubeInfo) {
                $cube_Id="cube_".$cubeInfo[0]['cubeId'];
                array_push($device_list,$cube_Id);
            }

            $userModel = $this->getServiceInstance('User');
            $time = $userModel->getAlarmConfig($user_id);
            $alarmModel = $this->getServiceInstance('Alarm');
            $alarmList = $alarmModel->getAlarmList($user_id,$device_list,$time,$pageNumber);
        }

        if ($alarmList) {

            $cubeModel = $this->getServiceInstance('Cube');
            $cubeInfo = $cubeModel->getCubeinfo('cubeCreatedbyFk',$user_id);
            $status = true;
            foreach($alarmList as $alarm){
                $alarm['lanUrl'] = "http://".$cubeInfo[0]['cubeLanIP'].$cubeInfo[0]['cubePath'].'/'.$alarm['alarmFileName'];
                $alarm['publicUrl'] = "http://".$cubeInfo[0]['cubePublicIP'].$cubeInfo[0]['cubePath'].'/'.$alarm['alarmFileName'];
                $alarmObject[] = $alarm;
            }
 	return new JsonModel(array("status" => $status, "response" => $alarmObject));
        }
	else{
            $status = false;
            $message = "No Alert found for this user";
            $data = "";
            return new JsonModel(array("status" => $status, "message" => $message));
        }
       
    }
    
    
    
     public function listAllAlarmsAction(){
        $deviceId = false;
        $status = false;
        $message = "Your Session Expired";
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if (!$user_id) {
            return new JsonModel(array("status" => $status, "message" => $message));
        }
        $data = array();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = json_decode(file_get_contents("php://input"), true);
            $deviceId = $data['device_id'];
	    $pageNumber = $data['page_number'];
        }
        if($deviceId){
            $alarmModel = $this->getServiceInstance('Alarm');
            $alarmList = $alarmModel->getAlarmList($user_id,$deviceId,$pageNumber);
        }
        else{
            $devieModel = $this->getServiceInstance('Device');
            $devieList = $devieModel->getDeviceIds($user_id);
            $device_list=array();
            foreach ($devieList as $value) {
                array_push($device_list,$value['deviceId']);
            }
	    $CubeModel = $this->getServiceInstance('Cube');
            $cubeInfo = $CubeModel->getCubeinfo('cubeCreatedbyFk',$user_id);
            if($cubeInfo) {
                $cube_Id="cube_".$cubeInfo[0]['cubeId'];
                array_push($device_list,$cube_Id);
            }

            $alarmModel = $this->getServiceInstance('Alarm');
            $alarmList = $alarmModel->getAllAlarmList($user_id,$device_list,$pageNumber);
        }

        if ($alarmList) {
            $cubeModel = $this->getServiceInstance('Cube');
            $cubeInfo = $cubeModel->getCubeinfo('cubeCreatedbyFk',$user_id);
            $status = true;
            foreach($alarmList as $alarm){
                $alarm['lanUrl'] = "http://".$cubeInfo[0]['cubeLanIP'].$cubeInfo[0]['cubePath'].'/'.$alarm['alarmFileName'];
                $alarm['publicUrl'] = "http://".$cubeInfo[0]['cubePublicIP'].$cubeInfo[0]['cubePath'].'/'.$alarm['alarmFileName'];
                $alarmObject[] = $alarm;
            }
            return new JsonModel(array("status" => $status, "response" => $alarmObject));
        }
        else{
            $status = false;
            $message = "No Alert found for this user";
            $data = "";
            return new JsonModel(array("status" => $status, "message" => $message));
        }
    }
   
    

    public function listVoiceMailAction(){
        $deviceId = false;
        $status = false;
        $message = "Your Session Expired";
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if (!$user_id) {
            return new JsonModel(array("status" => $status, "message" => $message));
        }
	$userModel = $this->getServiceInstance('User');
	$user_data = $userModel->selectdata($user_id);
	$user_token= $user_data[0][0]['userToken'];
        $data = array();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = json_decode(file_get_contents("php://input"), true);
            $deviceId = $data['device_id'];
        }
        if($deviceId){
            $alarmModel = $this->getServiceInstance('Alarm');
            $VoiceMailList = $alarmModel->getVoiceMailList($user_id,$deviceId);
        }
        else{
            $devieModel = $this->getServiceInstance('Device');
            $devieList = $devieModel->getDeviceIds($user_id);
            $device_list=array();
            foreach ($devieList as $value) {
                array_push($device_list,$value['deviceId']);
            }
            $alarmModel = $this->getServiceInstance('Alarm');
            $VoiceMailList = $alarmModel->getAllVoiceMailList($user_id,$device_list);
        }

        if ($VoiceMailList) {
          //print_r($VoiceMailList);die;
            $cubeModel = $this->getServiceInstance('Cube');
            $cubeInfo = $cubeModel->getCubeinfo('cubeCreatedbyFk',$user_id);
            $status = true;
            	$config = $this->getServiceLocator()->get('config');
        	$bucket = $config['s3-bucket'];
        	$client = S3Client::factory(array(
            				'key' => $config['s3-key'],
            				'secret' => $config['s3-secret']
       					 ));
       		$iterator = $client->getIterator('ListObjects', array(
    					'Bucket' => $bucket,
    					'Prefix' => $user_token,
    					'Delimiter' => '.jpg'

					));
		$array=array();
		foreach ($iterator as $object) {
		$val = explode("/",$object['Key']);
		array_push($array,$val[2]);
		}
		//print_r($array);die;
            foreach($VoiceMailList as $alarm){
                //$alarm['lanUrl'] = "http://".$cubeInfo[0]['cubeLanIP'].$cubeInfo[0]['cubePath'].'/'.$alarm['alarmFileName'];
                //$alarm['publicUrl'] = "http://".$cubeInfo[0]['cubePublicIP'].$cubeInfo[0]['cubePath'].'/'.$alarm['alarmFileName'];
		if (in_array($alarm['alarmFileName'], $array))
		{
		 $alarmObject[] = $alarm;
		}
                
            }
            return new JsonModel(array("status" => $status, "response" => $alarmObject));
        }
        else{
            $status = false;
            $message = "No Video found for this user";
            $data = "";
            return new JsonModel(array("status" => $status, "message" => $message));
        }
    }



    public function clearAlarmsAction(){
        $status = false;
        $message = "Your Session Expired";
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if (!$user_id) {
            return new JsonModel(array("status" => $status, "message" => $message));
        }
        $data = array();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = json_decode(file_get_contents("php://input"), true);
            $modelCommon = $this->getServiceInstance('Common');
            $time = $modelCommon->getCurrentTimeStamp();
        }
        if($time){

            $userModel = $this->getServiceInstance('User');
            $result = $userModel->updateAlarmConfig($user_id,$time);


            if($result){
                $status = true;
                $message = "Alerts Cleared";
                return new JsonModel(array("status" => $status, "message" => $message));
            }
            else{
                $status = false;
                $message = "Alerts Clearing Failed";
                return new JsonModel(array("status" => $status, "message" => $message));
            }
        }
        else{
            $status = false;
            $message = "Timestamp can not be null";
            $data = "";
            return new JsonModel(array("status" => $status, "message" => $message,"data" => $data));
        }
    }

    public function updateReadAction(){
        $status = false;
        $message = "Your Session Expired";
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if (!$user_id) {
            return new JsonModel(array("status" => $status, "message" => $message));
        }
        $data = array();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = json_decode(file_get_contents("php://input"), true);
            $alarmId = $data['alarmId'];
        }
        if($alarmId){
            $alarmModel = $this->getServiceInstance('Alarm');
            $result = $alarmModel->updateAlarmRead($alarmId);
            if($result){
                $status = true;
                $message = "Alarm Updated";
                return new JsonModel(array("status" => $status, "message" => $message));
            }
            else{
                $status = false;
                $message = "Alarm Updation Failed";
                return new JsonModel(array("status" => $status, "message" => $message));
            }
        }
        else{
            $status = false;
            $message = "AlarmId can not be null";
            $data = "";
            return new JsonModel(array("status" => $status, "message" => $message,"data" => $data));
        }
    }


    public function getUnreadCountAction(){
        $deviceId = false;
        $status = false;
        $message = "Your Session Expired";
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if (!$user_id) {
            return new JsonModel(array("status" => $status, "message" => $message));
        }
        $userModel = $this->getServiceInstance('User');
        $user_data = $userModel->selectdata($user_id);
        $user_token= $user_data[0][0]['userToken'];
        $data = array();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = json_decode(file_get_contents("php://input"), true);
            $deviceId = $data['device_id'];
        }
        if($deviceId){
            $alarmModel = $this->getServiceInstance('Alarm');
            $VoiceMailList = $alarmModel->getVoiceMailList($user_id,$deviceId);
        }
        else{
            $devieModel = $this->getServiceInstance('Device');
            $devieList = $devieModel->getDeviceIds($user_id);
            $device_list=array();
            foreach ($devieList as $value) {
                array_push($device_list,$value['deviceId']);
            }
            $alarmModel = $this->getServiceInstance('Alarm');
            $VoiceMailList = $alarmModel->getUnreadVoiceMailList($user_id,$device_list);
        }

        if ($VoiceMailList) {
            //print_r($VoiceMailList);die;
            $cubeModel = $this->getServiceInstance('Cube');
            $cubeInfo = $cubeModel->getCubeinfo('cubeCreatedbyFk',$user_id);
            $status = true;
            $config = $this->getServiceLocator()->get('config');
            $bucket = $config['s3-bucket'];
            $client = S3Client::factory(array(
                'key' => $config['s3-key'],
                'secret' => $config['s3-secret']
            ));
            $iterator = $client->getIterator('ListObjects', array(
                'Bucket' => $bucket,
                'Prefix' => $user_token,
                'Delimiter' => '.jpg'

            ));
            $array=array();
            foreach ($iterator as $object) {
                $val = explode("/",$object['Key']);
                array_push($array,$val[2]);
            }
            //print_r($array);die;
            foreach($VoiceMailList as $alarm){
                //$alarm['lanUrl'] = "http://".$cubeInfo[0]['cubeLanIP'].$cubeInfo[0]['cubePath'].'/'.$alarm['alarmFileName'];
                //$alarm['publicUrl'] = "http://".$cubeInfo[0]['cubePublicIP'].$cubeInfo[0]['cubePath'].'/'.$alarm['alarmFileName'];
                if (in_array($alarm['alarmFileName'], $array))
                {
                    $alarmObject[] = $alarm;
                }

            }
	$count= sizeof($alarmObject);

            return new JsonModel(array("status" => $status, "count" => $count));
        }
        else{
            $status = false;
            $message = "No Video found for this user";
            $data = "";
            return new JsonModel(array("status" => $status, "message" => $message));
        }
    }



    public function clearVoiceMailAction(){
        $status = false;
        $message = "Your Session Expired";
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if (!$user_id) {
            return new JsonModel(array("status" => $status, "message" => $message));
        }
        $data = array();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = json_decode(file_get_contents("php://input"), true);
            $userModel = $this->getServiceInstance('User');
            $user_data = $userModel->selectdata($user_id);
            $user_token= $user_data[0][0]['userToken'];
            $config = $this->getServiceLocator()->get('config');
            $bucket = $config['s3-bucket'];
            $client = S3Client::factory(array(
                'key' => $config['s3-key'],
                'secret' => $config['s3-secret']
            ));
            $iterator = $client->getIterator('ListObjects', array(
                'Bucket' => $bucket,
                'Prefix' => $user_token,
                'Delimiter' => '.jpg'

            ));
            $array=array();
            foreach ($iterator as $object) {
                $keys = array('Key');
                $a = array_fill_keys($keys, $object['Key']);

                array_push($array,$a);
            }
            $result = $client->deleteObjects(array(
                'Bucket'  => $bucket,
                'Objects' => $array
            ));

            if($result){
                $status = true;
                $message = "Voice mails Cleared";
                return new JsonModel(array("status" => $status, "message" => $message));
            }
            else{
                $status = false;
                $message = "Voice mail Clearing Failed";
                return new JsonModel(array("status" => $status, "message" => $message));
            }
        }

	else{
            $status = false;
            $message = "";
            $data = "";
            return new JsonModel(array("status" => $status, "message" => $message,"data" => $data));
        }
        
    }


}
