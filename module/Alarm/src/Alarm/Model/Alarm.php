<?php

/**
 * Alarm Model
 * 
 * @package Alarm
 * @author VVDN Technologies
 */

namespace Alarm\Model;

use Application\Entity\SandboxAlarm;
use Application\Model\AbstractOrmManager;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\ResultSetMapping;
use Zend\Config\Reader\Ini;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend_Db_Select;

class Alarm extends AbstractOrmManager implements ServiceLocatorAwareInterface {

    protected $serviceManager;


    public function getServiceLocator() {
        return $this->serviceManager;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceManager = $serviceLocator;
    }

    public function getRepository() {
        $repository = $this->getObjectManager($this->serviceManager)->getRepository('Application\Entity\SandboxAlarm');
        return $repository;
    }

    public function getEntityInstance() {
        if (null === $this->alarmEntityInstance) {
            $this->alarmEntityInstance = new SandboxAlarm();
        }
        return $this->alarmEntityInstance;
    }

    public function createAlarm($data,$deviceId,$userMode){
        $alarmEntity = $this->getEntityInstance();
        $objectManager = $this->getObjectManager($this->getServiceLocator());
        //$userProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId);
        $alarmEntity->setDeviceIdFK($deviceId);
        $alarmEntity->setAlarmTypeName(htmlspecialchars($data['reason']));
        $alarmEntity->setFileName(htmlspecialchars($data['file']));
	$alarmEntity->setThumbnail(htmlspecialchars($data['snapshot']));
        $modelCommon = $this->getServiceLocator()->get('Common');
        //$modelCommon = $this->getServiceInstance('Common');
        $time = $modelCommon->getCurrentTimeStamp();
        $alarmEntity->setTimeStamp(htmlspecialchars($time));
	$alarmEntity->setReadStatus(0);
        $deviceModel = $this->getServiceLocator()->get('Device');
        $PushNotificationModel = $this->getServiceLocator()->get('PushNotification');
        $device_info = $deviceModel->Device_info('deviceId',$deviceId);
	$user_id = $device_info[0]['device_createdby_fk'];
//$data['sensor_mac'] = "C6:FE:73:C1:5F:CF";
        $sensor_info = $deviceModel->getSensorinfo($data['sensor_mac']);
        switch($data['reason']){
//            case 'glass-break' :{
//                $alert= "Glass Break Detected on " .$sensor_info[0]['sensorName'];
//                break;
//            }
            case 'motion-detected' :{
                $alert= "Motion Detected on " .$device_info[0]['deviceName'];
                break;
            }
            case 'doorbell-press' :{
                $alert= "You have a visitor!";
                $devicePushModel = $this->getServiceLocator()->get('DevicePush');
                $pushCommand = "ignore_doorbell";
		$time_limit =30;
                $DevicePushId = $devicePushModel->InsertDevicePush($pushCommand,$deviceId,$user_id,$time_limit);
                break;
            }

            case 'door-opened' :{
                $alert= "Door open Detected on ".$sensor_info[0]['sensorName'];
                break;
            }
            case 'door-closed' :{
                $alert= "Door close Detected on ".$sensor_info[0]['sensorName'];
                break;
            }
            case 'battery-low' :{
                $alert= "Battery Low on " .$sensor_info[0]['sensorName'];
                break;
            }
	    case 'Battery-OK' :{
                $alert= "Battery Okay on " .$sensor_info[0]['sensorName'];
                break;
            }
	        case 'battery-low-doorbell' :{
                $alert= "Battery Low on " .$device_info[0]['deviceName'];
                break;
            }

            case 'successfully-connected' :{
                $alert = $device_info[0]['deviceName'] ." is Successfully connected to Accesspoint"."_".$device_info[0]['deviceIP'];
                break;
            }
            case 'connection-failure' :{
                $alert= "Accesspoint connection failure in " .$device_info[0]['deviceName'];
                break;
            }
            case 'temperature-exceeded' :{
                $alert="Temperature exceeding the limit in " .$device_info[0]['deviceName'];
                break;
            }
            case 'voice-mail' :{
                $alert="You have a new voice mail from " .$device_info[0]['deviceName'];
                break;
            }

            case 'video-saved' :{
                $alert="Video is Saved from " .$device_info[0]['deviceName'];
                break;
            }

            case 'device-connected' :{
                $alert=$device_info[0]['deviceName']." is Online";
                break;
            }

            case 'device-disconnected' :{
                $alert=$device_info[0]['deviceName']." is Offline";
                break;
            }

	     case 'fw-update' :{
                $alert="Firmware Update is available for ".$device_info[0]['deviceName'];
                break;
            }

	    case 'auto-update' :{
                $alert="Firmware Update started for ".$device_info[0]['deviceName'];
                break;
            }

        }
        if($userMode == 1){
           if($data['reason'] == "door-opened")
           {
               $sensorUser = $deviceModel->validateSensor($data['sensor_mac'],$user_id);
		       if($sensorUser)
		        {
			        $lastAlarmTime = $sensor_info[0]['sensorAlarmTS'];
               		if($time - $lastAlarmTime >= 60)
                    {
                            $modelCommon = $this->getServiceLocator()->get('Common');
                	    $currentTS = $modelCommon->getCurrentTimeStamp();
			    $time =  $deviceModel->updateAlarmTS($data['sensor_mac'],$currentTS); 
                            $device_list =  $deviceModel->getDeviceList($user_id);
                            $count = sizeof($device_list);
                            $devicePushModel = $this->getServiceLocator()->get('DevicePush');
                            for($x = 0; $x < $count; $x++)
                            {
                            $pushCommand = "start_alarm";
                            $DevicePushId = $devicePushModel->InsertDevicePushAlarm($pushCommand,$device_list[$x]['deviceId'],$user_id);
                            }
                            
                            $alarmEntity->setAlarmType(htmlspecialchars($alert));
                            $deviceProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId);
                            $alarmEntity->setDeviceIdFK($deviceProxyInstance);
                            $alarmId = $this->saveAlarm($alarmEntity);
			    $PushNotificationModel->makePushNotification($device_info[0]['device_createdby_fk'],$alert,$data['reason'],$device_info[0]['deviceIP'],$data['snapshot'],$deviceId,$DevicePushId,$data['file'],$alarmId);
			    
                            return $alarmId;
                    }
               		else
                    {
                            return true;
                    }
		        }

		        {
			    return true;
		        }
           }

           elseif($data['reason'] == "door-closed")
           {
               return true;
           }

           elseif($data['reason'] == "glass-break")
           {
               return true;
              /* $sensorUser = $deviceModel->validateSensor($data['sensor_mac'],$user_id);
               if($sensorUser)
               {
                   $PushNotificationModel->makePushNotification($device_info[0]['device_createdby_fk'],$alert,$data['reason'],$device_info[0]['deviceIP'],$data['snapshot'],$deviceId,$DevicePushId,$data['file']);
                   $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId);
                   $alarmEntity->setDeviceIdFK($userProxyInstance);
                   $alarmEntity->setAlarmType(htmlspecialchars($alert));
                   $alarmId = $this->saveAlarm($alarmEntity);
                   return $alarmId;
               }*/
           }

           elseif($data['reason'] == "battery-low" || $data['reason'] == "Battery-OK")
           {
                $sensorUser = $deviceModel->validateSensor($data['sensor_mac'],$user_id);
                if($sensorUser)
                {
                    $lastAlarmTime = $sensor_info[0]['sensorAlarmTS'];
                    if($time - $lastAlarmTime >= 60) {
                        $modelCommon = $this->getServiceLocator()->get('Common');
                        $currentTS = $modelCommon->getCurrentTimeStamp();
                        $time = $deviceModel->updateAlarmTS($data['sensor_mac'], $currentTS);
                        $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId);
                        $alarmEntity->setDeviceIdFK($userProxyInstance);
                        $alarmEntity->setAlarmType(htmlspecialchars($alert));
                        $alarmId = $this->saveAlarm($alarmEntity);
                        $PushNotificationModel->makePushNotification($device_info[0]['device_createdby_fk'], $alert, $data['reason'], $device_info[0]['deviceIP'], $data['snapshot'], $deviceId, $DevicePushId, $data['file'], $alarmId);
			$battery = $deviceModel->updateBatteryStatus($data['sensor_mac'], $data['reason']);
                        return $alarmId;
                    }
                }
           }

	   /*elseif($data['reason'] == "device-disconnected"){
               
 		$TalkBackModel = $this->getServiceLocator()->get('TalkBack');
                $time_limit =60;
                $DevicePushId = $TalkBackModel->InsertPush($data['reason'],$deviceId,$device_info[0]['device_createdby_fk'],$time_limit);
		return true;
           }
           elseif($data['reason'] == "device-connected"){
		$TalkBackPushModel = $this->getServiceLocator()->get('TalkBack');
		$clear = $TalkBackPushModel->updateOffline($deviceId);
               
		if($device_info[0]['deviceStatus'] == 0)
		{
		       $alarmEntity->setAlarmType(htmlspecialchars($alert));
		       $alarmId = $this->saveAlarm($alarmEntity);
			$PushNotificationModel->makePushNotification($device_info[0]['device_createdby_fk'],$alert,$data['reason'],$device_info[0]['deviceIP'],$data['snapshot'],$deviceId,$DevicePushId,$data['file'],$alarmId);	
		       $status = 1;
		       $deviceStatus = $deviceModel->updateDeviceStatus($deviceId,$status);
			return $alarmId;
		}	
		else
		{
		        return true;
		}	
           }*/

           elseif($data['reason'] == "device-disconnected"){
               
               $alarmEntity->setAlarmType(htmlspecialchars($alert));
               $alarmId = $this->saveAlarm($alarmEntity);
		$PushNotificationModel->makePushNotification($device_info[0]['device_createdby_fk'],$alert,$data['reason'],$device_info[0]['deviceIP'],$data['snapshot'],$deviceId,$DevicePushId,$data['file'],$alarmId);
               $status = 0;
               $deviceStatus = $deviceModel->updateDeviceStatus($deviceId,$status);
               return $alarmId;
           }
           elseif($data['reason'] == "device-connected"){
               
               $alarmEntity->setAlarmType(htmlspecialchars($alert));
               $alarmId = $this->saveAlarm($alarmEntity);
		$PushNotificationModel->makePushNotification($device_info[0]['device_createdby_fk'],$alert,$data['reason'],$device_info[0]['deviceIP'],$data['snapshot'],$deviceId,$DevicePushId,$data['file'],$alarmId);	
               $status = 1;
               $deviceStatus = $deviceModel->updateDeviceStatus($deviceId,$status);
		return $alarmId;	
           }

  	   elseif($data['reason'] == "fw-update")
           {
               $fwTime = $device_info[0]['deviceFWTime'];
               if($time - $fwTime >= 60)
               {
                   
                   $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId);
                   $alarmEntity->setDeviceIdFK($userProxyInstance);
                   $alarmEntity->setAlarmType(htmlspecialchars($alert));
                   $alarmId = $this->saveAlarm($alarmEntity);
		   $PushNotificationModel->makePushNotification($device_info[0]['device_createdby_fk'],$alert,$data['reason'],$device_info[0]['deviceIP'],$data['snapshot'],$deviceId,$DevicePushId,$data['file'],$alarmId);
                   $updatetime =  $deviceModel->updateFWTS($deviceId,$time);
                   return $alarmId;
               }
           }

           else
           {
               
                $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId);
                $alarmEntity->setDeviceIdFK($userProxyInstance);
                $alarmEntity->setAlarmType(htmlspecialchars($alert));
                $alarmId = $this->saveAlarm($alarmEntity);
		$PushNotificationModel->makePushNotification($device_info[0]['device_createdby_fk'],$alert,$data['reason'],$device_info[0]['deviceIP'],$data['snapshot'],$deviceId,$DevicePushId,$data['file'],$alarmId);
                return $alarmId;
           }
        }
        else
        {
            if($data['reason'] == "doorbell-press"){
             
                $alarmEntity->setAlarmType(htmlspecialchars($alert));
                $alarmId = $this->saveAlarm($alarmEntity);
		$PushNotificationModel->makePushNotification($device_info[0]['device_createdby_fk'],$alert,$data['reason'],$device_info[0]['deviceIP'],$data['snapshot'],$deviceId,$DevicePushId,$data['file'],$alarmId);
                return $alarmId;
            }
            elseif($data['reason'] == "door-closed")
            {
                return true;
            }
            elseif($data['reason'] == "battery-low")
            {
                return true;
            }
            /*elseif($data['reason'] == "device-disconnected"){
             
		$TalkBackModel = $this->getServiceLocator()->get('TalkBack');
                $time_limit =60;
                $DevicePushId = $TalkBackModel->InsertPush($data['reason'],$deviceId,$device_info[0]['device_createdby_fk'],$time_limit);
		return true;
            }
            elseif($data['reason'] == "device-connected"){
		$TalkBackPushModel = $this->getServiceLocator()->get('TalkBack');
		$clear = $TalkBackPushModel->updateOffline($deviceId);
               if($device_info[0]['deviceStatus'] == 0)
		{
		       $alarmEntity->setAlarmType(htmlspecialchars($alert));
		       $alarmId = $this->saveAlarm($alarmEntity);
			$PushNotificationModel->makePushNotification($device_info[0]['device_createdby_fk'],$alert,$data['reason'],$device_info[0]['deviceIP'],$data['snapshot'],$deviceId,$DevicePushId,$data['file'],$alarmId);	
		       $status = 1;
		       $deviceStatus = $deviceModel->updateDeviceStatus($deviceId,$status);
			return $alarmId;
		}	
		else
		{
		        return true;
		}
            }*/
      
            elseif($data['reason'] == "device-disconnected"){
               
               $alarmEntity->setAlarmType(htmlspecialchars($alert));
               $alarmId = $this->saveAlarm($alarmEntity);
		$PushNotificationModel->makePushNotification($device_info[0]['device_createdby_fk'],$alert,$data['reason'],$device_info[0]['deviceIP'],$data['snapshot'],$deviceId,$DevicePushId,$data['file'],$alarmId);
               $status = 0;
               $deviceStatus = $deviceModel->updateDeviceStatus($deviceId,$status);
               return $alarmId;
           }
           elseif($data['reason'] == "device-connected"){
               
               $alarmEntity->setAlarmType(htmlspecialchars($alert));
               $alarmId = $this->saveAlarm($alarmEntity);
		$PushNotificationModel->makePushNotification($device_info[0]['device_createdby_fk'],$alert,$data['reason'],$device_info[0]['deviceIP'],$data['snapshot'],$deviceId,$DevicePushId,$data['file'],$alarmId);	
               $status = 1;
               $deviceStatus = $deviceModel->updateDeviceStatus($deviceId,$status);
		return $alarmId;	
           }

           elseif($data['reason'] == "fw-update")
            {
                $fwTime = $device_info[0]['deviceFWTime'];
                if($time - $fwTime >= 60)
                {
                    
                    $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId);
                    $alarmEntity->setDeviceIdFK($userProxyInstance);
                    $alarmEntity->setAlarmType(htmlspecialchars($alert));
                    $alarmId = $this->saveAlarm($alarmEntity);
		    $PushNotificationModel->makePushNotification($device_info[0]['device_createdby_fk'],$alert,$data['reason'],$device_info[0]['deviceIP'],$data['snapshot'],$deviceId,$DevicePushId,$data['file'],$alarmId);
                    $updatetime =  $deviceModel->updateFWTS($deviceId,$time);
                    return $alarmId;
                }
            }
            else{
                $alarmEntity->setAlarmType(htmlspecialchars($alert));
                $alarmId = $this->saveAlarm($alarmEntity);
                return $alarmId;
            }
        }

    }


    public function createAlarmOffline($data,$deviceId)
    {
        $alarmEntity = $this->getEntityInstance();
        $objectManager = $this->getObjectManager($this->getServiceLocator());
        //$userProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId);
        $alarmEntity->setDeviceIdFK($deviceId);
        $alarmEntity->setAlarmTypeName(htmlspecialchars($data['reason']));
        $alarmEntity->setFileName(htmlspecialchars($data['file']));
	$alarmEntity->setThumbnail(htmlspecialchars($data['snapshot']));
        $modelCommon = $this->getServiceLocator()->get('Common');
        //$modelCommon = $this->getServiceInstance('Common');
        $time = $modelCommon->getCurrentTimeStamp();
        $alarmEntity->setTimeStamp(htmlspecialchars($time));
        $alarmEntity->setReadStatus(0);
        $deviceModel = $this->getServiceLocator()->get('Device');
        $PushNotificationModel = $this->getServiceLocator()->get('PushNotification');
        $device_info = $deviceModel->Device_info('deviceId', $deviceId);
        $user_id = $device_info[0]['device_createdby_fk'];
        $alert=$device_info[0]['deviceName']." is offline";
        $alarmEntity->setAlarmType(htmlspecialchars($alert));
		if($device_info[0]['deviceStatus'] == 1)
		{
			$alarmId = $this->saveAlarm($alarmEntity);
			$PushNotificationModel->makePushNotification($device_info[0]['device_createdby_fk'],$alert,$data['reason'],$device_info[0]['deviceIP'],$data['snapshot'],$deviceId,$DevicePushId,$data['file'],$alarmId);
			$status = 0;
			$deviceStatus = $deviceModel->updateDeviceStatus($deviceId,$status);
			return $alarmId;
		}
		else
		{
		return true;
		}

    }

    public function createAlarmCube($data,$cubeId,$userMode)
    {

        $alarmEntity = $this->getEntityInstance();
        $objectManager = $this->getObjectManager($this->getServiceLocator());
       // $cubeProxyInstance = $objectManager->getReference('Application\Entity\SandboxCube', $cubeId);
        $alarmEntity->setDeviceIdFK("cube_".$cubeId);
        $alarmEntity->setAlarmTypeName(htmlspecialchars($data['reason']));
        $alarmEntity->setFileName(htmlspecialchars($data['file']));
        $alarmEntity->setThumbnail(htmlspecialchars($data['snapshot']));
        $modelCommon = $this->getServiceLocator()->get('Common');
        //$modelCommon = $this->getServiceInstance('Common');
        $time = $modelCommon->getCurrentTimeStamp();
        $alarmEntity->setTimeStamp(htmlspecialchars($time));
        $alarmEntity->setReadStatus(0);
        $PushNotificationModel = $this->getServiceLocator()->get('PushNotification');
        $cubeModel =$this->getServiceLocator()->get('Cube');
        $cube_info = $cubeModel->getCubeinfo('cubeId',$cubeId);

        switch($data['reason']) {
            case 'fw-update-cube' : {
                $alert = "New Firmware is Available for Cube";
                break;
            }
	    case 'device-connected-cube' : {
                $alert = $cube_info[0]['cube_name']." is Online";
                $status = 1;
                $cubeStatus = $cubeModel->updateCubeStatus($cubeId,$status);
                break;
            }
            case 'device-disconnected-cube' : {
                $alert = $cube_info[0]['cube_name']." is Offline";
                $status = 0;
                $cubeStatus = $cubeModel->updateCubeStatus($cubeId,$status);
                break;
            }
        }

        $alarmEntity->setAlarmType(htmlspecialchars($alert));
        $alarmId = $this->saveAlarm($alarmEntity);
        $DevicePushId=1;
        $PushNotificationModel->makePushNotification($cube_info[0]['cube_createdby_fk'],$alert,$data['reason'],$cube_info[0]['cubePublicIP'],$data['snapshot'],$cubeId,$DevicePushId,$data['file'],$alarmId);
        return $alarmId;

    }

    public function saveAlarm(SandboxAlarm &$alarm)
    {
        $om = $this->getObjectManager($this->serviceManager);
        $om->persist($alarm);
        $om->flush();
        return $alarm->getalarmId();
    }
    
    public function getAlarmList($uid,$devicelist,$time,$page){
	if($page ==1){
		$start=0;
	}
	else{
		$start= $page*100-99;
	}
	$limit=100;
        $qb = $this->getRepository()->createQueryBuilder("alarm");
        $qb->where("alarm.deviceIdFK IN (:deviceId)")
            ->andWhere("alarm.alarmTimeStamp >= ?1")
        ->setParameter("deviceId", $devicelist)
        ->setParameter(1,$time);
	$qb->orderBy('alarm.alarmId', 'DESC');
        $qb->setFirstResult($start);
        $qb->setMaxResults($limit);
        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        return $result;
    }

    public function getAllAlarmList($uid,$devicelist,$page){
	if($page ==1){
                $start=1;
        }
        else{
                $start= $page*100-99;
        }
        $limit=100;

        $qb = $this->getRepository()->createQueryBuilder("alarm");
        $qb->where("alarm.deviceIdFK IN (:deviceId)")
            ->setParameter("deviceId", $devicelist);
        $qb->setFirstResult($start);
        $qb->setMaxResults($limit);
        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        return $result;
    }

    public function getVoiceMailList($uid,$devicelist){

        $qb = $this->getRepository()->createQueryBuilder("alarm");
        $qb->where("alarm.deviceIdFK IN (:deviceId)")
	    ->andWhere("alarm.alarmFileName != ?3")
            ->andWhere("alarm.alarmTypeName = ?1 OR alarm.alarmTypeName = ?2 OR alarm.alarmTypeName = ?4" )
            ->setParameter("deviceId", $devicelist)
            ->setParameter(1,"voice-mail")
	    ->setParameter(2,"motion-detected")
	    ->setParameter(3,"")
            ->setParameter(4,"video-saved");		
        /*$qb->setFirstResult($start);
        $qb->setMaxResults($limit);*/
        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        return $result;
    }

    public function getAllVoiceMailList($uid,$devicelist){
        $qb = $this->getRepository()->createQueryBuilder("alarm");
        $qb->where("alarm.deviceIdFK IN (:deviceId)")
	    ->andWhere("alarm.alarmFileName != ?3")
            ->andWhere("alarm.alarmTypeName = ?1 OR alarm.alarmTypeName = ?2 OR alarm.alarmTypeName = ?4" )
            ->setParameter(1,"voice-mail")
	    ->setParameter(2,"motion-detected")	
	    ->setParameter(3,"")
	    ->setParameter(4,"video-saved")	
            ->setParameter("deviceId", $devicelist);
        /*$qb->setFirstResult($start);
        $qb->setMaxResults($limit);*/
        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        return $result;
    }

    public function getUnreadVoiceMailList($uid,$devicelist){
        $qb = $this->getRepository()->createQueryBuilder("alarm");
        $qb->where("alarm.deviceIdFK IN (:deviceId)")
	    ->andWhere("alarm.alarmFileName != ?3")
	    ->andWhere("alarm.alarmReadStatus = 0")
            ->andWhere("alarm.alarmTypeName = ?1 OR alarm.alarmTypeName = ?2 OR alarm.alarmTypeName = ?4" )
            ->setParameter(1,"voice-mail")
	    ->setParameter(2,"motion-detected")	
	    ->setParameter(3,"")
	    ->setParameter(4,"video-saved")	
            ->setParameter("deviceId", $devicelist);
        /*$qb->setFirstResult($start);
        $qb->setMaxResults($limit);*/
        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        return $result;
    }
    
     public function updateThubnail($data) {
        $qb = $this->getRepository()->createQueryBuilder("alarm");
        $query = $qb->update()
            ->set("alarm.alarmThumbnail", "?1")
            ->where("alarm.alarmId = ?2")
            ->setParameter(1, $data['thumbnail'])
            ->setParameter(2, $data['alarmId'])
            ->getQuery();
        $query->execute();
        return true;

    }

    public function updateAlarmRead($alarmId) {
        $qb = $this->getRepository()->createQueryBuilder("alarm");
        $query = $qb->update()
            ->set("alarm.alarmReadStatus", "?1")
            ->where("alarm.alarmId = ?2")
            ->setParameter(1, 1)
            ->setParameter(2, $alarmId)
            ->getQuery();
        $query->execute();
        return true;

    }
    
}

