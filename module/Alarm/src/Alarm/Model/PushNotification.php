<?php

/**
 * PushNotification Model
 * 
 *
 * @author VVDN Technologies <meru_cnms@vvdntech.com>
 */
namespace Alarm\Model;

require VENDOR_PATH .'/aws/aws-autoloader.php';

use Exception;
use Application\Entity\SandboxPushToken;
use Application\Model\AbstractOrmManager;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\ResultSetMapping;
use Zend\Config\Reader\Ini;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

use Aws\Sns\Exception\EndpointDisabledException;
use Aws\Sns\Exception\InvalidParameterException;
use Aws\Common\Aws;

class PushNotification extends AbstractOrmManager implements ServiceLocatorAwareInterface {

    protected $serviceManager;


    public function getServiceLocator() {
        return $this->serviceManager;
    }
     public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceManager = $serviceLocator;
    }

    public function getRepository() {
        $repository = $this->getObjectManager($this->serviceManager)->getRepository('Application\Entity\SandboxPushToken');
        return $repository;
    }

    public function getEntityInstance() {
        if (null === $this->alarmEntityInstance) {
            $this->alarmEntityInstance = new SandboxPushToken();
        }
        return $this->alarmEntityInstance;
    }

    
    public function getAppToken($uid,$token,$platform,$appIdentifier) {
       //$userMode = $this->makePushNotification();
       $userModel = $this->getServiceLocator()->get('User');
        if($platform=="android") {
            $qb = $this->getRepository()->createQueryBuilder("token");
            $qb->addSelect('token');
            //$qb->where("token.tokenUserIdFK = :userId")
            //  ->setParameter("userId",$uid);
            $qb->where("token.tokenAppIdentifier = :appIdentifier")
                ->setParameter("appIdentifier", $appIdentifier);
            $qb->andWhere("token.tokenPlatform = :platform")
                ->setParameter("platform", $platform);
            $result = $qb->getQuery()->getArrayResult();
        }
        else{
            $qb = $this->getRepository()->createQueryBuilder("token");
            $qb->addSelect('token');
            $qb->where("token.tokenValue = :token")
                ->setParameter("token", $token);
            $qb->andWhere("token.tokenPlatform = :platform")
                ->setParameter("platform", $platform);
            $result = $qb->getQuery()->getArrayResult();
        }
        if(sizeof($result) > 0 ){
                $this->updateToken($result[0]['tokenId'], $uid, $token, $platform);
            if($platform!="android") {
                $path_to_config= VENDOR_PATH .'/config.php';
                $aws = Aws::factory($path_to_config);
                $client = $aws->get('Sns');
                $result = $client->setEndpointAttributes(array(
                    // EndpointArn is required
                    'EndpointArn' => $result[0]['tokenEndpoint'],
                    'Attributes' => array('Enabled'=>'true')
                ));
            }
        }
        else{
           $awsendpt=$this->create_end_point_from_token($platform,$token);
           $pushEntity = $this->getEntityInstance();
            $objectManager = $userModel->getObjectManager($this->getServiceLocator());
            $userProxyInstance = $objectManager->getReference('Application\Entity\SandboxUser', $uid);
            //$cubeEntity->setCubeCreatedbyFk($userProxyInstance);
            $pushEntity->setUserIdFK($userProxyInstance);
            $pushEntity->setTokenAppIdentifier($appIdentifier);
            $pushEntity->setTokenStatus(1);
            $pushEntity->setTokenValue($token);
            $pushEntity->setTokenPlatform($platform);
            $pushEntity->setTokenEndpt($awsendpt);
            $tokenId = $this->saveToken($pushEntity);   
        }
        return $result;
    }
    
    public function updateToken($tokenid,$uid,$token,$platform) {
        $awsendpt=$this->create_end_point_from_token($platform,$token);
        $qb = $this->getRepository()->createQueryBuilder("token");
        $query = $qb->update()
            ->set("token.tokenUserIdFK" , "?1")
            ->set("token.tokenValue" , "?2")
            ->set("token.tokenEndpoint" , "?3")
            ->set("token.tokenStatus" , "?5")
            ->where("token.tokenId = ?4")
            ->setParameter(1, $uid)
            ->setParameter(2, $token)
            ->setParameter(3, $awsendpt)
            ->setParameter(4, $tokenid)
            ->setParameter(5, "1")
            ->getQuery();
        $query->execute();
        return true;    
    }

    public function updateLogout($app_platform,$app_identifier){
        if($app_platform=="android"){
            $qb = $this->getRepository()->createQueryBuilder("token");
            $query = $qb->update()
                ->set("token.tokenStatus" , "?1")
                ->where("token.tokenAppIdentifier = ?2")
                ->setParameter(1, "0")
                ->setParameter(2, $app_identifier)
                ->getQuery();
            $query->execute();
            return true;
        }
        else{
            $qb = $this->getRepository()->createQueryBuilder("token");
            $query = $qb->update()
                ->set("token.tokenStatus" , "?1")
                ->where("token.tokenValue = ?2")
                ->setParameter(1, "0")
                ->setParameter(2, $app_identifier)
                ->getQuery();
            $query->execute();
            return true;
        }
    }
    public function setAppToken($data) {
        $pushEntity = $this->getEntityInstance();
        $objectManager = $this->getObjectManager($this->getServiceLocator());
        //$userProxyInstance = $objectManager->getReference('Application\Entity\SandboxDevice', $deviceId);
        $pushEntity->setUserIdFK($userid);
        $pushEntity->setTokenStatus(1);
        $pushEntity->setTokenValue($token);
        $tokenId = $this->saveToken($pushEntity);
        return $tokenId;
    }
    
    public function getPushEndPoint($uid) {
        $qb = $this->getRepository()->createQueryBuilder("token");
        $qb->addSelect('token');
        $qb->where("token.tokenUserIdFK = :userId")
            ->andWhere("token.tokenStatus = :status")
            ->setParameter("userId",$uid)
            ->setParameter("status","1");
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }
    
    public function create_end_point_from_token($device,$token)
	{
		
		$path_to_config= VENDOR_PATH .'/config.php';
		$aws = Aws::factory($path_to_config);
		$client = $aws->get('Sns');
		$arn=0;
		
		if($device=="ios_prod")

		  {

		   $PlatformApplicationArn="arn:aws:sns:us-west-2:214410961578:app/APNS/SandBox_IOS_Production";

		   //$PlatformApplicationArn="arn:aws:sns:us-east-1:584487216771:app/APNS_SANDBOX/zigbee";  //till sandbox

		  }

				else if($device=="ios_dev")

		  {

		   $PlatformApplicationArn="arn:aws:sns:us-west-2:214410961578:app/APNS_SANDBOX/SandBox_IOS";

		   //$PlatformApplicationArn="arn:aws:sns:us-east-1:584487216771:app/APNS_SANDBOX/zigbee";  //till sandbox

		  }

		  else if($device=="android")

		  {

		   $PlatformApplicationArn="arn:aws:sns:us-west-2:214410961578:app/GCM/Sandbox_GCM";

		   //$PlatformApplicationArn="arn:aws:sns:us-west-2:214410961578:app/GCM/SandboxAndroid"; 

		  }

		try
		{
			$result = $client->createPlatformEndpoint(array(
				 'PlatformApplicationArn' => $PlatformApplicationArn,
				 'Token' => $token
				 ));
			$arn=$result->get('EndpointArn');
		}
		catch(Exception $e)
		{
			
                   $status= $e->getStatusCode();
		   $error = $e->getmessage();
		  // $this->log("Exception while creating end point and error is ".$error." and status code is".$status);
		   return 0;   
		}
		return $arn;
	}
        
        public function makePushNotification($userid, $msginput = NULL,$type,$ip,$snapshot,$deviceId,$devicePush_id,$filename,$alarmId) {
//      echo "inside push notification";
        $data = json_decode(file_get_contents("php://input"), true);
        $pushlist=$this->getPushEndPoint($userid);
        if (sizeof($pushlist) == 0) {
            return 0;
        }
        
        $message = $msginput;
        $path_to_config = VENDOR_PATH . '/config.php';
        $aws = Aws::factory($path_to_config);

        $msg['GCM'] = "{\"data\":{\"message\":\"" . $message . "\",\"type\":\"" . $type . "\",\"ip\":\"" . $ip . "\",\"snapshot\":\"" . $snapshot . "\",\"device_id\":\"" . $deviceId . "\",\"devicePush_id\":\"" . $devicePush_id . "\",\"filename\":\"" . $filename . "\",\"alarmId\":\"" . $alarmId . "\"}}";

        //   $msg['default'] = "{ \"data\": { \"message\": \"" . $message . "\" } }";
        $msg['APNS_SANDBOX'] = "{\"aps\":{\"alert\": \"" . $message . "\" ,\"type\":\"" . $type . "\",\"ip\":\"" . $ip . "\",\"snapshot\":\"" . $snapshot . "\",\"device_id\":\"" . $deviceId . "\",\"devicePush_id\":\"" . $devicePush_id . "\",\"filename\":\"" . $filename . "\",\"alarmId\":\"" . $alarmId . "\"} }";
              $msg['APNS'] = "{\"aps\":{\"alert\": \"" . $message . "\",\"type\":\"" . $type . "\",\"ip\":\"" . $ip . "\",\"snapshot\":\"" . $snapshot . "\",\"device_id\":\"" . $deviceId . "\",\"devicePush_id\":\"" . $devicePush_id . "\",\"filename\":\"" . $filename . "\",\"alarmId\":\"" . $alarmId . "\"} }";


        $msg_json = json_encode($msg);

        $client = $aws->get('Sns');
        for ($i = 0; $i < sizeof($pushlist); $i++) {

            try {

                $path_to_config= VENDOR_PATH .'/config.php';
                $aws = Aws::factory($path_to_config);
                $client = $aws->get('Sns');
                $result = $client->setEndpointAttributes(array(
                    // EndpointArn is required
                    'EndpointArn' => $pushlist[$i]['tokenEndpoint'],
                    'Attributes' => array('Enabled'=>'true')
                ));
              
                $result = $client->publish(array(
                    'TargetArn' => $pushlist[$i]['tokenEndpoint'],
                    'Message' => $msg_json,
                    'Subject' => 'Sandbox push',
                    'MessageStructure' => 'json',
                ));
            } catch (EndpointDisabledException $e) {
                 $err = $e->getMessage();
               // echo $err;
                //   $this->log("Error while sending the push and the error is " . $err);
            } catch (InvalidParameterException $e) {
                     $err = $e->getMessage();
                 //    echo $err;
                //   $this->log("Error while sending the push and the error is " . $err);
            }
        }
      //  echo json_encode(array('msg' => 'push_sent'));
    }

        
        /*public function log($did)
	{
			  $log="At time stamp :".time()."data: ".$did.PHP_EOL;
					
						chdir('logs');
			 file_put_contents('log_push_'.date("j.n.Y").'.txt', $log, FILE_APPEND);
	}*/

        
        /*public function log($did)
	{
			  $log="At time stamp :".time()."data: ".$did.PHP_EOL;
					
						chdir('logs');
			 file_put_contents('log_push_'.date("j.n.Y").'.txt', $log, FILE_APPEND);
	}*/
   
    public function saveToken(SandboxPushToken &$push)
    {
        $om = $this->getObjectManager($this->serviceManager);
        $om->persist($push);
        $om->flush();
        return $push->getokenId();
    }

     public function updateTokenStatus($token,$identifier,$user_id) {
        $platform = "android";
         $qb = $this->getRepository()->createQueryBuilder("token");
         $query = $qb->update()
             ->set("token.tokenValue", "?1")
             ->where("token.tokenAppIdentifier = ?2")
             ->setParameter(1, $token)
             ->setParameter(2, $identifier)
             ->getQuery();
         $query->execute();

         $qb1 = $this->getRepository()->createQueryBuilder("token");
         $qb1->addSelect('token');
         $qb1->where("token.tokenAppIdentifier = ?1")
             ->setParameter(1, $identifier);
         $result = $qb1->getQuery()->getArrayResult();

         if (sizeof($result) > 0) {
             $this->updateToken($result[0]['tokenId'], $user_id, $token, $platform);
             return true;
         }
    }

    public function updateTokenIOS($token_new,$token_old,$user_id,$platform) {
        $qb = $this->getRepository()->createQueryBuilder("token");
        $query = $qb->update()
            ->set("token.tokenValue", "?1")
            ->set("token.tokenPlatform", "?2")
            ->where("token.tokenValue = ?3")
            ->setParameter(1, $token_new)
            ->setParameter(2, $platform)
            ->setParameter(3, $token_old)
            ->getQuery();
        $query->execute();
	}
     public function updateIOS($token_new,$user_id,$platform){
        $qb = $this->getRepository()->createQueryBuilder("token");
        $qb->addSelect('token');
        $qb->where("token.tokenValue = ?1")
            ->setParameter(1, $token_new);
        $result = $qb->getQuery()->getArrayResult();

        if (sizeof($result) > 0) {
            $this->updateToken($result[0]['tokenId'],$user_id,$token_new,$platform);
            return true;
        }

    }

    /*
     public function updateTokenStatus($data) {
        $token_status=1;
        $qb = $this->getRepository()->createQueryBuilder("alarm");
        $query = $qb->update()
            ->set("alarm.tokenStatus", "?1")
            ->where("alarm.tokenValue = ?2")
            ->setParameter(1, $token_status)
            ->setParameter(2, $data['token_value'])
            ->getQuery();
        $query->execute();
        return true;
    }
     */
}
