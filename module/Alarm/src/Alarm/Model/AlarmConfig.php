<?php

/**
 * AlarmConfig Model
 *
 * @package Alarm
 * @author VVDN Technologies <sandbox@vvdntech.com>
 */

namespace Alarm\Model;

use Application\Entity\SandboxAlarmConfig;
use Application\Model\AbstractOrmManager;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\ResultSetMapping;
use Zend\Config\Reader\Ini;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\Sql\Select;

class AlarmConfig extends AbstractOrmManager implements ServiceLocatorAwareInterface {

    protected $serviceManager;


    public function getServiceLocator() {
        return $this->serviceManager;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceManager = $serviceLocator;
    }

    public function getRepository() {
        $repository = $this->getObjectManager($this->serviceManager)->getRepository('Application\Entity\SandboxAlarm');
        return $repository;
    }

    public function getEntityInstance() {
        if (null === $this->alarmEntityInstance) {
            $this->alarmEntityInstance = new SandboxAlarmConfig();
        }
        return $this->alarmEntityInstance;
    }

    public function saveAlarmConfig(SandboxAlarmConfig &$user) {

        $om = $this->getObjectManager($this->serviceManager);
        $om->persist($user);
        $om->flush();
        return $user->getAlarmConfigId();
    }
}
