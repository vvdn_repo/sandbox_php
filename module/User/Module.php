<?php

/**
 * This class contains getAutoloaderConfig,onBootstrap functions   
 * @package User
 * @author VVDNTechnologies 
 */

namespace User;

use User\Model\AuthStorage;
use User\Model\TenantMsp;
use User\Model\User;
use User\Model\SandboxUserTable;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
use Zend\Authentication\AuthenticationService;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module implements AutoloaderProviderInterface {

    public function onBootstrap(MvcEvent $e) {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'SandboxUserTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new SandboxUserTable($dbAdapter, $sm);
                    return $table;
                },
                'User' => function($sm) {
                    $user = new User();
                    $user->setServiceLocator($sm);
                    return $user;
                },
                'AuthStorage' => function($sm) {
            return new AuthStorage();
        },

				'OtpAuthStorage' => function($sm) {

            return new AuthStorage();
        },
		
                'AuthService' => function($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $config = $sm->get('config');
            $dbTableAuthAdapter = new DbTableAuthAdapter($dbAdapter, 'meru_user', 'user_email', 'user_pwd', 'MD5(CONCAT(?, "' . $config['password_hash_salt'] . '"))');
            $authService = new AuthenticationService();
            $authService->setAdapter($dbTableAuthAdapter);
            $authService->setStorage($sm->get('AuthStorage'));

            return $authService;
        },    'OtpAuthService' => function($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $config = $sm->get('config');
            $dbTableAuthAdapter = new DbTableAuthAdapter($dbAdapter, 'meru_user', 'user_email', 'user_otp', 'MD5(CONCAT(?, "' . $config['password_hash_salt'] . '"))');
            $authService = new AuthenticationService();
            $authService->setAdapter($dbTableAuthAdapter);
            $authService->setStorage($sm->get('AuthStorage'));

            return $authService;
        },
            ),
        );
    }

}
