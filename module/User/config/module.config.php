<?php

/**
 * module.config.php handles the routes and paths for the user module 
 * @package Application
 * @author VVDN Technologies < >
 */
return array(
    'controllers' => array(
        'invokables' => array(
            'User\Controller\User' => 'User\Controller\UserController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'register' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/user/register',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'register',
                    ),
                ),
            ),
            'login' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/user/login',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'login',
                    ),
                ),
            ),

            'logout' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/user/logout',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'logout',
                    ),
                ),
            ),

             'account-success' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/user/account-success',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'accountSuccess',
                    ),
                ),
            ),

            'generatecaptcha' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/user/generatecaptcha',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'generatecaptcha',
                    ),
                ),
            ),
            'checkuseremail' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/user/checkuseremail',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'checkuseremail',
                    ),
                ),
            ),
            'forgotpassword' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/user/forgotpassword',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'forgotpassword',
                    ),
                ),
            ),
            'resetpassword' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/user/resetpassword',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'resetpassword',
                    ),
                ),
            ),
            'resetpwd' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/user/resetpwd',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'resetpwd',
                    ),
                ),
            ),
            'devices' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/user/devices',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'device',
                    ),
                ),
            ),
            'listalarms' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/user/listalarms',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'listAlarm'
                    )
                ),
            ),

	    'set-mode-user' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user/set-mode',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'setMode',
                    ),
                ),
            ),


            'get-mode-user' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user/get-mode',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'getMode',
                    ),
                ),
            ),

            'terms-of-services' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/terms-of-services',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'termsOfServices',
                    ),
                ),
            ),
            'privacy-policy' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/privacy-policy',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'privacyPolicy',
                    ),
                ),
            ),
            'contact-us' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/contact-us',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'contactUs',
                    ),
                ),
            ),

            'viewprofile' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/user/viewprofile',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'viewProfile',
                    ),
                ),
            ),
            'activate-account' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user/activate-account/:id',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'activateAccount',
                    ),
                ),
            ),
            'verify-email' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user/verify-email',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'verifyEmail',
                    ),
                ),
            ),


        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            'layout/register' => __DIR__ . '/../view/layout/register_login.phtml',
        ),
        'template_path_stack' => array(
            'user' => __DIR__ . '/../view',
        ),
    ),
    'email_whitelist' => array(
        'domains' => array(
        ),
        'patterns' => array(
            'mclouddev#range#@mailinator.com' => array(
                'startLimit' => 1,
                'endLimit' => 200
            ),
            'mcloudtest#range#@mailinator.com' => array(
                'startLimit' => 1,
                'endLimit' => 200
            )
        ),
        'emailids' => array(
        )
    ),
    'email_blacklist' => array(
        'domains' => array(
        ),
        'emailids' => array(
        )
    ),
    'email_validate_check' => array(
        'whitelist' => true,
        'blacklist' => true
    ),
);
