<?php
/**
 * User Login Form
 * @package User
 * @author VVDN Technologies < >
 */

namespace User\Form;
use Zend\Form\Form;
class UserLoginForm extends Form
{
    public function __construct()
    {
        parent::__construct('user_login');

        $this->add(array(
            'name' => 'user_email',
            'type' => 'Text',            
            'options' => array(
                'label' => 'Email Address',
            ),
        ));
        $this->add(array(
            'name' => 'user_pwd',
            'type' => 'Password',
            'options' => array(
                'label' => 'Password',
            ),
        ));
        
        $this->add(array(
            'name' => 'remember_me',
            'type' => 'CheckBox',
            'options' => array(
                'label' => 'Remember Me',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Submit',
                'id' => 'submit_login',
            ),
        ));
    }
}