<?php
/**
 * This file has the filters for User Forms 
 * @package User
 * @author VVDN Technologies < >
 */

namespace User\Form;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Validator;

class UserFormFilter implements InputFilterAwareInterface {

    protected $inputFilter;
    protected $dbAdapter;
    protected $formType;

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    public function setFormType($formType) {
        $this->formType = $formType;
    }

    public function getInputFilter() {
        if ($this->formType == "register") {
            $emailValidators = array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            \Zend\Validator\NotEmpty::IS_EMPTY 
                                => 'Please enter Your Email'
                        ),
                    ),
                ),
                new Validator\EmailAddress(),
                array(
                    'name' => 'Db\NoRecordExists',
                    'options' => array(
                        'field' => 'user_email',
                        'table' => \User\Model\SandboxUserTable::getTableName(),
                        'adapter' => $this->dbAdapter,
                        'messages' => array(
                            \Zend\Validator\Db\NoRecordExists::
                                ERROR_RECORD_FOUND => 'Email already exists'
                        ),
                    ),
                )
            );
        } else {
            $emailValidators = array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            \Zend\Validator\NotEmpty::IS_EMPTY 
                                => 'Please enter Your Email'
                        ),
                    ),
                ),
                new Validator\EmailAddress()
            );
        }
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $inputFilter->add(array(
                'name' => 'user_email',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => $emailValidators,
            ));

            $inputFilter->add(array(
                'name' => 'user_name',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY 
                                    => 'Please enter Your Name'
                            ),
                        ),
                    ),
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 100,
                            'messages' => array(
                                \Zend\Validator\StringLength::TOO_LONG 
                                    => 'Your name max length in 100'
                            ),
                        ),
                    ),
                ),
            ));

            $inputFilter->add(array(
                'name' => 'user_pwd',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY 
                                    => 'Please enter Password'
                            ),
                        ),
                    )
                ),
            ));

            $inputFilter->add(array(
                'name' => 'user_confirmpwd',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY 
                                    => 'Please confirm your Password'
                            ),
                        ),
                    )
                ),
            ));

            $inputFilter->add(array(
                'name' => 'org_name',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY 
                                    => 'Please enter Company Name'
                            ),
                        ),
                    ),
                ),
            ));
            $inputFilter->add(array(
                'name' => 'user_address',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                )
            ));

            $inputFilter->add(array(
                'name' => 'agree_terms',
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY 
                                    => 'You must agree to the terms of use.'
                            ),
                        ),
                    ),
                    array(
                        'name' => 'Identical',
                        'options' => array(
                            'token' => '1',
                            'messages' => array(
                                Validator\Identical::NOT_SAME 
                                    => 'You must agree to the terms of use.',
                            ),
                        ),
                    ),
                ),
            ));
            $inputFilter->add(array(
                'name' => 'remember_me',
                'required' => false
            ));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function setDbAdapter(\Zend\Db\Adapter\Adapter $dbAdapter) {
        $this->dbAdapter = $dbAdapter;
    }

}
