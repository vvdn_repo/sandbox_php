<?php

/**
 * User Model
 * 
 * @package User
 * @author VVDN Technologies < >
 */

namespace User\Model;

use Application\Entity\SandboxUser;
use Application\Model\AbstractOrmManager;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;
use Zend\Captcha\Image as ZendcaptchaImage;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\Container as SessionContainer;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplateMapResolver;
use Zend\Crypt\Key\Derivation\Pbkdf2;
use Zend\Math\Rand;

class User extends AbstractOrmManager implements ServiceLocatorAwareInterface {

    protected $serviceManager;
    protected $userEntityInstance;

    public function getServiceLocator() {
        return $this->serviceManager;
    }


    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceManager = $serviceLocator;
    }

    public function getRepository() {
        $repository = $this->getObjectManager($this->serviceManager)->getRepository('Application\Entity\SandboxUser');
        return $repository;
    }

    public function getEntityInstance() {
        if (null === $this->userEntityInstance) {
            $this->userEntityInstance = new SandboxUser();
        }
        return $this->userEntityInstance;
    }

    public function sendRegistrationSuccessEmail(MeruUser $user) {
        $view = new PhpRenderer();
        $resolver = new TemplateMapResolver();
        $resolver->setMap(array(
            'registerMailTemplate' => MODULE_APPLICATION_PATH
            . '/view/email/templates/register_success.phtml'
        ));
        $view->setResolver($resolver);
        $viewModel = new ViewModel();
        $viewModel->setTemplate('registerMailTemplate')
                ->setVariables(array(
                    'user' => $user
        ));
        $content = $view->render($viewModel);
        //Set Html Mail Type
        $text = new MimePart('');
        $text->type = "text/plain";

        $html = new MimePart($content);
        $html->type = "text/html";

        $body = new MimeMessage();
        $body->setParts(array($html));
        //$transport = new SendmailTransport();
        $transport = new SmtpTransport();
        $config = $this->serviceManager->get('config');
        $options = new SmtpOptions($config['smtp_options']);
        $transport->setOptions($options);
        $message = new Message();
        $message->addTo($user->getUserEmail())
                ->addFrom($config['sender_email'])
                ->setEncoding('UTF-8');
        $message->setSubject("Welcome to Meru Cloud")
                ->setBody($body);
        $transport->send($message);
    }

    /*
     * Load the user by useremail.
     *
     * @param string $useremail
     * @return Array $result
     */




    public function validateLogin($user) {
        $enc_pwd = $this->decryptPassword($user);
        $qb = $this->getRepository()->createQueryBuilder("user");
        $qb->addSelect('user.userEmail');
        $qb->where("user.userEmail = :userEmail")
            ->andwhere("user.userPwd = :userPwd")
            ->setParameter("userEmail",$user['user_email'])
            ->setParameter("userPwd",$enc_pwd);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }
    public function validateLoginOtp($user) {
       // $enc_pwd = $this->decryptPassword($user);
        $modelCommon = $this->getServiceLocator()->get('Common');
        $time = $modelCommon->getCurrentTimeStamp();
        $qb = $this->getRepository()->createQueryBuilder("user");
        $qb->addSelect('user.userEmail');
        $qb->where("user.userEmail = :userEmail")
            ->andwhere("user.userOtp = :userOtp")
            ->andwhere("user.userOtpTimestamp + 86400 > :time")
            ->setParameter("userEmail",$user['user_email'])
            ->setParameter("userOtp",$user['user_pwd'])
            ->setParameter("time",$time);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }
    public function selectdata($id) {
        $qb = $this->getRepository()->createQueryBuilder("user");
        $qb->addSelect('user.userId');
        $qb->where("user.userId = :userId")
            ->setParameter("userId",$id);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }
    public function encryptPassword($pass = "") {
        if (!empty($pass)) {
            $enc_pwd = array();
            $enc_pwd['salt'] = Rand::getString(strlen($pass),'abcdefghijklmnopqrstuvwxyz0123456789', true);
            $enc_pwd['key'] = Pbkdf2::calc('sha512', $pass,$enc_pwd['salt'], 10000, strlen($pass)*2);
            return $enc_pwd;
        }
    }
    public function decryptPassword($user) {
        if (!empty($user)) {
            $qb = $this->getRepository()->createQueryBuilder("user");
            $qb->addSelect('user.userEmail');
            $qb->where("user.userEmail = :userEmail")
                ->setParameter("userEmail",$user['user_email']);
            $result = $qb->getQuery()->getArrayResult();
            $salt = $result[0][0][userPwdsalt];
            $enc_pwd = Pbkdf2::calc('sha512', $user['user_pwd'],$salt, 10000, strlen($user['user_pwd'])*2);
            return $enc_pwd;
        }
    }

    public function validateEmailUnique($email) {
        $isUnique = "false";
        $qb = $this->getRepository()->createQueryBuilder("user");
        $qb->addSelect('user.userEmail');
        $qb->where("user.userEmail = :userEmail")
            ->setParameter("userEmail",$email);
        $result = $qb->getQuery()->getArrayResult();

        if (empty($result)) {
            $isUnique = "true";
        } else {
            $isUnique = "false";
        }
        return $isUnique;
    }


    public function loadByUserEmail($user) {
        $qb = $this->getRepository()->createQueryBuilder("user");
        $qb->addSelect('user.userEmail');
        $qb->where("user.userEmail = :userEmail")
            ->setParameter("userEmail",$user);
        $val = $qb->getQuery()->getArrayResult();
        $result=$val[0][0];

        return $result;
    }

    public function getUserById($id) {
        $qb = $this->getRepository()->createQueryBuilder("user");
        $qb->addSelect('org');
        $qb->leftJoin("user.userOrgidFk", "org");
        $qb->where("user.userId = :userId")
                ->setParameter("userId", $id);

        $result = $qb->getQuery()->getSingleResult(AbstractQuery::HYDRATE_ARRAY);
        return $result;
    }

    /*
     * Get user email by id
     */

    public function getEmailById($id) {
        $qb = $this->getRepository()->createQueryBuilder("user");
        $qb->addSelect('user.userEmail', 'user.userName');
        $qb->where("user.userId = :userId")
                ->setParameter("userId", $id);

        $result = $qb->getQuery()->getSingleResult();
        return $result;
    }

    public function getUidfromtoken($token) {
        $qb = $this->getRepository()->createQueryBuilder("user");
        $qb->addSelect('user.userId');
        $qb->where("user.userToken = :userToken")
            ->setParameter("userToken", $token);
        $val = $qb->getQuery()->getArrayResult();
        $result = $val[0][0]['userId'];
        return $result;


    }

    public function saveUser(SandboxUser &$user) {
        $om = $this->getObjectManager($this->serviceManager);
        $om->persist($user);
        $om->flush();
        return $user->getUserId();
    }


     public function updateAlarmConfig($user_id,$time) {
        $qb = $this->getRepository()->createQueryBuilder("user");
        $query = $qb->update()
            ->set("user.userAlarmTimestamp", "?1")
            ->where("user.userId = ?2")
            ->setParameter(1, $time)
            ->setParameter(2, $user_id)
            ->getQuery();
        $query->execute();
        return true;

    }

     public function getAlarmConfig($user_id) {
        $qb = $this->getRepository()->createQueryBuilder("user");
        $qb->addSelect('user.userAlarmTimestamp');
        $qb->where("user.userId = :userId")
            ->setParameter("userId", $user_id);
        $val = $qb->getQuery()->getArrayResult();
        $result = $val[0][0]['userAlarmTimestamp'];
        return $result;


    }    


 public function getUserList() {
        $qb = $this->getRepository()->createQueryBuilder("user");
        $selectColumns =  $selectColumns = array('user.userId','user.userName','user.userEmail');
        $qb->select($selectColumns);
        $qb->where("user.userStatus != 2");
        $result = $qb->getQuery()->getArrayResult();
        /*foreach($result as $key=>$row){
            $result[$key]['deviceRTSP']="rtsp://".$result[$key]['deviceIP']."/PSIA/Streaming/channels/2?videoCodecType=H.264";
        }*/
        return $result;
    }


     /**
     * Get the count of Active Users
     * @return count of active users
     */
    public function getActiveUsercount() {
        $qb = $this->getRepository()->createQueryBuilder("user");
        $qb->select($qb->expr()->countDistinct('user.userId') . 'as userCount');
        $qb->Where("user.userStatus = 1");
        $query = $qb->getQuery();
        $result = $query->getScalarResult();
        return $result[0]['userCount'];
    }

    public function getTotalUsercount() {
        $qb = $this->getRepository()->createQueryBuilder("user");
        $qb->select($qb->expr()->countDistinct('user.userId') . 'as userCount');
        $qb->Where("user.userStatus != 2");
        $query = $qb->getQuery();
        $result = $query->getScalarResult();
        return $result[0]['userCount'];
    }


    public function getUsersList1($filterData, $start, $limit, $count, $sortBy = array()) {
        $qb = $this->getRepository()->createQueryBuilder("user");
        $selectColumns = array($qb->expr()->count('user.userId') . ' AS totalUsers');
        $qb->where("user.userStatus != 2");
        if ($count != 1) {
            $selectColumns = array('user.userId', 'user.userName', 'user.userStatus', 'user.userEmail', 'user.userPhone', 'org.orgName', 'org.orgId', 'org.orgType');
        }
        $qb->select($selectColumns)
                ->leftJoin("user.userOrgidFk", "org");
        if (!empty($filterData['user_id'])) {
            $qb->AndWhere("user.userCreatedbyFk = :createdBy")
                    ->setParameter('createdBy', $filterData['user_id']);
        }
        if (empty($filterData['globalSearch'])) {
            if (!empty($filterData['org_id'])) {
                $qb->andWhere("user.userOrgidFk = :orgId")
                        ->setParameter('orgId', $filterData['org_id']);
            }
        } else {
            if ($filterData['orgType'] != 'csp') {
                $qb->andwhere("user.userOrgidFk IN (:orgIds)")
                        ->setParameter("orgIds", $filterData['OrgIds']);
            }
        }
        //role check in case of csp
        if (!empty($filterData['userRoleCheck'])) {
            if ($filterData['userRoleCheck'] == 'admin') {
                $qb->andwhere("user.userRole = 'admin' OR user.userRole = 'superadmin'");
            } else {
                $qb->andwhere("user.userRole = '" . $filterData['userRoleCheck'] . "' OR user.userRole = '" . $filterData['userRoleCheck'] . "-superadmin'");
            }
        }
        if ($filterData['user_name'] !== "") {
            $qb->andWhere($qb->expr()->like('user.userName', $qb->expr()->literal('%' . $filterData['user_name'] . '%'))
                    . ' OR ' . $qb->expr()->like('user.userEmail', $qb->expr()->literal('%' . $filterData['user_name'] . '%'))
                    . ' OR ' . $qb->expr()->like('user.userPhone', $qb->expr()->literal('%' . $filterData['user_name'] . '%'))
                    . ' OR ' . $qb->expr()->like('user.userStatus', $qb->expr()->literal('%' . $filterData['user_name'] . '%')));
        }
        if (!empty($limit)) {
            $qb->setFirstResult($start);
            $qb->setMaxResults($limit);
        }
        foreach ($sortBy as $column => $sortDir) {
            $qb->orderBy('user.' . $column, $sortDir);
        }
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }



    public function updateUser($data, $where) {
        $qb = $this->getRepository()->createQueryBuilder("user");
        $qb = $qb->update();
        $count = 1;
        foreach ($data as $key => $value) {
            $qb->set("user." . $key, "?" . $count)
                    ->setParameter($count, $value);
            $count++;
        }
        $query = $qb->where("user.userId = :userId")
                ->setParameter("userId", $where['userId'])
                ->getQuery();
        $query->execute();
    }

    public function updateUsersByIds($data, $userIds) {
        $qb = $this->getRepository()->createQueryBuilder("user");
        $qb = $qb->update()
                ->set("user.userStatus", "?1")
                ->set("user.userPwd", "?2")
                ->set("user.userModifiedbyFk ", "?3")
                ->set("user.userModifiedon", "?4")
                ->set("user.userEmail ", $qb->expr()->concat($qb->expr()->concat("user.userId", $qb->expr()->literal("-")), "user.userEmail"))
                ->setParameter(1, $data['userStatus'])
                ->setParameter(2, $data['userPwd'])
                ->setParameter(3, $data['userModifiedbyFk'])
                ->setParameter(4, $data['userModifiedon']);


        $query = $qb->where("user.userId IN (:userIds)")
                ->setParameter("userIds", $userIds)
                ->getQuery();

        $query->execute();
    }

    public function updateLastlogin(MeruUser $user, $userId) {
        $qb = $this->getRepository()->createQueryBuilder("user");
        $query = $qb->update()
                ->set("user.userLastlogin", "?1")
                ->where("user.userId = ?2")
                ->setParameter(1, $user->getUserLastlogin())
                ->setParameter(2, $userId)
                ->getQuery();
        $query->execute();
    }

    /**
     * Generate captcha and return the Captcha Id.
     * This will save the captcha word in session
     */
    public function generateCaptcha() {
        ZendcaptchaImage::$CN = ZendcaptchaImage::$C = ZendcaptchaImage::$VN = ZendcaptchaImage::$V = array("2", "3", "4", "5", "6", "7", "9");
        $captcha = new ZendcaptchaImage();
        $captcha->setWordLen('4')
                ->setHeight('60')
                ->setFont('public/sandboxapp/fonts/ariali.ttf')
                ->setImgDir('public/sandboxapp/img/captcha')
                ->setDotNoiseLevel('5')
                ->setLineNoiseLevel('5');
        $captcha->generate();
        $captchaId = $captcha->getId();
        $container = new SessionContainer('zend_form_captcha' . $captchaId);
        $container->captcha_word = $captcha->getWord();
        return $captchaId;
    }

    /**
     * Validate the Submitted captcha against the captcha word in session
     */
    public function validateCaptcha($captcha) {
        $captchaId = $captcha['id'];
        $captchaInput = $captcha['input'];
        $session = new SessionContainer('zend_form_captcha' . $captchaId);
        $captchaWord = $session->captcha_word;
        if ($captchaWord) {
            if ($captchaInput != $captchaWord) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public function createUserSession($user = array()) {
       // $user['user_orgid_fk'] = $user['userOrgidFk']['orgId'];
        $user['user_id'] = $user['userId'];
        $this->authService = $this->serviceManager->get('AuthService');
        $this->authService->getStorage()->write($user);
    }

    public function getEmailCount($useremail, $userId = 0) {
        $qb = $this->getRepository()->createQueryBuilder("user");
        $qb->where("user.userStatus != 2");
        $qb->select($qb->expr()->count('user.userId') . ' AS userCount')
                ->andWhere("user.userEmail = :userEmail")
                ->setParameter("userEmail", $useremail);
        if ($userId) {
            $qb->andWhere("user.userId != :userId")
                    ->setParameter("userId", $userId);
        }
        $result = $qb->getQuery()->getSingleScalarResult();
        return $result;
    }


     public function setMode($user_id,$user_mode) {
        $qb = $this->getRepository()->createQueryBuilder("user");
        $query = $qb->update()
            ->set("user.userMode", "?1")
            ->where("user.userId = ?2")
            ->setParameter(1, $user_mode)
            ->setParameter(2, $user_id)
            ->getQuery();
        $query->execute();
        return true;

    }

    public function getMode($user_id) {
        $qb = $this->getRepository()->createQueryBuilder("user");
        $qb->addSelect('user.userMode');
        $qb->where("user.userId = :userId")
            ->setParameter("userId", $user_id);
        $val = $qb->getQuery()->getArrayResult();
        $result = $val[0][0]['userMode'];
        return $result;

    }

    public function resetpwd($id, $password = "" , $salt){
        if (isset($id) && isset($password)) {
            $null = "";
            $qb = $this->getRepository()->createQueryBuilder("user");
            $query = $qb->update()
                ->set("user.userPwd", "?1")
                ->set("user.userPwdsalt", "?2")
                ->set("user.userOtp", "?4")
                ->set("user.userOtpstatus", "?4")
                ->set("user.userOtpTimestamp", "?4")
                ->where("user.userId = ?3")
                ->setParameter(1, $password)
                ->setParameter(2, $salt)
                ->setParameter(3, $id)
                ->setParameter(4, $null)
                ->getQuery();
            $query->execute();
            return true;
        }

    }

    public function resetPassword($email, $password = "", $otp = 0, $currentTS = "") {
        if (isset($email) && isset($password)) {
            $qb = $this->getRepository()->createQueryBuilder("user");
            if ($otp == 1) {

                $query = $qb->update()
                        ->set("user.userOtp", "?1")
                        ->set("user.userOtpstatus", "?2")
                        ->set("user.userOtpTimestamp", "?3")
                        ->where("user.userEmail = ?4")
                        ->setParameter(1, $password)
                        ->setParameter(2, $otp)
                        ->setParameter(3, $currentTS)
                        ->setParameter(4, $email)
                        ->getQuery();
            } else {
                $query = $qb->update()
                        ->set("user.userPwd", "?1")
                        ->set("user.userOtpstatus", "?2")
                        ->set("user.userOtp", "?3")
                        ->where("user.userEmail = ?4")
                        ->setParameter(1, $password)
                        ->setParameter(2, $otp)
                        ->setParameter(3, '')
                        ->setParameter(4, $email)
                        ->getQuery();
            }
            $query->execute();
            return true;
        }
        return false;
    }

    public function sendEmail($type = "", $data) {

        $view = new PhpRenderer();
        $resolver = new TemplateMapResolver();
        $templatefile = "";
        switch ($type) {
            case 'registration':
                $templatefile = "register_activate.phtml";
                $subject = "Welcome to Sandbox - Activation Email";
                break;
            case 'passwordreset':
                $templatefile = "password_reset.phtml";
                $subject = "Sandbox Cloud - Reset Password";
                break;
            case 'emptyhtml':
                $templatefile = "empty_html.phtml";
                $subject = $data['subject'];
                break;
            case 'passwordmodified':
                $templatefile = "password_changed.phtml";
                $subject = $data['subject'];
                break;
        }
        if (isset($templatefile) && $data['toEmail']) {
            $resolver->setMap(array(
                'mailTemplate' => MODULE_APPLICATION_PATH
                . '/view/email/templates/' . $templatefile
            ));
            $view->setResolver($resolver);
            $viewModel = new ViewModel();
            $viewModel->setTemplate('mailTemplate')
                    ->setVariables(array(
                        'data' => $data
            ));
            $content = $view->render($viewModel);
            //Set Html Mail Type
            $text = new MimePart('');
            $text->type = "text/plain";

            $html = new MimePart($content);
            $html->type = "text/html";

            $body = new MimeMessage();
            $body->setParts(array($html));
            //$transport = new SendmailTransport();
            $transport = new SmtpTransport();
            $config = $this->serviceManager->get('config');
            $options = new SmtpOptions($config['smtp_options']);
            $transport->setOptions($options);
            $message = new Message();
            $message->addTo($data['toEmail'])
                    ->addFrom($config['sender_email'])
                    ->setEncoding('UTF-8');
            $message->setSubject($subject)
                    ->setBody($body);
            $transport->send($message);
        }
    }

    public function generateActivationKey($data) {
        $ts = time();
        $userInfo = $data['userId'] . ":" . $data["userEmail"] . ":" . $ts;
        $key = base64_encode($userInfo);
        return $key;
    }

    public function decodeActivationKey($key) {
        $userInfo = base64_decode($key);
        $info = explode(":", $userInfo);
        $data['userId'] = $info["0"];
        $data['userEmail'] = $info["1"];
        $data['time'] = $info["2"];
        return $data;
    }

    /**
     * Get the count of Active Users in an organization
     * @param type $orgId
     * @return count of active users
     */
   
    public function setAndSaveUser(&$userEntity, $data, $userId, $role = "admin") {
        $objectManager = $this->getObjectManager($this->getServiceLocator());
        $organizationModel = $this->getServiceLocator()->get('Organization');
        $org = $organizationModel->getActiveOrganization();
        $orgId = $org['orgId'];
        $orgProxyInstance = $objectManager->getReference('Application\Entity\MeruOrg', $orgId);
        // change newline to comma address
        $commonModel = $this->getServiceLocator()->get('Common');
        $data->address = $commonModel->changeNl2Comma($data->address);

        $userEntity->setUserName($data->name);
        $userEntity->setUserPwd($this->encryptPassword($data->password));
        $userEntity->setUserEmail($data->email);
        $userEntity->setUserAddress($data->address);
        $userEntity->setUserOrgidFk($orgProxyInstance);
        $userEntity->setUserPhone($data->phone);
        $userEntity->setUserOtpstatus($data->otpStatus);
        $userEntity->setUserCreatedbyFk($userId);
        $userEntity->setUserStatus(0);
        $userEntity->setUserLastlogin(0);
        $userEntity->setUserRole($role);
        $this->saveUser($userEntity);
    }

    public function getAdminList($filterData, $start, $limit, $count, $sortBy = array()) {
        $qb = $this->getRepository()->createQueryBuilder("user");
        $selectColumns = array($qb->expr()->count('user.userId') . ' AS totalUsers');
        $qb->where("user.userStatus != 2");
        if ($count != 1) {
            $selectColumns = array('user.userId', 'user.userName', 'user.userStatus', 'user.userEmail', 'user.userPhone', 'user.userRole');
            $qb->leftJoin('user.meruTenantAdmin', 'tenantmsp');
            $selectColumns[] = $qb->expr()->countDistinct('tenantmsp.tmspTenantid') . 'as tenantCount';
        }
        $qb->select($selectColumns);
        $qb->addSelect('partial user.{userId,userOrgidFk}');
        if (!empty($filterData['orgList'])) {
            $qb->andWhere("user.userOrgidFk IN (:orgList)")
                    ->setParameter('orgList', $filterData['orgList']);
        }
        if ($filterData['user_name'] !== "") {
            $qb->andWhere($qb->expr()->like('user.userName', $qb->expr()->literal('%' . $filterData['user_name'] . '%'))
                    . ' OR ' . $qb->expr()->like('user.userEmail', $qb->expr()->literal('%' . $filterData['user_name'] . '%'))
                    . ' OR ' . $qb->expr()->like('user.userPhone', $qb->expr()->literal('%' . $filterData['user_name'] . '%')));
        }
//        if (isset($filterData['filter_superadmin'])) {
//            $qb->andWhere("user.userRole != 'superadmin' ");
//        }

        if ($filterData['sorting_column'] !== "") {
            
        }
        if (!empty($limit)) {
            $qb->setFirstResult($start);
            $qb->setMaxResults($limit);
        }

        foreach ($sortBy as $column => $sortDir) {
            $alias = "user";
            $qb->orderBy($alias . "." . $column, $sortDir);
        }
        $result = $qb->groupBy('user.userId')->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        return $result;
    }

    public function getOrgSuperAdmin($orgId) {
        $qb = $this->getRepository()->createQueryBuilder("user");
        $qb->select('user');
        $qb->where("user.userOrgidFk = :orgId")
                ->setParameter("orgId", $orgId);
        $qb->andWhere("user.userRole = :role")
                ->setParameter("role", "superadmin");
        try {
            $result = $qb->getQuery()->getSingleResult(AbstractQuery::HYDRATE_ARRAY);
        } catch (NoResultException $ex) {
            $result = array();
        }

        return $result;
    }

    public function getOrgUsers($orgId) {
        $qb = $this->getRepository()->createQueryBuilder("user");
        $qb->select('user');
        $qb->where("user.userOrgidFk = :orgId")
                ->setParameter("orgId", $orgId);
        try {
            $result = $qb->getQuery()->getResult(AbstractQuery::HYDRATE_ARRAY);
        } catch (NoResultException $ex) {
            $result = array();
        }
        return $result;
    }

    public function getOrgAdminAndSuperAdmin($orgId) {
        $filterData['roles'][] = 'superadmin';
        $filterData['roles'][] = 'admin';
        $qb = $this->getRepository()->createQueryBuilder("user");
        $qb->select('user');
        $qb->where("user.userOrgidFk = :orgId")
                ->setParameter("orgId", $orgId);
        $qb->andwhere($qb->expr()->in('user.userRole', ':role'))
                ->setParameter('role', $filterData['roles']);
        try {
            $result = $qb->getQuery()->getResult(AbstractQuery::HYDRATE_ARRAY);
        } catch (NoResultException $ex) {
            $result = array();
        }
        return $result;
    }

    public function getCSPDetails() {
        $filterData['search'] = 'csp';
        $qb = $this->getRepository()->createQueryBuilder("user");
        $qb->leftJoin("user.userOrgidFk", "org");
        $selectColumns = array('partial user.{userId , '
            . 'userEmail,userStatus,userRole}');
        $qb->select($selectColumns);
        $qb->addSelect("partial user.{ userId,userOrgidFk }");
        $qb->addSelect('partial org.{ orgId,orgName,orgType }');
        $qb->where("user.userStatus = 1");
        $qb->andWhere($qb->expr()->like('org.orgType', $qb->expr()->literal('%' . $filterData['search'] . '%')));
        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)
                ->getArrayResult();
        return $result;
    }

    public function generateAlphaNumericPassword() {
        $length = 8;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
