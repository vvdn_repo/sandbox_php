<?php

/**
 * MeruUserTable model
 * @package user
 * @author VVDN Technologies 
 */

namespace User\Model;

use Exception;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\ServiceManager\ServiceManager;

class SandboxUserTable extends AbstractTableGateway {

    /**
     * Allowed fields
     *
     * @var array
     */
    protected $_fields = array(
        'user_id',
        'user_orgid_fk',
        'user_name',
        'user_email'
    );

    /**
     * Userdata
     *
     * @var array
     */
    protected $_data = array();
    protected $table = 'sandbox_user';
    protected static $staticTableName = 'sandbox_user';
    protected $serviceManager;
    protected $adapter;

    public function __construct(Adapter $adapter, ServiceManager 
            $serviceManager) {
        $this->adapter = $adapter;
        $this->initialize();
        $this->resultSetPrototype = new HydratingResultSet();
        $this->serviceManager = $serviceManager;
    }

    public static function getTableName() {
        return self::$staticTableName;
    }

    /**
     * Setting the needed data
     *
     * @param array $data
     * @return User_Model_SandboxUserTable
     */
    public function setData(array $data) {
        if (!empty($this->_data)) {
            throw new Auth_Model_Exception(
            'Data already set'
            );
        }
        foreach ($data as $name => $value) {
            if (in_array($name, $this->_fields)) {
                $this->_data[$name] = $value;
            }
        }
        return $this;
    }

    /**
     * Getting the data
     *
     * @param string $key
     * @return mixed
     */
    protected function _getData($key) {
        if (!in_array($key, $this->_fields)) {
            throw new Auth_Model_Exception(
            'Given data is not available: ' . $key
            );
        }
        if (!isset($this->_data[$key])) {
            return null;
        }
        return $this->_data[$key];
    }

    /**
     * Get the user id
     *
     * @return int
     */
    public function getId() {
        return $this->_getData('user_id');
    }

    /**
     * Get useremail
     *
     * @return string
     */
    public function getUseremail() {
        return $this->_getData('user_email');
    }

    /**
     * Get userorg id
     *
     * @return string
     */
    public function getUserOrgId() {
        return $this->_getData('user_orgid_fk');
    }

    /**
     * Static factory method to load the user by username.
     *
     * @param string $username
     * @return User_Model_SandboxUserTable|null
     */
    public function loadByUserEmail($useremail) {
        $result = $this->select(function (Select $select) use ($useremail) {
            $select->where(array('user_email' => $useremail));
        });

        if (!$result) {
            throw new Exception("Could not find row user email " . $useremail);
        }
        return $result->toArray();
    }

    public function fetchAll() {
        $result = $this->select();
        return $result->toArray();
    }

    public function getUser($id) {
        $result = $this->select(function (Select $select) use ($id) {
            $select->where(array('user_id' => $id));
        });
        return $result->toArray();
    }

    public function saveUser($user) {
        if (!isset($user['user_id'])) {
            $this->insert($user);
            $lastInsertId = $this->adapter->getDriver()->getLastGeneratedValue();
            return $lastInsertId;
        } else {
            if ($this->getUser($user['user_id'])) {
                $this->update($user, array('user_id' => $user['user_id']));
            } else {
                throw new Exception('User id does not exist');
            }
        }
    }

    public function getUserCreatedBy($created_by) {
        $result = $this->select(function (Select $select) use ($created_by) {
            $select->columns(array('user_name', 'user_email', 'user_mobile'));
            $select->join('sandbox_org', 'sandbox_org.org_id = sandbox_user.user_orgid_fk', 
                    array('org_name'), 'left');
            $select->where(array('user_createdby_fk' => $created_by));
        });
        return $result->toArray();
    }

    public function encryptPassword($password = "") {
        if (!empty($password)) {
            $config = $this->serviceManager->get('config');
            $password = md5($password . $config['password_hash_salt']);
            return $password;
        }
    }

    public function updateUser($data, $where) {
        $this->update($data, $where);
    }

}
