<?php

/**
 * UserController handles all the actions comming from routes 
 * for the Application module  
 * @package Application
 * @author VVDN Technologies < >
 */

namespace User\Controller;

use User\Form\UserFormFilter;
use User\Form\UserLoginForm;
use User\Form\UserRegisterForm;
use Zend\Http\Header\SetCookie;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Zend\Config\Reader\Ini;
use Zend\Session\Container;
use Zend\Session\Container as SessionContainer;
use Zend\Math\Rand;
use Zend\Http\Client;
use Zend\Serializer\Serializer;
use Zend\Http\Response;


class UserController extends AbstractActionController {

    protected $sessionStorage;
    protected $authService;

    /*
     * This function is used to get specified service instance
     * @param service type
     * @return Object Instance of required ZF2 service
     */

    protected function getServiceInstance($service) {
        return $this->getServiceLocator()->get($service);
    }

    /*
     * This function will used to register users in the system
     * and to populate registration form
     * @return Array of registration status with user info
     */


    public function registerAction() {
        $view = $this->getServiceInstance('ViewRenderer');
        $view->headTitle(PAGE_TITLE . " | Sign Up");
        $this->layout('layout/register_login');
        $userRegisterForm = new UserRegisterForm();
        $userModel = $this->getServiceInstance('User');
        $request = $this->getRequest();
        $formData = array();
        if ($request->isPost()) {
            $dbAdapter = $this->getServiceInstance('Zend\Db\Adapter\Adapter');
            $userFormFilter = new UserFormFilter();
            $userFormFilter->setDbAdapter($dbAdapter);
            $userFormFilter->setFormType('register');
            $userRegisterForm->setInputFilter($userFormFilter->getInputFilter());
            $userRegisterForm->setData($request->getPost());
            $data = $this->params()->fromPost();
            $formData = $data;
            $captcha = array();
            $captcha['id'] = $data['captcha_ref'];
            $captcha['input'] = $data['user_captcha'];
            $validCaptcha = $userModel->validateCaptcha($captcha);
            if ((!$validCaptcha) && ($captcha['input'] != "9895abz!#mcloud")) {

                $logMessage = array('%EMAIL%' => $data['user_email']);
               // $eventlogInstance->createLog('tenant', 'verify-pattern', 0, "", 'failure', 'Tenant Registration failed', '', 0, 0, $logMessage);
                $this->flashMessenger()->addErrorMessage("Pattern Not matching!!");
                $formData['captchaId'] = $userModel->generateCaptcha();
                return array('form' => $userRegisterForm, 'formData' => $formData);
            }



            //Validate Email from whitelist
            $isValid = $this->validateEmailStatus($data['user_email']);
            $userModel = $this->getServiceInstance('User');
            $isUnique =$userModel->validateEmailUnique($data['user_email']);
            if ("true" === $isValid &&"true" === $isUnique ) {

                // BEGIN-Create User
                $userEntity = $userModel->getEntityInstance();

                $modelCommon = $this->getServiceInstance('Common');
                $time = $modelCommon->getCurrentTimeStamp();

                // Create Activation Key
                $activationKey = $userModel->generateActivationKey($data);

                $objectManager = $userModel->getObjectManager($this->getServiceLocator());
                $userEntity->setUserName(htmlspecialchars($data['user_name']));
                $enc_pwd = array();
                $enc_pwd = $userModel->encryptPassword($data['user_pwd']);

                $userEntity->setUserPwd($enc_pwd['key']);
                $userEntity->setUserPwdsalt($enc_pwd['salt']);
                $userEntity->setUserEmail(htmlspecialchars($data['user_email']));
                $userEntity->setUserMobile(htmlspecialchars($data['user_mobile']));
                //$userEntity->setemailConfirmation($activationKey);
                $userEntity->setUserStatus(0); //0-> InActive Account
		$userEntity->setUserAlarmTimestamp($time);
		$userEntity->setUserMode(2);

 		$config = parse_ini_file('config/config.ini');
                $bucket_name = $config['bucket-name'];
                $access_key = $config['access-key'];
                $secret_key = $config['secret-key'];
		$userEntity->setUserAwsStatus(0);
                $userEntity->setUserAwsBucket($bucket_name);
                $userEntity->setUserAwsAccessKey($access_key);
                $userEntity->setUserAwsSecretKey($secret_key);
        
                $userModel->saveUser($userEntity);


                //$this->initializeUser($data['user_email']);
               // $this->createDefaultSitegroup();
                //Send Registration Successful EMail
                //$userModel->sendRegistrationSuccessEmail($userEntity);
                $info["userId"] = $userEntity->getUserId();
                $info["userName"] = $userEntity->getUserName();
                $info["userEmail"] = $userEntity->getUserEmail();
                $info["toEmail"] = $userEntity->getUserEmail();
                $info["activationKey"] = $userModel->generateActivationKey($info);

                $login_time=time();
                $rand_num= Rand::getString(5,'abcdefghijklmnopqrstuvwxyz0123456789', true);
                $user_token="sb_".$info["userId"]."_".$rand_num;
                $userEntity->setUserToken($user_token);
                $userModel->saveUser($userEntity);
		

		//create SIP account
                $url = $config['sip-url'];
                $Data['user_token'] = $user_token;
                $Data['user_id'] = $info["userId"];
                $Data['user_pwd'] = $data['user_pwd'];
                $data = json_encode($Data);
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data))
                );

                $result = curl_exec($ch);
                curl_close($ch);
		

                $userModel->sendEmail("registration", $info);
               // $logMessage = array('%EMAIL%' => $info["userEmail"]);
               $alert =  "Account Created Successfully. Please confirm your mail.  Click here to". "<a href='login'>" . "  Login". "</a>";
                $this->flashMessenger()->addErrorMessage($alert);
                //return $this->redirect()->toUrl(BASE_PATH . '/user/login');

            } else {
                $errorMessages = $userRegisterForm->getMessages();
                foreach ($errorMessages as $errorMessage) {
                    $this->flashMessenger()->addErrorMessage(reset($errorMessage));
                    break;
                }
                if ("false" === $isValid) {
                    $this->flashMessenger()->addErrorMessage("Email ID Not Allowed");
                }
		else if ("false" === $isUnique) {
                    $this->flashMessenger()->addErrorMessage("Email ID Already Used");
                }
            }


        }
        //Generate Captcha
        $formData['captchaId'] = $userModel->generateCaptcha();
        return array('form' => $userRegisterForm, 'formData' => $formData);
    }

    /*
     * This function will used to login users in the system
     * and to populate login form
     * @return Array of login status with login form
     */

    public function loginAction() {
        $view = $this->getServiceInstance('ViewRenderer');
        //$eventlogInstance = $this->getServiceInstance('Eventlog');
        $view->headTitle(PAGE_TITLE . " Login");
//        $this->layout('layout/register_login');
        $this->layout('layout/register_login');
        $userLoginForm = new UserLoginForm();
        $userModel = $this->getServiceInstance('User');
        $request = $this->getRequest();
        $this->authService = $this->getServiceInstance('AuthService');
        $formData=Array();
        if ($request->isPost()) {
            $dbAdapter = $this->getServiceInstance('Zend\Db\Adapter\Adapter');
            $userFormFilter = new UserFormFilter();
            $userFormFilter->setDbAdapter($dbAdapter);
            $userFormFilter->setFormType('login');
            $userLoginForm->setInputFilter($userFormFilter->getInputFilter());
            $userLoginForm->setData($request->getPost());
            $data = $this->params()->fromPost();
            $userEntity = $userModel->getEntityInstance();
            $userData=$userModel->validateLogin($data);
            $userDataOtp=$userModel->validateLoginOtp($data);
            if($userData)
            {
                if($userData[0][0]['userStatus'] == 0) {
                    $this->flashMessenger()->addErrorMessage("Account is Not Activated");
                }
                else if($userData[0][0]['userStatus'] == 1) {

                    $session = new Container('base');
                    $session->setExpirationSeconds( 31536000 );
                    $session->offsetSet('user_id',$userData[0][0][userId]);
                    $session->offsetSet('user_email',$userData[0][0][userEmail]);
                    $this->redirect()->toUrl(BASE_PATH . '/userhome');
                }

            }
            if($userDataOtp)
            {
                if($userDataOtp[0][0]['userStatus'] == 1) {
                    $session = new Container('base');
                    $session->offsetSet('user_id',$userDataOtp[0][0][userId]);
                    $this->redirect()->toUrl(BASE_PATH . '/user/resetpwd');
                }
            }
            else
            {
                $this->flashMessenger()->addErrorMessage("Incorrect user name or password!");
            }
        }

    }

  

    /*
     * This function will be used to check login form message
     * @return true or fale
     */

    protected function checkLoginFormMessages($userLoginForm) {
        $messages = $userLoginForm->getMessages();
        if (empty($messages)) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * This function will be used to logout a logged in user
     * @return viewModel
     */

    public function logoutAction() {
        $session_user = new Container('base');
        $session_user->getManager()->getStorage()->clear();
        return $this->redirect()->toRoute('login');
    }


    /*
     * This action function will be used to display Terms Of Service
     * todo definition yet to be defined
     */

    public function termsOfServicesAction() {
        $view = $this->getServiceInstance('ViewRenderer');
        $view->headTitle(PAGE_TITLE . " | Terms Of Services");
        $this->layout('layout/termspolicyLayout');
        // Get Organisation Data
        $mspOrgData = $this->organizationBrandingData('terms.html');
        $this->_view = new ViewModel();
        $this->layout()->setVariable('orgLogo', $mspOrgData['orgLogo']);
        $this->layout()->setVariable('mspOrgName', $mspOrgData['orgName']);
        return new ViewModel(array(
            'name' => $mspOrgData['fileContent']
        ));
    }

    public function privacyPolicyAction() {
        $view = $this->getServiceInstance('ViewRenderer');
        $view->headTitle(PAGE_TITLE . " | Privacy Policy");
        $this->layout('layout/termspolicyLayout');
        // Get Organisation Data
        $mspOrgData = $this->organizationBrandingData('policy.html');
        $this->_view = new ViewModel();
        $this->layout()->setVariable('orgLogo', $mspOrgData['orgLogo']);
        $this->layout()->setVariable('mspOrgName', $mspOrgData['orgName']);
        return new ViewModel(array(
            'name' => $mspOrgData['fileContent'],
        ));
    }

    public function contactUsAction() {
        $view = $this->getServiceInstance('ViewRenderer');
        $view->headTitle(PAGE_TITLE . " | Contact Us");
        $this->layout('layout/termspolicyLayout');
        // Get Organisation Data
        $mspOrgData = $this->organizationBrandingData('contact.html');
        $this->_view = new ViewModel();
        $this->layout()->setVariable('orgLogo', $mspOrgData['orgLogo']);
        $this->layout()->setVariable('mspOrgName', $mspOrgData['orgName']);
        return new ViewModel(array(
            'name' => $mspOrgData['fileContent'],
        ));
    }

  

    public function activateAccountAction() {
        $encodedkey = $this->params('id');
        $now = time();
        $past = strtotime('-1 days', $now);
        $userData = array('userStatus' => 0);
        //$eventlogInstance = $this->getServiceInstance('Eventlog');
        if (!empty($encodedkey)) {
            //$userId = base64_decode($encodedUserId);
            $userModel = $this->getServiceInstance('User');
            $data = $userModel->decodeActivationKey($encodedkey);
            if ($data['userEmail'] != "" && $data['userId'] != "") {
                $user = $userModel->loadByUserEmail($data['userEmail']);
                $this->authService = $this->getServiceInstance('AuthService');
                $this->authService->clearIdentity();
                if ($user['userId'] == $data['userId'] && $user['userStatus'] == 0) {

                        $userData['userStatus'] = 1;
                        $userModel->updateUser($userData, array('userId' => $user['userId']));
                        $this->flashMessenger()->addSuccessMessage("Account Activated Successfully");
                        $this->authService = $this->getServiceInstance('AuthService');
//                        $this->initializeUser($data['userEmail']);
                        $logMessage = array('%EMAIL%' => $data['userEmail']);
                       // $eventlogInstance->createLog('user', 'activate', $user['userId'], $user['userName'], 'success', "", "", 0, 0, $logMessage);
                        return $this->redirect()->toRoute('account-success');
  
                } else if ($user['userStatus'] == 1) {
                    return $this->redirect()->toRoute('account-success');
                }
            }
        }
        if ($userData['userStatus'] == 1) {
            return $this->redirect()->toRoute('account-success');
        } else {
            return $this->redirect()->toRoute('login');
        }
    }


   public function accountSuccessAction() {
        $view = $this->getServiceInstance('ViewRenderer');
        $this->layout('layout/account-success');
    }
   
    public function generatecaptchaAction() {
        $response = array();
        $response['status'] = "success";
        $userModel = $this->getServiceInstance('User');
        $response['captchaId'] = $userModel->generateCaptcha();
        return new JsonModel($response);
    }

    public function checkuseremailAction() {

        $request = $this->getRequest();
        $email = $request->getPost('user_email');
        $response = array();
        $response['availability'] = "0";
        $response['message'] = "Already Used";
        $response['status'] = "false";
        $response['email'] = $email;
        $eventlogInstance = $this->getServiceInstance('Eventlog');
        $userModel = $this->getServiceInstance('User');
        $userCount = $userModel->getEmailCount($email);
        if ($userCount == 0) {
            $response['availability'] = "1";
            $response['message'] = "Available";
            $response['status'] = $this->validateEmailStatus($email);
            if ("false" === $response['status']) {
                $response['message'] = "Not Allowed";
            }
        }
        else {
            //$logMessage = array("%EMAIL%" => $email);
            //$eventlogInstance->createLog('user', 'verify-email', 0, "", 'failure', "", "", 0, 0, $logMessage);
        }

        return new JsonModel($response);
    }


    public function forgotpasswordAction() {
        $view = $this->getServiceInstance('ViewRenderer');
        $view->headTitle(PAGE_TITLE . " | Reset Password");
        $this->layout('layout/register_login');
        $request = $this->getRequest();
        $response = array();
        $response['status'] = 'failure';
        $response['email'] = '';
        if ($request->isPost()) {
            $email = htmlspecialchars($request->getPost('user_email'));
            $response['email'] = $email;
            $userModel = $this->getServiceInstance('User');
            $userCount = $userModel->getEmailCount($email);
            if ($userCount == 1) {
                $newPass = rand(78900000, 98700023);
               // $encPass = $userModel->encryptPassword($newPass);
                //echo $newPass . "  -->" . $encPass;
                $modelCommon = $this->getServiceInstance('Common');
                $currentTS = $modelCommon->getCurrentTimeStamp();
                $status = $userModel->resetPassword($email, $newPass, 1, $currentTS);
                if ($status) {
                    $response['status'] = 'success';
                    $user = $userModel->loadByUserEmail($email);
                    $data['toEmail'] = $email;
                    $data['userPwd'] = $newPass;
                    $data['userName'] = $user['userName'];
                    $userModel->sendEmail('passwordreset', $data);
                    //$logIpAddress = $_SERVER['REMOTE_ADDR'];
                   // $eventLog = $this->getServiceInstance('Eventlog');
                    //$logMessage = array('%IPADDRESS%' => $logIpAddress);
                   // $eventLog->createLog('user', 'otp', $user['userId'], $user['userName'], 'success', 'Password Reset', "", 0, 0, $logMessage);
                }
            }
        } else
            $response['status'] = 'loadpage';
        return $response;
    }

    /**
     * User can reset the password using current password
     * @return string
     */
    public function resetpwdAction()
    {
        $view = $this->getServiceInstance('ViewRenderer');
        $view->headTitle("Reset Password");
        $this->layout('layout/register_login');
        $request = $this->getRequest();
        $this->authService = $this->getServiceInstance('AuthService');

        if ($request->isPost()) {
            $userModel = $this->getServiceInstance('User');
            $npwd = $request->getPost('newPassword');
            $session = new Container('base');
            $id = $session->offsetGet('user_id');
	    if($id){
            $encPass = $userModel->encryptPassword($npwd);
            $data = $userModel->selectdata($id);
            $modelCommon = $this->getServiceInstance('Common');
            $currentTS = $modelCommon->getCurrentTimeStamp();
            $time_elapsed = $currentTS - $data[0][0]['userOtpTimestamp'];
            if ($time_elapsed > 86400) {
                $this->flashMessenger()->addErrorMessage("OTP password has expired!");
                return $this->redirect()->toRoute('resetpwd');
            }
            else {
                $status = $userModel->resetpwd($data[0][0]['userId'], $encPass['key'],$encPass['salt']);
                if ($status) {
                    $user = $userModel->selectdata($id);
                    //create SIP account
                    $url = "52.11.8.100/sandbox/sip.php";
                    $Data['user_token'] = $user[0][0]['userToken'];
                    $Data['user_id'] = $user[0][0]['userId'];
                    $Data['user_pwd'] = $data['user_pwd'];
                    $data = json_encode($Data);
                    $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'Content-Type: application/json',
                            'Content-Length: ' . strlen($data))
                    );

                    $result = curl_exec($ch);
                    curl_close($ch);


                    $this->flashMessenger()->addErrorMessage("Your password has been updated successfully. Please login with new Password");

                    return $this->redirect()->toRoute('login');
                }
                else {
                    $this->flashMessenger()->addErrorMessage("Failed to update the password!");
                    $response['message'] = "Failed to update the password!";
                }
            }
	}
           else{
               $this->flashMessenger()->addErrorMessage("Session expired!");
               return $this->redirect()->toRoute('resetpwd');
           }


        }
    }

    public function resetpasswordAction() {
        $view = $this->getServiceInstance('ViewRenderer');
        $eventLog = $this->getServiceInstance('Eventlog');
        $view->headTitle("Reset Password");
        $this->layout('layout/register_login');
        $request = $this->getRequest();
        $response = array();
        $response['status'] = 0;
        $response['message'] = '';
        $ajax = false;

        if ($request->isPost()) {
            $cpwd = $request->getPost('currentPassword');
            $npwd = $request->getPost('newPassword');

            if (!$cpwd && !$npwd) {
                $ajax = true;
                $data = json_decode(file_get_contents("php://input"));
                $cpwd = $data->currentPassword;
                $npwd = $data->newPassword;
            }
            $userModel = $this->getServiceInstance('User');
            $this->authService = $this->getServiceInstance('AuthService');
            $userAuth = $this->authService->getStorage()->read();

            $userId = $userAuth['user_id'];
            $user = $userModel->getUserById($userId);
            $encPass = $userModel->encryptPassword($cpwd);
            $encCurrentPass = $userModel->encryptPassword($npwd);
            if ($userAuth['userOtpstatus'] == 1) {
                if ($cpwd == $npwd) {
                    $this->flashMessenger()->addErrorMessage("New password cannot be the same as current password!");
                    return $this->redirect()->toRoute('resetpassword');
                } else if ($userAuth['userPwd'] == $encCurrentPass) {
                    $this->flashMessenger()->addErrorMessage("New password cannot be the same as current password!");
                    return $this->redirect()->toRoute('resetpassword');
                } else {
                    if ($userAuth['userOtp'] == $encPass || $userAuth['userPwd'] == $encPass) {

                        $modelCommon = $this->getServiceInstance('Common');
                        $currentTS = $modelCommon->getCurrentTimeStamp();
                        $time_elapsed = $currentTS - $userAuth['userOtpTimestamp'];

                        if ($userAuth['userOtp'] == $encPass && $time_elapsed > 3600) {
                            $this->flashMessenger()->addErrorMessage("OTP password has expired!");
                            return $this->redirect()->toRoute('resetpassword');
                        } else {
                            $encPass = $userModel->encryptPassword($npwd);
                            $status = $userModel->resetPassword($user['userEmail'], $encPass);
                            if ($status) {
                                $this->authService->clearIdentity();
                                $this->flashMessenger()->addSuccessMessage("Your password has been updated successfully");
                                $eventLog->createLog('user', 'reset-password-otp', $user['userId'], $user['userName'], 'success', 'Password Reset');
                                return $this->redirect()->toRoute('login');
                            } else
                                $response['message'] = "Failed to update the password!";
                        }
                    } else
                        $this->flashMessenger()->addErrorMessage("Current password is not matching!");
                    return $this->redirect()->toRoute('resetpassword');
                }
            }
            else {

                if ($cpwd == $npwd) {
                    //$this->authService->clearIdentity();
                    if (!$ajax) {
                        $this->flashMessenger()->addErrorMessage("New password cannot be the same as current password!");
                        return $this->redirect()->toRoute('resetpassword');
                    } else {
                        $response['message'] = "New password cannot be the same as current password!";
                    }
                } else {
                    if ($encPass == $user['userPwd']) {
                        $encPass = $userModel->encryptPassword($npwd);
                        $status = $userModel->resetPassword($user['userEmail'], $encPass);
                        if ($status) {
                            $eventLog->createLog('user', 'reset-password', $user['userId'], $user['userName'], 'success', 'Password Reset');
                            if (!$ajax) {
                                $this->authService->clearIdentity();
                                $this->flashMessenger()->addSuccessMessage("Your password has been updated successfully");
                                //$this->initializeUser($user['userEmail']);
                                return $this->redirect()->toRoute('login');
                            } else {
                                $response['status'] = 1;
                                $response['message'] = "Your password had been updated successfully!";
                            }
                        } else
                            $response['message'] = "Failed to update the password!";
                    }
                    else {
                        if (!$ajax) {
                            $this->flashMessenger()->addErrorMessage("Current password is not matching!");
                            return $this->redirect()->toRoute('resetpassword');
                        } else {
                            $response['message'] = "Current password is not matching!";
                        }
                    }
                }
            }
        }
        if ($ajax)
            return new JsonModel($response);
        else
            return $response;
    }


    public function deviceAction(){

        $session = new SessionContainer('base');
        $val = $session->offsetGet('user_id');

        if($val)
        {
            $view = $this->getServiceInstance('ViewRenderer');
            $view->headTitle(PAGE_TITLE . " | Home");
            $this->layout('layout/home');

        }
        else{
            $this->redirect()->toUrl(BASE_PATH . '/user/login');
        }
    }

    public function listAlarmAction(){

        $session = new SessionContainer('base');
        $val = $session->offsetGet('user_id');

        if($val)
        {
            $view = $this->getServiceInstance('ViewRenderer');
            $view->headTitle(PAGE_TITLE . " | Home");
            $this->layout('layout/home');

        }
        else{
            $this->redirect()->toUrl(BASE_PATH . '/user/login');
        }
    }

    /*
     * Check whether the Email address is already registered or not
     */


   
    public function setModeAction()
    {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            if ($request->isPost()) {
                $data = json_decode(file_get_contents("php://input"), true);
                $userMode = $data['user_mode'];
                $userModel = $this->getServiceInstance('User');
                $user = $userModel->setMode($user_id,$userMode);
                if($user) {
                     $status = true;
                    $message = "User Mode Configured Successfully";
                    $command= "set-mode";
		            $data['user_mode'] = $userMode;
                    $DeviceModel = $this->getServiceInstance('Device');
                    $device_list =  $DeviceModel->getDeviceList($user_id);
                    $count = sizeof($device_list);
                    $DevicePushModel = $this->getServiceInstance('DevicePush');
                    $time_limit = 2;
                    for($i=0;$i<$count;$i++)
                    {
                        $DevicePushModel->InsertDevicePush($command,$device_list[$i]['deviceId'],$user_id,$time_limit,$userMode);
                        
                    }

                }
                else {
                    $status = false;
                    $message = "User Mode Setting Failed";
                }
            } else {
                $status = false;
                $message = "Fail";
            }
        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }

    public function getModeAction()
    {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            if ($request->isPost()) {
                $UserModel = $this->getServiceInstance('User');
                $UserMode = $UserModel->getMode($user_id);
                if( isset($UserMode)) {
                    $status = true;
                    $user_mode = $UserMode;
                    return new JsonModel(array("status" => $status, "user_mode" => $user_mode));
                }
                else {
                    $status = false;
                    $message = "User Mode is not available";
                }
            } else {
                $status = false;
                $message = "Fail";
            }
        }
        else{
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }

  

    public function callhttp($data) {
        $config = parse_ini_file('config/config.ini');
        $client = new Client();
        $client->setUri($config['node_ip'] . ":" . $config['node_port']);
        $client->setMethod('POST');
        $data = array("commandId" => $data['commandId'], "command" => $data['command'], "device_id" => $data['device_id'], "requestId" => $data['requestId'],  "userMode" => $data['user_mode']);
        $asd = json_encode($data);
        $client->setRawBody($asd);
        $response = $client->send();
        return $response;
    }


    public function verifyEmailAction() {
        $status = array();
        $request = $this->getRequest();
        $eventlogInstance = $this->getServiceInstance('Eventlog');
        $status['status'] = true;
        if ($request->isPost()) {
            $data = json_decode(file_get_contents("php://input"), true);
            $status['userId'] = $data['userId'];
            $status['userEmail'] = $data['userEmail'];
            $userModel = $this->getServiceInstance('User');
            $userCount = $userModel->getEmailCount($data['userEmail'], $data['userId']);
            if ($userCount < 1) {
                $status['available'] = 1;
            } else {
                $status['available'] = 0;
                $status['message'] = "Email address already registered!!";
                $logMessage = array("%EMAIL%" => $data['userEmail']);
                $eventlogInstance->createLog('user', 'duplicate-email', 0, "", 'failure', "", "", 0, 0, $logMessage);
            }
        }
        return new JsonModel($status);
    }

    public function validateEmailStatus($email) {
      $isValid = "false";
        $config = $this->getServiceInstance('config');
        // Allowed
        $allowedDomains = $config['email_whitelist']['domains'];
        $allowedEmails = $config['email_whitelist']['emailids'];
        // Not Allowed
        $notAllowedDomains = $config['email_blacklist']['domains'];
        $notAllowedEmails = $config['email_blacklist']['emailids'];
        // Validation Status
        $validateStatus = $config['email_validate_check'];
        $emailDomain = substr(strrchr($email, "@"), 1);
        // Check for whitelisted data
        if ($validateStatus['whitelist']) {
            if (in_array($emailDomain, $allowedDomains)) {
                return $isValid = "true";
            } elseif (in_array($email, $config['email_whitelist']['emailids'])) {
                return  $isValid = "true";
            } elseif (empty($allowedDomains)) {
                return  $isValid = "true";
            } else {
                foreach ($config['email_whitelist']['patterns'] as $pattern => $patternLimit) {
                    $emailParts = explode("#range#", $pattern);
                    $isMatch = preg_match('/^' . $emailParts[0] . '(\d+)' . $emailParts[1] . '$/', $email, $matches);
                    if ($isMatch && $matches[1] >= $patternLimit['startLimit'] && $matches[1] <= $patternLimit['endLimit']) {
                        return $isValid = "true";
                        break;
                    }
                }
            }
        }

    }


}
